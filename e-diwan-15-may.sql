--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ckeditor_assets; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ckeditor_assets (
    id integer NOT NULL,
    data_file_name character varying NOT NULL,
    data_content_type character varying,
    data_file_size integer,
    assetable_id integer,
    assetable_type character varying(30),
    type character varying(30),
    width integer,
    height integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.ckeditor_assets OWNER TO postgres;

--
-- Name: ckeditor_assets_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ckeditor_assets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ckeditor_assets_id_seq OWNER TO postgres;

--
-- Name: ckeditor_assets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ckeditor_assets_id_seq OWNED BY ckeditor_assets.id;


--
-- Name: comments; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE comments (
    id integer NOT NULL,
    lesson_id integer,
    title character varying,
    comment text,
    status_id integer,
    user_id integer,
    semester_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.comments OWNER TO postgres;

--
-- Name: comments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comments_id_seq OWNER TO postgres;

--
-- Name: comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE comments_id_seq OWNED BY comments.id;


--
-- Name: lesson_lines; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE lesson_lines (
    id integer NOT NULL,
    lesson_id integer,
    line_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.lesson_lines OWNER TO postgres;

--
-- Name: lesson_lines_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE lesson_lines_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lesson_lines_id_seq OWNER TO postgres;

--
-- Name: lesson_lines_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE lesson_lines_id_seq OWNED BY lesson_lines.id;


--
-- Name: lessons; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE lessons (
    id integer NOT NULL,
    name character varying,
    title character varying,
    objective text,
    content text,
    poem_id integer,
    poet_id integer,
    document character varying,
    user_id integer,
    status integer,
    semester_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.lessons OWNER TO postgres;

--
-- Name: lessons_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE lessons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lessons_id_seq OWNER TO postgres;

--
-- Name: lessons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE lessons_id_seq OWNED BY lessons.id;


--
-- Name: line_references; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE line_references (
    id integer NOT NULL,
    line_id integer,
    content text,
    media character varying,
    reference_type integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.line_references OWNER TO postgres;

--
-- Name: line_references_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE line_references_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.line_references_id_seq OWNER TO postgres;

--
-- Name: line_references_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE line_references_id_seq OWNED BY line_references.id;


--
-- Name: lines; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE lines (
    id integer NOT NULL,
    stanza_id integer,
    text character varying,
    text2 character varying,
    media character varying,
    description character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.lines OWNER TO postgres;

--
-- Name: lines_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE lines_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lines_id_seq OWNER TO postgres;

--
-- Name: lines_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE lines_id_seq OWNED BY lines.id;


--
-- Name: poems; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE poems (
    id integer NOT NULL,
    description text,
    author character varying,
    title character varying,
    audio character varying,
    document character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    poet_id integer,
    avatar character varying
);


ALTER TABLE public.poems OWNER TO postgres;

--
-- Name: poems_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE poems_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.poems_id_seq OWNER TO postgres;

--
-- Name: poems_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE poems_id_seq OWNED BY poems.id;


--
-- Name: poets; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE poets (
    id integer NOT NULL,
    name character varying,
    year character varying,
    biography text,
    media character varying,
    document character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.poets OWNER TO postgres;

--
-- Name: poets_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE poets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.poets_id_seq OWNER TO postgres;

--
-- Name: poets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE poets_id_seq OWNED BY poets.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO postgres;

--
-- Name: semesters; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE semesters (
    id integer NOT NULL,
    name character varying,
    user_id integer,
    status integer,
    activated boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.semesters OWNER TO postgres;

--
-- Name: semesters_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE semesters_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.semesters_id_seq OWNER TO postgres;

--
-- Name: semesters_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE semesters_id_seq OWNED BY semesters.id;


--
-- Name: stanzas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE stanzas (
    id integer NOT NULL,
    poem_id integer,
    location integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.stanzas OWNER TO postgres;

--
-- Name: stanzas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE stanzas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stanzas_id_seq OWNER TO postgres;

--
-- Name: stanzas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE stanzas_id_seq OWNED BY stanzas.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    provider character varying DEFAULT 'email'::character varying NOT NULL,
    uid character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying,
    last_sign_in_ip character varying,
    confirmation_token character varying,
    confirmed_at timestamp without time zone,
    confirmation_sent_at timestamp without time zone,
    unconfirmed_email character varying,
    name character varying,
    nickname character varying,
    image character varying,
    email character varying,
    roles text,
    tokens json,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    status_id integer,
    approved boolean DEFAULT false NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: word_references; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE word_references (
    id integer NOT NULL,
    reference_type integer,
    word character varying,
    word_indexes character varying,
    media character varying,
    stanza_id integer,
    poem_id integer,
    line_id integer,
    description text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    word_search character varying,
    description_search text
);


ALTER TABLE public.word_references OWNER TO postgres;

--
-- Name: word_references_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE word_references_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.word_references_id_seq OWNER TO postgres;

--
-- Name: word_references_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE word_references_id_seq OWNED BY word_references.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ckeditor_assets ALTER COLUMN id SET DEFAULT nextval('ckeditor_assets_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comments ALTER COLUMN id SET DEFAULT nextval('comments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY lesson_lines ALTER COLUMN id SET DEFAULT nextval('lesson_lines_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY lessons ALTER COLUMN id SET DEFAULT nextval('lessons_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY line_references ALTER COLUMN id SET DEFAULT nextval('line_references_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY lines ALTER COLUMN id SET DEFAULT nextval('lines_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY poems ALTER COLUMN id SET DEFAULT nextval('poems_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY poets ALTER COLUMN id SET DEFAULT nextval('poets_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY semesters ALTER COLUMN id SET DEFAULT nextval('semesters_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stanzas ALTER COLUMN id SET DEFAULT nextval('stanzas_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY word_references ALTER COLUMN id SET DEFAULT nextval('word_references_id_seq'::regclass);


--
-- Data for Name: ckeditor_assets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY ckeditor_assets (id, data_file_name, data_content_type, data_file_size, assetable_id, assetable_type, type, width, height, created_at, updated_at) FROM stdin;
1	image9.jpg	image/jpeg	7354	1	User	Ckeditor::Picture	120	90	2017-03-30 09:01:45.02102	2017-03-30 09:01:45.02102
15	Picture1.jpg	image/jpeg	8765	1	User	Ckeditor::Picture	120	90	2017-03-30 13:53:07.499943	2017-03-30 13:53:07.499943
16	Picture2.jpg	image/jpeg	8407	1	User	Ckeditor::Picture	133	90	2017-03-30 13:56:20.000033	2017-03-30 13:56:20.000033
17	image10.jpg	image/jpeg	5943	1	User	Ckeditor::Picture	142	94	2017-03-30 14:00:19.662893	2017-03-30 14:00:19.662893
18	Picture3.jpg	image/jpeg	8097	1	User	Ckeditor::Picture	120	81	2017-03-30 14:01:13.510894	2017-03-30 14:01:13.510894
19	image11.jpg	image/jpeg	5719	1	User	Ckeditor::Picture	136	151	2017-03-30 14:01:41.830118	2017-03-30 14:01:41.830118
20	Picture4.jpg	image/jpeg	5286	1	User	Ckeditor::Picture	120	75	2017-03-30 14:02:34.895938	2017-03-30 14:02:34.895938
21	image12.jpg	image/jpeg	6687	1	User	Ckeditor::Picture	217	162	2017-03-30 14:02:54.908014	2017-03-30 14:02:54.908014
22	Picture5.jpg	image/jpeg	7911	1	User	Ckeditor::Picture	120	102	2017-03-30 14:03:50.037254	2017-03-30 14:03:50.037254
23	Picture6.jpg	image/jpeg	6420	1	User	Ckeditor::Picture	120	90	2017-03-30 14:05:13.989278	2017-03-30 14:05:13.989278
24	Picture8.jpg	image/jpeg	9761	1	User	Ckeditor::Picture	120	124	2017-03-30 14:08:56.030736	2017-03-30 14:08:56.030736
25	Picture9.jpg	image/jpeg	6275	1	User	Ckeditor::Picture	120	101	2017-03-30 14:12:58.378771	2017-03-30 14:12:58.378771
26	Picture7.jpg	image/jpeg	9924	1	User	Ckeditor::Picture	120	103	2017-03-30 14:17:37.196437	2017-03-30 14:17:37.196437
28	image13.jpg	image/jpeg	2588	1	User	Ckeditor::Picture	136	91	2017-03-30 14:24:33.05941	2017-03-30 14:24:33.05941
29	Picture10.png	image/png	32262	1	User	Ckeditor::Picture	120	107	2017-03-30 14:27:58.03914	2017-03-30 14:27:58.03914
30	Picture11.png	image/png	16932	1	User	Ckeditor::Picture	120	93	2017-03-30 14:28:18.601827	2017-03-30 14:28:18.601827
31	Picture12.jpg	image/jpeg	5169	1	User	Ckeditor::Picture	120	80	2017-03-30 14:32:05.527046	2017-03-30 14:32:05.527046
32	Picture13.jpg	image/jpeg	11271	1	User	Ckeditor::Picture	120	139	2017-03-30 14:34:50.681061	2017-03-30 14:34:50.681061
34	Picture14.jpg	image/jpeg	11841	1	User	Ckeditor::Picture	120	126	2017-03-30 14:37:39.474394	2017-03-30 14:37:39.474394
35	image14.jpg	image/jpeg	6430	1	User	Ckeditor::Picture	142	142	2017-03-30 14:38:35.341954	2017-03-30 14:38:35.341954
36	Picture15.jpg	image/jpeg	6676	1	User	Ckeditor::Picture	120	127	2017-03-30 14:50:31.74251	2017-03-30 14:50:31.74251
37	Picture16.jpg	image/jpeg	6674	1	User	Ckeditor::Picture	120	91	2017-03-30 14:54:21.114527	2017-03-30 14:54:21.114527
38	Picture17.jpg	image/jpeg	6278	1	User	Ckeditor::Picture	120	108	2017-03-30 14:56:56.479029	2017-03-30 14:56:56.479029
39	image15.jpg	image/jpeg	11241	1	User	Ckeditor::Picture	142	211	2017-03-30 14:59:47.734217	2017-03-30 14:59:47.734217
40	image1.jpeg	image/jpeg	19989	1	User	Ckeditor::Picture	142	177	2017-03-30 15:09:30.593567	2017-03-30 15:09:30.593567
41	Picture18.jpg	image/jpeg	7380	1	User	Ckeditor::Picture	120	176	2017-03-30 16:07:30.64094	2017-03-30 16:07:30.64094
42	Picture19.jpg	image/jpeg	6705	1	User	Ckeditor::Picture	120	112	2017-03-30 16:10:11.930066	2017-03-30 16:10:11.930066
43	Picture20.jpg	image/jpeg	7749	1	User	Ckeditor::Picture	120	139	2017-03-30 16:14:41.900559	2017-03-30 16:14:41.900559
44	Picture21.jpg	image/jpeg	7322	1	User	Ckeditor::Picture	120	123	2017-03-30 16:16:56.580711	2017-03-30 16:16:56.580711
45	1.jpg	image/jpeg	25223	1	User	Ckeditor::Picture	275	183	2017-05-15 02:56:02.859785	2017-05-15 02:56:02.859785
46	1.jpg	image/jpeg	5586	1	User	Ckeditor::Picture	100	66	2017-05-15 03:15:30.754455	2017-05-15 03:15:30.754455
\.


--
-- Name: ckeditor_assets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ckeditor_assets_id_seq', 46, true);


--
-- Data for Name: comments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY comments (id, lesson_id, title, comment, status_id, user_id, semester_id, created_at, updated_at) FROM stdin;
1	6	\N	Imtiaz	\N	35	1	2017-04-26 01:04:57.182327	2017-04-26 01:04:57.182327
2	6	\N	👍🏻	\N	62	1	2017-04-26 01:05:03.774989	2017-04-26 01:05:03.774989
3	6	\N	Aiwahh	\N	16	1	2017-04-26 01:05:04.469423	2017-04-26 01:05:04.469423
4	6	\N	ممتاز!!	\N	45	1	2017-04-26 01:05:06.65021	2017-04-26 01:05:06.65021
5	6	\N	boekkk ahhh	\N	47	1	2017-04-26 01:05:07.447172	2017-04-26 01:05:07.447172
6	6	\N	جميل !! 😍	\N	7	1	2017-04-26 01:05:10.665798	2017-04-26 01:05:10.665798
7	6	\N	ممتاز.... جميييييلللللل	\N	59	1	2017-04-26 01:05:14.579933	2017-04-26 01:05:14.579933
8	6	\N	👍👍👍👍👍	\N	15	1	2017-04-26 01:05:14.817089	2017-04-26 01:05:14.817089
9	6	\N	Assalamualaikum\r\n	\N	36	1	2017-04-26 01:05:15.628725	2017-04-26 01:05:15.628725
10	6	\N	سبحان الله... 	\N	29	1	2017-04-26 01:05:24.886395	2017-04-26 01:05:24.886395
11	6	\N	ممتاز... فتح الله لكم.	\N	9	1	2017-04-26 01:05:25.938928	2017-04-26 01:05:25.938928
12	6	\N	I'm impress	\N	17	1	2017-04-26 01:05:28.240895	2017-04-26 01:05:28.240895
13	7	\N	هذه رائعة جدا!!	\N	60	1	2017-04-26 01:05:28.272352	2017-04-26 01:05:28.272352
14	6	\N	ممتاز ! 😬 ألف مبروك 👏🏽👏🏽	\N	5	1	2017-04-26 01:05:28.446309	2017-04-26 01:05:28.446309
15	6	\N	ممتاز!!!!	\N	27	1	2017-04-26 01:05:29.474728	2017-04-26 01:05:29.474728
16	6	\N	ممتاز وفرحت جدا\r\n\r\n\r\n	\N	55	1	2017-04-26 01:05:30.146632	2017-04-26 01:05:30.146632
17	6	\N	splendid !  	\N	6	1	2017-04-26 01:05:33.209512	2017-04-26 01:05:33.209512
18	6	\N	الممتاز.. تبارك الله..	\N	14	1	2017-04-26 01:05:34.274514	2017-04-26 01:05:34.274514
19	6	\N	السلام عليكم	\N	52	1	2017-04-26 01:05:44.69801	2017-04-26 01:05:44.69801
20	6	\N	Terbaik ..memang padu apps nie sayabrasa sngat gembira dan lebih smngat utk belajar syair jahili	\N	25	1	2017-04-26 01:05:45.271174	2017-04-26 01:05:45.271174
21	6	\N	this app really nice n benefits	\N	20	1	2017-04-26 01:05:48.932487	2017-04-26 01:05:48.932487
22	6	\N	Terbaik apps ni. 💪💃	\N	10	1	2017-04-26 01:05:50.486491	2017-04-26 01:05:50.486491
23	6	\N	أيوه! ممتاز جدا! 	\N	13	1	2017-04-26 01:05:52.714394	2017-04-26 01:05:52.714394
24	6	\N	Record tak boleh buka. \r\nMemudahkan student 😍	\N	65	1	2017-04-26 01:05:52.977019	2017-04-26 01:05:52.977019
25	6	\N	ممتع	\N	37	1	2017-04-26 01:06:00.104398	2017-04-26 01:06:00.104398
26	6	\N	very exciting and feeling amazed.thank you to the founder	\N	38	1	2017-04-26 01:06:01.131968	2017-04-26 01:06:01.131968
27	6	\N	هذا الموقع ممتاز ورائع.. :)	\N	46	1	2017-04-26 01:06:13.807689	2017-04-26 01:06:13.807689
28	6	\N	تمام	\N	28	1	2017-04-26 01:06:21.37357	2017-04-26 01:06:21.37357
29	6	\N	Bravo	\N	11	1	2017-04-26 01:06:21.724275	2017-04-26 01:06:21.724275
30	6	\N	It's fantastic! And a great apps for the new learner for us and also it's easy for us the non-native speaker in arabic language to learn more in arabic poetry	\N	12	1	2017-04-26 01:06:26.773644	2017-04-26 01:06:26.773644
31	6	\N	Jahili dah advance	\N	21	1	2017-04-26 01:06:27.541697	2017-04-26 01:06:27.541697
32	6	\N	هذا التطبيق ممتاز جدا و يسهل الطلبة لتعرف الشاعر وشعره بالطرق ممتعة و سهولة . 	\N	23	1	2017-04-26 01:06:39.400256	2017-04-26 01:06:39.400256
33	6	\N	Comment	\N	64	1	2017-04-26 01:06:40.145857	2017-04-26 01:06:40.145857
34	6	\N	ممتاز جدا!	\N	26	1	2017-04-26 01:06:41.165841	2017-04-26 01:06:41.165841
35	6	\N	جيد	\N	56	1	2017-04-26 01:06:42.490912	2017-04-26 01:06:42.490912
36	6	\N	Oh mai goddd such a great platform to those who wanna learn more about  jahili shiir  	\N	24	1	2017-04-26 01:06:43.681516	2017-04-26 01:06:43.681516
37	6	\N	👍	\N	42	1	2017-04-26 01:06:46.893948	2017-04-26 01:06:46.893948
38	1	\N	Leave a comment	\N	60	1	2017-04-26 01:06:49.153784	2017-04-26 01:06:49.153784
39	6	\N	This apps is so friendly and I really love this good job!! new apps	\N	30	1	2017-04-26 01:06:49.621444	2017-04-26 01:06:49.621444
40	6	\N	Daaaa	\N	45	1	2017-04-26 01:06:54.628092	2017-04-26 01:06:54.628092
41	6	\N	\r\n😱MasyaAllah, im soo amazed by this app. . Tabarakallah	\N	8	1	2017-04-26 01:06:57.5421	2017-04-26 01:06:57.5421
42	6	\N	تمام طويل جدا\r\nأظن بأن هذا البرنامج ممتاز جدا\r\nلأننا نستفيد منه كثيرا في تعلم اللغة العربية وتعلم الشعر الجاهيلي	\N	40	1	2017-04-26 01:06:59.191128	2017-04-26 01:06:59.191128
43	6	\N	ممتاز\r\n	\N	53	1	2017-04-26 01:07:03.214431	2017-04-26 01:07:03.214431
44	6	\N	هذا الموقع يسهل الطلبة في تعلم النصوص الجاهلية خاصة المعلقات	\N	39	1	2017-04-26 01:07:07.823495	2017-04-26 01:07:07.823495
45	6	\N	😍😍😍😍	\N	65	1	2017-04-26 01:07:18.303671	2017-04-26 01:07:18.303671
46	1	\N	مفيد جدا! 	\N	67	1	2017-04-26 01:07:33.834281	2017-04-26 01:07:33.834281
47	6	\N	المعلومات المقدمة تسهل الطلبة لفهم الأبيات الشعرية وتجذبهم لتعلمها! الشعر كان مملا بالنسبة لي من قبل لكن خلال هذا الموقع لكان أفضل! 👍🏻	\N	4	1	2017-04-26 01:07:35.35269	2017-04-26 01:07:35.35269
48	6	\N	Very good	\N	32	1	2017-04-26 01:07:35.997028	2017-04-26 01:07:35.997028
49	6	\N	apps yang sangat baik dan bagus dalam menyampaikan maklumat. info yang banyak. I love this apps	\N	22	1	2017-04-26 01:07:46.451953	2017-04-26 01:07:46.451953
50	6	\N	apps yang sangat baik dan bagus dalam menyampaikan maklumat. info yang banyak. I love this apps	\N	22	1	2017-04-26 01:07:48.638034	2017-04-26 01:07:48.638034
51	6	\N	apps yang sangat baik dan bagus dalam menyampaikan maklumat. info yang banyak. I love this apps	\N	22	1	2017-04-26 01:07:50.676725	2017-04-26 01:07:50.676725
52	6	\N	جمييييلللل!!!\r\nممكن هذا التطبيق لو يوجد تركيب الجمل عن هذه الأبيات الشعر، هذا جميل جدا	\N	43	1	2017-04-26 01:08:02.179529	2017-04-26 01:08:02.179529
53	6	\N	هذا التطبيق رائع جدا لأن يمكننا أن نتبحر في جميع العلوم العربية من حيث النحو والبلاغة.	\N	19	1	2017-04-26 01:08:16.287432	2017-04-26 01:08:16.287432
54	6	\N	This apps is so friendly and I really love this good job!! new apps	\N	30	1	2017-04-26 01:08:16.383431	2017-04-26 01:08:16.383431
55	6	\N	This apps is so friendly and I really love this good job!! new apps	\N	30	1	2017-04-26 01:08:17.323751	2017-04-26 01:08:17.323751
56	6	\N	This apps is so friendly and I really love this good job!! new apps	\N	30	1	2017-04-26 01:08:18.22474	2017-04-26 01:08:18.22474
57	1	\N	ما شاء الله...مفيد جدا	\N	68	1	2017-04-26 01:08:21.516555	2017-04-26 01:08:21.516555
58	6	\N	easy to use and surely helping in to  learn arabic poet	\N	31	1	2017-04-26 01:08:33.42558	2017-04-26 01:08:33.42558
59	6	\N	This apps is good and easyto use because there are many information that can help me in Arabic poet	\N	50	1	2017-04-26 01:08:38.134108	2017-04-26 01:08:38.134108
60	6	\N	Sangat membantu proses pembelajaran\r\n	\N	61	1	2017-04-26 01:08:58.091578	2017-04-26 01:08:58.091578
61	6	\N	هذا التطبيق  ممتاز  ومفيد جيدا 	\N	51	1	2017-04-26 01:09:00.031284	2017-04-26 01:09:00.031284
62	6	\N	terbaiklah.....😄	\N	48	1	2017-04-26 01:09:13.885812	2017-04-26 01:09:13.885812
63	7	\N	رائعععععععع	\N	59	1	2017-04-26 01:10:16.032443	2017-04-26 01:10:16.032443
64	7	\N	جميل	\N	15	1	2017-04-26 01:10:21.47588	2017-04-26 01:10:21.47588
65	7	\N	Terbaik! 	\N	65	1	2017-04-26 01:10:26.134627	2017-04-26 01:10:26.134627
66	7	\N	Masyaallah\r\n	\N	64	1	2017-04-26 01:10:47.098099	2017-04-26 01:10:47.098099
67	7	\N	واااااوووووو\r\n\r\n	\N	36	1	2017-04-26 01:11:13.622412	2017-04-26 01:11:13.622412
68	7	\N	ممتاز	\N	16	1	2017-04-26 01:11:21.023467	2017-04-26 01:11:21.023467
69	1	\N	👍👍👍👍	\N	29	1	2017-04-26 01:11:22.19115	2017-04-26 01:11:22.19115
70	7	\N	ممتاز	\N	16	1	2017-04-26 01:11:24.542889	2017-04-26 01:11:24.542889
71	3	\N	الله يبارك فيكم....	\N	59	1	2017-04-26 01:11:32.472929	2017-04-26 01:11:32.472929
72	7	\N	👍👍👍	\N	26	1	2017-04-26 01:11:41.570778	2017-04-26 01:11:41.570778
73	7	\N	مشاء الله.. مفيد جدا.. بارك الله لك	\N	47	1	2017-04-26 01:11:43.875623	2017-04-26 01:11:43.875623
74	7	\N	👍🏽👍🏽👍🏽	\N	5	1	2017-04-26 01:11:58.119298	2017-04-26 01:11:58.119298
75	1	\N	الحق اليقين	\N	13	1	2017-04-26 01:12:02.77277	2017-04-26 01:12:02.77277
76	7	\N	good job\r\n	\N	55	1	2017-04-26 01:12:05.621739	2017-04-26 01:12:05.621739
77	1	\N	جمييييييييل...	\N	9	1	2017-04-26 01:12:12.063599	2017-04-26 01:12:12.063599
78	1	\N	Setiap hari nak bukak sini! Terbaik	\N	10	1	2017-04-26 01:12:12.812201	2017-04-26 01:12:12.812201
79	1	\N	جمييييييييييييل	\N	59	1	2017-04-26 01:12:17.598709	2017-04-26 01:12:17.598709
80	7	\N	Awesome apps	\N	35	1	2017-04-26 01:12:29.608414	2017-04-26 01:12:29.608414
81	7	\N	👍👍👍👍👍	\N	17	1	2017-04-26 01:12:30.838113	2017-04-26 01:12:30.838113
82	3	\N	👍🏼👍🏼👍🏼👍🏼	\N	9	1	2017-04-26 01:12:31.707424	2017-04-26 01:12:31.707424
83	1	\N	رائع جدا!	\N	26	1	2017-04-26 01:12:37.819993	2017-04-26 01:12:37.819993
84	3	\N	Nice	\N	24	1	2017-04-26 01:12:42.615615	2017-04-26 01:12:42.615615
85	6	\N	apps yang sangat baik dan bagus. I love this apps	\N	22	1	2017-04-26 01:12:46.088761	2017-04-26 01:12:46.088761
86	7	\N	ممتعة جدا!!	\N	43	1	2017-04-26 01:12:47.957211	2017-04-26 01:12:47.957211
87	7	\N	arabic poetry is now something easy to learn 👏👏👏	\N	8	1	2017-04-26 01:12:56.584059	2017-04-26 01:12:56.584059
88	1	\N	this app really nice	\N	20	1	2017-04-26 01:12:59.837935	2017-04-26 01:12:59.837935
89	3	\N	ممتاز!	\N	26	1	2017-04-26 01:13:03.972426	2017-04-26 01:13:03.972426
90	1	\N	mabruk	\N	42	1	2017-04-26 01:13:04.560364	2017-04-26 01:13:04.560364
91	7	\N	Baek seyhh	\N	24	1	2017-04-26 01:13:07.95956	2017-04-26 01:13:07.95956
92	7	\N	ما أجمل الصوت !!	\N	7	1	2017-04-26 01:13:14.630351	2017-04-26 01:13:14.630351
93	7	\N	          رائعة جدا.......😍😍😍😍	\N	48	1	2017-04-26 01:13:16.164929	2017-04-26 01:13:16.164929
94	1	\N	this app really nice	\N	20	1	2017-04-26 01:13:22.296642	2017-04-26 01:13:22.296642
95	7	\N	ممتاااااااز	\N	9	1	2017-04-26 01:13:41.744725	2017-04-26 01:13:41.744725
96	3	\N	مفيد جدا	\N	16	1	2017-04-26 01:13:42.24445	2017-04-26 01:13:42.24445
97	1	\N	Great😃	\N	17	1	2017-04-26 01:13:48.25912	2017-04-26 01:13:48.25912
98	1	\N	😍	\N	48	1	2017-04-26 01:13:55.125317	2017-04-26 01:13:55.125317
99	3	\N	رائع	\N	20	1	2017-04-26 01:14:07.32795	2017-04-26 01:14:07.32795
100	3	\N	😍😍😍😄	\N	48	1	2017-04-26 01:14:36.733878	2017-04-26 01:14:36.733878
101	3	\N	Bagusnyaaa👏	\N	17	1	2017-04-26 01:14:39.871037	2017-04-26 01:14:39.871037
102	3	\N	جميل	\N	42	1	2017-04-26 01:17:13.319122	2017-04-26 01:17:13.319122
103	1	\N	Sangat membantu	\N	16	1	2017-04-26 01:17:44.791289	2017-04-26 01:17:44.791289
104	6	\N	👍👍	\N	70	1	2017-04-26 01:20:57.340175	2017-04-26 01:20:57.340175
105	6	\N	Mumtazzzzzzzzzz	\N	79	1	2017-04-26 09:37:49.260097	2017-04-26 09:37:49.260097
106	6	\N	mumtazzz jiddannn	\N	101	1	2017-04-26 09:38:01.639523	2017-04-26 09:38:01.639523
107	6	\N	جميل جدا	\N	117	1	2017-04-26 09:38:34.275117	2017-04-26 09:38:34.275117
108	6	\N	wowwww bawak smartphone je lps nii	\N	101	1	2017-04-26 09:38:38.91806	2017-04-26 09:38:38.91806
109	6	\N	مشاءالله .. هذا البرنامج ممتازا .... 	\N	98	1	2017-04-26 09:38:42.527317	2017-04-26 09:38:42.527317
110	6	\N	هذا التطبيق جيد	\N	105	1	2017-04-26 09:39:13.466707	2017-04-26 09:39:13.466707
111	6	\N	مدهش جدا!!!!\r\n\r\n	\N	93	1	2017-04-26 09:39:15.85862	2017-04-26 09:39:15.85862
112	6	\N	رائع جدا 😍😍	\N	85	1	2017-04-26 09:39:30.999706	2017-04-26 09:39:30.999706
113	6	\N	هذا التطبيق ممتاز.. 👍👍	\N	102	1	2017-04-26 09:39:31.332741	2017-04-26 09:39:31.332741
114	6	\N	❤️ جميل جدا	\N	78	1	2017-04-26 09:39:36.331922	2017-04-26 09:39:36.331922
115	6	\N	Sangat membantu. Tapi audio tak boleh dgr	\N	89	1	2017-04-26 09:39:39.40993	2017-04-26 09:39:39.40993
116	6	\N	هذا الموقع رائع جدا وسوف يسهل الدارسون في مجال الآداب	\N	116	1	2017-04-26 09:39:47.601465	2017-04-26 09:39:47.601465
117	6	\N	i hope there is an option for designing our own account\r\n\r\n	\N	104	1	2017-04-26 09:39:52.389303	2017-04-26 09:39:52.389303
118	6	\N	ايوه ! رائع جدا :) هذا الموقيع سهلني 🙃 	\N	103	1	2017-04-26 09:39:53.682029	2017-04-26 09:39:53.682029
119	6	\N	excellent	\N	120	1	2017-04-26 09:39:54.627939	2017-04-26 09:39:54.627939
120	6	\N	رائع، جذاب، جميل 🤗	\N	124	1	2017-04-26 09:39:55.632445	2017-04-26 09:39:55.632445
121	6	\N	ممتاز!!!!	\N	76	1	2017-04-26 09:39:56.051597	2017-04-26 09:39:56.051597
122	6	\N	ممتاز	\N	96	1	2017-04-26 09:39:57.072629	2017-04-26 09:39:57.072629
123	6	\N	ممتعة جدا! \r\n	\N	111	1	2017-04-26 09:39:57.793288	2017-04-26 09:39:57.793288
124	6	\N	love it	\N	107	1	2017-04-26 09:39:58.35595	2017-04-26 09:39:58.35595
125	6	\N	Terbaiklahh!! 😍😍	\N	90	1	2017-04-26 09:39:58.715829	2017-04-26 09:39:58.715829
126	6	\N	Wowww.. Apps ni nampak macam memudahkan students and bagus.. Cumaa lebih kan warna dan corak supaya tak hambar..  Tapi overall okayy..  Goodjob.	\N	81	1	2017-04-26 09:39:59.180597	2017-04-26 09:39:59.180597
127	6	\N	المعلومات واضحة ومفيدة...	\N	122	1	2017-04-26 09:39:59.332286	2017-04-26 09:39:59.332286
128	6	\N	I love this app	\N	119	1	2017-04-26 09:40:03.535756	2017-04-26 09:40:03.535756
129	6	\N	رائعة جدا..	\N	84	1	2017-04-26 09:40:05.777553	2017-04-26 09:40:05.777553
130	6	\N	Awesome !! تعجب !	\N	112	1	2017-04-26 09:40:13.033813	2017-04-26 09:40:13.033813
131	6	\N	Alhamdulillah.. mujur ada e-diwan. 😄👍	\N	77	1	2017-04-26 09:40:17.722873	2017-04-26 09:40:17.722873
132	6	\N	رائعة وممتازة جزا 👍👍	\N	92	1	2017-04-26 09:40:18.747554	2017-04-26 09:40:18.747554
133	6	\N	quite nice but not interesting enough..	\N	95	1	2017-04-26 09:40:18.887658	2017-04-26 09:40:18.887658
134	6	\N	هذا التطبيق مفيد جدا	\N	97	1	2017-04-26 09:40:19.688562	2017-04-26 09:40:19.688562
135	6	\N	سبحان الله ،، 	\N	109	1	2017-04-26 09:40:21.615589	2017-04-26 09:40:21.615589
136	6	\N	👍🏻👍🏻Good app	\N	80	1	2017-04-26 09:40:21.995729	2017-04-26 09:40:21.995729
137	6	\N	aiwahhh..sangat bagus dan membantu..selepas ini tidak lagi perlu bawa \r\n buku tebal2..perlu pembaikan warna yang menarik 	\N	114	1	2017-04-26 09:40:22.576711	2017-04-26 09:40:22.576711
138	6	\N	شكرا!!! يسهل لنا	\N	126	1	2017-04-26 09:40:25.582568	2017-04-26 09:40:25.582568
139	6	\N	Mumtaz...daebakk👍👍...kene bagi seri sikit.. ececey...spaya org seronok n jadi selalu nak bukak .. hihih	\N	118	1	2017-04-26 09:40:26.646095	2017-04-26 09:40:26.646095
140	6	\N	it is so fantastic and amazing apps	\N	108	1	2017-04-26 09:40:30.497577	2017-04-26 09:40:30.497577
141	6	\N	ممتاز!رائع جدا!أشعر بالتعجب.\r\nThis apps i'm sure will help us as a student in the future..\r\nMemudahkan kami!\r\n	\N	82	1	2017-04-26 09:40:34.803998	2017-04-26 09:40:34.803998
142	6	\N	رائع الجدل..  🤗	\N	113	1	2017-04-26 09:40:35.364819	2017-04-26 09:40:35.364819
143	6	\N	هذا البرنامج جيد لتعرف الشعر.ولكن لو يوجد الفيديو عن هذا الشعر يمكن  رائعة.	\N	91	1	2017-04-26 09:40:40.027869	2017-04-26 09:40:40.027869
144	6	\N	ما شاء الله! أظن بعد ذلك،لا نحتاج لنا لنحمل الكتب في الفصل. 	\N	123	1	2017-04-26 09:40:43.624766	2017-04-26 09:40:43.624766
145	6	\N	comment section kena baiki sikit	\N	86	1	2017-04-26 09:40:47.512478	2017-04-26 09:40:47.512478
146	6	\N	هذه تطبيقات رائعة جدا. يمكن أن يزدد الترجمة الشعر في اللغة المعينة أيضا إذا الشخص من غير الناطقين اللغة العربية تستفيد منها. \r\n	\N	83	1	2017-04-26 09:40:53.33848	2017-04-26 09:40:53.33848
147	6	\N	Mungkin boleh  guna moving text..biar nampak menarik..n mungkin tambah lagu2 arab  time bukak apps ni	\N	88	1	2017-04-26 09:40:59.185648	2017-04-26 09:40:59.185648
148	6	\N	Good apps for students  	\N	87	1	2017-04-26 09:40:59.671363	2017-04-26 09:40:59.671363
149	6	\N	Website yang membantu pelajar memahami dan mendalami bahasa Arab dan tarikh adab... 	\N	94	1	2017-04-26 09:41:02.655903	2017-04-26 09:41:02.655903
150	6	\N	 , \r\nwe can learn with fun 	\N	106	1	2017-04-26 09:41:07.761696	2017-04-26 09:41:07.761696
151	6	\N	😱😱😱😱\r\nWhat's a good apps!!\r\nممتعة !!\r\nMungkin blh menarikkan lagi dgan gambor2 pada setiap paraghrph.. Sebab sepertimana yg kita tau.. Sejarah ni kan..hahah.. Ok.. Thats all.. Goodluck team ptof Rahmah!! 	\N	100	1	2017-04-26 09:41:09.768899	2017-04-26 09:41:09.768899
152	6	\N	ممتعة وسهلة للطلاب	\N	110	1	2017-04-26 09:41:14.639104	2017-04-26 09:41:14.639104
153	6	\N	Best betul apps ni 👍👍👍👍 1000 bintanggg	\N	121	1	2017-04-26 09:41:19.21157	2017-04-26 09:41:19.21157
154	6	\N	apps yg sangat membantu pelajar ! Tp display and theme kurang menarik, it will be nice kalau banyak warna 👍🏻	\N	75	1	2017-04-26 09:42:01.317522	2017-04-26 09:42:01.317522
155	7	\N	the font is so tiny	\N	104	1	2017-04-26 09:42:49.874564	2017-04-26 09:42:49.874564
156	7	\N	⭐️⭐️⭐️⭐️⭐️	\N	103	1	2017-04-26 09:43:00.013562	2017-04-26 09:43:00.013562
157	6	\N	this app is very interesting and very useful to us as a student and it is very easy to search 	\N	127	1	2017-04-26 09:43:22.488744	2017-04-26 09:43:22.488744
158	7	\N	مشاءالله!	\N	79	1	2017-04-26 09:43:35.99204	2017-04-26 09:43:35.99204
159	7	\N	احب هذا الموقع جدا!	\N	116	1	2017-04-26 09:43:55.682831	2017-04-26 09:43:55.682831
160	7	\N	 font tiny kat laptop shja\r\n	\N	106	1	2017-04-26 09:46:17.876391	2017-04-26 09:46:17.876391
161	7	\N	good	\N	99	1	2017-04-26 09:46:23.117316	2017-04-26 09:46:23.117316
162	1	\N	Nice	\N	128	1	2017-04-26 10:36:03.199331	2017-04-26 10:36:03.199331
163	7	\N	Test #1	\N	2	3	2017-05-07 10:18:46.636387	2017-05-07 10:18:46.636387
164	7	\N	Test admin	\N	1	3	2017-05-07 10:55:28.413159	2017-05-07 10:55:28.413159
\.


--
-- Name: comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('comments_id_seq', 164, true);


--
-- Data for Name: lesson_lines; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY lesson_lines (id, lesson_id, line_id, created_at, updated_at) FROM stdin;
3	6	9	2017-03-31 05:35:01.120065	2017-03-31 05:35:01.120065
4	6	10	2017-03-31 05:35:01.124549	2017-03-31 05:35:01.124549
5	6	11	2017-03-31 05:35:01.126405	2017-03-31 05:35:01.126405
6	7	1	2017-04-25 15:39:03.354811	2017-04-25 15:39:03.354811
7	7	2	2017-04-25 15:39:03.378777	2017-04-25 15:39:03.378777
11	9	31	2017-05-15 03:18:08.581151	2017-05-15 03:18:08.581151
12	9	32	2017-05-15 03:18:08.583139	2017-05-15 03:18:08.583139
13	9	33	2017-05-15 03:18:08.584897	2017-05-15 03:18:08.584897
14	9	34	2017-05-15 03:18:08.586799	2017-05-15 03:18:08.586799
15	9	35	2017-05-15 03:18:08.588659	2017-05-15 03:18:08.588659
16	9	36	2017-05-15 03:18:08.590385	2017-05-15 03:18:08.590385
\.


--
-- Name: lesson_lines_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('lesson_lines_id_seq', 16, true);


--
-- Data for Name: lessons; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY lessons (id, name, title, objective, content, poem_id, poet_id, document, user_id, status, semester_id, created_at, updated_at) FROM stdin;
1	Lesson 1	الشعر في الجاهلية	\N	<p style="text-align: right;">&nbsp;.كان الشعر في الجاهلية ديوان العرب، والمصور لآمالهم وآلامهم وحياتهم ومشاهد الوجود بينهم، أودعوه وقائعهم ومفاخرهم واحسابهم وأنسابهم وأيامهم وآثارهم وذكرياتهم وأوصاف بيئتهم</p>\r\n\r\n<p style="text-align: right;">.والشعر الجاهلي صورة ناطقة ببلاغتهم وسحرهم وشدة تأثيرهم وقوة بلاغتهم وجلالة أثرهم في حياة العرب في جزيرتهم طول هذا العصر الجاهلي الغابر</p>\r\n\r\n<p style="text-align: right;">&nbsp;</p>\r\n\r\n<p style="text-align: right;"><strong>قدر الشعر</strong></p>\r\n\r\n<p style="text-align: right;">.كانت القبيلة إذا نبغ فيها الشاعر أتت القبائل فهنأتها بذلك ووضعت الأطعمة واجتمع النساء يلعبن بالمزاهر كما يصنعن في الأعراس، وكانوا لا يهنئون إلا بغلام يولد أو شاعر ينبغ أو فرس تنتج</p>\r\n\r\n<p style="text-align: right;">ولم يترك العرب شيئا مما وقعت عليه أعينهم أو وقع إلا نظموه في سمط من الشعر حتى إنك ترى مجموع أشعارهم ديوانا فيه من عوائدهم وأخلاقهم وآدابهم وأيامهم وما يستحسنون ويستهجنون، ولذلك قالوا: كان الشعر ديوان العرب ومعدن حكمتها وكنزز أدبها</p>\r\n\r\n<p style="text-align: right;">&nbsp;</p>\r\n\r\n<p style="text-align: right;"><strong>تأثير الشعر في النفوس</strong></p>\r\n\r\n<p style="text-align: right;">كان الشعر في الجاهلية هو المقيد لأيامها والشاهد على أخبارها والناطق بمجدها والمصور لمفاخرها.وكان لكل قبيلة شاعرها الذي يذود عنها ويناضل عن شرفها ويساجل خصومها وينازلهم وكانت العرب أمة يسحرها البيان وتروعها البلاغة ويستبد باعجابها الشعر المجيد البليغ</p>\r\n\r\n<p style="text-align: right;">كان الشعر قوة فعالة في الحياة الجاهلية، وكان له تأثيره في نفوسهم وسلطانه في حياتهم وقدره وخطره فيما بينهم، يرفع الخامل ويضع الفذ العظيم، وينوه بشأن القبيلة ويزري بأعدائها وخصومها، ويحفظه الناس ويروونه في كل مكان</p>\r\n\r\n<p style="text-align: right;">&nbsp;</p>\r\n\r\n<p style="text-align: right;"><strong>منزلة الشاعرفي الجاهلية</strong></p>\r\n\r\n<p style="text-align: right;">كان للشاعر في الجاهلية منزلة كبيرة وشأن خطيرلأنه هو الذي ينطق بمجد القبيلة ويشيد بمآثرها ومفاخرها وأحسابها وأبطالها، ويرد على خصومها ويذود عنها أعداءها، وكانت القبيلة في الجاهلية تفرح إذا نبغ فيها شاعر فرحا عظيما</p>\r\n\r\n<p style="text-align: right;">&nbsp;</p>\r\n\r\n<p style="text-align: right;"><strong>:قال ابن رشيق </strong></p>\r\n\r\n<p style="text-align: right;">وكانت القبيلة من العرب إذا نبغ فيها شاعر أتت القبائل فهنأتها بذلك وصنعت الأطعمة واجتمعت النساء يلعبن بالمزاهر كما يصنعن في الأفراس وتتابشر الرجال والولدان لأنه حماية لأعراضهم وذب عن أحسابهم وتخليد لمآثرهم وإشادة بذكرهم وكانوا يهنأون بغلام يولد أو شاعر ينبغ أو فرس تنتج</p>\r\n\r\n<p style="text-align: right;">.للشاعر مكانته الكبيرة في الحياة الاجتماعية في الجاهلية فهو لسان القبيلة وحكمها، ينطق فلا يعارض، ويستشفع فيشفع، ويقول فيصغي لقوله، ويشير فلا يرد له رأي</p>\r\n\r\n<p style="text-align: right;">.وكان الشاعر أرفع قدرا من الخطيب فلما كثر الشعراء وكثر الشعر صار الخطيب أعظم قدرا من الشاعر</p>\r\n\r\n<p style="text-align: right;">&nbsp;</p>\r\n\r\n<p style="text-align: right;"><strong>شاعرية العرب</strong></p>\r\n\r\n<p style="text-align: right;">.يذكر ابن قتيبة أن أبا ضمضم أنشد شعرا لمائه شاعر كلهم اسمهم عمرو</p>\r\n\r\n<p style="text-align: right;">ما هو السر في شاعرية العرب؟</p>\r\n\r\n<p style="text-align: right;">&nbsp;</p>\r\n\r\n<p style="text-align: right;"><strong>سر شاعرية العرب</strong></p>\r\n\r\n<p style="text-align: right;">.هو الصحراء وجوها الذي يوحى بالشعر ويلهم الخيال ويذكى العاطفة ويثير الشعور</p>\r\n\r\n<p style="text-align: right;">هو حياة العرب نفسهم في الجاهلية، هذه الحياة الحماسية التي دفعتهم إليها عواطفهم الثائرة، ووحدتهم في قلب هذه الصحراء الواسعة المترامية الأطراف، ورجوع العربي إلى عاطفته أكثر مما يرجع إلى عقله وتنقله في بطن الصحراء على جمله أو فرسه وسط الرمال والجبال والمتاهات، وحاجاته إلى الغناء في هذه الوحدة الشاملة ليسلي هموم نفسه وأحزانها وآلآمها</p>\r\n\r\n<p style="text-align: right;">.الاستعداد الفطري للشعر في نفس كل عربي وعربية مما كان يذكيه الخيال، ويولده التأمل وتلك الحياة الشبيهة بالحياة الصوفية المتبتلة</p>\r\n\r\n<p style="text-align: right;">ظروف الحياة الاجتماعية في الصحراء وكثرة حروبهم وغاراتهم وحاجاتهم إلى الترنم بمفاخرهم ومآثرهم وأحسابهم وأنسابهم، مما لا بد فيه من نوع جميل من الأدب يخلده ويرويه ويذيعه بين الناس وليس هذا النوع سوى الشعر</p>\r\n\r\n<p style="text-align: right;">.قوة حافظة العرب وانهم أمة أمية تعتمد على الذاكرة لا على التدوين والشعر أسهل في الحافظة رواية وأبقى تعلقا بالخاطر</p>\r\n\r\n<p style="text-align: right;">.اللغة العربية نفسها فإنها بإقرار الباحثين في أصول اللغات لغة شعرية برنين مفرداتها وأساليبها وروحها ومعانيها</p>\r\n\r\n<p style="text-align: right;">.الفراغ الكثير الذي كان يتمتع به العربي لعدم اشتغاله بصناعة أو تدبير ملك وسياسة أو إدارة مرافق دولة</p>\r\n\r\n<p style="text-align: right;">.كان العرب شعراء بالفطرة، ونظم الشعر كل عربي وعربية، كما نظمه الملوك والأمراء والسادة والحكماء والقرصان والصعاليك والعبيد وسواهم من شتى الطبقات</p>\r\n\r\n<p style="text-align: right;">&nbsp;</p>\r\n\r\n<p style="text-align: right;"><strong>شياطين الشعراء</strong></p>\r\n\r\n<p style="text-align: right;">.كان العقل البشري في العصر الجاهلي لم يستحكم نضوجه الفكري والاجتماعي، وكان الناس كثيرا ما يعيشون على الأوهام. وللعرب أوهام كثيرة نزلت عندهم منزلة الحقائق</p>\r\n\r\n<p style="text-align: right;">.فقد كانوا يعتقدون بوجود الغول وأنها تتشكل وتتلون في ضروب الثياب والصور</p>\r\n\r\n<p style="text-align: right;">.وأدى ذلك إلى أن جعل العرب للشياطين شعراء يلهمونهم الشعر والخيال، وسموهم بأسماء وافتخر كل شاعر بشيطانه</p>\r\n\r\n<p style="text-align: right;">&nbsp;</p>\r\n\r\n<p style="text-align: right;"><strong>شياطين العرب</strong></p>\r\n\r\n<p style="text-align: right;">وصور الجاهليون عقيدتهم حول شياطين الشعراء في شعرهم</p>\r\n\r\n<p style="text-align: right;">:فقال شاعر</p>\r\n\r\n<p style="text-align: right;">فان شيطاني أمير الجن# يذهب بي في الشعر كل فن</p>\r\n\r\n<p style="text-align: right;">:وقال حسان</p>\r\n\r\n<p style="text-align: right;">ولي صاحب من بني الشيصبان # فطورا أقول وطورا هوه</p>\r\n\r\n<p style="text-align: right;">&nbsp;</p>\r\n\r\n<p style="text-align: right;">:<strong>ومن شياطين الشعراء</strong></p>\r\n\r\n<p style="text-align: right;">&nbsp;الشيصبان &ndash; صاحب حسان بن ثابت</p>\r\n\r\n<p style="text-align: right;">لافظ - شيطان امرئ القيس</p>\r\n\r\n<p style="text-align: right;">هبيد &ndash; شيطان عبيد</p>\r\n\r\n<p style="text-align: right;">هاذر &ndash; صاحب النابغة</p>\r\n\r\n<p style="text-align: right;">مسحل &ndash; صاحب الأعشى</p>\r\n	\N	\N	\N	\N	1	\N	2017-03-30 07:16:39.206209	2017-03-31 03:19:28.332037
3	Lesson 2	المدخل إلى العصر الجاهلي	\N	<p style="text-align: right;"><strong>الجاهلية</strong></p>\r\n\r\n<p style="text-align: right;">وُردَتْ لفظة الجاهلية في القرآن الكريم والحديث الشريف ، وهي ليست من الجهل الذي هو ضدُّ العِلم ، ولكن من الجهل الذي هو السّفه والغضب والأنفة، فقد جاء في الذكر الحكيم في سورة البقرة : {قَالُوا أَتَتَّخِذُنَا هُزُوًا قَالَ أَعُوذُ بِاللهِ أَنْ أكُونَ مِنَ الجَاهِلِيْنَ}. وفي &nbsp;الحديث الشريف أن رسول الله صلى الله عليه وسلم قال لأبي ذر وقد عيّر رجلاً بأمه : {أنك امرؤ فيك جاهلية} ، أي فيك روح الجاهلية من سفهٍ وطيرٍ وحُمقٍ. وهي أمور أوضح ما كانت في حياة العرب قبل الإسلام.</p>\r\n\r\n<p style="text-align: right;">.وقد سميت شبه جزيرة لأن الماء يحيط بها من ثلاث جهات. وتعدّ شبه الجزيرة العربية أهم مواطن العرب</p>\r\n\r\n<p style="text-align: right;">.ويغلب على طبيعتها الجدب وأكثر سكانها بدو رحل لا يستقرون في مكان بل هم يرتحلون حيث مواطن الكلأ والعشب ، وليست شبه الجزيرة العربية كلها جدبًا وإنما هناك مناطق خصبة كاليمن، وقرى الطا</p>\r\n\r\n<p style="text-align: right;">:وينقسم عرب شبه الجزيرة إلى&nbsp;</p>\r\n\r\n<p style="text-align: right;">عدنانيين وهم عرب الشمال وأغلبهم بدوٍ رحلٍ</p>\r\n\r\n<p style="text-align: right;">قحطانيين وهم عرب الجنوب وتغلب عليهم الحضارة</p>\r\n\r\n<p style="text-align: right;">&nbsp;</p>\r\n\r\n<p style="text-align: right;">.والنظام السائد هو النظام القبليّ</p>\r\n\r\n<p style="text-align: right;">.والقبيلة هي أساس النظام الاجتماعي للعرب في الجاهلية</p>\r\n\r\n<p style="text-align: right;">.ولهذه القبائل المتحالفة وغير المتحالفة مجلس يضمّ شيوخ عشائرها ويرأسه سيّد القبيلة</p>\r\n\r\n<p style="text-align: right;">.وسيد القبيلة أو شيخها هو الذي يقودها في الحروب ويقسّم الغنائم بين أفرادها. ويستقبل الوفود ، ويعقد الصلح والمحالفات</p>\r\n\r\n<p style="text-align: right;">.ولا بد أن يتّصف بالشجاعة والحلم والكرم والنجدة وحفظ الجواد وغالبًا ما تورث السيادة في القبيلة</p>\r\n\r\n<p style="text-align: right;">.والرباط الذي يربط أفراد القبيلة إنما هو العصبية</p>\r\n\r\n<p style="text-align: right;">&nbsp;ينقسم العرب إلى بدو وحضر. فأما البدو وهم القسم الغالب ، فكانوا يعيشون على الرعي ، يرعون الماشية والأغنام ، يأكلون لحومها ويشربون ألبانها</p>\r\n\r\n<p style="text-align: right;">.وأغلب البدو يعيشون في شهامة ونجد وصحراء النفوذ والدهناء والبحري</p>\r\n\r\n<p style="text-align: right;">وأما الحضر فقد كانوا يسكونون المدن مثل مكة والمدينة وخيبر واليمن وامارتى الغساسنة والمناذرة وهؤلاء عرفوا الزراعة والتجارة. واشتهرت من بينهم مكة بتجارتها العظيمة وأسواقها المشهورة</p>\r\n\r\n<p style="text-align: right;">.ولقد بعثت حياة الصحراء في العربي خصالاً متعددة منها المحمود وغير المحمود أو المذموم</p>\r\n\r\n<p style="text-align: right;">&nbsp;:فمن <strong><u>خصالهم المحمودة</u></strong></p>\r\n\r\n<p style="text-align: right;">الكرم.</p>\r\n\r\n<p style="text-align: right;">الشجاعة</p>\r\n\r\n<p style="text-align: right;">الوفاء</p>\r\n\r\n<p style="text-align: right;">العفّة والأنفة وإباء الضيم وإغاثة الملهوف وحماية الضعيف</p>\r\n\r\n<p style="text-align: right;">&nbsp;</p>\r\n\r\n<p style="text-align: right;">.خصال مذمومة وأهمها شرب الخمر ولعب الميسر واستباحة النساء</p>\r\n\r\n<p style="text-align: right;">. كانت كثرة العرب في الجاهلية وثنيّةً ،&nbsp;فقد تعددت آلهتهم وأصنامهم ، وانتشرت عبادة الشمس والنجوم ، وجعلوا للحج أربعة أشهر سمّوها الأشهر الحرم وهي رجب وذو القعدة وذو الحجة والمحرم</p>\r\n\r\n<p style="text-align: right;">.ومن العرب من أنكر الأوثان واعتزل الخمر والسكر وحرّم الأزلام. وهم المعرَّفون باسم &quot;الحنفاء&quot; ، وهم بقايا دين إبراهيم عليه السلام وقد عرفوا هؤلاء التوحيد</p>\r\n\r\n<p style="text-align: right;">.وعرف العرب اليهودية لاستقرار بعض اليهود في اليمن ويثرب&nbsp;</p>\r\n\r\n<p style="text-align: right;">.وعرف العرب النصرانية وكانت أكثر انتشارًا في شمالي الجزيرة إذ كان يدين بها المناذرة والغساسنة ، فنصرانية النعمان وملوك غسان معروفة مشهورة</p>\r\n\r\n<p style="text-align: right;">.نظم الشعراء الجاهليون في فنون الشعر المختلفة من فخر ومدح ووصف وغزل وهجاء ورثاء وحكمة واعتذار</p>\r\n\r\n<p style="text-align: right;">.كان لعرب الجاهلية أسواق كثيرة يجتمعون فيها للتجارة وتبادل المنافع وللأدب كسوق عكاظ ومـجنّة وذي المجاز</p>\r\n	\N	\N	\N	\N	2	\N	2017-03-30 08:22:07.464187	2017-03-31 03:19:44.876733
6	Lesson 3	معلقة لَبِيدٍ بن رَبِيعَة	\N		3	2	\N	\N	3	\N	2017-03-31 05:35:01.116658	2017-03-31 05:35:46.006881
7	Lesson 4	معلقة امرئ القيس	\N		1	1	\N	\N	2	\N	2017-04-25 15:39:03.345176	2017-04-25 15:39:03.345176
9	النابغة	LESSON 5 TEST	\N		6	4	\N	\N	2	\N	2017-05-15 03:18:08.577639	2017-05-15 03:18:08.577639
\.


--
-- Name: lessons_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('lessons_id_seq', 9, true);


--
-- Data for Name: line_references; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY line_references (id, line_id, content, media, reference_type, created_at, updated_at) FROM stdin;
3	1	<p dir="rtl">نلاحظ بلاغة التقديم في ذكره للحبيب قبل المنزل، لأن الحبيب هو المقصود، والمنزل تأكيد لوجوده. ومما يلفت النظر إليه أن الشاعر بدأ صدر البيت بجملة إنشائية، تصدرها بأسلوب الأمر، وانتهى بجملة خبرية في عجز البيت، والجمل في البيت موصولة، تتصدرها علامات العطف. واستخدام حروف العطف لها موقع من النفس؛ حيث استخدم &quot;الواو&quot; في صدر البيت لتفيد الصحبة، بينما استخدامه &quot;الفاء&quot; يفيد التتابع أي أن &quot;الدخول&quot; أولا، وتليها &quot;حومل&quot; ثانيا.</p>\r\n	\N	3	2017-03-30 12:58:30.665149	2017-03-30 12:58:30.665149
4	9	<p style="text-align: right;">إنّ الإنسان يبلى بعد ما يموت لأنّ الأرض تفنيه وتأكل جسمه، أمّا النجوم التي تطلع والجبال الراسية، ومخازن الماء المبنية أو القصور المشيدة فإنّها تبقى بعد الإنسان ولا تبلى</p>\r\n	\N	1	2017-03-30 12:58:33.866586	2017-03-30 12:58:33.866586
5	1	يطلب من رفيقيه وصاحبيه أن يقفا ويعيناه على البكاء بسبب ذكره حبيبتَه التي فارقها والمنازلَ التي كانت فيها في مُنْقَطَعِ الرمل الذي يَقع بين الدخول فحومل. 	\N	1	2017-03-30 12:59:23.966692	2017-03-30 12:59:23.966692
7	9	<p style="text-align: right;">يوجد في عجز البيت تقديم وتأخير في قوله (وتبقى الجبال بعدنا والمصانع)، وأصل الكلام (وتبقى الجبال والمصانع بعدنا). وغرض تقديم (بعدنا) على (المصانع) هو مناسبة القافية، فالقصيدة عينية، فجاءت (المصانع) في هذا المقام مناسبة للقافية. كما يوجد في البيت مطابقة في قوله (تبلى &ndash; تبقى)، فتبقى معناها تفنى أي تنتهي وهي مقابلة لمعنى تبقى أي ماتزال باقية</p>\r\n	\N	3	2017-03-30 13:03:45.687445	2017-03-30 13:03:45.687445
8	2	<p dir="rtl">إن كثافة ظلام الليل في هوله وصعوبته ووحشته ونكارة أمره تشبه موج البحر الذي حلّ فيه الظلام، وإن هذا الليل قد طال زمنه وأرخى وسدل ستوره عليَّ فزاد الظلامُ مع أنواعٍ من الهموم وضُروبٍ من الأحزان، ليختبرني ويبتلي ما عندي من الصبر على هذه الشدائد والنوائب أو الجزع منها. وفي هذا يمدح الشاعر نفسه بالصبر والجَلَدِ.&nbsp;</p>\r\n	\N	1	2017-03-30 13:04:17.224111	2017-03-30 13:04:17.224111
9	10	<p style="text-align: right;">&nbsp; يقول: كنتُ أعيشُ بجانب جار جَيِّدٍ يُبْخَلُ به ولا يُفَرَّطُ فيه، وقد فارقني من أخي أَرْبَدَ جارٌ نافع مفيدٌ لذلك حزنت على فراقه وفقده بالموت</p>\r\n	\N	1	2017-03-30 13:04:33.756571	2017-03-30 13:04:33.756571
11	2	<p dir="rtl">&nbsp;يستخدم الشاعر مباشرة تشبيه مرسل مجمل، حيث شبه الليل بأمواج البحر، فذكر المشبه، وهو &quot;الليل&quot;، والمشبه به &quot;أمواج البحر&quot;، وأداة التشبيه &quot;الكاف&quot;، وحذف الشاعر وجه الشبه.</p>\r\n	\N	3	2017-03-30 13:05:56.15145	2017-03-30 13:05:56.15145
13	10	<p style="text-align: right;">في البيت محاز عقلي في قوله: (وَقَـدْ كُنْـتُ فِي أَكْنَافِ جَارِ مَضِنَّةٍ). فقد أسند الشاعر كونه إلى جوانب جاره، مع أنَّه لم يكن بجانبه، بل كان يسكن في بيت يجاور بيت ذلك الشخص، لأنَّ الذي كان بجوار الجيرة هو منزله. فالمجاز هنا مجاز عقلي علاقته المكانية أو الحالية</p>\r\n	\N	3	2017-03-30 13:09:15.480041	2017-03-30 13:09:15.480041
14	11	<p style="text-align: right;">&nbsp; أنا لا أجزع من المصائب التي تحِلُّ بي بفراقِ أخٍ وابن عمٍ ولا أجزعُ لميّتٍ ماتَ من أهلي لأنّ قلبي قد عوَّدتْهُ المصائبُ، ثم إن كلَّ إنسان لا بدّ أن يفجعه الدهرُ في يومٍ من أيّامه وأنا واحد من هؤلاء المفجوعين فلا أُبالي</p>\r\n	\N	1	2017-03-30 13:10:14.662498	2017-03-30 13:10:14.662498
16	11	<p style="text-align: right;">في البيت مجاز عقلي في قوله (إنْ فرَّق الدهر بيننا). فقد أسند الشاعر التفريق أو الفراق إلى الدهر، مع أنَّ الدهر في حقيقته لا يفرِّق، فأسند الفعل إلى غير فاعله. فالذي فرَّق بينهم هي الحوادث التي حدثت في الدهر، وليس الدهر بذاته. فالمجاز هنا مجاز عقلي علاقته الزمانية</p>\r\n	\N	3	2017-03-30 13:12:53.113176	2017-03-30 13:12:53.113176
17	3	<p dir="rtl">قلت لليل لما طالَ زمنه وجَلَبَ حزنه وآلامه ومدّ ظهره وصلبه وزادت مؤخرته امتدادًا وتطاولاً وبَعُدَ صدره، قلت له: انكشِفْ أيها الليل الطويل بالهم والحزن؛ لأن المغهومَ يستطيل ليلَه، والمسرورَ يستقصر ليله، وطول الليل الذي أحسّ به الشاعر دليل على مقاساة الأحزان والشدائد والسهر المتولد من الغمِّ.</p>\r\n	\N	1	2017-03-30 13:13:24.461728	2017-03-30 13:13:24.461728
18	12	<p style="text-align: right;">&nbsp; إذا أتاني مال جديد فلا أفرح به، وكذلك لا أفرحُ بأيِّ شيئ يُسِرُّ، لأنّه لا يثير في نفسي فرحًا، ولا أجزع إن مسني الضر بسبب ما أوجده الدهر من النكبات والكوارث والمصائب، فلا تُورِثُني جَزَعًا لتكرُّرِها</p>\r\n	\N	1	2017-03-30 13:13:43.371055	2017-03-30 13:13:43.371055
20	12	<p style="text-align: right;">يوجد في البيت مجازان من المجازات العقلية، وذلك في صدر البيت وفي عجزه. فقال (يأتيني طريف بفرحة). فالطريف لا يأتي بالفرحة لكنه أسند الإتيان إلى الطرافة، فأسند الفعل إلى غير فاعله الحقيقي، فالذي يأتي بالطريف هو أفعال الإنسان وأعماله، وليس الطريف نفسه، فهذه الأفعال والأعمال كانت سببًا في الطرافة، فالمجاز هنا عقلي علاقته السببية.وقوله (أحدث الدهر جازعًا) مجاز عقلي علاقته الزمانية، وذلك لأنَّه أسند الفعل إلى غير فاعله الحقيقي. فقد أسند الحدوث إلى الدهر مع أنَّ الدهر لم يحدث شيئاً، لكن الأحداث من مصائب ومحاسن هي التي أحدثت تلك الأحداث</p>\r\n	\N	3	2017-03-30 13:15:30.818462	2017-03-30 13:15:30.818462
22	13	<p style="text-align: right;">إن النّاسَ الأحياءَ يموتون في هذه الحياة الدنيا، ومَثَلُهُمْ كالديار العامرةِ اليوم بأهلها وزروعها ومائها، فإنّها تَقْفُرُ غدًا من أهلها وخيراتها، وكلّ ذلك إلى تغير وزوال</p>\r\n	\N	1	2017-03-30 13:16:26.237624	2017-03-30 13:16:26.237624
24	3	<p dir="rtl">يتحدث الشاعر إلى الليل، وتظهر الاستعارة المكنية في الكلمات الآتية؛ &quot;تمطى، أردف،ناء&quot;، وكل هذه الصفات لناقته، ولكنها أُسندت إلى الليل، فالمشبه به محذوف وهو &quot;الناقة&quot;، ورمز إليه بشئ من لوازمه، مع قرينة لفظية مانعة من أداء المعنى الحقيقي &quot;صلب،أعجاز،كلكل&quot;.</p>\r\n	\N	3	2017-03-30 13:18:10.593482	2017-03-30 13:18:10.593482
25	13	<p style="text-align: right;">في البيت تشبيه مرسل واضح. فقدشبَّه الناس بالديار التي كان يسكنها الناس، ثم انتهت بخروج الناس منها. فالمشبَّه الناس، أي حياة الناس، والمشبه به (الديار التي خلَتْ من الناس بعد أن كانت حيَّة بها، ووجه الشبه (الفناء والانتهاء)، وأداة التشبيه - الكاف</p>\r\n	\N	3	2017-03-30 13:20:09.141373	2017-03-30 13:20:09.141373
26	14	<p style="text-align: right;">كلّ إنسان يَخْبُو بعد توقُّدٍ حين يُدركه الأجلُ وتوافيه المنيةُ ومَثَلَهُ مَثَلُ النار تكونُ ساطعةَ الضوءِ ثم تصيرُ رمادًا بعد ذلك</p>\r\n	\N	1	2017-03-30 13:20:56.438595	2017-03-30 13:20:56.438595
28	14	<p style="text-align: right;">تشبيه مرسل آخر. فقد شبع الشاعر الإنسان، أو بمعنى أصح حياة الإنسان، شبهها بشهاب النار وضوئها. فالمشبَّه (المرء &ndash; أي الإنسان) والمشبه به (شهاب النار وضوئه)، ووجه الشبه هو فترة هذا الضوء الذي حتمًا سينتهى أجله. وأداة التشبيه الكاف</p>\r\n	\N	3	2017-03-30 13:24:40.188616	2017-03-30 13:24:40.188616
29	15	<p style="text-align: right;">إنّ البرّ والإحسان ما أضمره القلب من أنواع الخير والتقوى وينفع في الآخرة، وأمّا المالُ فهو لك ما عمرت في هذه الحياة فإذا مِتَّ فلا شيئ لكَ منه، لأنّه وديعة بين يديك</p>\r\n	\N	1	2017-03-30 13:25:27.338643	2017-03-30 13:25:27.338643
32	4	<p dir="rtl">يطالب الليلَ الطويلَ المظلمَ الذي سبب له الأحزان والمصائب بأن ينجلي ويرحل بصبح يحيل الظلام ضياءً، فَلَيسَ الصبح بأفضل من هذا الليل القاسي إذا بقي يقاسي الهموم نهارًا كما عاناها ليلاً.</p>\r\n	\N	1	2017-03-30 13:46:57.771422	2017-03-30 13:46:57.771422
34	1	<p dir="rtl">&nbsp;صوّر الشاعر صورة من نسيج خياله حيث تخيّل أنه يسير مع صاحبيه، فاستوقفهما على عادة العرب حتى يشاركانه في شوقه وتحنانه إلى تلك الديار التي تسكنها محبوبته، وهي كائنة في ذلك الموقع الذي يكثر فيه تساقط الرمل،وهذا الموقع يقع بين الدخول فحومل.</p>\r\n\r\n<p dir="rtl"><img alt="" src="/uploads/ckeditor/pictures/1/content_image9.jpg" style="width: 120px; height: 90px;" /></p>\r\n	\N	2	2017-03-30 13:51:54.964076	2017-03-30 13:51:54.964076
35	4	<p dir="rtl"><b>يخاطب الشاعر الليل وكأنه إنسان حيّ يعي ما يُقال، فهنا شبه الشاعر الليل بإنسان ناطق، وحذف هذا الإنسان الناطق، وهو المشبه به، والقرينة حالية في ظني.</b></p>\r\n	\N	3	2017-03-30 13:53:35.290471	2017-03-30 13:53:35.290471
36	9	<p style="text-align: right;">يصوِّر لنا الشاعر حقيقة وجود الإنسان في هذه الدنيا. فهو منتهي وسوف يُفنى، بينما الأشياء التي تحيط من حوله من النجوم والجبال والقصور فهي باقية حتى بعد موته</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/15/content_Picture1.jpg" style="width: 120px; height: 90px;" /></p>\r\n	\N	2	2017-03-30 13:54:20.671026	2017-03-30 13:54:20.671026
37	10	<p style="text-align: right;">جد الصورة الفنية في هذا البيت عندما أشار الشاعر بقوله (أكناف جار مضنَّة). فكلمة الأكناف صوَّرت حال المنزل الذي نزل فيه، أي أن جميع أطراف المنزل كان لها نصيب من حسن هذا الجوار. كما أنَّ كلمة (مضنة) لها جمالاً أكثر من كلمة - ضنين)</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/16/content_Picture2.jpg" style="width: 133px; height: 90px;" /></p>\r\n	\N	2	2017-03-30 13:56:40.859577	2017-03-30 13:56:40.859577
38	2	<p style="text-align: right;">يرسم الشاعر صورة جميلة لليل، وهو في كثافة ظلمته، إذا أرخى سدوله أي دخل، كأمواج البحر في كثافتها وزرقتها، وعندئذ تبدأ هموم الشاعر. فقد جاء الليل ليختبره، ويمتحن مدى صبره على هذا الابتلاء</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/17/content_image10.jpg" style="width: 142px; height: 94px;" /></p>\r\n	\N	2	2017-03-30 14:00:26.756849	2017-03-30 14:00:26.756849
39	11	<p style="text-align: right;">يصف الشاعر ما يحدث في هذه الدنيا. وهي في أغلبيتها مفجعة لأي شخصٍ كان. فالمصائب التي تحل على الإنسان لا يتوقعها. وهذا هو سبب الجزع والفزع. وقد ذاق منها الشاعر الكثير والكثير حتى أصبحت بالنسبة له شيء عادي لا يبالي بها</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/18/content_Picture3.jpg" style="width: 120px; height: 81px;" /></p>\r\n	\N	2	2017-03-30 14:01:24.148703	2017-03-30 14:01:24.148703
40	3	<p dir="rtl">يصف الشاعر الليل &ndash; في الظاهر- ولكن حقيقة يأخذ وصفه من وصف ناقته التي بدأت في الجلوس، والمعروف أن الناقة تأخذ زمنا طويلا نسبيا في حالة الجلـوس، وتقـوم بحركـات مختلفة لذلك.</p>\r\n\r\n<p dir="rtl"><img alt="" src="/uploads/ckeditor/pictures/19/content_image11.jpg" style="width: 136px; height: 151px;" /></p>\r\n	\N	2	2017-03-30 14:01:47.243351	2017-03-30 14:01:47.243351
41	12	<p style="text-align: right;">يعبر الشاعر عن حالته وهو في هذا السن. فقد أخذ الكثير من أهوال هذه الدنيا. فكل ما يأتيه الآن لا يفرحة ولا يفزعه. فقد أصبحت مألوفة لديه</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/20/content_Picture4.jpg" style="width: 120px; height: 75px;" /></p>\r\n	\N	2	2017-03-30 14:02:40.406571	2017-03-30 14:02:40.406571
42	4	<p dir="rtl">يرى الشاعر أن ليله قد طال، ويتمنى أن ينكشف الليل ويطلع النهار، أي يظهر الصبح، وإن كان الصبح ليس بأحسن حالا من الليل، ففي كلاهما قلق واضطراب</p>\r\n\r\n<p dir="rtl"><b>.<img alt="" src="/uploads/ckeditor/pictures/21/content_image12.jpg" style="width: 217px; height: 162px;" /></b></p>\r\n	\N	2	2017-03-30 14:03:10.356457	2017-03-30 14:03:10.356457
43	13	<p style="text-align: right;">يصور لنا الشاعر حال الناس ويشبههم باليار التي خلت سكانها منها بعد أن كانت تملأ نوراً وضجيجاً بهم. لكنها الآن فارغة ميِّتة</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/22/content_Picture5.jpg" style="width: 120px; height: 102px;" /></p>\r\n	\N	2	2017-03-30 14:03:56.360986	2017-03-30 14:03:56.360986
44	14	<p style="text-align: right;">وصورة أخرى يخرجها لنا الشاعر من خفايا خلجاته، فهو يشبه الإنسان هنا بالنار المضيئة. فنحن نعلم أنَّ النار لا تكون مضيئة دائمًا، بل سيلأتيها وقت فتخفت ويضعف ضوؤها. وهذا هو حال الإنسان، فإنَّه في يوم من الأيام سيضعف ويهرم شيئًا فشيئًا حتى تلقاه المنيَّة، فينتهي تمامًا</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/23/content_Picture6.jpg" style="width: 120px; height: 90px;" /></p>\r\n	\N	2	2017-03-30 14:05:28.118983	2017-03-30 14:05:28.118983
48	17	<p style="text-align: right;">&nbsp; إنّ الناسَ والأهل يذهبون إلى الآخرة جماعة بعد جماعة ونحن نبقى بعدهم أحياءً ثم نلحق بهم ونرحل من هذه الدنيا، وذلك شبيه بالإبل التي تسير ويتخلف بعضها عن القطيع ثم يزجرها الراعي لها لتلحق بالركب وتنضم إليه</p>\r\n	\N	1	2017-03-30 14:10:28.851077	2017-03-30 14:10:28.851077
50	17	<p style="text-align: right;">في البيت تشبيه مرسل. فقد شبَّه الشاعر رحيل السلف ثم لحوقنا بهم بالإبل التي تخلَّفت عن القطيع ثم يأتي الراعي ويزجرها لتلحق بالسابقات. فالمشبه (نحن الذي تخلَّفنا عن السلف) في قوله: ونخلف بعدهم، والمشبه به (الإبل المتأخرة عن بقية القطيع) في قوله: التاليات، ووجه الشبه (اللحوق بالأوائل)، وأداة التشبيه - الكاف)</p>\r\n	\N	3	2017-03-30 14:11:19.712762	2017-03-30 14:11:19.712762
51	17	<p style="text-align: right;">يرسم الشاعر لنا صورة هذه الحياة. فالأولون ذهبوا، وها نحن نخلفهم، لكننا بعد حين سنلحق بهم، وسيأتي من بعدنا من يخلفنا، لكنهم أيضاً سيرحلون إلى حيث رحلنا مع من رحل من السابقين منا</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/25/content_Picture9.jpg" style="width: 120px; height: 101px;" /></p>\r\n	\N	2	2017-03-30 14:13:04.501377	2017-03-30 14:13:04.501377
52	15	<p style="text-align: right;">يوجد في صدر البيت كناية عن الموصوف في قول الشاعر (وما البر إلاّ مضمرات من التقى). فالكناية هنا في كلمة (مضمرات). فقد أراد الشاعر بها موطن هذا الإضمار وهو القلب. فالقلب هو المكان الذي يضمر فيه الخفايا من التقوى وفعل البر والإحسان. ولكنه انصرف عن ذكر القلب، وجاء بصفة من صفاته وهي الإضمار، أي إضمار الأفعال والأقوال</p>\r\n	\N	3	2017-03-30 14:16:55.824665	2017-03-30 14:16:55.824665
53	15	<p style="text-align: right;">هنا صورة فنية جميلة أشار إليها الشاعر وذلك في قوله (البر مضمرات &ndash; والمال معمرات). فقد جعل الشاعر البر والإحسان وكل الأعمال المعنوية من الأشياء التي لا يعلمها سوى المرء نفسه، فهي من الأفعال المضمرة المخفية التي تكمن في القلوب. ثم جعل المال وجميع المظاهر الحسية من المعمرات التي يمكن أن نراها ونلاحظها بالأعيُن، لهذ استخدم كلمة (معمرات) لأننا يمكن رؤيتها فهي تعمَّر أمام أعيننا</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/26/content_Picture7.jpg" style="width: 120px; height: 103px;" /></p>\r\n	\N	2	2017-03-30 14:17:41.53613	2017-03-30 14:17:41.53613
55	5	<p dir="rtl">يخاطب الليل بقوله: فيا عجبًا لك من ليل فيه نجومٌ كأنَّها شُدَّتْ في الجَبَل وصخوره الصلبة بحبل قويّ محكم الفتل فهي ثابتة لا تزول ولذلك أصبح الليل طويلاً عليه لشدّة ما يعاني من الهموم ومقاساة الأحزان فيه.</p>\r\n	\N	1	2017-03-30 14:21:01.850299	2017-03-30 14:21:01.850299
56	16	<p>&nbsp; إنّ المالَ الذي بيد الإنسان وإن أهل الإنسان وأقاربه وديعة وأمانة استودعها الله تعالى هذا الإنسان فهو وكيل ومؤتمن ليحافظ عليها من الضياع والتلف، لأنّ مصير الوديعة والأمانة أن تعود إلى صاحبها في يوم من الأيام فلا بدّ أن تعود سالمة كما هي</p>\r\n\r\n<p>&nbsp;</p>\r\n	\N	1	2017-03-30 14:21:04.654226	2017-03-30 14:21:04.654226
57	16	<p style="text-align: right;">نلاحظ الوصل في البيت بين الجملتين (وَمَا المَـالُ وَالأَهْلُـونَ إِلاَّ وَدِيعَـةٌ) و بين (وَلاَ بُـدَّ يَـوماً أَنْ تُـرَدَّ الـوَدَائِـعُ)، أضف إلى هذا فإنَّ هناك وصل بين هذا البيت والبيت الذي سبقه. فقد وصل الشاعر جملة (وما المال والأهلون ..) بجملة (وما المال إلاّ معمرات ودائع). فالبيتان جكلتان خبرية ومتفقان معنى ومضمون، والتناسب بينهما وضح، فاستوجب وصل الجملتين ببعضها البعض. ثم إنَّ جملة (وما المال والأهلون إلاّ وديعة) اتَّفقت معنًا</p>\r\n	\N	3	2017-03-30 14:22:37.788572	2017-03-30 14:22:37.788572
59	16	<p style="text-align: right;">وصورة فنية جميلة أخرى عندما استخدم الشاعر كلمة (وديعة) أي أمانة. فالشاعر يرى أنَّ هذه الأشياء التي اكتسبها وامتلكها الإنسان ما هي إلاَّ أشياء موضوعة له ليحافظ عليها ثم يعيدها كما كانت. وكأنه يقول: أنَّ هناك مالك حقيقي لهذه الأموال والأشياء الأخرى، ولست أنت أيها الإنسان</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/24/content_Picture8.jpg" style="width: 120px; height: 124px;" /></p>\r\n	\N	2	2017-03-30 14:23:46.698351	2017-03-30 14:23:46.698351
61	18	<p style="text-align: right;">&nbsp; إنّ الناسَ والأهل يذهبون إلى الآخرة جماعة بعد جماعة ونحن نبقى بعدهم أحياءً ثم نلحق بهم ونرحل من هذه الدنيا، وذلك شبيه بالإبل التي تسير ويتخلف بعضها عن القطيع ثم يزجرها الراعي لها لتلحق بالركب وتنضم إليه</p>\r\n	\N	1	2017-03-30 14:25:23.270754	2017-03-30 14:25:23.270754
62	5	<p dir="rtl">يتعجب الشاعر من الليل، فيقول له: يالك من ليل، أي يتعجب من هذا الليل الطويل الذي لا تريد نجومه أن تبرح من مكانها، وكأنها رُبِطَتْ وشُدَّتْ بالجبل. فكأنَّ الشاعر يقول لهذا الليل العجيب: متى تريد أن تنـزاح عني، نجومك ثابتة لا تتحرك أبداً.</p>\r\n\r\n<p dir="rtl"><img alt="" src="/uploads/ckeditor/pictures/28/content_image13.jpg" style="width: 136px; height: 91px;" /></p>\r\n	\N	2	2017-03-30 14:25:28.311132	2017-03-30 14:25:28.311132
63	5	<p dir="rtl">هنا يشبه الشاعر نجوم الليل في ثباتها وعدم تحركها كما لو كانت قد شُدَّتْ بشيء مفتول قوي إلى جانب هذا الجبل (يبذل). فالمشبَّه (نجوم الليل) الثابتة، والمشبه به الجملة الفعلية (شُدَّت بيَذْبُل)، ووجه الشبه (الثبات) وهو محذوف، وأداة التشبيه (كأنَّ).</p>\r\n	\N	3	2017-03-30 14:26:06.83725	2017-03-30 14:26:06.83725
64	18	<p style="text-align: right;">يوجد في عجز البيت حذف في قوله (وآخر رافع) أي وعامل آخر، فغرض هذا الحذف هو الإختصار لمعلوميته كما أنَّه يفهم من السياق. فالناس عاملان، ذكر أحدهما، وبالتالي فإنَّ الآخر هو العامل الثاني</p>\r\n	\N	3	2017-03-30 14:26:32.780535	2017-03-30 14:26:32.780535
65	18	<p style="text-align: right;">صورة فنية جميلة حين جعل الشاعر هذه الدنيا في طائفتين من الناس، ومثلهما بعاملين، عامل يبني، والآخر يهدم. وهذه بالفعل هي حقيقة بني آدم. فهناك من يعمِّر الديار وهناك من يخرِّب ما عمَّره الأول</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/29/content_Picture10.png" style="width: 120px; height: 107px;" /><img alt="" src="/uploads/ckeditor/pictures/30/content_Picture11.png" style="width: 120px; height: 93px;" /></p>\r\n	\N	2	2017-03-30 14:28:34.409683	2017-03-30 14:28:34.409683
66	19	<p style="text-align: right;">إنّ الناس نوعان كما ذكر في البيت السابق، فمنهم سعيد يأخذ بنصيبه وحصّته المقدرةِ له، ومنهم شقي قانع وراضٍ بمعيشته التي هو عليها رغم قسوتها</p>\r\n	\N	1	2017-03-30 14:29:28.577527	2017-03-30 14:29:28.577527
67	19	<p style="text-align: right;">في البيت مطابقة الإيهام في اللفظتين (سعيد &ndash; شقي). فلفظة (سعيد) ليست بضد (شقي) بل التي ضدها هي كلمة (حزين). فالسعادة التي قصدها الشاعر هنا هي السعادة التي جاءت من الراحة والرفاهية. لهذا جاء بكلمة شقي لأنَّ الشقاء هو المضاد الحقيقي لراحة البال والنفس. فجاءت السعادة توهم بأنها السرور، مع أنها في الحقيقة هي السعادة التي حصلها من نصيبه الطيب في الحياة</p>\r\n	\N	3	2017-03-30 14:30:52.691561	2017-03-30 14:30:52.691561
68	19	<p style="text-align: right;">وتصوير آخر لحال الناس. فهناك فئة سعيدة، وهناك فئة تعيسة شقية. لكن الشاعر جعل الفئة الشقية هي الفئة الشقية القانعة بقدرها، ونسي أنْ يقسم هذه الفئة إلى قسمين. فالشقية يوجد منها من رضيت بنصيبها واقتنعت، كما يوجد منها من قنطت وكفرت بالششقاء الذي حلَّ بها. مع هذا فإنَّ الصورة الفنية التي صوَّرها الشاعر كانت جميلة خاصةً عندما جاء بالمطابقو في سعيد وشقي</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/31/content_Picture12.jpg" style="width: 120px; height: 80px;" /></p>\r\n	\N	2	2017-03-30 14:32:24.874817	2017-03-30 14:32:24.874817
69	20	<p style="text-align: right;">إِنْ تأخَرَ أجلي وأبطأتْ منيتي وامتدّ عمري فإنَّ أمامي ضعفًا وشيخوخة تجعلني أتوكأ على العصا التي تلازمها وتحنو عليها أصابع يدي</p>\r\n	\N	1	2017-03-30 14:33:20.341421	2017-03-30 14:33:20.341421
70	20	<p style="text-align: right;">يوجد في البيت كناية عن الشيخوخة وذلك في قوله (لزوم العصى تحنى عليه الأصابع). فمعلوم أنَّ الشيخوخة والكبر في السن يأتي بالهرم وهو الضعف وقلة الحيلة. وهذا الضعف وقلة الحيلة يتطلب الإعتماد على العصى للتوكُّؤ عليها. وعندما تعمد إلى العصى لتمسك به فإنَّك حتمًا تحنى أصابعك عليه لتمسك به. فالشاعر هنا بدل أن يذكر الشخوخة صراحة، كنى هذه الشيخوخة ببعض مواصفاتها، وهذا ما يسمى بكناية الموصوف لأن الشيخوخة يوصف بأنها سبب في لزوم الإعتماد على العصى.</p>\r\n\r\n<p style="text-align: right;">&nbsp;</p>\r\n	\N	3	2017-03-30 14:34:02.161037	2017-03-30 14:34:02.161037
71	20	<p style="text-align: right;">يصوِّر الشاعر حال من تأخَّر عنه الأجل. فتأخُّر الأجل عن موعده &ndash; كما يظنُّه الشاعر ويعبر عنه &ndash; لا يعني أنَّ المرء يعيش في قوة الشباب، بل هو في سنٍ يلزمه التوكُّؤ على العصا، بل أنَّ الضعف والعجز يزداد يوماً عن يوم كلما تأخر عنه الأجل</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/32/content_Picture13.jpg" style="width: 120px; height: 139px;" /></p>\r\n	\N	2	2017-03-30 14:34:57.110829	2017-03-30 14:34:57.110829
72	21	<p style="text-align: right;">&nbsp; يصف حاله إذا تراخى به الأجل وعاش عمرًا طويلاً بأنّهُ سيحدِّثُ الجيلَ الجديدَ عن أخبار القرون السالفة، وأنّه سيبلغ من الضعف حالةً تجعله يسير على الأرض سيرًا لينًا متوكأ على العصا كلما قَامَ بقيَ ظهرُه منحنيًا كأنّه راكع</p>\r\n	\N	1	2017-03-30 14:35:38.992419	2017-03-30 14:35:38.992419
73	6	<p dir="rtl">قد أُغَادِرُ صباحًا والطيورُ لا تزالُ في عشها وأوكارها نائمة، وانطلق بفرس منجرد يرجع في السير ضخم ويستطيع من سرعته أن يُلْحِقَ الوحوشَ والأوابدَ هزيمةً فيصير بمنزلة القيد لها.</p>\r\n	\N	1	2017-03-30 14:35:56.086685	2017-03-30 14:35:56.086685
74	21	<p style="text-align: right;">في البيت تشبيه مرسل في قول الشاعر (أَدِبُّ كَـأَنِّي كُـلَّمَا قُـمْتُ رَاكِـعُ). فقد شبَّه الشاعر حالته وهو شيخٌ كبير في السنّ بحالته في شبابه وهو يركع. فالمشبَّه (حاله وهو في سن الهرم والضعف، والمشبه به حاله أثناء الركوع، ووجه الشبه (الشكل المقوَّس الذي نراه على الشخص الذي يركع)، وأداة التشبيه كأنَّ . كما أنَّ في البيت كناية عن موصوف. فالشاعر لم يصرَّح في البيت بلفظة الشيخوخة أو الهرم أو الكبر في السن، لكنَّه جاء بإحدى مواصفات الرجل الشيخ الكبير في السن وهي إخبار الققص والحكايات التي تتحدث عن الأسلاف والسابقين. فهذه الصفة من مواصفات الرجال الكبار في السن. كما أنَّه أشار في البيت السابق بصفة من صفات الكبار في السن وهي التوكُّؤ على العصا، فجاء هذا البيت متمِّماً لماقبله</p>\r\n	\N	3	2017-03-30 14:36:50.891834	2017-03-30 14:36:50.891834
75	21	<p style="text-align: right;">يصوِّر لنا الشاعر حالة معظم الكبار في السن. فهذه الفئة من الناس تحب الحديث عن الأسلاف وأخبارهم وأيامهم. بل أنَّ الجيل الجديد إن أحب أن يسأل عن الماضي فإنَّه يبحث عن الشيوخ والمسنين وذلك لإيمانهم أنَّ هذا الأخبار ستكون حتمًا موجوده في حوزتهم. فهم فقط الذين عاشوا تلك الأيام. ثمَّ رسم الشاعر صورة الشيخ الكبير في السن والذي أصابه الهرم والضعف، رسمه على شكل ذلك الشاب الذي تراه يركع. فأنت ترى هذا الشيخ يركع، مع أنَّه لا يركع، بل هذا هو حاله أأأأثناء القيام والوقوف</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/34/content_Picture14.jpg" style="width: 120px; height: 126px;" /></p>\r\n	\N	2	2017-03-30 14:37:45.82069	2017-03-30 14:37:45.82069
76	6	<p dir="rtl">أكَّد لنا الشاعر حين قال (وقد أغتدي والطير في وكناتها) بأنَّه يستيقظ مبكراً قبل بزوغ الشمس، والطيور ماتزال في وكناتها وأوكارها ليغدو إلى الصيد بفرسه الجواد السريع القوي ذو الشعر القصير، حين وصفه في الشطر الثاني (بمنجرد قيد الأوابد هيكل). وصفه بكلمتين اثنتين، لكنها كانت وما تزال من أروع ما وصف عن الفرس، وهي (قيد الأوابد). فقد عبَّرت هاتان الكلمتان عن مدى سرعة فرسه ولباقته، وأنَّه لن يفرَّ منه فريسته مهما كانت سرعتها، لأنَّه يلاحقها ويكون في أعقابها مباشرة، فلا تستطيع الإفلات منه والانفكاك عنه.</p>\r\n\r\n<p dir="rtl"><img alt="" src="/uploads/ckeditor/pictures/35/content_image14.jpg" style="width: 142px; height: 142px;" /></p>\r\n	\N	2	2017-03-30 14:38:40.779607	2017-03-30 14:38:40.779607
77	22	<p style="text-align: right;">&nbsp; يستكمل الشاعر الأبيات السابقة في وصف نفسه حين يطول عمره وتتراخى منيته فَيُشَبِّهُ نفسه بالسيف القديم الذي بدَّلَ غِلافَه بسبب قدم عهد الحدَّادِ الذي صنعه، مع أنّه لا يزال سيفًا قاطعًا. فكنَّى عن جَسَدِهِ المتغيِّر بغمد السيف القديم وكنَّى عن عزّته وقوّته وحِدَّته بالسيف القاطع الذي لا يبالي بقدم جفنه وغمده. فهو عزيز رغم قِدَمِ جسمه</p>\r\n	\N	1	2017-03-30 14:38:43.812158	2017-03-30 14:38:43.812158
78	6	<p dir="rtl">أجاد الشاعر الإيجاز البليغ حين استعار (قيْد الأوابد) لسرعة فرسه. ففرسه سريع وخفيفٌ حين يعْدُو، خاصة إذا أراد اللحوق بفريسته، فهو لا يخطأ في النيل منها.&nbsp;</p>\r\n\r\n<p dir="rtl">وفي الشطر الأول من البيت إطناب بالتذييل حين قال (والطير في وكناتها). فقد ذكر (وقد أغتدي بمنجردٍ) لكنّه أطنب، يريد من هذا أن يبيِّن لنا أنّه يستيقظ مبكراً جداً وحتى قبل أن تبزغ الشمس، ومن ثمَّ يغدو بفرسه قيد الأوابد.</p>\r\n	\N	3	2017-03-30 14:39:15.756311	2017-03-30 14:39:15.756311
79	22	<p style="text-align: right;">يوجد في البيت تشبيه مرسل. فقد شبَّه الشاعر حاله في الهرم والضعف، شبهه بالسيف القديم الذي أخذت عليه طول الزمان، ولكنه مع هذا فهو مايزال سيفًا قاطعًا. فالمشبه هو حاله في الشيخوخة والهرم، والمشبه به هو السيف القديم، ووجه الشبه القوة والصلابة.، وأداة التشبيه مثل</p>\r\n	\N	3	2017-03-30 14:49:32.235832	2017-03-30 14:49:32.235832
80	22	<p style="text-align: right;">يصوِّر لنا الشاعر نفسه بالقوة والصلابة مع أنَّه وصل مرحلة الشيخوخة. وقد رسم هذه الصورة على السيف القديم الذي يستحسن استبدال بيته لقِدَمِه، لكن السيف نفسه مايزال قاطعًا صارمًا</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/36/content_Picture15.jpg" style="width: 120px; height: 127px;" /></p>\r\n	\N	2	2017-03-30 14:50:53.556629	2017-03-30 14:50:53.556629
82	23	<p style="text-align: right;">في البيت حذف موصوف في قوله (فدانٍ للطلوع وطالع) فأصل الكلام فإنسان دانٍ للطلوع وإنسانٌ طالعٌ. فقد حذف الموصوف (إنسان) لوجود الدلالة عليه وذلك في قوله (إنَّ المنية موعد عليك)، فعليك يعني به أنت أيها الإنسان</p>\r\n	\N	3	2017-03-30 14:56:09.372215	2017-03-30 14:56:09.372215
84	7	<p dir="rtl">يصف الفرسَ بأنه صالحٌ للكر وللفر وللإقبال وللإدبار، وهو سريع كسرعة الصخرة العظيمة الصُلب التي تنحدر من أعلى الجبل بسبب مياه السيول التي تدفعها للانحدار من فوق إلى أسفل.</p>\r\n	\N	1	2017-03-30 14:57:51.79565	2017-03-30 14:57:51.79565
85	7	<p dir="rtl">لم يكتفِ الشاعر بوصفه فرسه (مقيِّد الأوابد) بل هو &ndash; أي الفرس &ndash; مدرَّبٌ أحسن تدريبٍ على أصول الكر والفر والإقبال والإدبار. وأن هذه الحركات كلها يستطيع أن يؤديها في قوة وسرعة ونشاط في آنٍ واحد</p>\r\n\r\n<p dir="rtl">.<img alt="" src="/uploads/ckeditor/pictures/39/content_image15.jpg" style="width: 142px; height: 211px;" /></p>\r\n	\N	2	2017-03-30 15:00:06.803398	2017-03-30 15:00:06.803398
86	23	<p style="text-align: right;">ي البيت حذف موصوف في قوله (فدانٍ للطلوع وطالع) فأصل الكلام فإنسان دانٍ للطلوع وإنسانٌ طالعٌ. فقد حذف الموصوف (إنسان) لوجود الدلالة عليه وذلك في قوله (إنَّ المنية موعد عليك)، فعليك يعني به أنت أيها الإنسان</p>\r\n	\N	3	2017-03-30 15:00:07.891431	2017-03-30 15:00:07.891431
87	7	<p dir="rtl">نلاحظ المطابقة واضحة في الكرِّ والفر، وفي الإقبال والإدبار، لكنَّه لما قال (معاً) زادها بلاغة وكمالاً. فالمراد من (معاً) قُرْبُ الحركة وسرعتها في الإقبال و الإدبار، وكذلك في الكر والفر<sup>[1]</sup>، وكأنه يفعلها كلها في آنٍ واحد.&nbsp;</p>\r\n\r\n<p dir="rtl">ثمَّ شبَّه سرعة هذه الحركات بالصخرة الصلبة التي يقذفها السيل من مكانٍ عالٍ. ووجه الشبه هو السرعة، أما وجه الشبه في صلابة الصخرة فهي ترجع إلى صلابة الفرس، وكأنه يقول أن هذا الفرس صلب الجسم قوي، وسريع في تحركاته. وأداة التشبيه (الكاف).</p>\r\n	\N	3	2017-03-30 15:00:39.579551	2017-03-30 15:00:39.579551
88	23	<p style="text-align: right;">يعبر الشاعر عما يؤمن به تجاه الموت، فالموت مكتوب على كل إنسان مهما طال عمره، فهو حتما ملاقيه، إمَّا عن قريب أو بعيد. فمهما تدعو فإنَّ الموت قادم إليك عاجلاً أم آجلاً</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/37/content_Picture16.jpg" style="width: 120px; height: 91px;" /></p>\r\n	\N	2	2017-03-30 15:01:18.672854	2017-03-30 15:01:18.672854
89	24	<p style="text-align: right;">&nbsp; ينادي عاذلته التي تلومه قائلاً لها: ما يُعلمكِ من الذي يرجع من الفتيان الذين رحلوا عن هذه الحياة إلا ظنًّا وتخمينًا بعودته دون جزم ولا قطعٍ بوقت تلك العودة</p>\r\n	\N	1	2017-03-30 15:01:59.430749	2017-03-30 15:01:59.430749
90	24	<p style="text-align: right;">يوجد في صدر البيت حذف للجار والمجرو في قوله (أعاذل ما يدريك إلاّ تظنيَا) فقد حذف الشاعر الجار والمجرور (عن الخبر &ndash; أو عن العودة &ndash; أو عن العلم بهم) في قوله: وما يدريكِ... وكان سبب هذا الحذف هو إفادة التعميم مع الاختصار</p>\r\n	\N	3	2017-03-30 15:04:07.938336	2017-03-30 15:04:07.938336
91	24	<p style="text-align: right;">تصوير حسي يقدمه الشاعر لنا، حيث أنَّه وضع قضية الموت أما أعيننا. فمن ذا الذي يستطيع أن يرجع الفتيان الذين رحلوا عن هذه الدنيا. فلو كان هناك من يقول أنا أستطيع إرجاع من رحل عن هذه الدنيا، فهذا كلُّه ظنًا وتوهُّمًا</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/38/content_Picture17.jpg" style="width: 120px; height: 108px;" /></p>\r\n	\N	2	2017-03-30 15:05:39.578963	2017-03-30 15:05:39.578963
92	25	<p style="text-align: right;">&nbsp; إنّ العاذلةَ تبكِّي الأصدقاءَ الأحداثَ الصغارَ في السنِّ حُزنًا على الذين فقدوا شبابهم وماتوا قبل سنّ الكهولة</p>\r\n	\N	1	2017-03-30 15:06:30.53446	2017-03-30 15:06:30.53446
93	8	<p dir="rtl">وصف الفرس بالقوة من خلال لونه بأنه بين الأسود والأبيض، وأنه أَمْلَسُ الظهر لاكتناز اللحم عليه وامتلائه شبيهًا بالصخرة الصفاة الملساء التي لا ينبت فيها شيء ولا يثبت فوقها ماء المطر، كذلك ظهر الفرس يزل من ظهره مقعد الفارس لملاسته.&nbsp;</p>\r\n	\N	1	2017-03-30 15:07:56.69019	2017-03-30 15:07:56.69019
94	8	<p dir="rtl">يصف لنا الشاعر فرسه ذا اللوْن الأحمر الداكن، وظهره الأملس الناعم حتى أنّ سرجه لينزلق من فوقه لملاسة ظهره واكتناز لحمه كما ينزلق المطر من فوق الحجر الصلب الأملس.</p>\r\n\r\n<p dir="rtl"><img alt="" src="/uploads/ckeditor/pictures/40/content_image1.jpeg" style="width: 142px; height: 177px;" /></p>\r\n	\N	2	2017-03-30 15:09:37.090039	2017-03-30 15:09:37.090039
95	8	<p dir="rtl">يشبه الشاعر ملاسة ظهر الفرس لاكتناز اللحم عليه بالصخرة الملساء التي لا ينبت فيها شيء. والتشبيه هنا مرسل، فالمشبه ظهر الفرس، والمشبه به (الصفواء) الصخرة الملساء، ووجه الشبه هنا الملاسة التي حذفها الشاعر لمعلومية مظانه لدى السامع. وأداة التشبيه (الكاف).</p>\r\n	\N	3	2017-03-30 15:10:12.671262	2017-03-30 15:10:12.671262
96	25	<p style="text-align: right;">ي البيت فصل واضح بين الجملة في صدر البيت وبين الجملة في عجز البيت، وسبب هذا الفصل هو الإتحاد التام بين الجملتين، حيث أن الجملة الثانية (ألا إنَّ أخدان الشباب الرعارع) جاءت توكيدًا للجملة الأولى (تبكي على إثر الشباب الذي مضى). فمعنى الجملتين واحد وهو أن الشباب راحل لا محالة</p>\r\n	\N	3	2017-03-30 16:02:18.451934	2017-03-30 16:02:18.451934
97	25	<p style="text-align: right;">يعبر الشاعر عن حقيقة لا محالة منها، وهي أنَّ السدشباب راحل إلى الموت دون شك، فإذا رحل فإنَّ سيترك أثرًا مؤلما في نفس الحي حين يذكره</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/41/content_Picture18.jpg" style="width: 120px; height: 176px;" /></p>\r\n	\N	2	2017-03-30 16:07:38.258012	2017-03-30 16:07:38.258012
98	26	<p style="text-align: right;">يوجّه الشاعر سؤلاً إنكاريًّا بأنّ على عاذلته أن لا تجزع مما قَدَّرَ اللهُ حدوثه في الدهر. فإنّ ما قدّر من المصائب والقوارع سيجري ويحصل حتى ولو كان على كريم شريف</p>\r\n	\N	1	2017-03-30 16:09:02.222632	2017-03-30 16:09:02.222632
99	26	<p style="text-align: right;">في البيت مجاز عقلي في قوله (أحدث الدهر بالفتى). فقد أسند الفعل (الحدوث) إلى غير فاعله الحقيقي وهو (الدهر). فالدهر لم يحدث شيئًا بالفتى، ولكن الأحداث من مصائب وغيرها هي التي أحدثت الجزع من تلك تالأحداث التي رآها تقع بالفتى. فالمجاز هنا عقلي علاقته الزمانية</p>\r\n	\N	3	2017-03-30 16:09:29.537876	2017-03-30 16:09:29.537876
100	26	<p style="text-align: right;">يعبِّر الشاعر عن حقيقة لا محالة، وهي أنَّ كل ما هو صائر في الدنيا فهو نصيب لا يستثنى منه أحد، لا غنيهم ولا فقيرهم. فكلٌ مصاب بقوارع</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/42/content_Picture19.jpg" style="width: 120px; height: 112px;" /></p>\r\n	\N	2	2017-03-30 16:10:19.722351	2017-03-30 16:10:19.722351
101	27	<p style="text-align: right;">&nbsp; يقسم الشاعر بحياة غيره أو بنفسه على سبيل التجريد، بِأَنَّ ما قدره الله وقضاه غيب لا يعلمه إلا هو سبحانه فلا الخيلُ التي تضرب الحصى برجليها حين سيرها ولا الطيور في فضائها تعلم ما يصنعه الله سبحانه وتعالى</p>\r\n	\N	1	2017-03-30 16:11:50.757002	2017-03-30 16:11:50.757002
102	27	<p style="text-align: right;">في عجز البيت يوجد حذف للمسند الفعل في قوله (وَلاَ زَاجِـرَاتُ الطَّيرِ) أي ولا تدري زاجرات الطير، فحذف الفعل (تدري) للاحتراز، فقد ذكره في صدر البيت في قوله: ولا تدري الضوارب</p>\r\n	\N	3	2017-03-30 16:14:04.857755	2017-03-30 16:14:04.857755
103	27	<p style="text-align: right;">ي البيت صورة فنية جميلة تعبِّر عن إيمان شخصٍ بالقضاء والقدر وأنَّ كل شيء بيد الله وفي علم غيبه. وهذا الغيب لا يعلمه إلاّ هو سبحانه وتعالى. لكن الشاعر عدل عن ذكر هذه الجلمة، وجاء بالضوارب بالحصى وبزاجرات الطير، وذلك لأن العرب في تلك الحقبة كانت تؤمن بهذه الخرافات والتنجيمات</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/43/content_Picture20.jpg" style="width: 120px; height: 139px;" /></p>\r\n	\N	2	2017-03-30 16:14:48.172671	2017-03-30 16:14:48.172671
104	28	<p style="text-align: right;">إنّ نزول الموت بالإنسان وإن نزول المطر من السماء غيب يعلمه الله تعالى فلا خيلَ تعلمُ ولا طيرَ يعلمُ، وأنا صادق فيما أقول، وإن كذّبتموني فعليكم سؤال تلك المخلوقات عن أجل الفتى وعن نزول المطر فلن تجيبكم لأنّها لا تعلم ذلك وبذلك تعلمون صدق قولي. إشارة إلى قوله تعالى: إِنَّ اللَّهَ عِندَهُ عِلْمُ السَّاعَةِ وَيُنَزِّلُ الْغَيْثَ وَيَعْلَمُ مَا فِي الأَرْحَامِ وَمَا تَدْرِي نَفْسٌ مَّاذَا تَكْسِبُ غَداً وَمَا تَدْرِي نَفْسٌ بِأَيِّ أَرْضٍ تَمُوتُ إِنَّ اللَّهَ عَلِيمٌ خَبِيرٌ - لقمان: 34</p>\r\n	\N	1	2017-03-30 16:15:50.569616	2017-03-30 16:15:50.569616
131	38	<p dir="RTL">في البيت تشبيه مرسل في قوله (كأنني إلى الناس مطلي به القار أجرب). فقد شبَّه الشاعر نفسه بالإبل المريض الذي طُلِيَ بالزفت. فالمشبه الشاعر، والمشبه به الإبل أو البعير المطلي بالزفت، ووجه الشبه إستنفار الناس عنه، وأداة التشبيه الكاف.</p>\r\n	\N	3	2017-05-13 07:09:02.63899	2017-05-13 07:09:02.63899
105	28	<p style="text-align: right;">في البيت مجاز عقلي في قوله (متى الفتى يذوق المنايا)، أي متي يذوق الفتى الموت. فالموت لا يُذاق، فهو ليس بطعامٍ ولا شراب، ولا يمكن لمسه ومعرفة حاله. لكنَّه أجاز أن يكون للموت مذاق سيشعر كل إنسان بمرارته أو حلاوته. فالتجوُّز هنا في الجملة كلها، وليس في لفظة المذاق فحسب. فالتذوُّق جاء بمعناه في وضع اللغة، وقد أسند إلى فاعله الحقيقي وهو الفتى، لكنَّ الذي أراده الشاعر أن يتذوَّقه الفتي &ndash; وهو المنايا &ndash; لا يمكن تذوُّقه، وحتى لو حصل المذاق فلا يمكن الإخبار عنه، حيث أنَّه إذا حصل المذاق إنتهى أمر ذلك الفتى، لكن الشاعر أجاز هذا التذوُّق، واستثنى الإجابة عن حالة هذا المذاق</p>\r\n	\N	3	2017-03-30 16:16:17.598048	2017-03-30 16:16:17.598048
106	28	<p style="text-align: right;">هذا البيت جاء تصديقًا للبيت الذي سبقه. فقد أقسم الشاعر في البيت السابق بأن كل مايصنعه الله فهو في علم الغيب لا يعلم عنه مخلوق. والصورة الفنية المكتسبة من هذا البيت نجدها في قوله (سلوهن إن كذَّبتموني)، فـ(هن) تعود إلى ذلك الدجل والخرافات التي كانت الناس تؤمن بها وتصدِّق تنجيماته، فكأنه يقول: بعد أن أقسمت بحياتي وبحياتكم فأنا صادق في قولي، ومع هذا فإن كان هناك شك وارد، وتريدون تكذيبي فاسألوا تلك الضوارب وزاجرات الطير</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/44/content_Picture21.jpg" style="width: 120px; height: 123px;" /></p>\r\n	\N	2	2017-03-30 16:17:05.095741	2017-03-30 16:17:05.095741
108	31	<p dir="RTL">نجد الصورة الفنية في هذا البيت يكمن في إدخال الشاعر تحيته للملك (أبيت اللعن) داخل معرض السياق. فالسياق يغرض إلى إلتماس العفو والسماح إن كان هناك لوم وعتاب من الملك.</p>\r\n	\N	2	2017-05-13 04:44:07.156293	2017-05-13 04:44:07.156293
109	31	<p dir="RTL">يوجد في صدر البيت حذف للفاعل في قوله (أتاني أبيت اللعن أنَّك..) أي أتاني الخبر أبيت اللعن أنَّك... وسبب هذا الحذف يرجع إلى غرضين بلاغيتين، أحدهما: أنَّ الفاعل معلوم ولا يحتاج إلى ذكر، فهو مفهوم من سياق الكلام حين قال (أنَّك لمتني). وهذا يعني أنَّ الذي جاءه هو خبر لوْم الملك. فكان حذفه أبلغ، بل أنَّ ذكره يُذْهِب فصاحة القول.</p>\r\n\r\n<p dir="RTL">والغرض الآخر هو الإيجاز في&nbsp; الكلام. فقد جاء الشاعر بلفظ تحية الملك (أبيت اللعن)، فكان من الأحرى أن يختصر الكلام ويوجز القول.</p>\r\n	\N	3	2017-05-13 04:44:26.5075	2017-05-13 04:44:26.5075
111	32	<p dir="RTL">يصور لنا الشاعر حالته النفسية بعد أن جاءه لوم الملك. فهو لا يستطيع النوم مما يعانيه من التفكير والقلق الذي حل به بعد أن جاءه الخبر.&nbsp;</p>\r\n	\N	2	2017-05-13 04:59:56.486665	2017-05-13 04:59:56.486665
113	32	<p style="text-align: right;">في البيت تشبيه مرسل وحذف لحال المبيت. فقد حذف الشاعر الحال في قوله (فبتُّ) أي فبت عليلاً ومريضًا وسقيمًا. وغرض هذا الحذف هو إفادة التعميم مع الاختصار. فالشاعر ما أراد أن يصف حاله الذي أحلَّ به عقب وصول خبر لوم الملك عليه، لكنه شبَّه هذه الحال حتى يكون حاله أكثر أثرًا ووقوع في السامع. وهذا هو التشبيه الذي جاء في البيت، فقد شبه الشاعر حالة مبيته بحالة مبيت شخصٍ على فراش مملوء بالشوك. فالمشبه حالة مبيت الشاعر المحذوفة في البيت، والمشبه به حالة من يبيت على فراش مشوَّك، ووجه الشبه الألم والوجع، وأداة التشبيه (كأن).&nbsp;</p>\r\n	\N	3	2017-05-13 05:02:33.878038	2017-05-13 05:02:33.878038
114	33	<p style="text-align: right;">أقسمتُ يمينًا بالله حتى لا أترك في نفسك شكًّا ولا ظنًّا أيها الملك النعمان، ومن يقسم بالله فقد استوفي المقسم عليه فإنّه لا شيء بعد الله يلجأ إليه الإنسان ويذهب، والحلف بالله توثيق لليمين أمّا المُقسم عليه فسيأتي في البيت اللاحق.</p>\r\n	\N	1	2017-05-13 05:55:16.603823	2017-05-13 05:55:16.603823
115	33	<p dir="RTL">إن استخدام الشاعر الحلف في صدر البيت في قوله (حلفت..) ثم أردف هذا الحلف بقوله (وليس وراء الله للمرء مذهب)&nbsp; أعطى للسامع ثقة في أنَّ ما يقوله هو الصحيح، وإنَّ كلَّ ما قيل عنه هو إفتراءات أرادوا الإطاحة به وبسمعته عند الملك.</p>\r\n	\N	2	2017-05-13 05:55:44.215619	2017-05-13 05:55:44.215619
129	38	<p style="text-align: right;">يطلب الشاعر من النعمان بن المنذر أن لا يتركه ولا يتوعده بالقتل والعقوبة بل يعفو عنه، لأنّه إن لم يعفُ عنه ويتركه للناس، فإنّ الناس سيتدافعونه ويبعدونه عن أنفسهم لأنَّ الملك غاضب عليه فيكون منبوذًا كما ينبذ البعير الأجربُ الذي طُلي جسمه بالزفت ليبرأ من مرضه.</p>\r\n	\N	1	2017-05-13 07:08:32.908319	2017-05-13 07:08:32.908319
130	38	<p dir="RTL">يصوِّر الشاعر حالته إذا لم يعفُ عنه الملك. فجعل نفسه في مقام ذلك الإبل المصاب بالجرب والمرض. وهو بهذه الصورة لا يعني أنَّه مريض فحسب، لكنه عنى بأنَّ الرض الذي أصابه سيبعد الناس عنه. فمن من الناس يريد أن يصاحب أو حتى يقترب من رجل جربان مريض بداء العدوى؟</p>\r\n	\N	2	2017-05-13 07:08:47.19005	2017-05-13 07:08:47.19005
116	33	<p dir="RTL">في البيت تقديم للجار والمجرو على المفعول به في قوله (فَلَمْ أَترُكْ لِنَفْسِكَ رِيبَـةً) فقد قدَّم (لنفسك) على (ريبةً)، وهو تقديم على نية التأخير، وغرضه البلاغي للعناية والاهتمام. فالمقصود من هذا الحلف هو جذب الملك أولاً بأن هذا الحلف من أجلك لكي تصدِّق قولي.</p>\r\n\r\n<p dir="RTL">كما أنَّ في البيت وصل بين جملتي البيت، صدره وعجزه. فوصل (حلفت فلم أترك لنفسك ريبة) بجملة (ليس وراء الله للمرء مذهب). وسبب هذا الوصل المناسبة التامة والجهة الجامعة بين الجملتين. فالجملة الأولى فيها قوله (حلفت)، والجملة الثانية فيها المحلوف به الذي حذفه في الجملة الأولى، ولكنه جاء به إشارة مؤكدة وذلك في قوله (وليس وراء الله) أي ليس بعد الحلف بالله للمرء مذهب، لهذا وصل بين الجملتين لوجود المناسبة والرابطة التامة بينهما.</p>\r\n	\N	3	2017-05-13 05:56:07.538659	2017-05-13 05:56:07.538659
117	34	<p style="text-align: right;">يبيّن أنّ الشاعر المُقسَم عليه باليمين الذي ذكره في البيت السابق هو بأنّ ما وصلك من خيانتي لكَ كذبٌ وافتراء وأنَّ الذي بلَّغَكَ بخيانتي قد غشك وكذب عليك وأنّه واشٍ يزين كذبه ويحسنه حتى تصدّقه.</p>\r\n	\N	1	2017-05-13 06:09:17.400086	2017-05-13 06:09:17.400086
118	34	<p dir="RTL">يصوِّر الشاعر حال ذلك الرجل الذي ذهب إلى الملك وفتن عليه. فوصفه بالواشي الذي لا يقول سوى كذبًا، ويجمِّل كلامه الكاذب ليصل إلى مكان الصدق. ثم جاء بكلمة (أغش)، وهذه الكلمة جمعت حرف الشين في كلمة (واشي) فأعطى البيت صورة فنية جميلة توضِّح أنَّ كلام ذلك الرجل كلام كذب في كذب.</p>\r\n	\N	2	2017-05-13 06:09:35.294567	2017-05-13 06:09:35.294567
119	34	<p dir="RTL">في عجز البيت يوجد حذف لمضاف إليه في قوله (لمبلغك الواشى أغش وأكذب)، أي أغش الناس وأكذبهم. فقد حذف الشاعر كلمة الناس لمعلوميتها من سياق الكلام. فالواشي هو ن صنف من الناس يتصف بالكذب وتجميل كلامه الكاذب، إذن فإنَّ كلمة أغش يعني أغش الناس. وأكذب يعني أكذب الناس.</p>\r\n	\N	3	2017-05-13 06:09:51.424903	2017-05-13 06:09:51.424903
120	35	<p dir="RTL">يبيّن الشاعر مكانته عند الغسانيين الذين استضافوه بعد فراره من النعمان بن المنذر بأنّه ذو حظوة عندهم وفي مكان رحب فسيح من الأرض يفعل فيه ما يشاء دون ريبة من ذهاب وإقبال وإدبار، وأنّه متمكن فيهم أيَّما تمكن.</p>\r\n	\N	1	2017-05-13 06:33:32.136103	2017-05-13 06:33:32.136103
121	35	<p dir="RTL">يصف الشاعر نفسه بأنه ذو مكانة رفيعة بين ملوك الغساسنة. وهذا الوصف نجده في قوله (لي جانب من الأرض، أي أنَّ أرض الغساسنة ليست كلها تطرده ويفر منها، بل أنَّ هناك جانب كبير منها ترحب به وتضعه في مكانة ومنزلة رفيعة.</p>\r\n	\N	2	2017-05-13 06:33:57.395795	2017-05-13 06:33:57.395795
122	35	<p dir="RTL">في البيت كناية عن الصفة في قوله (جانب من الأرض). فالمقصود من جانب من الأرض أي مُتَّسَع من الرزق والعيش. فمن الأرض يأتي الرزق، فلو قيل أنَّ له جانب من الأرض أن أن له جانب من الرزق، وهذا يعني أنَّه في متسع من العيش.&nbsp; بهذا عدل الشاعر عن التصريح باتساع الرزق والمعيشة وجاء ما يرمز إليه وهو الحصول على جانب من الأرض.</p>\r\n	\N	3	2017-05-13 06:34:21.463155	2017-05-13 06:34:21.463155
123	36	<p dir="RTL">يصف الشاعر ملوك الغساسنة الذين أكرموه بعد أن فَرَّ من النعمان بن المنذر الذي توعده بالقتل نتيجة وشابة به، بأن هؤلاء الملوك ومن معهم من أبناء عمومته قد أكرموه حين وفد إليهم وتصرف في أموالهم كما يشاء وأصبح قريبًا منهم، وكأنّه يشير إلى أنّ صلته بالغساسنة لا تستوجب غضب النعمان وسخطه عليه ولا تعدَّ هذه الصلة خيانة له بل هي محافظة على الصداقة فوجب على الشاعر أن يشكرهم شكرًا أَثار غضب النعمان عليه.</p>\r\n	\N	1	2017-05-13 06:42:56.326756	2017-05-13 06:42:56.326756
124	36	<p dir="RTL">يصور لنا الشاعر مكانته وقدره بين ملوك الغساسنة. وهذا ما نلاحظه في قوله (أحكَّم في أموالهم وأقرَّب). فكيف لامرءٍ أن يجد منزلة التحكيم بين الأمراء والسلاطين إذا لم يُعْطى سعة من التقدير والإحترام؟</p>\r\n	\N	2	2017-05-13 06:43:13.826266	2017-05-13 06:43:13.826266
125	36	<p dir="RTL">يوجد في عجز البيت حذف للجار والمجرور وذلك في قوله (وأقرَّب) أي أقرَّبُ منهم. فحذف الشاعر (منهم) لمناسبة القافية. فالقصيدة بائية أوجبت الشاعر حذف (منهم) حتى تقع القافية.</p>\r\n	\N	3	2017-05-13 06:43:31.14148	2017-05-13 06:43:31.14148
126	37	<p style="text-align: right;">ينبه الشاعرُ النعمانَ بن المنذر بأن ما صنعه مع الغساسنة وما قدمه لهم من شكر كان واجبًا عليه مقابل كرمهم واستضافتهم له وليس ذنبا يُغضبك فأنتَ قد أوجدت لك أصدقاء وأكرمتهم وقربتهم فشكروك على ذلك ولم تَرَ في شكرهم لك ذنبًا يؤاخذون عليه وهذا هو حالي مع ملوك الغساسنة الذين شكرتُهم وأثنيت عليهم فلماذا تغضب؟!.</p>\r\n	\N	1	2017-05-13 06:49:59.574155	2017-05-13 06:49:59.574155
127	37	<p dir="RTL">نجد الصورة الفنية في هذا البيت كامنة، ولا تظهر إلاّ بعد أن تفسِّر معنى (اصطنعتهم). فالمقصود من هذه الكلمة هو جعلتهم بصناعتك وعملك معهم أصدقاء. فهذه هي الصنعة الطيِّبة. وعليه، فإنَّ الصورة الفنية الكامنة والتي استخرجناها من البيت هي شكر الأصدقاء تجاه فعل المعروف، ومن ثمَّ عدم الغضب تجاه من يفعل الخير تجاه الأصدقاء.</p>\r\n	\N	2	2017-05-13 06:50:22.85437	2017-05-13 06:50:22.85437
128	37	<p dir="RTL">في البيت تقديم وتأخير في قوله (فلم ترهم في شكر ذلك أذنبوا) وأصل الكلام (فلم ترهم أذنبوا في شكر ذلك). لكن الشاعر قدَّم وأخر في الألفاظ مراعاة للقافية والوزن العروضي حيث أنَّ القصيدة بائية على الضم.</p>\r\n	\N	3	2017-05-13 06:55:59.574417	2017-05-13 06:55:59.574417
132	39	<p dir="RTL">يخاطب الشاعر المَلِكَ النعمان بن المنذر مادحًا له بأنّ الله تعالى أعطاه منزلة عالية ورفعة وشرفًا ومكانة جعلته أعلى من كلّ مالك بل إن كلّ واحد من هؤلاء دون مكانتك ومتعلق دونك يتردّد ويضطرب لأنّه لا يستطيع الوصول إلى منزلتك الرفيعة.</p>\r\n	\N	1	2017-05-13 07:13:44.526444	2017-05-13 07:13:44.526444
133	39	<p dir="RTL">نجد الصورة الفنية في هذا البيت في استخدام الشاعر كلمة (يتذبذب). فهذه الكلمة تحمل في طياتها معاني التقهقر والتذمر والتردد. وكأن الشاعر يريد أن يقول: أنَّ الله أعطاك من العزة ما يجعل الملوك الآخرون يتقهقرون أمامك، ويترددون إذا ما فكر أحدهم أن يصيبك بسوء، ويحتارون في أمرك بما فيك من عزة يتمنى كل أحدٍ منهم أن ينالها.</p>\r\n	\N	2	2017-05-13 07:13:59.624039	2017-05-13 07:13:59.624039
134	39	<p dir="RTL">في البيت استعارة تصريحية في قوله (سورة). فقد شبه الشاعر السلطة والمكانة العالية بـ(السورة وهي الجدار الذي يحيط المدينة ويعطيها مكانة لكل من أراد الإقتراب منها). فجاء الشاعر بالمشبه (السورة) وحذف المشبه به وهو السلطة والمكانة العالية ورمز إليه بشيء من لوازمه وهو (ترى كل ملك يتذبذب. والقرينة المانعة لفظية وهي (ترى كل ملك دونها).</p>\r\n	\N	3	2017-05-13 07:14:17.33415	2017-05-13 07:14:17.33415
135	40	<p style="text-align: right;">إنّك أيها الملك النعمان بن المنذر تشبه الشمس، والملوك الآخرون يشبهون النجوم، فإذا طلعتْ الشمسُ طُمِسَتْ معالمُ النجوم لا يظهر منها شيء، فكذلك الملك إذا برز بحضرة الملوك الآخرين فإنّ هؤلاء الملوك سيخفتُ نورُهُم وتقلّ درجاتهم عن درجتك وتكون الكلمة لك تغمرهم بمجدك وإحسانك.</p>\r\n	\N	1	2017-05-13 07:24:32.661319	2017-05-13 07:24:32.661319
136	40	<p dir="RTL">نجد أن الشاعر رسم صورة هذا الكون. فكانت صورته متمثِّلة شمس واحدة ومجموعة من الكواكب. وكأن الشاعر درس علم الأجرام السماوية، وعرف أحجام الكواكب والأقمار والنجوم. وهذ ما استنتجناه من قوله: إنك شمس، والملوك كواكب. فقد وضع دنيته في بضع أناس، جعل النعمان هو الذي يعلوهم ويكبرهم وينفرد بسلطانه عنهم، أمَّا الباقون فهم سواء يختلفون حجماً وصورة وتوهُّجًا.</p>\r\n	\N	2	2017-05-13 07:24:54.48079	2017-05-13 07:24:54.48079
137	40	<p dir="RTL">في البيت تشبيه مؤكد في قوله (إنَّك شمس)، فقد شبَّه الشاعر النعمان بن المنذر بالشمس في ضياءها وكبرها وسلطانها على المجموعات القمرية ألخرى. فالمشبه (النعمان بن المنذر) زالمشبه به (الشمس) ووجه الشبه الكبر والضياء والسلطنة وانفراده، فلا توجد شمس أخرى في هذه الدنيا سوى هي وحدها. وأداة التشبيه محذوفة لأن النابغة أراد أن يجعل المشبه هو المشبه به نفسه.</p>\r\n\r\n<p style="text-align: right;"><span dir="RTL">وتشبيه مؤكد ثاني في قوله (الملوك كواكب)، فالمشبه (الملوك) والمشبه به (كواكب) ووجه الشبه صغر الحجم وكثرتها وتفاوتها وعدم الإضاءة منها. وأداة التشبيه محذوفة للغرض&nbsp; الآنف نفسه</span></p>\r\n	\N	3	2017-05-13 07:25:16.432831	2017-05-13 07:25:16.432831
138	41	<p style="text-align: right;">يقول النابغةُ للملك النعمان بن المنذر: إذا أنت لا تصلح أخاك وصاحبك وتجمع شعثه وما تفرّق من أمره وتعفو عن زلّته وتصاحبه على ما فيه من عيب فإنّه لم يبق لك صديق إذ لا يوجد في الرجال المهذّب الخالص الكامل المبرأ من كلّ عيب.</p>\r\n	\N	1	2017-05-13 07:35:38.919886	2017-05-13 07:35:38.919886
139	41	<p dir="RTL">نجد الصورة الفنية هنا في سؤال الشاعر (أي الرجال المهذب؟) وهو في الحقيقو سؤال استنكاري. فالجملة التي سبقت الؤال كأنه الجزء الأكبر من الإجابة. فهو يقول: أنَّك أيها الملك تعفو عن أخيك المخطأ وتأخذ بيده. ثم يسأل: هل هناك رجل مهذب خالص لا يخطأ؟ وكأنَّه يقول: أنا استحق العفو، فاعفُ عني.</p>\r\n	\N	2	2017-05-13 07:36:00.626472	2017-05-13 07:36:00.626472
140	41	<p dir="RTL">في البيت أسلوب التفات. فقدعدل الشاعر عن أسلوب الفعل الماضي إلى أسلوب فعل الأمر، وذلك في قوله (لست بمسْتبقٍ أخًا لا تلمُّه على شعث) ثم جاء بالاستفهام الإنكاري في قوله (أي الرجال المهذَّب؟) أي قل لي أيُّ الرجال المهذب موجود؟ . فلم يقل الشاعر: (ولست بمستبقٍ أخًا لا تلمه على شعث وليس من الرجال المهذب موجود) ليكون موازنًا في المعنى والزمن.</p>\r\n	\N	3	2017-05-13 07:36:15.966003	2017-05-13 07:36:15.966003
141	42	<p style="text-align: right;">يقول الشاعر: إن كنتُ مظلومًا قد ظلمتني بغضبك وعقابك أيها الملك فإنما أنا عبد من عبيدك ظلمته وليس من حقّي أن أغضب عليك وإن كنتَ أيها الملك صاحبَ عفوٍ وصفحٍ ورضا وتركٍ لما غضبتَ من أجله عَلَيَّ فإنَّ مثلك من الملوك والعظماء يعفون ويزيلون العتب عمن غضبوا عليه.</p>\r\n	\N	1	2017-05-13 07:48:41.535998	2017-05-13 07:48:41.535998
142	42	<p dir="RTL">يصور الشاعر هذه الدنيا بأنَّ فيها الظلم والمظلوم، كما أنَّ فيها أيضًا العفو وصاحبه. فجعل نفسه عبدًا مظلومًا، ويستحق العفو والمغفرة. لهذا صوَّر نفسه إنسانً مظلومًا أولاً حتى يشدَّ انتباه الملك الذي وصفه بعد حين بأنَّه أهلاً للعفو والسماح. فهذا المظلوم وذلك المسامح قريبان من بعضهما البعض وكل منهما يستحق الآخر.</p>\r\n	\N	2	2017-05-13 07:48:55.741719	2017-05-13 07:48:55.741719
143	42	<p dir="RTL">في البيت حذفٌ للمسند إليه المبتدأ وذلك في قوله (فعبد ظلمته) وأصل الكلام (فأنا عبدٌ..). وغرض هذا الحذف هو الاحتراز عن ذكر مالا داعي لذكره وهو ما يسمى (بالاحتراز عن العبث). فجملة (أنا عبد ظلمته) وقعت بعد الفاء المقترنة بجواب الشرط (إن أكُ..)، فمن الدواعي البلاغية التي ترفع من شأن القيمة البلاغية في الكلام هو حذف المسند إليه إذا اقترن بفاء جواب الشرط، وذلك لأنَّه قد ذُكِرَ في سؤال الشرط (إن أكُ) أي إن أكون أنا.</p>\r\n	\N	3	2017-05-13 07:49:09.451698	2017-05-13 07:49:09.451698
146	31	<p dir="RTL">إنّ الشاعر يحيي الملكَ النعمانَ بن المنذر بالتحية المعهودة في زمن الجاهلية وهي: [أبيت اللعن]، أي: امتنعتَ من أن تأتي بما تُلْعَنُ عليه، وغالبًا ما تكون هذه التحية للملوك. وبعد التحية يقول مخاطبًا الملك: لقد وصلني الخبرُ بأنَّكَ تَلُومني وتعتب عليَّ، وإن لومكَ هذا يزيد من همومي ويتعبني، لأنّك ذو كرم وإحسانٍ عليَّ.</p>\r\n	\N	1	2017-05-15 03:14:14.709892	2017-05-15 03:14:14.709892
147	32	<p style="text-align: right;">يصف الشاعر حاله بعد وصول نبأ لوم الملك ومعاداته له: أنّه سهر ليله من شدّةِ السَّقم والألم والمرض، وكأنّ النساءَ اللواتي زرنه فرشن له شجرًا مليئًا بالشوك مما جعل فراشه مرتفعًا ممزوجًا بهذا الشوك ويجدد هذا الشوك مرّة بعد أخرى مما يترك لديه زيادة في الألم.</p>\r\n\r\n<p style="text-align: right;"><img alt="" src="/uploads/ckeditor/pictures/46/content_1.jpg" style="width: 100px; height: 66px;" /></p>\r\n	\N	1	2017-05-15 03:15:39.695351	2017-05-15 03:15:39.695351
148	43	<p dir="RTL">تتساءل الشاعرة وتخاطب نفسها عن أسباب الألم الموجود في عينها، هل لأنَّ في عينها أَلَمًا من مرضٍ نزل بها؟ أم أن بها وسخًا؟ أم دموعٌ ذرفتها وسالت من عينها بسبب خلّو الدار من أهلها؟ وتعني به أخاها صخرًا الذي قتل وغاب عن الدار.</p>\r\n	\N	1	2017-05-15 03:31:58.847096	2017-05-15 03:31:58.847096
149	43	<p dir="RTL">نجد الصورة الفنية في هذا البيت واقعة عندما نتخيَّل كيف بهذه الخنساء تخاطب نفسها (هل بعيني قذى أم مرض؟) وهي تعلم أنَّه لا يوجد شيء من هذا كلِّه بعينها، بل الذي جرى بعينها هو تلك الدموع التي انفاضت حزنًا على موت أخيها.</p>\r\n	\N	2	2017-05-15 03:59:12.682432	2017-05-15 03:59:12.682432
150	43	<p dir="RTL">في البيت تقديم وتأخير في قول الشاعرة (إذ خلت من أهلها الدار) فأصل الكلام: إذ خلت الدار من أهلها، لأن (الهاء) في أهلها تعود إلى الدار، لكن الشاعر أخَّر الدار وقدَّم الجار والمجرور (من أهلها). وسبب هذا التأخير والتقديم هو مناسبة القافية، فالقصيدة رائية.</p>\r\n	\N	3	2017-05-15 03:59:30.73032	2017-05-15 03:59:30.73032
151	44	<p dir="RTL">تشبّه الخنساءُ دموع عينها بالماء الكثير الفائض وهو يسيل على خديها كثيرًا وغريزًا، وذلك بسبب تذكرها أخاها صخرًا فَحِينَ يخطر على بالها فهي تحزن حزنًا شديدًا عليه.</p>\r\n	\N	1	2017-05-15 04:12:27.500153	2017-05-15 04:12:27.500153
152	44	<p dir="RTL">وهاهي الآن تعترف بأن ما بعينها هو الحزن والأسى على فراق الأخ الحبيب. وبالتالي تظهر الصورة الفنية مكتملة إذا قرأنا البيتين معاً، تساؤلها ما الذي حدث بعينها، ثم جوابها هي نفسها عن ذلك السؤال.</p>\r\n	\N	2	2017-05-15 04:12:43.210661	2017-05-15 04:12:43.210661
153	44	<p dir="RTL">في البيت تشبيه مرسل وحذف وذلك في قول الشاعرة (كأنَّ عيني .. فيضٌ...). فقد حذفت الشاعرة كلمة (دموع) وذكرت عينيها فقط، فأصل الكلام: كأن دموع عيني، وذلك لأنها شبَّهت تلك الدموع التي أسكبتها عينيها كالسيل الفيضاني الذي ينحدر بكثرة على خدها. فالمشبه (دموع العين) والمشبه به (الفيضان أو السيل العارم) ووجه الشبه السيل الغزير لكل، وأداة التشبيه (كأن).</p>\r\n	\N	3	2017-05-15 04:13:11.821447	2017-05-15 04:13:11.821447
154	45	<p style="text-align: right;">كأنَّ هذا البيت والذي قبله يأتيان إجابة على تسؤلات الخنساء في البيت الذي قبلها. فالخنساء تبكي حزنًا على أخيها صخرٍ وهي عَبرَى دَائِمَةُ الدموع وقد اشتقاقتْ إليه وجزعت على فراقه وعليه تراب باطن الأرض في ظلمة وعمق التراب وارتفاعه فوقه أشبارٌ عديدة.</p>\r\n	\N	1	2017-05-15 04:27:04.351467	2017-05-15 04:27:04.351467
155	45	<p dir="RTL">قامت الشاعرة بالتعبير نيابة عن عينها. وهذه صورة فنية من فنون القول عندما يقوم الشخص بإنطاق مالا ينطق، ويعبر عنه بالنيابة. فنلاحظ ما تقوله الشاعرة عن مشاعر عينها التي أخذت تبكى لموت أخيها &ndash; أي أخ الخنساء الشاعرة &ndash; فأصبحت هذه العين ولهانة عليه. ولو جئنا للحقيقة الواقعية نجد أن التحنان والوله هو تحنان ووله الخنساء نفسها، وأن الحزن هو حزنها نفسها، أمَّ العين فهي عضو من أعضائها فقط.</p>\r\n	\N	2	2017-05-15 04:27:34.537355	2017-05-15 04:27:34.537355
156	45	<p dir="RTL">في البيت حذف للمسند إليه وهو الفاعل (العين) فأصل الكلام: تبكي العين.. وقد حذفت الشاعرة الفاعل لأنَّه معلوم، فقد ذُكْرَ في البيت السابق حين قالت: كأنَّ عيني.</p>\r\n	\N	3	2017-05-15 04:28:07.776186	2017-05-15 04:28:07.776186
157	46	<p style="text-align: right;">إنّ الخنساء ستبقي تبكي وتصيح حزنًا على أخيها صخرٍ مدةَ حياتها وعمرها في حال أني مقصرة وفاترة عن أداء هذا الحزن تجاه صخر لأني مقصرة بحقه ولا أستطيع إيفاءَهُ حقّه.</p>\r\n	\N	1	2017-05-15 04:36:19.32347	2017-05-15 04:36:19.32347
158	46	<p dir="RTL">في هذا البيت اتضحت معالم من كان يبكي حقيقة. ففي البيت السابق قالت الخنساء أن عينها تبكي على صخر ولهًا وحزنًا، أمَّا في هذا البيت صرَّحت أنها أخذت تبكي، وما انفكَّت عن البكاء.</p>\r\n	\N	2	2017-05-15 04:36:34.149936	2017-05-15 04:36:34.149936
173	52	<p dir="RTL">نلاحظ أن استخدام الشاعرة عبارة (وراد ماء) جعل للبيت جمالاً فنياً، ليس لأنها كناية، ولكن لأن في هذه العبارة كلمة (ورّاد)، ثم في عجز البيت (الـمَوارد)، ثم كلمة أخرى من مشتقات (ورد) وهي (ورده)، وكأن البيت يحمل لحناً موسيقيًا من باب (ورد يورد وردًا في وردِه فهو ورَّاد من أهل الموارد).</p>\r\n	\N	2	2017-05-15 08:33:17.197137	2017-05-15 08:33:17.197137
159	46	<p dir="RTL">في البيت ثلاثة مواضع للفصل والوصل. موضعان للوصل وموضع للفصل. فموضعا الوصل نجدهما في قول الشاعرة (تبكي خناس فما تنفك ما عمرت) والثاني قولها (لها عليه رنين وهي مفتار). فالموضع الأول عطفت الشاعرة جملة (تبكي خناس) على جملة (ماتنفك ما عمرت)، وسبب هذا الوصل هو وجود جهة جامعة ومناسبة تامة بين الجملتين، ومع الكلام أنَّ الخنساء تبكي على أخيها وماتزال تبكي عليه مادامت على قيد الحياة. أمَّا الموضع الثاني للوصل فهو قول الشاعرة (لها عليه رنين وهي مفتار) وسبب هذا الوصل هو أنَّ الجملتين اتفقتا خبريتين، وكلتاهما تخبر عن نواحها وصراخها في البكاء على أخيها.</p>\r\n\r\n<p dir="RTL">أمَّا موضع الفصل فهو الفصل بين جملة صدر البيت (فما تنفك ما عمرت) وبين جملة عجز البيت (لها عليه رنين). فلم تقل الشاعرة: فما تنفك ما عمرت ولها رنين.. بل فصلت الجملتين عن بعضهما وكان سبب هذا الفصل هو كمال الانقطاع بين الجملتين، فلا توجد رابطة ومناسبة في المعنى بين الجملتين، فالجملة الأولى تخبر بأنها ستبكي على أخيها مادامت على قيد الحياة، بينما الجملة الثانية وصفٌ لصياحها بالرنين.&nbsp;</p>\r\n	\N	3	2017-05-15 04:36:56.518767	2017-05-15 04:36:56.518767
160	47	<p dir="RTL">إنّ الخنساء ثبت لها حقّ البكاء على أخيها صخرٍ لأنّ الدهر حيَّرها وأفقدها صوابها بفقدها صخرًا، إنّ الدهر كثير الضرر وقد استعارت الشاعرة الريب للدهر فاستطاع أن يسكر الخنساء من شدَّة الحزن والألم.</p>\r\n	\N	1	2017-05-15 04:56:50.358373	2017-05-15 04:56:50.358373
161	47	<p dir="RTL">هذا البيت تثبيتًا للبيت السابق، وذلك ما نلاحظه من تكرارها لجملة (تبكي خناس..). فـ(تبكي خناس) في البيت السابق كان تصريحًا منها بالبكاء، و(تبكي خناس) في هذا البيت تصريح بأحقية البكاء على أخيها. فهي ترى أنَّ لها الحق في أن تبكي وتبكي على أخيها الذي فارق الدنيا، فقد رابها الدهر بفعلته هذه وأضرَّها كثيرًا. ونحن هنا نلاحظ التناسق الفني بين البيتين في استخدام الشاعرة جملة (تبكي خناس..) مرتين متعاقبتين.</p>\r\n	\N	2	2017-05-15 04:57:20.488812	2017-05-15 04:57:20.488812
162	47	<p dir="RTL">في البيت مجاز عقلي في قول الشاعرة (إذ رابها الدهر..). فالدهر لا يريب أحدًا، وإنما الأحداث غير المتوقعة التي تحدث هي التي تريب المرء وتصيبه بالحيرة، وبهذا نجد أنَّ الشاعرة أسندت الريبة والشك إلى الدهر وهو إسناد إلى غير فاعله الحقيقي، ففاعله الحقيقي هو الحوادث وأهوال الوقائع، فهو إذن مجاز عقلي.</p>\r\n	\N	3	2017-05-15 04:57:40.981325	2017-05-15 04:57:40.981325
163	48	<p dir="RTL">لا مفر ولا محيد من الموت، الذي في حدوثه عظات وعبر. وهكذا شأن الدهر في صروفه وأحداثه تقلبات وتغيرات ومراحل تأتي المرحلة تلو المرحلة.</p>\r\n	\N	1	2017-05-15 05:18:45.236272	2017-05-15 05:18:45.236272
164	48	<p dir="RTL">تصرِّح الشاعرة بحقيقة هذه الدنيا وهي أن الموت واقع لا محالة، كما أنَّ الدهر لا يثبت على حال واحد، وأحداثه في تطورات وتغيرات مستمرة. فلابد من توقُّع مالا يُتَوَقَّع.</p>\r\n	\N	2	2017-05-15 05:19:01.40303	2017-05-15 05:19:01.40303
165	48	<p dir="RTL">في البيت جناس تام في قول الشاعرة (صرفها &ndash; صرفه)، فالكلمة الأولى تعني حدوث الموت أي وقوعه، أما الكلمة الثانية (صرفه) تعني أحداث الدهر.</p>\r\n	\N	3	2017-05-15 05:19:18.130221	2017-05-15 05:19:18.130221
166	49	<p style="text-align: right;">تمدح الشاعرة أخاها صخرًا أبا عمروٍ بأنّه قبل قتله كان سيّدًا في قومه فَنِعْمَ المُسَوَّدُ الذي يقدم العون والنصر لمن يدعوه ويستنجد به فهو شجاع كريم.</p>\r\n	\N	1	2017-05-15 06:59:06.883154	2017-05-15 06:59:06.883154
167	49	<p dir="RTL">نجد الصورة الفنية في هذا البيت استخدام الشاعرة (قد) مع الفعل الماضي (كان). ودخول قد على الفعل الماضي معناه التأكيد. وهي بهذا الأسلوب تريد التعبير بكل تأكيد أن أخاها صخر سيَّد قومه.</p>\r\n	\N	2	2017-05-15 06:59:25.715558	2017-05-15 06:59:25.715558
168	49	<p dir="RTL">في البيت كنية لصخر وهي (أبو عمرو)، ومجيء هذه الكنية كان لسبب وزن البيت. فلو استبدلت الشاعرة الكنية وجعلت اسمه (صخر) كما في بيت (تبكي خناس على صخر..) لذهب وزن البيت، لكن استخدامها الكنية (أبو عمرو) جعل البيت على وزنه وبحره، فالقصيدة من بحر البسيط (مستفعلن فاعلن مستفعلن فعلن).</p>\r\n	\N	3	2017-05-15 06:59:42.05688	2017-05-15 06:59:42.05688
169	50	<p dir="RTL">تصف الشاعرة أخاها صخرًا بأنّه قوي في طبيعته يعطي العطاء كرمًا إذا منع الناسُ العطاءَ. أمّا في الحرب فهو مقدام بصدره تجاه العدو دون خوف ولا تردد وأنّه يدق أعناق الأعداء دقًا لقوته وشجاعته.</p>\r\n	\N	1	2017-05-15 08:27:02.531754	2017-05-15 08:27:02.531754
170	50	<p dir="RTL">بعد أن فرغت الشاعرة من البيت السابق الذي أكَّدَتْ به أنَّ أخاها سيد قومه، تبدأ الآن بسرد خصال ومواصفات وأخلاق سيد القوم. فهذا البيت والأبيات الآتية كلها أبيات مدح ووصف وتباهٍ بمواصفات أخيها وخصاله وسيادته بين الناس.</p>\r\n	\N	2	2017-05-15 08:27:20.409134	2017-05-15 08:27:20.409134
171	50	<p dir="RTL">في البيت كناية في قول الشاعرة (جريء الصدر). فكنَّتْ الشاعرة أخاها بالشجاعة والإقدام في الحروب بقولها هذا. فالمقصود بجريء الصدر هو أنَّه لا يخاف أن يصيب صدره بالأسهم والرماح، بل هو تاركٌ صدره مفتوح أمام الأعداء وأمام أسهمهم ورماحهم. فعدلت الشاعرة بتعبيرها عن التصريح إلى أسلوب الرمز والكناية.</p>\r\n	\N	3	2017-05-15 08:27:44.057578	2017-05-15 08:27:44.057578
172	52	<p style="text-align: right;">تنادي الشاعرة أخاها صخرًا وتصفه بأنّه مقدام على الموت لا يخاف ولا يهاب في الوقت الذي خافه أهل الحروب الآخرون وأنذر بعضهم بعضًا، وليس يُعيَّرُ أحدٌ إذا عجز عنه من صعوبةِ ورده.</p>\r\n	\N	1	2017-05-15 08:32:57.233186	2017-05-15 08:32:57.233186
174	52	<p dir="RTL">في البيت كناية عن الشجاعة وعدم الخوف من الموت في الحرب وذلك في قول الشاعرة (وراد ماء). فالورَّاد هو المقدم في الحرب، ووراد ماء يعني المقدم على الموت، ففي الحرب إحتمال إلى الموت، لكنَّ المقدم في الحرب يعني المقدم إلى الموت، والذي يقدم إلى الموت يعني أنَّه لا يهابه، والذي لا يهاب الموت هو الشجاع الباسل. فعدلت الشاعرة عن ذكر كل صفات الشجاعة في الحرب إلى الكناية وقالت وراد ماء أي شجاع لا يهاب الموت.</p>\r\n	\N	3	2017-05-15 08:33:36.975026	2017-05-15 08:33:36.975026
175	53	<p style="text-align: right;">تستعير الشاعرة وصف [السَّبَنْتَى] الذي هو للسبوع وتصف به أخاها صخرًا، يجامع الجَرْأةِ، فصخر جريءٌ يَقْدُمُ بصدره إلى الحرب الشديدة ومعه سلاحان في جسمه أنيابٌ وأظفارُ.</p>\r\n	\N	1	2017-05-15 08:40:57.55552	2017-05-15 08:40:57.55552
176	53	<p dir="RTL">رسمت الشاعرة لنا شكل أخيها. هذا السبع المفترس. رسمته بأنياب تلمع من شدة حدتها، وأظفار طوال تنقض على العدو. فهذه هي سلاحه إذا ما هاج إلى الهيجاء المعضلة.</p>\r\n	\N	2	2017-05-15 08:41:12.548117	2017-05-15 08:41:12.548117
177	53	<p dir="RTL">في البيت استعارة تصريحية في قول الشاعرة (السبنتي). فالسبنتي معناها السَّبْع إما أسدا أو نمرا أو ذئبا. وقصدت بها الخنساء أخاها صخر. فاستعارت السبتي لأخيها والعلاقة بينهما المشابهة في القوة والشجاعة، والقرينة المانعة من إرادة المعنى الحقيقي لفظية وهي (مشي السبنتي.. له سلاحان ..).</p>\r\n	\N	3	2017-05-15 08:41:30.544623	2017-05-15 08:41:30.544623
178	54	<p dir="RTL">تصف الشاعرة حبّها وحنانها لأخيها صخرٍ بأنّه مثل الناقة العجول التي فقدت ولدها الصغير ووضع جلده أمامها محشيًا بالتبن أو بغيره فهي تدور حوله ولها صوت ترفعه مرّة وتخفضه أخرى.</p>\r\n	\N	1	2017-05-15 09:26:13.825272	2017-05-15 09:26:13.825272
179	54	<p dir="RTL">تخيَّلت الشاعرة صورة الناقة التي مات عنها أولادها الصغار. تخيَّلتها وتخيَّلت ما حلَّ بهذه الناقة من مصيبة غير متوقَّعة حيث أنَّ الموت أخذ أولادها قبل أن يحين الأوان. ثم جعلت نفسها في مكان تلك الناقة الحزينة، وأخذت تعبِّر عن الناقة بدلاً من أن تعبِّر عن نفسها.</p>\r\n	\N	2	2017-05-15 09:26:32.61174	2017-05-15 09:26:32.61174
180	54	<p dir="RTL">في البيت استعارة تصريحة وطباق. فالاستعارة في قول الشاعرة (عجول)، فالعجول هي الناقة التي مات عنها أولادها الصغار، فاستعارت لنفسها (العجول) تعبيرًا عن حبها لأخيها الذي رحل عنها للأبد، فهي تبكى وتنوح حنيناً على أخيها. فاستعارت (عجول) لنفسها والعلاقة بينهما المشابهة في الحب والتحنان وألم الفراق، والقرينة المانعة قرينة معنوية تفهم من السياق.</p>\r\n\r\n<p dir="RTL">أمّا الطباق فقول الشاعرة (إعلان &ndash; إسرار) وهو مطابقة الإيجاب</p>\r\n	\N	3	2017-05-15 09:26:52.395843	2017-05-15 09:26:52.395843
181	55	<p style="text-align: right;">تخبر الشاعرة بأنّها قَلِقَةٌ تُقبل وتُدبر من شدّة ما بها من الاضطراب والقلق الشديد على ولدها. وكأنها وحشية إذا غَفَلَتْ رَعَتْ وإذا تذكرت فَقْدَ ولدها لم يَقرَّ قرارُها.</p>\r\n	\N	1	2017-05-15 09:29:58.857338	2017-05-15 09:29:58.857338
182	55	<p dir="RTL">نجد الجمال الفني في هذا البيت واقع في (الإقبال والإدبار) الذي أوجدته الشاعرة في حركة تلك الناقة. فهذا الإقبال وذلك الإدبار أعطى صورة القلق والاضطراب الذي حلَّ على الناقة من سوء ما أصابها. فهي لم تجد بدًا سوى الذهاب والإياب بين هنا وهناك.</p>\r\n	\N	2	2017-05-15 09:30:14.749768	2017-05-15 09:30:14.749768
183	55	<p dir="RTL">في البيت مطابقة في قول الشاعرة (إقبال &ndash; إدبار)، وهي مطابقة الإيجاب.</p>\r\n	\N	3	2017-05-15 09:38:53.333954	2017-05-15 09:38:53.333954
184	56	<p style="text-align: right;">هذه الناقة لا تَسْمَنُ مما تأكله من الأرض في طيلة الدهرِ حتى لو كان الجوّ ربيعًا لأنها تحن إلى ولدها وتمد حنينها عليه</p>\r\n	\N	1	2017-05-15 09:43:11.201887	2017-05-15 09:43:11.201887
185	56	<p dir="RTL">صوَّرت الشاعرة نفسها بأنَّ تحنانها على فراق أخيها أكبر من مستلزمات الحياة من أكل وشرب ورفاهية. فوضعت هذه الصورة على العجول، تلك الناقة التي فقدت أولادها الصغار. فهذه الناقة مهما تأكل وتأكل حتى وإن أكلت في موسم الخضرة فإنها لن ينفعها أكلها.</p>\r\n	\N	2	2017-05-15 09:43:28.894077	2017-05-15 09:43:28.894077
186	56	<p dir="RTL">في البيت حذف لفاعل في قول الشاعرة (لا تسمن الدهر في أرض...)، وأصل الكلام: لا تسمن العجول أو الناقة ...)، وسبب هذا الحذف هو لمعلوميته، لأنَّه مذكور في البيتين السابقين. فحذفه أبلغ من ذكره.</p>\r\n\r\n<p dir="RTL">كما أن في البيت إطناب بالاعتراض في قول الشاعرة (وإن ربعت..)، فهذه الجملة جاءت إعتراضية لقولها (لاتسمن الدهر في أرضٍ)، والغرض من الاعتراض هو التنبيه على أنَّ الناقة لا تسمن مهما أكلت، حتى لو كانت في موسم الربيع فتحنانها على فراق أولادها أكبر من أن تأكل وتسمن.</p>\r\n	\N	3	2017-05-15 09:43:46.244065	2017-05-15 09:43:46.244065
187	57	<p style="text-align: right;">إنّ الشاعرة تربط هذا البيت بقولها ما عجول في الأبيات السابقة، أي: ما ناقة تحنّ على ولدها يومًا من الأيام أشدَّ من حنيني وشوقي إلى صخر والدهر يأتي بالحلو والمُرِّ.</p>\r\n	\N	1	2017-05-15 09:49:13.536891	2017-05-15 09:49:13.536891
188	57	<p dir="RTL">نلاحظ الصورة الفنية في هذا البيت واقعٌ في أن الشاعرة جعلت لأحداث الدهر مذاق حسِّي، فمن هذه الأحداث ما هو حلو الطعم ومنها مُرٌ. هذا يعني أنَّ ليس كل أحداث الدهر ممبوذة ومُرَّةٌ، بل هناك ما يأتي منه حلو المذاق بشيراً للسرور، ومنها مر المذاق نذيرًا ونعيًا.</p>\r\n	\N	2	2017-05-15 09:49:27.899049	2017-05-15 09:49:27.899049
189	57	<p dir="RTL">في البيت مطابقة الإيجاب في قول الشاعرة (إحلاء &ndash; إمرار)، بمعنى أنَّ للدهر أحداث حلوة وأحداث مرة. فالحلو ضدُّه المرُّ.</p>\r\n	\N	3	2017-05-15 09:49:44.515233	2017-05-15 09:49:44.515233
\.


--
-- Name: line_references_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('line_references_id_seq', 189, true);


--
-- Data for Name: lines; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY lines (id, stanza_id, text, text2, media, description, created_at, updated_at) FROM stdin;
1	1	قِفَا نَبْـكِ مِـنْ ذِكْرَى حَبِيبٍ وَمَنْزِلِ        بِسِقْطِ اللِّـوَى بَينَ الدَّخُـولِ فَحَومَلِ	\N	\N	\N	2017-03-30 07:47:02.307178	2017-03-30 07:47:02.307178
2	1	وَلَيـلٍ كَمَـوجِ البَحْرِ أَرْخَى سُـدُولَهُ      عَـلَـيَّ بِأَنْـوَاعِ الهُـمُـومِ لِيَبْـتَلِي	\N	\N	\N	2017-03-30 07:47:19.974725	2017-03-30 07:47:19.974725
3	1	فَقُلْـتُ لَـهُ لَـمَّـا تَمَطَّى بِصُلْبِـهِ  وَأَرْدَفَ أَعْـجَـازًا وَنَـاءَ بِكَـلْكَلِ	\N	\N	\N	2017-03-30 07:47:34.887211	2017-03-30 07:47:34.887211
4	1	أَلاَ أَيُّـهَا اللَّيـلُ الطَّـوِيلُ أَلاَ انْجَلِي بِصُبْـحٍ وَمَـا الإِصْبَـاحُ مِنْكَ بِأَمْثَلِ	\N	\N	\N	2017-03-30 07:47:51.60978	2017-03-30 07:47:51.60978
5	1	فَيَا لَـكَ مِنْ لَيـلٍ كَـأَنَّ نُجُـومَـهُ بِكُـلِّ مُغَـارِ الفَتْـلِ شُـدَّتْ بِيَذْبُلِ	\N	\N	\N	2017-03-30 07:48:09.795334	2017-03-30 07:48:09.795334
6	1	وَقَـدْ أَغْتَـدِي وَالطَّيرُ فِي وُكُنَـاتِهَا بِمُنْـجَـرِدٍ قَيـدِ الأَوَابِـدِ هَيـكَلِ	\N	\N	\N	2017-03-30 07:48:25.110852	2017-03-30 07:48:25.110852
7	1	مِـكَـرٍّ مِـفَـرٍّ مُقْبِـلٍ مُدْبِـرٍ مَعًا كَجُـلْمُودِ صَخْرٍ حَطَّهُ السَّيلُ مِنْ عَلِ	\N	\N	\N	2017-03-30 07:48:43.567071	2017-03-30 07:48:43.567071
8	1	كُمَيتٍ يَزِلُّ اللَّبْـدُ عَـنْ حَـالِ مَتْنِهِ كَمَـا زَلَّتِ الصَّـفْـوَاءُ بِـالمُتَنَـزِّلِ	\N	\N	\N	2017-03-30 07:52:35.681562	2017-03-30 07:52:35.681562
9	3	بَلِيـنَا وَمَا تَبْلَى النُّجُـومُ الطَّـوَالِعُ  وَتَبْـقَى الجِبَـالُ بَعْـدَنَا والمَصَـانِعُ	\N	\N	\N	2017-03-30 09:54:44.906224	2017-03-30 09:54:44.906224
10	3	وَقَـدْ كُنْـتُ فِي أَكْنَافِ جَارِ مَضِنَّةٍ  فَفَـارَقَنِي جَـارٌ بِـأَرْبَـدَ نَـافِـعُ	\N	\N	\N	2017-03-30 09:55:15.980938	2017-03-30 09:55:15.980938
11	3	فَلاَ جَـزِعٌ إِنْ فَـرَّقَ الدَّهْـرُ بَينَنَا  وَكُـلُّ فَتىً يَوماً بِهِ الدَّهْـرُ فَـاجِـعُ	\N	\N	\N	2017-03-30 09:55:42.837983	2017-03-30 09:55:42.837983
12	3	فَلاَ أَنَـا يَـأْتِينِي طَرِيـفٌ بِفَرْحَـةٍ   وَلاَ أَنَـا مِمَّا أَحْـدَثَ الدَّهْـرُ جَازِعُ\v	\N	\N	\N	2017-03-30 09:56:17.406512	2017-03-30 09:56:17.406512
13	3	وَمَا النَّـاسُ إِلاَّ كَالدِّيَـارِ وَأَهْـلُهَا بِهَا يَـومَ حَلُّـوهَا وَغَـدْوًا بَـلاَقِـعُ	\N	\N	\N	2017-03-30 09:57:31.7882	2017-03-30 09:57:31.7882
14	3	وَمَا الـمَـرْءُ إلاَّ كَالشِّهَابِ وَضَوئِهِ  يَحُـورُ رَمَـاداً بَعْـدَ إِذْ هُوَ سَـاطِعُ\v	\N	\N	\N	2017-03-30 09:58:12.02054	2017-03-30 09:58:12.02054
15	3	وَمَا البِـرُّ إِلاَّ مُضْمَـرَاتٌ مِنَ التُّقَى\vوَمَا المَـالُ إِلاَّ مُـعْـمَـرَاتٌ وَدَائِـعُ	\N	\N	\N	2017-03-30 10:00:29.950417	2017-03-30 10:00:29.950417
16	3	وَمَا المَـالُ وَالأَهْلُـونَ إِلاَّ وَدِيعَـةٌ  وَلاَ بُـدَّ يَـوماً أَنْ تُـرَدَّ الـوَدَائِـعُ\v	\N	\N	\N	2017-03-30 10:01:12.72009	2017-03-30 10:01:12.72009
17	3	وَيَمْضُـونَ أَرْسَـالاً وَنَخْلُفُ بَعْدَهُمْ  كَمَا ضَـمَّ أُخْرَى التَّالِيَـاتِ المُشَـايِعُ\v	\N	\N	\N	2017-03-30 10:01:50.729931	2017-03-30 10:01:50.729931
18	3	وَمَا النَّـاسُ إِلاَّ عَـامِـلاَنِ فَعَـامِلٌ  يُتَبِّـرُ مَـا يَبْنِي وَآخَـرُ رَافِـعُ\v	\N	\N	\N	2017-03-30 10:03:12.791207	2017-03-30 10:03:12.791207
19	3	فَمِنْـهُمْ سَعِيـدٌ آخِـذٌ لِنَصِيـبِـهِ  وَمِنْـهُمْ شَـقِيٌّ بِالمَعِيـشَةِ قَـانِـعُ	\N	\N	\N	2017-03-30 10:04:21.008137	2017-03-30 10:04:21.008137
20	3	أَلَيـسَ وَرَائِي إِنْ تَـرَاخَـتْ مَنِيَّتِي\vلُـزُومُ العَـصَا تُحْنَى عَلَيـهَا الأَصَابِعُ	\N	\N	\N	2017-03-30 10:04:48.996674	2017-03-30 10:04:48.996674
21	3	أُخَبِّرُ أَخْبَـارَ القُـرُونِ الَّتِي مَضَـتْ  أَدِبُّ كَـأَنِّي كُـلَّمَا قُـمْتُ رَاكِـعُ	\N	\N	\N	2017-03-30 10:05:28.687737	2017-03-30 10:05:28.687737
22	3	فَأَصْبَحْـتُ مِثْـلَ السَّيفِ غَيَّرَ جَفْنَهُ  تَقَـادُمُ عَهْـدِ القَيـنِ وَالنَّصْلُ قَاطِعُ\v	\N	\N	\N	2017-03-30 10:06:00.931806	2017-03-30 10:06:00.931806
23	3	فَلاَ تَبْعَـدَنْ إِنَّ المَنِـيَّـةَ مَـوعِـدٌ  عَلَيـكَ فَـدَانٍ للِطُّلُـوعِ وَطَـالِـعُ	\N	\N	\N	2017-03-30 10:07:48.437122	2017-03-30 10:07:48.437122
24	3	أَعَـاذِلَ مَـا يُـدْرِيـكِ إِلاَّ تَظَنِّـياً  إِذَا ارْتَـحَلَ الفِتْـيَانُ مَـنْ هُوَ رَاجِعُ\v	\N	\N	\N	2017-03-30 10:08:20.122873	2017-03-30 10:08:20.122873
25	3	تُبَـكِّي عَلَى إِثْرِ الشَّبَابِ الَّذِي مَضَى  أَلاَ إِنَّ أَخْـدَانَ الشَّبَـابِ الرَّعَـارِعُ	\N	\N	\N	2017-03-30 10:08:58.392334	2017-03-30 10:08:58.392334
26	3	أَتَجْزَعُ مِمَّا أَحْـدَثَ الدَّهْـرُ بِالفَتَى  وَأَيُّ كَـرِيمٍ لَـمْ تُصِبْـهُ القَـوَارِعُ\v	\N	\N	\N	2017-03-30 10:09:29.361655	2017-03-30 10:09:29.361655
27	3	لَعَمْرُكَ مَا تَـدْرِي الضَّوَارِبُ بِالحَصَى   وَلاَ زَاجِـرَاتُ الطَّيرِ مَا اللّهُ صَـانِـعُ\v	\N	\N	\N	2017-03-30 10:10:02.330166	2017-03-30 10:10:02.330166
28	3	سَلُوهُنَّ  إِنْ  كَذَّبْتُمُونِي مَتَـى الفَتَى  يَذُوقُ المَنَـايَا  أَو مَتَـى الغَيـثُ وَاقِعُ	\N	\N	\N	2017-03-30 10:10:33.525486	2017-03-30 10:10:33.525486
31	6	أَتَانِي أَبَيتَ اللَّعْنَ أَنَّـكَ لُمْتَنِـي    \t#\tوَتِلْكَ الَّتِي أُهتَمُّ مِنْهَا وأَنْصَـبُ	\N	\N	\N	2017-05-13 04:41:56.380847	2017-05-13 04:41:56.380847
32	6	فَبِتُّ كَأَنَّ العَائِـدَاتِ فَرَشْنَنِـي  \t#\tهَرَاساً بِهِ يُعلَى فِرَاشِي ويُقْشَبُ	\N	\N	\N	2017-05-13 04:55:39.063964	2017-05-13 04:55:39.063964
33	6	حَلَفْتُ فَلَمْ أَترُكْ لِنَفْسِكَ رِيبَـةً  \t#\tوَلَيسَ وَرَاءَ الله لِلمَـرْءِ مَذهَبُ	\N	\N	\N	2017-05-13 05:15:39.627566	2017-05-13 05:15:39.627566
34	6	لَئِنْ كُنْتَ قَدْ بُلِّغْتَ عَنِّي خِيَانَـةً  \t#\tلَمُبْلِغُكَ الوَاشِي أَغَشُّ وَأَكْذَبُ	\N	\N	\N	2017-05-13 06:02:26.954833	2017-05-13 06:02:26.954833
35	6	وَلَكِنَّنِي كُنْتُ امْرَأً لِيَ جَانِـبٌ  \t#\tمِنَ الأَرْضِ فِيهِ مُسْتَرَادٌ وَمَذْهَبُ	\N	\N	\N	2017-05-13 06:31:47.24093	2017-05-13 06:31:47.24093
36	6	مُلُوكٌ وَإِخْـوَانٌ إِذَا مَـا أَتَيتُهُمْ  \t#\tأُحَكَّـمُ فِي أَمْوَالِهِمْ وَأُقَـرَّبُ	\N	\N	\N	2017-05-13 06:40:56.903763	2017-05-13 06:40:56.903763
37	6	كَفِعْلِكَ فِي قَومٍ أَرَاكَ اصْطَنَعْتَهُمْ  \t#\tفَلَمْ تَرَهُمْ فِي شُكْرِ ذَلِكَ أَذْنَبُوا	\N	\N	\N	2017-05-13 06:47:49.756427	2017-05-13 06:47:49.756427
38	6	فَلاَ تَتْرُكَنِّـي بِالوَعِيدِ كَأَنَّنِـي  \t#\tإِلَى النَّاسِ مَطَلِيٌّ بِهِ القَارُ  أَجْرَبُ	\N	\N	\N	2017-05-13 07:07:13.586813	2017-05-13 07:07:13.586813
39	6	أَلَمْ تَرَ أَنَّ اللهَ أَعطَـاكَ سُـورَةً  \t#\tتَرَى كُلَّ مَلْكٍ دُونَهَا يَتَذَبْـذَبُ	\N	\N	\N	2017-05-13 07:12:10.706286	2017-05-13 07:12:10.706286
40	6	فَإِنَّكَ شَمْسٌ وَالمُلُـوكُ كَوَاكِبٌ  \t#\tإِذَا طَلَعَتْ لَمْ يَبْدُ مِنْهُنَّ كَوكَبُ	\N	\N	\N	2017-05-13 07:17:41.159511	2017-05-13 07:17:41.159511
41	6	وَلَسْتَ بِمُسْتَبْـقٍ أَخاً لاَ تَلُمُّـهُ  \t#\tعَلَى شَعَثٍ أَيُّ الرِّجَالِ المُهَذَّبُ ؟	\N	\N	\N	2017-05-13 07:33:06.572467	2017-05-13 07:33:06.572467
42	6	فَإِنْ أَكُ مَظْلُوماً فَعَبْـدٌ ظَلَمْتَـهُ  \t#\tوَإِنْ تَكُ ذَا عُتْبَى فَمِثْلُكَ يُعْتِبُ	\N	\N	\N	2017-05-13 07:46:21.692254	2017-05-13 07:46:21.692254
43	7	قَذًى بِعَينِكِ أَمْ بِالعَينِ عُوَّارُ    \t#\tأَمْ ذَرَفَتْ إِذْ خَلَتْ مِنْ أَهْلِهَا الدَّارُ	\N	\N	\N	2017-05-15 03:31:31.472277	2017-05-15 03:31:31.472277
44	7	كَأَنَّ عَينِي لِذِكْرَاهُ إِذَا خَطَرَتْ    \t#\tفَيضٌ يَسِيلُ عَلَى الخَدَّينِ مِدْرَارُ	\N	\N	\N	2017-05-15 04:10:07.182145	2017-05-15 04:10:07.182145
45	7	تَبْكِي لِصَخْرٍ هِيَ العَبْرَى وَقَدْ وَلَهَتْ  \t#\tوَدُونَهُ مِنْ جَدِيدِ التُّرْبِ أَسْتَـارُ	\N	\N	\N	2017-05-15 04:17:07.154319	2017-05-15 04:17:07.154319
46	7	تَبْكِي خُنَاسٌ فَمَا تَنْفَكُّ مَا عَمَرَتْ  \t#\tلَهَا عَلَيـهِ رَنِيـنٌ وَهْيَ مِفْتَـارُ	\N	\N	\N	2017-05-15 04:34:47.196409	2017-05-15 04:34:47.196409
47	7	تَبْكِي خُنَاسٌ عَلَى صَخْرٍ وَحَقَّ لَهَا  \t#\tإِذْ رَابَهَا الدَّهْرُ إِنَّ الدَّهْرَ ضَرَّارُ	\N	\N	\N	2017-05-15 04:51:24.303689	2017-05-15 04:51:24.303689
48	7	لاَ بُدَّ مِنْ مِيتَةٍ فِي صَرْفِهَا عِبَـرٌ  \t#\tوَالدَّهْرُ فِي صَرْفِهِ حَولٌ وَأَطْوَارُ	\N	\N	\N	2017-05-15 05:04:40.187314	2017-05-15 05:04:40.187314
49	7	قَدْ كَانَ فِيكُم أَبُو عَمْروٍ يَسُودُكُمُ  \t#\tنِعْمَ المُعَمَّـمُ لِلدَّاعِيـنَ نَصَّـارُ	\N	\N	\N	2017-05-15 06:53:01.427225	2017-05-15 06:53:01.427225
50	7	صُلْبُ النَّحِيزَةِ وَهَّابٌ إِذَا مَنَعُوا  \t#\tوَفِي الحُرُوبِ جَرِيءُ الصَّدْرِ مِهْصَارُ	\N	\N	\N	2017-05-15 08:09:52.171588	2017-05-15 08:09:52.171588
52	7	يَا صَخْرُ وَرَّادَ مَـاءٍ قَـدْ تَنَاذَرَهُ  \t#\tأَهْـلُ المَوَارِدِ مَا فِي وِرْدِهِ عَارُ	\N	\N	\N	2017-05-15 08:31:09.322642	2017-05-15 08:31:09.322642
53	7	مَشَى السَّبَنْتَى إِلَى هَيجَاءَ مُعْضِلَةٍ  \t#\tلَهُ سِلاَحَـانِ: أَنْيَابٌ وَأَظْفَـارُ	\N	\N	\N	2017-05-15 08:38:57.114973	2017-05-15 08:38:57.114973
54	7	وَمَا عَجُـولٌ عَلَى بَوٍّ تُطِيفُ بِهِ  \t#\tلَهَا حَنِينَـانِ: إِعْـلاَنٌ وَإِسْـرَارُ	\N	\N	\N	2017-05-15 08:44:45.104379	2017-05-15 08:44:45.104379
55	7	تَرْتَعُ مَا رَتَعَتْ حَتَّى إِذَا ادّكَرَتْ  \t#\tفَإِنَّـمَا هِـيَ إِقْبَـالٌ وَإِدْبَـارُ	\N	\N	\N	2017-05-15 09:29:10.302354	2017-05-15 09:29:10.302354
56	7	لاَ تَسْمَنُ الدَّهْرَ فِي أَرْضٍ وَإِنْ رُبِعَتْ  \t#\tفَإِنَّمَا هِيَ تَحْنَـانٌ وَتَـسْجَـارُ	\N	\N	\N	2017-05-15 09:41:55.115117	2017-05-15 09:41:55.115117
57	7	يَومًا بِأَوجَـدَ مِنِّي يَـومَ فَارَقَنِي  \t#\tصَخْرٌ وَلِلدَّهْرِ إِحْـلاَءٌ وَإِمْرَارُ	\N	\N	\N	2017-05-15 09:46:49.845989	2017-05-15 09:46:49.845989
\.


--
-- Name: lines_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('lines_id_seq', 57, true);


--
-- Data for Name: poems; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY poems (id, description, author, title, audio, document, created_at, updated_at, poet_id, avatar) FROM stdin;
6	\N	\N	الاعتذار	\N	\N	2017-05-13 04:41:22.393638	2017-05-13 07:54:50.042535	4	\N
1	\N	\N	وصف الليل	_____1.wav	\N	2017-03-30 07:46:36.194242	2017-05-15 03:08:19.165887	1	\N
7	\N	\N	الرثاء	\N	\N	2017-05-15 03:31:04.576028	2017-05-15 03:31:04.576028	5	\N
3	\N	\N	معلقة لَبِيدٍ بن رَبِيعَة 	_____1.wav	\N	2017-03-30 09:53:31.031825	2017-05-15 07:36:06.125016	2	\N
\.


--
-- Name: poems_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('poems_id_seq', 7, true);


--
-- Data for Name: poets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY poets (id, name, year, biography, media, document, created_at, updated_at) FROM stdin;
1	امرئ القيس	\N	<p dir="rtl">هو: امرؤ القيس بن حُجْرٍ بن الحارث بن عَمرو المقصور بنِ حُجْرٍ الكِنْديّ من قبيلة كِنده باليمن.</p>\r\n\r\n<p style="text-align: right;">.وكنيته: أبو الحارثِ، وقيل: أبو وَهْبٍ، وأبو زيد</p>\r\n\r\n<p style="text-align: right;">.ولقبه: &quot;الملكُ الضِّلِّيلُ&quot;، و &quot;ذُو القُروح&rdquo;</p>\r\n\r\n<p dir="rtl">وقيل: اسمه &quot;حُنْدُج&quot; وأما امرؤ القيس فلقبه لكنه غَلَبَ عليه حتى أصبح اسمًا له. ومعنى امرئ القيس: رجل الشدّة، وقيل غير ذلك&nbsp;</p>\r\n\r\n<p dir="rtl">&nbsp;</p>\r\n\r\n<p dir="rtl">ولد امرؤ القيس في الجزيرة العربية في مضارب بني أسد وتنقل في نجد واليمامة والبحرين واليمن ولا يعرف تاريخ ولادته، لكنَّ المؤرخين ذكروا أنه في أوائل القرن السادس الميلادي قبل البعثة النبوية. ولم يكن امرؤ القيس ذا علاقة طيبة مع والده لكثرة عبثه ولهوه وشذوذه، فلما أتاه مقتلُ أبيه قال: &quot;ضَيَّعَني صغيرًا، وحَمَّلَني دَمَهُ كبيرًا، لا صَحْوَ اليومَ ولا سُكرَ غدًا، اليومَ خمرٌ وغدًا أمرٌ&quot; فذهبت مثلاً. وقام امرؤ القيس بالثأر لأبيه من بعض القبائل حتى انتهى المطاف به إلى قيصر ملك الروم الذي أكرمه وزوده بقوّة من جيشه، لكنَّ وشايةً دفعت قيصر إلى إرساله جبة مسمومة إلى امرئ القيس وما أن لبسها حتى تقرح جلده، ثم مات ودفن إلى جنب قبر امرأة من بنات الروم ماتت في انقره ودفنت في سفح جبل يقال له: عسيب. وقد قال هذه الأبيات بعدما أخبروه بقصتها.</p>\r\n\r\n<p dir="rtl">&nbsp;</p>\r\n\r\n<p dir="rtl">أَجَـارَتَـنَـا إِنَّ المــَزَارَ قَـرِيـبُ &nbsp;وَإِنِّي مُقِيـمٌ مَـا أَقَـامَ عَسِـيـبُ</p>\r\n\r\n<p dir="rtl">أَجَـارَتَـنَـا إِنَّا غَرِيـبَـانِ هَـهُنَـا &nbsp;وَكُـلُّ غَـرِيـبٍ لِلغَـرِيـبِ نَسِيبُ</p>\r\n\r\n<p dir="rtl">فَـإِنْ تَصِلِيـنَـا فَـالقَـرَابَةُ بَينَـنَا &nbsp;وَإِنْ تَصْرِمِـينَا فَـالقَـرِيبُ غَـرِيبُ</p>\r\n\r\n<p dir="rtl">&nbsp;</p>\r\n\r\n<p dir="rtl"><b>شعر امرئ القيس:</b></p>\r\n\r\n<p dir="rtl">لُقب امرؤ القيس بأمير الشعراء في الجاهلية، وحامل لواء الشعراء، وقد حظي شعرهُ باهتمام بالغ من الرواة والنقاد، وفاضت كتب اللغة والنحو والبلاغة والنقد بأشعاره، وله ديوان شعر مطبوعٌ مشهورٌ، وجعلتْ قصيدته التي نحن بصدد الحديث عنها أُولى المعلقات؛ لإجماع الرواة عليها؛ ولاشتمالها على صور ومضامين بلاغية؛ ولتعدد أغراضها الشعرية. لذلك أصبح امرؤ القيس من الطبقة الأولى من فحول الشعراء الجاهليين؛ لأنه أول من فتح الشعر واستوقف وبكى في الدمن ووصف ما فيها، وهو أول من شبه الخيل بالعصا... وما إلى ذلك كثير.</p>\r\n\r\n<p dir="rtl">&nbsp;</p>\r\n\r\n<p dir="rtl"><b>معلقة امرئ القيس:</b></p>\r\n\r\n<p dir="rtl">ذكر أبو بكر محمد بن القاسم الأنباري عن الفرزدق أنه تحدث عن [يومِ دارةَ جَلْجَلِ]، بأن امرأ القيس كان عاشقًا لابنة عمه يقال لها: عُنَيزَةَ، وأنه طلبها زمانًا فلم يصلْ إليها حتى كان يوم الغدير وهو يوم دارة جلجل الذي ارتحل فيه الحيّ فتقدم الرجال وتخلّف النساء والعبيد والأُجراء والاثقال، وتخلّف معهم امرؤ.</p>\r\n\r\n<p dir="rtl">&nbsp;</p>\r\n\r\n<p dir="rtl">فأتاهنّ امرؤ القيس فأخذ ثيابهن وجمعها وقعد عليها، وقرر أن لا يعطي أيَّ جارية ثوبها ولو ظلت في الغدير إلى الليل حتى تخرج كلّ واحدة عارية لتأخذ ثوبها، ففعلن ذلك حتى لم تبق إلا عُنيزة فاضطرت للخروج عارية فنظر إليها مقبلة ومدبرة ثم لبست ثيابها ثم نحر لهنّ ناقته وشوى لحمها على النار فأكلهنَ وشَبِعنَ ثم ارتحلْنَ وقد حملت كلّ واحدة منهن شيئًا من أثاث امرئ القيس لأنه ذبح لهنَّ ناقته، وركب مع عُنيزة على بعيرها فكان يميل إليها ويدخل رأسه في خدرها ويقبلها، فإذا مال هودجها قالت: &quot;يا امرأ القيس، قد عقرتَ بعيري! حتى إذا كان قريبًا من الحيّ نزل فأقام وأتى أهله ليلاً وقال في ذلك شعرًا منه هذه المعلقة.&nbsp;</p>\r\n	\N	\N	2017-03-30 07:41:42.522978	2017-03-30 07:41:42.522978
2	 لَبِيدٍ بن رَبِيعَة 	\N	<p style="text-align: right;"><strong>ترجمة لبيد بن ربيعة</strong></p>\r\n\r\n<p style="text-align: right;">لبيدٌ بنُ ربيعة بن عامر بن مالك بن جعفر بن كلاب، من هوازن قيس، ويكنى: أبو عقيل. ويقال لأبيه ربيعة: [رَبيعُ المُقترِين] لجوده وكرمه وسخائه، وقد مات أبو لبيد صغيرًا قتيلاً في حربٍ كانت بين بني عامر وبني لبيد.وأُمُّ لبيدٍ عبسيّةٌ اسمها: تَامِرةُ بنتُ زِنْبَاع العبسية. وعمّه أبو بَرَاء عامرُ بن مالك [ملاعب الأسِنَّة].ويُعَدُّ لبيدٌ من الشعراء المجيدين والفرسان المشهورين ومن المعمرين ومن الأجواد الذين يكرمون غيرهم، وقد بكى أخاه أَرْبَدَ كما بكت الخنساء أخاها صخرًا وعدّه ابن سلام في الطبقة الثالثة من فحول الشعراء، وقرنه بالنابغة الجعدي، وأبي ذؤيب الهذلي، والشماخ بن ضرار.وفي شعره حكم كثيرةٌ، ويعدُّ أشهر شعراء الرثاء في الجاهلية، كما أنّ الخنساء تعدّ أشهر شواعر الرثاء الجاهليات</p>\r\n\r\n<p style="text-align: right;">:ومما قاله في هذا المنتدى</p>\r\n\r\n<p style="text-align: right;">&nbsp;أَلاَ كُلُّ شَيىءٍ مَا خَلاَ اللهَ بَاطِلُ &nbsp; &nbsp;وكُلُّ نَعِيمٍ لاَ مَحَالَةَ زَائِلُ&nbsp;</p>\r\n\r\n<p style="text-align: right;">وفي هذا قال رسول الله صلّى الله عليه وسلم: [إنّها أصدق كلمة قالها لبيد]. ثم بعدها أسلم لبيد رضي الله عنه. وحسُنَ إسلامه، وكان من المؤلفة قلوبهم. وهاجر إلى المدينة ثم نزل الكوفة في أيام عمر بن الخطاب رضي الله عنه.</p>\r\n\r\n<p style="text-align: right;">:ولما بلغ سبعًا وسبعين سنة أنشأ يقول</p>\r\n\r\n<p style="text-align: right;">وَقَدْ حَمَلْتُكَ سبعًا بعد سَبْعِينَا &nbsp; &nbsp;بَاتَتْ تَشَكَّى إِلَيَّ النَّفْسُ مُجِهِشَةً</p>\r\n\r\n<p style="text-align: right;">وفي الثـلاثِ وفَـاءٌ للثلاثينَا &nbsp; &nbsp; &nbsp;فإن تُزادي ثـلاثًا تبـلُغِي أملا</p>\r\n\r\n<p style="text-align: right;">:حتى بلغ مائة وعشرين سنة، وقيل غير ذلك، فأنشأ يقولُ</p>\r\n\r\n<p style="text-align: right;">توفى لبيدٌ سنة 41 هجرية أي سنة 662 ميلادية تقريبًا.[ويروى أنّه حين حضرته الوفاة قال لابن أخيه: أي بُنَيَّ، إِنَّ أَبَاكَ لم يَمُتْ وَلَكِنَّهُ فَنِي، فَإِذَا قُبِضَ أَبُوكَ فَغَمِّضْهُ وَأَقْبِلْهُ القِبْلَةَوَسَجِّهِ بِثَوبِهِ، وَلاَ تَصْرُخَنَّ عَلَيْهِ صَارِخَةٌ، وَانْظُرْ جَفْنَتِي التي كُنْتُ أَصْنَعُهَا، فَاصْنَعْهَا وَأَجِدْ صِنَاعَتَهَا، ثُمَّ احْمِلْهَا إِلَى مَسْجِدِكَ وَمَنْ كَانَ يَغْشَانِي عَلَيهَا، فَإِذَا قَالَ الإِمَامُ: سَلاَمٌ عَلَيكُمْ قَدِّمْهَا إِلَيهِمْ يَأْكُلُوهَا، فَإِذَا فَرَغُوا فَقُلْ: احْضُرُوا جَنَازَةَ أَخِيكُمْ لَبِيدٍ، فَقَدْ قَبَضَهُ اللهُ تَبَارَكَ وَتَعَالَى].ويبدو أنّ لبيدًا خاطب ابنَ أخيهِ لأنّه لم يكن له ولدٌ ذكر سوى بنتين، والمقصود بالجفنةِ الطبقُ الكبير الذي يضع فيه الطعام للناس، فقد كانتْ للبيدٍ جفنتان يغدو بهما ويروح في كلّ يوم إلى مسجد قومه فيطعمهم</p>\r\n\r\n<p style="text-align: right;">&nbsp;</p>\r\n\r\n<p style="text-align: right;">:<strong>شعره</strong></p>\r\n\r\n<p style="text-align: right;">وسُؤالُ هذا الناس كَيفَ لَبيدُ &nbsp; وَلَقَدْ سَئِمْتُ مِنَ الحياةِ وطُولها</p>\r\n\r\n<p style="text-align: right;">:<strong>شعره</strong></p>\r\n\r\n<p style="text-align: right;">&nbsp; :رأى النابغةُ الذبيانيُّ لبيدًا في بلاط النعمان بن المنذر مع أعمامه وكان صغير السنّ، فقال له: يا غلام إِنَّ عينَيكَ لعينا شاعرٍ، أَفَتَقرضُ من الشعر شيئًا؟ قال لبيدٌ: نعم يا عَمُّ، قال النابغة: فأنشدني شيئًا مما قلتَهُ، فأنشده قوله</p>\r\n\r\n<p style="text-align: right;">ِسَلْمَى بِالمـذَايُبِ فَالقِفَـالِ &nbsp; &nbsp; &nbsp; أَلَمْ تُلْمِمْ عَلَى الدِّمَـنِ الخَـوَالِي</p>\r\n\r\n<p style="text-align: right;">:فقال له النابغة: أنت أشعر بني عامر، زدني فأنشده قوله</p>\r\n\r\n<p style="text-align: right;">فَبِعَاقِـلٍ فَالأَنْعَمَيـنِ رُسُـومُ &nbsp; &nbsp; طَلَلٌ لِخَـولَةَ بِالرُّسَيسِ قَـدِيمٌ</p>\r\n\r\n<p style="text-align: right;">&nbsp;</p>\r\n\r\n<p>&nbsp; &nbsp; &nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align: right;">&nbsp;</p>\r\n	\N	\N	2017-03-30 09:08:16.423905	2017-03-30 09:30:36.866974
4	النابغة الذبيانيّ	\N	<p dir="RTL">هو: زياد بن معاوية بن ضِباب بن جَنَاب بن يربوع بن غيظ بن مرة بن عوف. من قيس عيلان بن مضر. وأمّه عاتكة بنت أُنيس من بني أشجع الذبيانية فهو ذبياني الأب والأم.</p>\r\n\r\n<p dir="RTL">ويكنى: أبا أُمَامَةَ، وأبَا ثُمَامَة وأبَا عقرب، وهنَّ بناته.</p>\r\n\r\n<p dir="RTL">ويلقب: بالنابغة وبهذا اللقب اشتهر، ولقب بذلك لنبوغه في الشعر في كبره لأنّه لم يقل شعرًا وهو صغير وهذا ربما يناقض ما سنذكره عن أول شعر قاله، وقيل: لُقِّبَ بالنابغة لقوله:</p>\r\n\r\n<p dir="RTL">&nbsp;</p>\r\n\r\n<p dir="RTL">وَحَلَّـتْ فِي بَنِي القَينِ بنِ جَسْرٍ # &nbsp;فَقَدْ نَبَغَتْ لَنَـا مِنْهُمْ شُـؤُونُ</p>\r\n\r\n<div style="clear:both;">&nbsp;</div>\r\n\r\n<p dir="RTL">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; والنابغة أحد فحول الشعراء في الجاهلية وعده بن سلام من الطبقة الأولى، وقرنه بامرئ القيس والأعشى وزهير، لذلك تبوَّأ النابغة مكانة رفيعة في شعر العصر الجاهلي وتناقلت الأجيال شعره إلى يومنا هذا.</p>\r\n\r\n<p dir="RTL">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; روي عن الأصمعي أنّه قال: أوّل ما تكلم به النابغة من الشعر أنّه حضر مع عمّه عند رجل، وكان عمُّه يشاهد به الناس ويخاف أن يكون عيبًا فوضع الرجل كأسًا في يده وقال:</p>\r\n\r\n<table align="center" border="0" cellpadding="0" cellspacing="0" dir="rtl" style="width:68.14%;" width="68%">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style="width:46.26%;height:44px;">\r\n\t\t\t<p dir="RTL">تطيب كُؤوسنا لـولا قذاهـا<br />\r\n\t\t\t&nbsp;</p>\r\n\t\t\t</td>\r\n\t\t\t<td style="width:6.82%;height:44px;">\r\n\t\t\t<p align="center" dir="RTL">#</p>\r\n\t\t\t</td>\r\n\t\t\t<td style="width:46.92%;height:44px;">\r\n\t\t\t<p dir="RTL">ويحـتملُ الجليـسُ على أذاهـا<br />\r\n\t\t\t&nbsp;</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<div style="clear:both;">&nbsp;</div>\r\n\r\n<p dir="RTL">&nbsp;</p>\r\n\r\n<p dir="RTL">فقال النابغة وقد غضب:</p>\r\n\r\n<table align="center" border="0" cellpadding="0" cellspacing="0" dir="rtl" style="width:68.14%;" width="68%">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style="width:46.26%;height:44px;">\r\n\t\t\t<p dir="RTL">قـذاهـا أنّ صـاحبَها بخيـل<br />\r\n\t\t\t&nbsp;</p>\r\n\t\t\t</td>\r\n\t\t\t<td style="width:6.82%;height:44px;">\r\n\t\t\t<p align="center" dir="RTL">#</p>\r\n\t\t\t</td>\r\n\t\t\t<td style="width:46.92%;height:44px;">\r\n\t\t\t<p dir="RTL">يحـاسب نفسه بكم اشتراهـا<br />\r\n\t\t\t&nbsp;</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<div style="clear:both;">&nbsp;</div>\r\n\r\n<p dir="RTL">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; وروي أن الخليفة عمر بن الخطاب رضي الله عنه قال: يا معشر غطفان من الذي يقول:</p>\r\n\r\n<table align="center" border="0" cellpadding="0" cellspacing="0" dir="rtl" style="width:68.14%;" width="68%">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style="width:46.26%;height:44px;">\r\n\t\t\t<p dir="RTL">أَتَيتُـكَ عَـاريًا خَلِـقًا ثيـابي<br />\r\n\t\t\t&nbsp;</p>\r\n\t\t\t</td>\r\n\t\t\t<td style="width:6.82%;height:44px;">\r\n\t\t\t<p align="center" dir="RTL">#</p>\r\n\t\t\t</td>\r\n\t\t\t<td style="width:46.92%;height:44px;">\r\n\t\t\t<p dir="RTL">عَلى خَـوفٍ تظنُّ بيَ الظنـون<br />\r\n\t\t\t&nbsp;</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<div style="clear:both;">&nbsp;</div>\r\n\r\n<p dir="RTL">قالوا: النابغة، قال: ذاك أشعر شعرائكم، وكان الخليفة عبد الملك بن مروان شديد الإعجاب والافتتان بشعره.</p>\r\n\r\n<p dir="RTL">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; وسئل حماد الراوية: بأي شيء يقدم النابغة على غيره؟ فقال: بالاكتفاء بالبيت الواحد من شعره لا بل بنصف البيت، لا بل بربعه.</p>\r\n\r\n<p dir="RTL">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; وكانت تُضرب للنابغة قبة من جلد في سوق عكاظ فتأتيه الشعراء لتعرض عليه أشعارها وتتحاكم إليه.</p>\r\n\r\n<p dir="RTL">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; وكان النابغة من أخصَّاءِ النعمان بن المنذر ملك الحيرة إلى أن حصلت بينهما وقيعة وخلاف فافترقا وهرب إلى ملوك غسان بالشام إلى أن قدم النابغة اعتذارًا للنعمان بقصيدة مشهورة هي معلقته، فقبل اعتذاره ورضي عنه، وكان مطلع تلك القصيدة:</p>\r\n\r\n<table align="center" border="0" cellpadding="0" cellspacing="0" dir="rtl" style="width:68.14%;" width="68%">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style="width:46.26%;height:44px;">\r\n\t\t\t<p dir="RTL">يَـا دَارَ مَيَّةَ بِالعَلْيَاء فَالسَّنَـدِ<br />\r\n\t\t\t&nbsp;</p>\r\n\t\t\t</td>\r\n\t\t\t<td style="width:6.82%;height:44px;">\r\n\t\t\t<p align="center" dir="RTL">#</p>\r\n\t\t\t</td>\r\n\t\t\t<td style="width:46.92%;height:44px;">\r\n\t\t\t<p dir="RTL">أَقْوَتْ وَطَالَ عَلَيهَا سَالفُ الأَمَدِ<br />\r\n\t\t\t&nbsp;</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<div style="clear:both;">&nbsp;</div>\r\n\r\n<p dir="RTL">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; مات النابغة بحدود سنة 18 قبل الهجرة أي سنة 604 ميلادية، ولم يشهد بعثة النبي صلى الله عليه وسلم التي كانت سنة بحدود 610 ميلادية.</p>\r\n\r\n<p dir="RTL">&nbsp;</p>\r\n\r\n<p dir="RTL"><strong>القصيدة المختارة:</strong></p>\r\n\r\n<p dir="RTL">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; كان النابغة الذبياني على صلة وثيقة بملوك الحيرة وبخاصة بالنعمان بن المنذر أبي قابوس، وقد حصل خلاف بين النابغة والمنذر بسبب وشاية حتى هرب النابغة خوفًا من القتل، وقيل لأنّ النابغة صور امرأة النعمان في شعرهِ تصويرًا وكأنّها عارية أمامه مما زرع في قلب النعمان الغيرة والتوعد بقتله ففرَّ النابغة وقيل غير ذلك مما عدّه النعمان خيانةً وغدرًا حتى هدر دمه.</p>\r\n\r\n<p dir="RTL">قَدَّمَ النابغةُ اعتذاراتٍ متكرّرةً للنعمان حتى يقبل عذره ويعفو عنه، وهذه القصيدة القصيرة التي اخترناها تكشف عن الأمر في وضوح وتظهر السبب الحقيقي لغضب النعمان بن المنذر، فجاءت القصيدة تحمل عتابًا واعتذارًا في الوقت نفسه، وكان مطلع هذه القصيدة:</p>\r\n\r\n<table align="center" border="0" cellpadding="0" cellspacing="0" dir="rtl" style="width:68.14%;" width="68%">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td style="width:46.26%;height:44px;">\r\n\t\t\t<p dir="RTL">أَتَانِي أَبَيتَ اللَّعْنَ أَنَّـكَ لُمْتَنِـي<br />\r\n\t\t\t&nbsp;</p>\r\n\t\t\t</td>\r\n\t\t\t<td style="width:6.82%;height:44px;">\r\n\t\t\t<p align="center" dir="RTL">#</p>\r\n\t\t\t</td>\r\n\t\t\t<td style="width:46.92%;height:44px;">\r\n\t\t\t<p dir="RTL">وَتِلْكَ الَّتِي أُهتَمُّ مِنْهَا وأَنْصَـبُ<br />\r\n\t\t\t&nbsp;</p>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<div style="clear:both;">&nbsp;</div>\r\n\r\n<p style="text-align: right;"><span dir="RTL">وهكذا كان النابغة عرضةً لخيانة الأيّام وشماتة الحاسدين، وبعد أن التحق النابغة بقبيلته لتحميه، ثم رجع إلى النعمان بعد أن عفا عنه وصفح وقربه إليه مرّة أخرى.</span></p>\r\n	\N	\N	2017-05-13 04:40:16.248838	2017-05-15 03:20:12.297705
5	الخنساء	\N	<p dir="RTL">هي تُمَاضِرُ بنتُ عمروٍ بن الحَرْث بن الشَّريد، من بني سليم من قيس عيلان. ولقبها الخنساء ومعناه الضبية، وقد غلب عليها هذا اللقب. وكنيتها: أمّ عمرو. ولدت في الجاهلية وماتت في الإسلام سنة 23هـ في أوّل خلافة عثمان بن عفان رضي الله عنه.</p>\r\n\r\n<p dir="RTL">وهي من نجد وقد عاشت أكثر عمرها في الجاهلية وأدركت الإسلام فأسلمت. وكان رسول الله صلى الله عليه وسلّم معجبًا بشعرها. خطبها دُرَيدُ بن الصِّمَّةِ فامتنعت وردَّته، وجرت بينهما مساجلات شعرية. وتزوّجت رواحة بن عبد العزيز السُّلَمي، فولدت له عبد الله، وكنيته أبو شجرة، ثم تزوّجها مرداس بن أبي عامر السُّلَمي فولدت له يزيدَ ومعاوية وعمرًا.</p>\r\n\r\n<p dir="RTL">والخنساء من شواعر العرب المشهورات وقد شهد لها بالتقدم ذوو النقد الأدبي قديمًا وحديثًا. وأنها أشعر شاعرات العرب على الإطلاق.</p>\r\n\r\n<p dir="RTL">وقد عدّها ابن سلاّم في طبقات الشعراء من طبقة أصحاب المراثي. وكان أكثرُ شعرها وأجودُه رثاءها لأخويها صخرٍ ومعاويةَ. أمّا صخرٌ فكان أخاها لأبيها، وأحبهما إليها لحلمه وجوده وشجاعته. وأمّا معاويةُ فأخوها لأبيها وأمّها. وقد قتلا في الجاهلية فبكتهما ورثتهما.</p>\r\n\r\n<p dir="RTL">وكان للخنساء أربعة بنين شاركوا في حرب القادسية لفتح فارس وسارتْ معهم وحضرت وقعة القادسية سنة 16ست عشرة هجرية، وأوصتهم بوصية تناقلتها كتب التاريخ والأدب. وقد استشهدوا جميعًا واحدًا بعد واحدٍ فبلغها الخبر فقالت: [الحمد لله الذي شرفني بقتلهم، وأرجو من ربي أن يجمعني بِهم في مستقر الرحمة]. وكان عمر بن الخطاب رضي الله عنه يعطيها أرزاق بنيها الأربعة حتى قُبِضَ.</p>\r\n	\N	\N	2017-05-15 03:30:33.87846	2017-05-15 03:30:33.87846
\.


--
-- Name: poets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('poets_id_seq', 5, true);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY schema_migrations (version) FROM stdin;
20161122025226
20161123100327
20161123103007
20161123114354
20161125070542
20161125073633
20161126005716
20161126104754
20161201130233
20161209024215
20161209044721
20161215075801
20161215075803
20161215075804
20170314124217
20170329145628
20170515054514
\.


--
-- Data for Name: semesters; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY semesters (id, name, user_id, status, activated, created_at, updated_at) FROM stdin;
1	Default Semester	1	1	f	2017-03-30 06:46:00.839421	2017-03-30 06:46:00.839421
3	Semester 2, 2015/2016	1	\N	t	2017-04-27 10:31:41.179985	2017-04-27 10:31:44.309521
\.


--
-- Name: semesters_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('semesters_id_seq', 3, true);


--
-- Data for Name: stanzas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY stanzas (id, poem_id, location, created_at, updated_at) FROM stdin;
1	1	\N	2017-03-30 07:46:36.206489	2017-03-30 07:46:36.206489
3	3	\N	2017-03-30 09:53:31.034534	2017-03-30 09:53:31.034534
6	6	\N	2017-05-13 04:41:22.579017	2017-05-13 04:41:22.579017
7	7	\N	2017-05-15 03:31:04.579044	2017-05-15 03:31:04.579044
\.


--
-- Name: stanzas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('stanzas_id_seq', 7, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (id, provider, uid, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, confirmation_token, confirmed_at, confirmation_sent_at, unconfirmed_email, name, nickname, image, email, roles, tokens, created_at, updated_at, status_id, approved) FROM stdin;
29	email	712dc338-191b-4330-9222-c3728244ec0e	$2a$11$RgnkX9lqoVULILUQmlHaButXY.bzmhRgz45Z2zzxavU.V1FF1ypRe	\N	\N	\N	1	2017-04-26 00:58:38.898681	2017-04-26 00:58:38.898681	183.171.93.23	183.171.93.23	\N	\N	\N	\N	\N	\N	\N	wnhusna95@gmail.com	---\n- '3'\n	{}	2017-04-26 00:51:41.867628	2017-04-26 00:58:38.90449	2	t
21	email	404f002e-345a-4ec8-a334-9c83fc531bb3	$2a$11$EcPbsN2AUtR2U2N5t2.lv.o7/Uoyr3bcSWs6cbtbXqh13Yno.oUMK	\N	\N	\N	2	2017-04-26 01:02:23.71067	2017-04-26 00:54:37.102221	183.171.64.252	183.171.64.252	\N	\N	\N	\N	\N	\N	\N	smujibullah.iium@gmail.com	---\n- '3'\n	{}	2017-04-26 00:50:14.737092	2017-04-26 01:02:23.716163	2	t
30	email	6b331a2d-d352-457c-8433-ffcde5c6d12a	$2a$11$nKdihzKDSU2T.dDTdjQYuu3uZS82MhV2bhlGoL/jHFtq3k9f8BDJ6	\N	\N	\N	2	2017-04-26 01:03:51.337681	2017-04-26 00:52:42.590431	210.48.222.13	123.136.107.84	\N	\N	\N	\N	\N	\N	\N	amilynayusof@gmail.com	---\n- '3'\n	{}	2017-04-26 00:51:51.65012	2017-04-26 01:03:51.343062	2	t
4	email	b19825ec-07b6-413a-b95a-c17704960a0e	$2a$11$/P3oo1bHE33qnuTxh6LyruO9qwXoS6ZJK9uPkkUr2gobQbK9o3oTW	\N	\N	\N	2	2017-04-26 01:15:26.854816	2017-04-26 00:49:35.65147	183.171.90.144	183.171.90.144	\N	\N	\N	\N	\N	\N	\N	ikmal1194@gmail.com	---\n- '3'\n	{}	2017-04-26 00:46:14.314178	2017-04-26 01:15:26.864669	2	t
20	email	1b18636b-357c-4c86-bb05-730b7db32832	$2a$11$Ni43ni5/rHtVwdrTt.LWLOWQtN2UhheOmnQDKob.vzQ84R/iNY0Hq	\N	\N	\N	1	2017-04-26 00:50:50.303519	2017-04-26 00:50:50.303519	183.171.81.119	183.171.81.119	\N	\N	\N	\N	\N	\N	\N	suriacheyi@gmail.com	---\n- '3'\n	{}	2017-04-26 00:50:02.449731	2017-04-26 00:50:50.308774	2	t
23	email	a99bf277-ba7e-4de4-9604-ef88e855d844	$2a$11$CW1SPH0QpDvSH82rgTzvg.FkQhFqAkNoUmWH5pBFjqPpYcsDv6uBa	\N	\N	\N	1	2017-04-26 00:50:50.469599	2017-04-26 00:50:50.469599	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	khalidahalid@gmail.com	---\n- '3'\n	{}	2017-04-26 00:50:23.040633	2017-04-26 00:50:50.474839	2	t
22	email	33428890-efcb-4176-89ec-b6e3401e5876	$2a$11$ANev9EZRmTcAznGUkXzV6OBrbr8FIzrpLISGnQCkCR7cADfq1/EVO	\N	\N	\N	1	2017-04-26 00:51:11.381263	2017-04-26 00:51:11.381263	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	fiqa19141@gmail.com	---\n- '3'\n	{}	2017-04-26 00:50:20.294475	2017-04-26 00:51:11.386193	2	t
10	email	47f94f24-342c-4eeb-bc34-d951e6ed3758	$2a$11$U6EHNiXUhjeyawBiq16ZcuoJxEBzRjPg1yWwFPTDbJow5zVZPki9K	\N	\N	\N	1	2017-04-26 00:49:31.462856	2017-04-26 00:49:31.462856	183.171.82.81	183.171.82.81	\N	\N	\N	\N	\N	\N	\N	lyanafatihah94@gmail.com	---\n- '3'\n	{}	2017-04-26 00:47:35.665187	2017-04-26 01:16:49.49192	2	t
72	email	eb42d4c3-8db6-4a97-942e-2f8382eb55e2	$2a$11$DbkxST94YNG4F9IMnZBMXe5nwKCp9cawDhp8tks6vPeMlvZGJCsXa	\N	\N	\N	1	2017-04-26 01:20:34.636215	2017-04-26 01:20:34.636215	183.171.82.81	183.171.82.81	\N	\N	\N	\N	\N	\N	\N	mafatinatina@yahoo.com	---\n- '3'\n	{}	2017-04-26 01:17:53.427479	2017-04-26 01:21:08.270971	2	t
19	email	3d744d33-8ce8-44c4-957a-c190576ab1a8	$2a$11$qzUXWjAj4zRcB7ToyZ4G2eObbBuQvglqfXWyOpssOzSJxCf9kcImi	\N	\N	\N	1	2017-04-26 00:50:49.685473	2017-04-26 00:50:49.685473	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	acid.layot@gmail.com	---\n- '3'\n	{}	2017-04-26 00:49:53.062136	2017-04-26 01:21:58.527226	2	t
15	email	374acfe2-0f49-4d2d-8f40-81d384040a05	$2a$11$yZlQDZrMEN/4IqMUCcopBuL97pXPhdohlkUpaPVUb76SqVpILXtgy	\N	\N	\N	1	2017-04-26 00:49:35.822194	2017-04-26 00:49:35.822194	183.171.68.156	183.171.68.156	\N	\N	\N	\N	\N	\N	\N	anafarhanaadilah@gmail.com	---\n- '3'\n	{}	2017-04-26 00:48:42.940998	2017-04-26 01:21:58.77213	2	t
7	email	1bbd02c0-5594-4093-a45e-8f8264bb0dae	$2a$11$YE/sN9bEeWMS9rZ/1N347enZUwPhWzsPzWyq5kj1mb/OS4B4PYi4C	\N	\N	\N	1	2017-04-26 00:49:46.676183	2017-04-26 00:49:46.676183	183.171.81.24	183.171.81.24	\N	\N	\N	\N	\N	\N	\N	eykalivepul10@gmail.com	---\n- '3'\n	{}	2017-04-26 00:46:48.769286	2017-04-26 01:22:27.319164	2	t
9	email	69ccea08-91ac-47f2-9ba2-37b32b56bae0	$2a$11$2UN0nPMJTC.ENB.9EEc4vOmNdeRyvUi.U.ofvOr4CWlxt1jG3a5Wu	\N	\N	\N	1	2017-04-26 00:49:42.804747	2017-04-26 00:49:42.804747	183.171.65.239	183.171.65.239	\N	\N	\N	\N	\N	\N	\N	nazrishahidanmdyusof@gmail.com	---\n- '3'\n	{}	2017-04-26 00:47:28.55547	2017-04-26 01:22:37.431368	2	t
24	email	e1b738f3-f0a0-4e2a-9e6c-bb22efbcc4da	$2a$11$Q6f8pjYQPMMs9AvmPX03fODx6dYh9cqZLyNkLkWYSx8giEzp4uxtu	\N	\N	\N	1	2017-04-26 00:51:00.607988	2017-04-26 00:51:00.607988	183.171.90.144	183.171.90.144	\N	\N	\N	\N	\N	\N	\N	amiranuar95@gmail.com	---\n- '3'\n	{}	2017-04-26 00:50:28.292776	2017-04-26 01:22:52.440696	2	t
12	email	d98b37a1-6d2f-4b5e-8627-2a2cb7f0c87e	$2a$11$YP5lMxmCrmejsNbFgnIhDuI3B.pbz4nVYFrigaRgQm4ZnqxhTEZI.	\N	\N	\N	1	2017-04-26 00:49:46.086994	2017-04-26 00:49:46.086994	183.171.89.95	183.171.89.95	\N	\N	\N	\N	\N	\N	\N	hidayahsoyuti94@gmail.com	---\n- '3'\n	{}	2017-04-26 00:48:03.871256	2017-04-26 01:23:13.244411	2	t
28	email	42a71827-e032-445e-b27a-48e1f088d187	$2a$11$fiU1qpI3CqHNm3oWCy6hDOEdUk/yCmJfYgkiigTDjeQ3Z5qSHS/lm	\N	\N	\N	1	2017-04-26 00:52:05.885371	2017-04-26 00:52:05.885371	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	sueyhanz219@gmail.com	---\n- '3'\n	{}	2017-04-26 00:51:32.228701	2017-04-26 01:23:35.159063	2	t
11	email	ada13bf0-3e99-44fa-aaca-e41c826b9411	$2a$11$/J/lWmb9BFye2ZkCwZN6N.a/R/A1q9uTL7/6UW5Upazyj0Pehhba.	\N	\N	\N	1	2017-04-26 00:50:13.361939	2017-04-26 00:50:13.361939	183.171.87.143	183.171.87.143	\N	\N	\N	\N	\N	\N	\N	zatin_nitaqain@yahoo.com	---\n- '3'\n	{}	2017-04-26 00:47:51.866715	2017-04-26 01:23:50.978971	2	t
17	email	6b5efd24-7105-40b5-8e98-70ec43defd10	$2a$11$/y9silPqK02hiNtUoOZFBOlm6uCNQKkaHmA.TWcmCs4RnCEsHCS/i	\N	\N	\N	1	2017-04-26 00:50:02.109183	2017-04-26 00:50:02.109183	113.210.179.238	113.210.179.238	\N	\N	\N	\N	\N	\N	\N	vyadawiah_adda@yahoo.com	---\n- '3'\n	{}	2017-04-26 00:49:23.957017	2017-04-26 02:15:41.235302	2	t
26	email	5733a766-7078-4749-872b-076f52a4617c	$2a$11$23PHHwSam0IUXERiXGgTcevJsXaFlajtcK7KxuOjVvtjk9K/OuVe2	\N	\N	\N	1	2017-04-26 00:51:43.044913	2017-04-26 00:51:43.044913	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	afiqahyusri.95@gmail.com	---\n- '3'\n	{}	2017-04-26 00:51:22.149559	2017-04-26 00:51:43.050354	2	t
135	email	2fed6623-9823-46a0-8d60-c1ebc5c1e3d9	$2a$11$KZewOKyrkXz.gmiy72LtfOa3Jh.7xjm35n2HTSTKGk5MDi.RDRIpW	\N	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	abi.dzar@ibnjaafar.com	---\n- '3'\n	{}	2017-05-11 09:43:58.662783	2017-05-11 09:46:12.099572	2	f
25	email	d4200dd9-a198-4f10-a931-b91aaa4bfcfe	$2a$11$4VJ/au4O9va/yYENrPlmzON4rvt1TMGYjN6F/P4jGJqsQCIpY9l1u	\N	\N	\N	1	2017-04-26 00:54:46.696981	2017-04-26 00:54:46.696981	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	syaffiqraffi@gmail.com	---\n- '3'\n	{}	2017-04-26 00:51:12.557182	2017-04-26 00:54:46.702057	2	t
3	email	b3d7572e-ffca-482a-9368-ff4f9bc4a91c	$2a$11$dRvphEoETJwKv28KzITFTu5Bktpbtn2rrgu68fySNS7mAWwV.58UW	\N	\N	\N	1	2017-04-25 14:02:27.856135	2017-04-25 14:02:27.856135	123.136.107.128	123.136.107.128	\N	\N	\N	\N	\N	\N	\N	aimranrafee11@gmail.com	---\n- '3'\n	{}	2017-04-25 13:58:29.390406	2017-04-25 14:02:27.861532	2	t
8	email	5d21ecc7-1557-49e5-bae9-1912bf06aa32	$2a$11$Wwaf5oRu.0Ov95wdbl9G1unZpWJCApPCeFfWxqAlO6eje4tr2zExG	\N	\N	\N	1	2017-04-26 00:49:30.387791	2017-04-26 00:49:30.387791	115.164.189.201	115.164.189.201	\N	\N	\N	\N	\N	\N	\N	miszsyasya@ymail.com	---\n- '3'\n	{}	2017-04-26 00:47:00.962913	2017-04-26 00:49:30.39308	2	t
16	email	683564bf-a42d-4368-9314-2843df392ee5	$2a$11$VmMM0.ZMdvHt3/Wt0zZFi.CqPFjL/AMHSxGAGGDFzz7aV8GaBiZZq	\N	\N	\N	1	2017-04-26 00:49:35.283387	2017-04-26 00:49:35.283387	183.171.83.14	183.171.83.14	\N	\N	\N	\N	\N	\N	\N	sueha.ezani@gmail.com	---\n- '3'\n	{}	2017-04-26 00:49:15.089164	2017-04-26 00:49:35.288661	2	t
6	email	3f63dfac-a347-4c68-8a7e-0d2f7bb0aff8	$2a$11$Zh9dY3HXpLKKRbJ52c6T1.dKzW8x0dODpR7J2CxQSnBY.Od6M/UA2	\N	\N	\N	1	2017-04-26 00:49:37.382834	2017-04-26 00:49:37.382834	183.171.88.119	183.171.88.119	\N	\N	\N	\N	\N	\N	\N	faa143johann@gmail.com	---\n- '3'\n	{}	2017-04-26 00:46:37.572758	2017-04-26 00:49:37.388042	2	t
5	email	cad7ba63-6f3b-43de-bc46-ba302e463a15	$2a$11$yr/X4iXbcyctkxWwKcN4J.GksBRsP7lj.FQ/Mx9WY4VE5TPFKbMSa	\N	\N	\N	1	2017-04-26 00:49:41.158717	2017-04-26 00:49:41.158717	183.171.74.119	183.171.74.119	\N	\N	\N	\N	\N	\N	\N	iman_manal95@yahoo.com	---\n- '3'\n	{}	2017-04-26 00:46:29.356948	2017-04-26 00:49:41.164039	2	t
13	email	4c9cf1c9-02b3-490e-a275-a7b90b304cbb	$2a$11$h6TA6KeW0rBkDkg6WkpLYuDvmQpfns6A8sFMNeEdYUZpFUewxnVom	\N	\N	\N	1	2017-04-26 00:49:46.512676	2017-04-26 00:49:46.512676	183.171.67.90	183.171.67.90	\N	\N	\N	\N	\N	\N	\N	imanraihan95@gmail.com	---\n- '3'\n	{}	2017-04-26 00:48:24.059804	2017-04-26 00:49:46.517584	2	t
18	email	34d4c632-0577-4925-8bac-38ea9ee60f83	$2a$11$NJ.1SXGLJ3DY2fVr67XliOg2i3PPHKXP1mqXvMrFDQNkxwBLQdjDG	\N	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	norsaiyidah94@gmali.com	---\n- '3'\n	{}	2017-04-26 00:49:45.250594	2017-04-26 00:50:19.129984	2	t
14	email	ade98c12-7f99-4d8e-a760-755c361f44b8	$2a$11$FpDkc92C48kIcmj3vhYzwuC4k.N2NbvUu.GDenojJC5Sg19aQe0US	\N	\N	\N	1	2017-04-26 00:50:19.495151	2017-04-26 00:50:19.495151	183.171.80.244	183.171.80.244	\N	\N	\N	\N	\N	\N	\N	nurfahiem@gmail.com	---\n- '3'\n	{}	2017-04-26 00:48:40.263141	2017-04-26 00:50:19.500183	2	t
41	email	d634c3af-892c-4ed8-8737-b1843eb75390	$2a$11$k/wSOCIKvQCrKb1vXQh5XuN5OLfXI/Wh4LMSjUHdIFp65lcDvB69G	\N	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	azziraiium@gmail.con	---\n- '3'\n	{}	2017-04-26 00:55:11.69336	2017-04-26 00:55:28.914623	2	t
32	email	bf54015e-4cd0-42ca-bd2f-5d2eba3dbe41	$2a$11$BMN011BY4PS7B94n7ZRp8Owx1zhPDDcU1UNCvRf67tZjcm4KLf1Dy	\N	\N	\N	2	2017-04-26 01:21:04.620107	2017-04-26 00:54:00.029055	123.136.107.183	123.136.106.35	\N	\N	\N	\N	\N	\N	\N	husnaburhanuddin@gmail.com	---\n- '3'\n	{}	2017-04-26 00:52:48.530123	2017-04-26 01:21:04.626313	2	t
40	email	0e6ab21e-4eea-45f9-988c-514e701938ca	$2a$11$Q0d39Dd7MVD62u/7beniHOPYc8DDupA5a2I.4q/MVA6.wwV3nYvC6	\N	\N	\N	1	2017-04-26 00:55:41.808084	2017-04-26 00:55:41.808084	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	mohd.idaz95@gmail.com	---\n- '3'\n	{}	2017-04-26 00:55:08.312485	2017-04-26 00:55:41.813164	2	t
56	email	7db8a206-fa88-422a-84ca-6aa9c911d81e	$2a$11$HmFDpBQ1ipESO0BUJnHaR.Obb0NLelUDhVlOA9A5UC4Kh3m6OWeTu	\N	\N	\N	1	2017-04-26 01:00:45.101031	2017-04-26 01:00:45.101031	183.171.87.143	183.171.87.143	\N	\N	\N	\N	\N	\N	\N	sierahsohaimy95@gmail.com	---\n- '3'\n	{}	2017-04-26 00:58:29.256465	2017-04-26 01:00:45.106371	2	t
38	email	1b285085-00b8-452d-bada-854ad04b51cb	$2a$11$K78n5L2VGmlI4HKsrblCduN4qbBiUdMs9cyhglgZcIOqopIMo4u56	\N	\N	\N	1	2017-04-26 00:55:59.627404	2017-04-26 00:55:59.627404	113.210.205.160	113.210.205.160	\N	\N	\N	\N	\N	\N	\N	allysuhaili9@gmail.com	---\n- '3'\n	{}	2017-04-26 00:53:53.282591	2017-04-26 00:55:59.632717	2	t
129	email	0bb42a6e-0819-48db-83ee-f4a5aeaf9d34	$2a$11$zABEwKQFfOfwI2Td.YTfuuKOFiYrx6Lp0H18EQEIJvbd7a3zQ/6MW	\N	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	fityanummaj93@gmail.com	---\n- '3'\n	{}	2017-04-26 14:59:52.22398	2017-04-26 14:59:52.235514	2	f
50	email	36f7c452-791e-4077-8767-5855f9d81bf7	$2a$11$tecC/Sw/85r97Fm2kM/19el6IF5Ufcg/cmAHZGh8cLoiXyp8q6U1e	\N	\N	\N	1	2017-04-26 00:58:21.159715	2017-04-26 00:58:21.159715	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	sakinah_hanum@yahoo.com	---\n- '3'\n	{}	2017-04-26 00:57:10.663403	2017-04-26 01:24:10.571491	2	t
33	email	89b19670-92ae-447b-92bb-af2bd039d0ba	$2a$11$otKEml81pDZMGys595RXfOegHibZrYML9GUBFFxOPoUHAMYYsd9FK	\N	\N	\N	1	2017-04-26 00:53:41.32416	2017-04-26 00:53:41.32416	113.210.191.46	113.210.191.46	\N	\N	\N	\N	\N	\N	\N	norsaiyidah94@gmail.com	---\n- '3'\n	{}	2017-04-26 00:53:06.072159	2017-04-26 01:24:17.191919	2	t
31	email	8d14e88a-0025-445a-8622-67356f9bbb76	$2a$11$g5z6nUpElEDHLXcCH9pr6Od4ykN79rwQ1607rY1SSDybye14o8GW.	\N	\N	\N	1	2017-04-26 00:53:09.041521	2017-04-26 00:53:09.041521	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	warapauzi95@gmail.com	---\n- '3'\n	{}	2017-04-26 00:52:36.929676	2017-04-26 01:24:34.805645	2	t
42	email	0b2206c6-64a6-4ce1-9238-eafcedd28c57	$2a$11$cH7ABd3iIoABuTgbG4Ijy.ko5HrhggYrLv.D6taZ0Ps.oijyxmauG	\N	\N	\N	1	2017-04-26 00:56:10.82316	2017-04-26 00:56:10.82316	113.210.179.238	113.210.179.238	\N	\N	\N	\N	\N	\N	\N	ffatinazwanis@yahoo.com	---\n- '3'\n	{}	2017-04-26 00:55:38.156806	2017-04-26 00:56:10.828371	2	t
48	email	2a204834-5ea2-433d-a575-91afb9ea50ae	$2a$11$c8VCpj2ff80NtI4dUJTI.uOonH0L.0lfG02ms7Hzelhfxrc2Eck1S	\N	\N	\N	2	2017-04-26 01:07:11.766811	2017-04-26 00:57:08.655482	113.210.200.118	183.171.83.85	\N	\N	\N	\N	\N	\N	\N	intan_nurina@ymail.com	---\n- '3'\n	{}	2017-04-26 00:56:30.834598	2017-04-26 01:27:34.641296	2	t
60	email	87ae2044-7eb2-4516-a3d6-377c5454194a	$2a$11$No9UoOEkkpTPfGZGXMBj2OKkKyAPUbO07RrySWNMQ7EF5GnvoTA2.	\N	\N	\N	1	2017-04-26 01:01:37.344786	2017-04-26 01:01:37.344786	123.136.106.210	123.136.106.210	\N	\N	\N	\N	\N	\N	\N	izzatyusri1994@gmail.com	---\n- '3'\n	{}	2017-04-26 01:00:17.989653	2017-04-26 01:01:37.350416	2	t
43	email	1bc62053-cbb5-4cf9-9861-2ac2071f1cd2	$2a$11$Hz31IzRPfJmuBAXZf/WaVOjHFwVQzACuhkDKalkR8BhQEppBvjLjO	\N	\N	\N	1	2017-04-26 01:02:40.220206	2017-04-26 01:02:40.220206	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	massturamohd95@gmail.com	---\n- '3'\n	{}	2017-04-26 00:56:02.44782	2017-04-26 01:32:39.413982	2	t
62	email	bd50ef7f-ea2d-4202-8a72-211a272acdb9	$2a$11$XxZJ9KZwZcSkVww/m7908O89YIQEoanP18s0xNCDhYwT/FeGGyV7m	\N	\N	\N	1	2017-04-26 01:01:44.091403	2017-04-26 01:01:44.091403	183.171.67.172	183.171.67.172	\N	\N	\N	\N	\N	\N	\N	eyshabellaa@gmail.com	---\n- '3'\n	{}	2017-04-26 01:00:53.947818	2017-04-26 01:01:44.098474	2	t
34	email	ae6ed23a-d17b-4b3f-b958-eaa167182a12	$2a$11$U2o9HbeWFTtB14LvUfJlXO8IxEJPBzKH2kdfpR0r36cdr1vo11u0.	\N	\N	\N	2	2017-04-26 01:02:34.24865	2017-04-26 00:54:04.359535	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	f33qah@gmail.com	---\n- '3'\n	{}	2017-04-26 00:53:06.955036	2017-04-26 01:48:23.541404	2	t
46	email	4d53b8f5-9ea0-4c7a-adca-2b2b4b91f777	$2a$11$CfN/RoX0MB3QHq.D.P9foe7YEq.StwNWMWh1i.pja5SHRLOOvRgR6	\N	\N	\N	1	2017-04-26 01:02:00.833912	2017-04-26 01:02:00.833912	183.171.75.52	183.171.75.52	\N	\N	\N	\N	\N	\N	\N	safiyahchoco94@gmail.com	---\n- '3'\n	{}	2017-04-26 00:56:23.273914	2017-04-26 01:02:00.841055	2	t
59	email	06299caa-bdfc-4c93-b837-8918c99ffacf	$2a$11$h.z/p0TBnVlkRuOv4JG8yekVbX/qdsvWLmgrbj1cWgbaH.SRipgn2	\N	\N	\N	1	2017-04-26 01:02:13.90697	2017-04-26 01:02:13.90697	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	azzira1607@gmail.com	---\n- '3'\n	{}	2017-04-26 01:00:08.954532	2017-04-26 01:02:13.912076	2	t
51	email	6e17e37c-b1ce-46c6-8391-b8e0f878d8f5	$2a$11$JpZG2e/rpQgDxtl0mCMj4ODWqiZnBh2mYrNuPnI7w6eF/49Ke3Lhu	\N	\N	\N	1	2017-04-26 01:02:42.07911	2017-04-26 01:02:42.07911	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	nazarzakariya@gmail.com	---\n- '3'\n	{}	2017-04-26 00:57:11.040454	2017-04-26 01:02:42.084587	2	t
37	email	da9bdfbb-ec1d-4187-82a8-d51b34eaa6e9	$2a$11$85QH/SK02ccrGdG6yH7dB.ThXM7tO5b6Ex35Aq3WeFx0JFt59hG6C	\N	\N	\N	1	2017-04-26 01:02:48.694232	2017-04-26 01:02:48.694232	183.171.75.52	183.171.75.52	\N	\N	\N	\N	\N	\N	\N	hanahamid95@gmail.com	---\n- '3'\n	{}	2017-04-26 00:53:23.169162	2017-04-26 01:02:48.699266	2	t
53	email	644b44a2-908b-49ab-a0ac-f5be73d243ac	$2a$11$kOKJQYJJ/qu2E3ndLfIUxuYr9OSKR5ZEbedI0i3B5wgjM50rg4FbG	\N	\N	\N	1	2017-04-26 01:03:34.371041	2017-04-26 01:03:34.371041	115.164.177.8	115.164.177.8	\N	\N	\N	\N	\N	\N	\N	mahmudah_1995@yahoo.com	---\n- '3'\n	{}	2017-04-26 00:58:18.740611	2017-04-26 01:03:34.376596	2	t
61	email	c8b111fc-ca45-4920-9011-39b114057484	$2a$11$kG60wqX.OrW26HKPugpnGOKx7vFF5yWzMorxzU2h0KFp/UzB8hKYW	\N	\N	\N	1	2017-04-26 01:06:26.672297	2017-04-26 01:06:26.672297	183.171.77.241	183.171.77.241	\N	\N	\N	\N	\N	\N	\N	syuhadaabdulmunir@gmail.com	---\n- '3'\n	{}	2017-04-26 01:00:37.095659	2017-04-26 01:06:26.677761	2	t
39	email	775bbec4-b614-44a6-88f7-99d2ab4aa6e1	$2a$11$v8ElCORry5RSQ0//rKNPRuvZdUmUNYhPpobvJmFl7RtvrPfBQZC62	\N	\N	\N	1	2017-04-26 00:55:16.371743	2017-04-26 00:55:16.371743	113.210.188.58	113.210.188.58	\N	\N	\N	\N	\N	\N	\N	fariqhemal@gmail.com	---\n- '3'\n	{}	2017-04-26 00:54:50.958835	2017-04-26 00:55:16.377335	2	t
44	email	977ff3f2-f95a-4c73-a5ad-b34e0eb50634	$2a$11$mmgz2D5/LpSTTg2S6D2fCelgNIG2AsIWojMyPV2YRBlyRrnj..Tsa	\N	\N	\N	2	2017-04-26 01:16:04.313255	2017-04-26 00:57:10.830279	123.136.107.183	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	nuradibahsamat@gmail.com	---\n- '3'\n	{}	2017-04-26 00:56:10.657182	2017-04-26 01:16:04.321249	2	t
47	email	9cf26ac2-3673-4bcc-b112-fa13c931d672	$2a$11$rrAWSyxAlnplorLv2.XgzuoDpytLMTbjaIcAz6v8I7.Az.yRTx0qm	\N	\N	\N	2	2017-04-26 01:16:53.938039	2017-04-26 00:58:25.731847	183.171.95.1	183.171.95.1	\N	\N	\N	\N	\N	\N	\N	ummayman9449@gmail.com	---\n- '3'\n	{}	2017-04-26 00:56:28.115823	2017-04-26 01:16:53.944969	2	t
49	email	82940f29-327c-4075-9665-b94b8d507b8c	$2a$11$eQSonW/LqcEWrnNgCcrZi.lfx6q51PjN10rvRboVCC9N406F7s8N2	\N	\N	\N	1	2017-04-26 00:57:34.703266	2017-04-26 00:57:34.703266	123.136.107.153	123.136.107.153	\N	\N	\N	\N	\N	\N	\N	hafizah9401@gmail.com	---\n- '3'\n	{}	2017-04-26 00:56:53.940435	2017-04-26 00:57:57.074763	2	t
45	email	596465e8-7fdd-498f-b7d0-d999e6cbe073	$2a$11$JqGS0fnaQd3rU3BfYCleSewRlIMQ/MjlOmZutlyaAqBPKrvisCW0u	\N	\N	\N	1	2017-04-26 00:58:44.73366	2017-04-26 00:58:44.73366	183.171.75.63	183.171.75.63	\N	\N	\N	\N	\N	\N	\N	azlina.nand.nand@gmail.com	---\n- '3'\n	{}	2017-04-26 00:56:15.72783	2017-04-26 00:58:44.738716	2	t
54	email	e856d8cd-8db7-4a19-b277-8f4783389c5d	$2a$11$oUantTvzyNYxLfhza0wuSOpaWTRWxCv28HtYpUv8ESpsd/Zd0J1T.	\N	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	azziraiium@gmail.com	---\n- '3'\n	{}	2017-04-26 00:58:24.622454	2017-04-26 00:59:11.229738	2	t
55	email	33412404-93c6-428b-820a-4b08a067ba73	$2a$11$l3mZRNAMzbbH.X4Bm5g9seNHU46VZUguuoWjY07x3CQhoT/W34dCq	\N	\N	\N	1	2017-04-26 00:59:49.635733	2017-04-26 00:59:49.635733	183.171.83.14	183.171.83.14	\N	\N	\N	\N	\N	\N	\N	fatmahnabihah@gmail.com	---\n- '3'\n	{}	2017-04-26 00:58:28.534096	2017-04-26 00:59:49.643202	2	t
57	email	6b67de2e-c9b4-4e0a-b8bf-8081f70401c4	$2a$11$0yR2R95yfrt3PZyYbmbLNuI7RmXkLcKlP10m5Y1HurOK396twWfmS	\N	\N	\N	1	2017-04-26 00:59:28.109369	2017-04-26 00:59:28.109369	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	ariffyusni@gmail.com	---\n- '3'\n	{}	2017-04-26 00:58:43.020766	2017-04-26 01:00:04.281278	2	t
58	email	b7c77eae-9879-4f43-9cac-3052a84b9a35	$2a$11$FHQGhSExHWIQqvdh0KchUuCMjpV6ZI6YjIvKEQyDWlDgMeTfulDti	\N	\N	\N	1	2017-04-26 00:59:53.145911	2017-04-26 00:59:53.145911	123.136.107.153	123.136.107.153	\N	\N	\N	\N	\N	\N	\N	khadijahmasdar@gmail.com	---\n- '3'\n	{}	2017-04-26 00:59:02.389126	2017-04-26 01:00:11.316729	2	t
36	email	dee78b98-97e3-49f2-9856-828a92398abc	$2a$11$x2AjFfbV7l6VnO4HkJXsJepQmQxph6JKFiFXxab1a5JL6EveRButO	\N	\N	\N	1	2017-04-26 01:00:59.468189	2017-04-26 01:00:59.468189	183.171.93.23	183.171.93.23	\N	\N	\N	\N	\N	\N	\N	zuraidah0308@gmail.com	---\n- '3'\n	{}	2017-04-26 00:53:16.33628	2017-04-26 01:00:59.473832	2	t
68	email	6fc22a61-2b55-4986-b501-d979416ffbf4	$2a$11$SY9ZY0UMTkBMaEdQSEH7meUPdlCH0FxpR5iTibJIh/TcUTwq8sdiO	\N	\N	\N	1	2017-04-26 01:06:25.162507	2017-04-26 01:06:25.162507	113.210.178.58	113.210.178.58	\N	\N	\N	\N	\N	\N	\N	hanaliza95@gmail.com	---\n- '3'\n	{}	2017-04-26 01:05:43.696429	2017-04-26 01:17:50.281645	2	t
70	email	236e2d40-edb9-40b7-a5ef-cef51d874638	$2a$11$tkfN4z3ZjbLhINcrZG6zT.XzDJPLuhOTBIvlii3npQrzbQIoxe0AG	\N	\N	\N	1	2017-04-26 01:18:17.657195	2017-04-26 01:18:17.657195	183.171.67.172	183.171.67.172	\N	\N	\N	\N	\N	\N	\N	shidanasir94@gmail.com	---\n- '3'\n	{}	2017-04-26 01:11:48.956912	2017-04-26 01:18:17.662907	2	t
67	email	866c7fea-67aa-441d-baa3-a1a0c99c0872	$2a$11$B2BANkzgH7N5HQF2OFCDsupi.fOs0qgLFTnTwwq3X5e4erL9mMsqy	\N	\N	\N	1	2017-04-26 01:04:07.664668	2017-04-26 01:04:07.664668	183.171.75.52	183.171.75.52	\N	\N	\N	\N	\N	\N	\N	syakirien95@gmail.com	---\n- '3'\n	{}	2017-04-26 01:03:32.04486	2017-04-26 01:20:42.488947	2	t
79	email	57737d0c-b041-4b2a-8d11-6c2739bb455d	$2a$11$SZlvS7uERM6IOtnbKGs1UeBV4SdSFNA1oC35xalkDYkkTE.uJ7AG2	\N	\N	\N	1	2017-04-26 09:25:56.730632	2017-04-26 09:25:56.730632	183.171.90.19	183.171.90.19	\N	\N	\N	\N	AIN AZIATUL ARDANI HOLEK	\N	\N	ainholek97@gmail.com	---\n- '3'\n	{}	2017-04-26 09:25:27.279891	2017-04-26 10:02:45.988397	2	t
77	email	2b5593c2-a7e4-4fcc-83cb-47ea5c011c82	$2a$11$9OzSjMjSvV/5.aAnXMz5oehIMEYlH6T6nHW91IRjnyHvbXfh2Vxxq	\N	\N	\N	1	2017-04-26 09:26:15.329637	2017-04-26 09:26:15.329637	115.164.92.168	115.164.92.168	\N	\N	\N	\N	\N	\N	\N	amsyarbaharudin@gmail.com	---\n- '3'\n	{}	2017-04-26 09:25:10.028044	2017-04-26 09:48:01.812428	2	t
27	email	96042398-5f2e-4512-a8c0-938456ace5e6	$2a$11$/Omdp9Oe/7xwpMys6hmO8O4HCacUKgSLeZP8HECkS.Jwvzw0X1ipe	\N	\N	\N	2	2017-04-26 01:01:33.263425	2017-04-26 00:54:17.023797	123.136.107.153	123.136.107.153	\N	\N	\N	\N	\N	\N	\N	nurshakirasaliman@gmail.com	---\n- '3'\n	{}	2017-04-26 00:51:26.969163	2017-04-26 01:01:33.26867	2	t
73	email	22d7ffd0-71d3-414b-8848-1d6550d2bb56	$2a$11$5yTXZrx78edWy/4QQXTXnu.glcDUcfcU7XHkXSBVFsBmQhlxnTRqS	\N	\N	\N	1	2017-04-26 01:22:44.854568	2017-04-26 01:22:44.854568	183.171.82.81	183.171.82.81	\N	\N	\N	\N	\N	\N	\N	liyanarafi@yahoo.com	---\n- '3'\n	{}	2017-04-26 01:22:06.676099	2017-04-26 01:22:44.8598	2	t
76	email	bb4cebf4-11f7-419b-a8a7-8d0811bbc1a9	$2a$11$VctaRq2Yj3B380EMbKPjSO73GfSD14XGscaBxjZFOKh6gFrTtFR5O	\N	\N	\N	1	2017-04-26 09:26:25.517094	2017-04-26 09:26:25.517094	115.164.218.39	115.164.218.39	\N	\N	\N	\N	\N	\N	\N	nurulhannabadrulhisham@gmail.com	---\n- '3'\n	{}	2017-04-26 09:24:57.611312	2017-04-26 09:55:07.307219	2	t
71	email	519a1ebe-de81-4f99-be52-0b50c37ac744	$2a$11$zLnT.Bxd.zUQPx1.JHt2E.SP8iGrzar7KgF6Aevkf.Qg/h.duGd7.	\N	\N	\N	1	2017-04-26 01:30:47.124932	2017-04-26 01:30:47.124932	113.210.178.58	113.210.178.58	\N	\N	\N	\N	\N	\N	\N	nurulbushraahmad@gmail.com	---\n- '3'\n	{}	2017-04-26 01:12:39.788586	2017-04-26 01:31:34.415209	2	t
52	email	73a88356-fc9e-420c-972e-929142693a30	$2a$11$7uxjnnjldH7ebzGK3FoQheFQ44g3oxhL1oPzcq.G0s.gNvNFkQ.ZG	\N	\N	\N	2	2017-04-26 01:02:32.411057	2017-04-26 00:58:56.735787	113.210.121.166	113.210.121.166	\N	\N	\N	\N	Amirul arif	\N	\N	maabmn94@gmail.com	---\n- '3'\n	{}	2017-04-26 00:58:15.090678	2017-04-26 01:32:41.516052	2	t
88	email	c3d5336f-4bb8-4b3a-9584-9716ef1bc192	$2a$11$1eVr6.4GJzZirsAquRn9ZuAAZJsM5YxB9UwrHIb6WXT0d7BZD4MrG	\N	\N	\N	1	2017-04-26 09:26:45.002099	2017-04-26 09:26:45.002099	183.171.76.129	183.171.76.129	\N	\N	\N	\N	\N	\N	\N	aliabasirah97@gmail.com	---\n- '3'\n	{}	2017-04-26 09:26:11.205285	2017-04-26 09:26:45.007749	2	t
83	email	4762e7e8-51bc-469d-bb03-9b1c7f53b8d4	$2a$11$9OURMK7kwZZZxR.ciAFD1en54q5RAdLYJM.q9SIGA46JoFsNEgjqm	\N	\N	\N	1	2017-04-26 09:26:45.984687	2017-04-26 09:26:45.984687	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	thesevere97@gmail.com	---\n- '3'\n	{}	2017-04-26 09:25:42.894644	2017-04-26 09:26:45.990211	2	t
80	email	b53961cd-0efc-468a-a521-e50884d8e22f	$2a$11$oYZwPjxMv5rNiw4pgIuki.1asyRV8xV/2x1IwRljY8886fMYoX22u	\N	\N	\N	1	2017-04-26 09:26:20.724033	2017-04-26 09:26:20.724033	183.171.73.95	183.171.73.95	\N	\N	\N	\N	\N	\N	\N	husnanaimah@gmail.com	---\n- '3'\n	{}	2017-04-26 09:25:28.949369	2017-04-26 09:58:17.684014	2	t
89	email	1f49f487-f1b4-4deb-b4f9-2005a18c5df0	$2a$11$inKvR5sbXGMlDw.HGice9.IxatbMpmsoiiMYi2hNpSO0gkR99oL9W	\N	\N	\N	1	2017-04-26 09:26:55.443245	2017-04-26 09:26:55.443245	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	hilmi97zul@gmail.com	---\n- '3'\n	{}	2017-04-26 09:26:12.953058	2017-04-26 09:26:55.448583	2	t
66	email	f5511f9b-da8e-400b-b852-a1e5ed1d4eb6	$2a$11$uWFev7wDnP3VTRhfLaBt2uqZfxT17EutF5pScoMXblF.3YfmogCj.	\N	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	izzatimokhtar95@gmail.com	---\n- '3'\n	{}	2017-04-26 01:01:45.721454	2017-04-26 01:03:19.845328	2	t
90	email	80c84cc3-861b-4043-a70c-13afd12fcb8a	$2a$11$b.HjwQxJemPAO.HFPVCXfu5SBYajrWvhn/dyBK1t1CLBSAXEyRufW	\N	\N	\N	1	2017-04-26 09:27:02.471792	2017-04-26 09:27:02.471792	115.164.57.169	115.164.57.169	\N	\N	\N	\N	\N	\N	\N	nurulsalihah38@gmail.com	---\n- '3'\n	{}	2017-04-26 09:26:36.026221	2017-04-26 09:27:02.477184	2	t
85	email	4aa7c0b1-457b-495c-afca-6da67ee20ba2	$2a$11$J7SlgWD/WCv6QbSkcJM6NOv5/073a8ImSegVbepOELXSK.hjM2ybO	\N	\N	\N	1	2017-04-26 09:27:06.113768	2017-04-26 09:27:06.113768	183.171.75.196	183.171.75.196	\N	\N	\N	\N	\N	\N	\N	umairahizzati97@gmail.com	---\n- '3'\n	{}	2017-04-26 09:26:07.952196	2017-04-26 09:27:06.119211	2	t
63	email	c4fe0974-121a-45d0-9c5f-82f17f06acad	$2a$11$d7FnAmo9xCMvoD90gfWuUugKgCw4Z3lnYz/Dq4rpod87N32RFAtvW	\N	\N	\N	1	2017-04-26 01:03:38.189008	2017-04-26 01:03:38.189008	115.164.177.8	115.164.177.8	\N	\N	\N	\N	\N	\N	\N	sapiyahahmad93@gmail.com	---\n- '3'\n	{}	2017-04-26 01:01:14.262983	2017-04-26 01:03:38.19414	2	t
65	email	fa5e8333-f38e-4299-a9a2-26fe298fdd2b	$2a$11$DwmuZAFguvjoMenNL0hdFuep8yM7D45iGmO5QHQfGgSNWowRjHRr.	\N	\N	\N	1	2017-04-26 01:03:41.363122	2017-04-26 01:03:41.363122	183.171.72.82	183.171.72.82	\N	\N	\N	\N	\N	\N	\N	wahidaizzaty@gmail.com	---\n- '3'\n	{}	2017-04-26 01:01:36.25323	2017-04-26 01:03:41.368581	2	t
86	email	b5ff5416-1bd1-471d-9edb-fb6ffdf0bb78	$2a$11$5sqqHhUXTtNyXrpt3pa9MuUY01oc4ke/kOlui0nEYbqZXHRrrYDtS	\N	\N	\N	1	2017-04-26 09:27:20.804379	2017-04-26 09:27:20.804379	113.210.101.116	113.210.101.116	\N	\N	\N	\N	\N	\N	\N	maryamk9712@gmail.com	---\n- '3'\n	{}	2017-04-26 09:26:10.171196	2017-04-26 09:27:20.809916	2	t
84	email	be90364c-a664-4a38-994b-b75589115655	$2a$11$nbn3l.F.ss4d.7dbT4/niuKDdE5f8UhZeNQ91FxJjynvs9KU2fLSK	\N	\N	\N	1	2017-04-26 09:28:12.699769	2017-04-26 09:28:12.699769	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	adibahzulkarnain@gmail.com	---\n- '3'\n	{}	2017-04-26 09:26:07.234019	2017-04-26 09:28:12.705011	2	t
64	email	a86c0108-45c2-4ddb-9a89-31353589283b	$2a$11$tsj9knSQg8SD5NbhXo6nd.tMBK/nf1Pr1GMTy7G1/I/C7BdyRilAm	\N	\N	\N	1	2017-04-26 01:05:50.805039	2017-04-26 01:05:50.805039	183.171.64.252	183.171.64.252	\N	\N	\N	\N	\N	\N	\N	mohamadarifiium@gmail.com	---\n- '3'\n	{}	2017-04-26 01:01:18.494352	2017-04-26 01:05:50.810757	2	t
87	email	f1239508-e420-4159-93c8-74dc4214d2ad	$2a$11$7ryySNDy85xpVH55rPJ5u.t2lelhmQlOftnLIWgRXG1xwEIXiAZem	\N	\N	\N	2	2017-04-26 09:30:39.277347	2017-04-26 09:26:33.791533	115.164.182.118	115.164.182.118	\N	\N	\N	\N	\N	\N	\N	akimbek97@yahoo.com	---\n- '3'\n	{}	2017-04-26 09:26:10.719921	2017-04-26 09:30:39.282879	2	t
78	email	b926c89a-5f82-45d3-84db-7dca4b6e21c9	$2a$11$LBLGAgKjsMQb7ch9hfYbcuAk3iRIgG5V1vGVP9ul6iMQicnb5qIsq	\N	\N	\N	2	2017-04-26 09:31:09.024835	2017-04-26 09:26:29.364802	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	ainzly13@gmail.com	---\n- '3'\n	{}	2017-04-26 09:25:17.049282	2017-04-26 09:31:09.03016	2	t
69	email	bd1376ab-cbb0-4684-95a0-08e02ef49a5c	$2a$11$N5V6Laf7z6YCamifuKqwnOJWwsDw73F/6bw2wmPJd1ZciTPrGsiSW	\N	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	nurulbushraahmad@gmai.com	---\n- '3'\n	{}	2017-04-26 01:10:01.945898	2017-04-26 01:16:25.561999	2	t
82	email	2224cf6f-8a51-48b4-a539-e97d01e76156	$2a$11$K5oHjlQqYinOYM2M1GcRtOm6TkPK0/dVpPGTU5p9vUyQje6oe.0ee	\N	\N	\N	1	2017-04-26 09:26:19.308172	2017-04-26 09:26:19.308172	183.171.89.10	183.171.89.10	\N	\N	\N	\N	\N	\N	\N	hamrarazali@gmail.com	---\n- '3'\n	{}	2017-04-26 09:25:31.205124	2017-04-26 09:26:19.313715	2	t
81	email	e7867e8d-8c94-40ab-a785-18baf43caba1	$2a$11$cfo89UbSwqzbUX8lXkDG2eN3Cv3JH.Hw6znJf7IAGJcthOoqSN50K	\N	\N	\N	1	2017-04-26 09:26:21.455797	2017-04-26 09:26:21.455797	113.210.118.82	113.210.118.82	\N	\N	\N	\N	\N	\N	\N	ninafauzan97@gmail.com	---\n- '3'\n	{}	2017-04-26 09:25:30.512719	2017-04-26 09:26:21.461542	2	t
75	email	ffdb260a-8ab9-4c12-957a-9704456a0625	$2a$11$nWWO6275C0gKOdsm3hD1SuZoygITF7iybtDjRJTvUCN76OU4tsXw2	\N	\N	\N	1	2017-04-26 09:26:21.894426	2017-04-26 09:26:21.894426	183.171.89.238	183.171.89.238	\N	\N	\N	\N	\N	\N	\N	nursalwamz@gmail.com	---\n- '3'\n	{}	2017-04-26 09:24:51.709895	2017-04-26 09:26:21.900032	2	t
74	email	87386525-8819-48f2-8a9b-2dda8870d2f9	$2a$11$r59eCok9Zw18dYfaPdGiDe8kXWVOTXuyXADQWxsshKXKb.VC2IKkS	\N	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	onefaithfillahi@gmail.com	---\n- '3'\n	{}	2017-04-26 01:59:14.245718	2017-04-26 09:26:32.476001	2	t
111	email	7d195692-df05-404b-b43a-d013e652bce0	$2a$11$LdcZOuoJ5r6iTLZciQnyduxMrjFqMTw635LadlDMOrWGUEz7mCcjq	\N	\N	\N	1	2017-04-26 09:31:11.609537	2017-04-26 09:31:11.609537	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	mytwelvesstars@gmail.com	---\n- '3'\n	{}	2017-04-26 09:30:45.684866	2017-04-26 10:09:04.701361	2	t
108	email	d1caec92-a12c-4635-bd62-55684e35dbc0	$2a$11$H28LI7ormW2iEaEvtWJ1/.vz5DZz.wTQhVRe9ovbH3/FsFDlX7YCa	\N	\N	\N	3	2017-05-15 06:06:20.846428	2017-05-14 17:55:43.765255	183.78.45.154	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	hananiwyda@gmail.com	---\n- '3'\n	{}	2017-04-26 09:30:34.992438	2017-05-15 06:06:20.852153	2	t
91	email	5daa2988-f913-4cf5-8e7f-79a5bfbac84d	$2a$11$8.rlk0P1U601GG7/MOV3oO.fJlFYImMMmFBm/LuIllNNxqOxXU2jq	\N	\N	\N	1	2017-04-26 09:28:45.221875	2017-04-26 09:28:45.221875	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	mnurulawatif@gmail.com	---\n- '3'\n	{}	2017-04-26 09:28:10.2821	2017-04-26 09:28:45.228321	2	t
105	email	e8fea565-9049-48c7-b0b6-6c683d9b1aba	$2a$11$hY8E8ELUd8dFE/8QnxSQV.6eLsvF7Q6imNSLfADN4n170AJM2SSiu	\N	\N	\N	1	2017-04-26 09:30:44.481074	2017-04-26 09:30:44.481074	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	jsylover97@gmail.com	---\n- '3'\n	{}	2017-04-26 09:30:09.828457	2017-04-26 10:10:30.396818	2	t
96	email	63b52ad9-4267-46c6-9311-a2acceb4e6c5	$2a$11$RfpxZOak9vrOd1cU3JRdk.bNirGEkFIxzRl12Ug1Qt6VkvNGok7DO	\N	\N	\N	1	2017-04-26 09:30:03.923824	2017-04-26 09:30:03.923824	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	aqilahtaib97@gmail.com	---\n- '3'\n	{}	2017-04-26 09:28:57.803888	2017-04-26 10:21:20.589896	2	t
104	email	f80f25fd-a70a-4854-a095-53232a4c5c0a	$2a$11$irs70ueAfjvNIV0zdnefRuhHmpsDZn9mjlN.xApCDSi5YQtmK6.i6	\N	\N	\N	5	2017-05-14 09:35:55.570374	2017-04-26 22:38:28.046364	210.48.222.13	210.48.222.13	\N	\N	\N	\N	Fikri Mahmuda	\N	\N	fikrimahmuda71@gmail.com	---\n- '3'\n	{}	2017-04-26 09:30:08.031658	2017-05-14 09:35:55.577092	2	t
130	email	6100346c-d0bd-44eb-ae19-ae28b1f291fa	$2a$11$hZMvwL.RLzL.FQFCoO8NjOGjNwtj2gM2JMUf3.Zk/pMt38asEY/k.	\N	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	sitisarahali0@gmail.com	---\n- '3'\n	{}	2017-04-26 14:59:55.508627	2017-04-26 15:00:26.960296	2	f
113	email	92be93fb-f35c-47e5-9dae-7e8e34abc74f	$2a$11$7edxKGNVydU8Wx4Pgn2fH.gBLKl26HXg2ghpRyUoRV0A9/eUCxNay	\N	\N	\N	1	2017-04-26 09:32:06.882409	2017-04-26 09:32:06.882409	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	munziralladani@gmail.com	---\n- '3'\n	{}	2017-04-26 09:31:42.949351	2017-04-26 09:32:06.887618	2	t
115	email	f5f69958-3b15-4bf1-8fe9-e985961b7ac8	$2a$11$uToBNh8AX5q0lJS0.6QU.OzoEV4rGza.l/OgXfCDlwn3yqpKgXHmq	\N	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	lyanafadzil97@gmail.con	---\n- '3'\n	{}	2017-04-26 09:32:11.87814	2017-04-26 09:32:30.052771	2	t
112	email	d5281060-b3b4-4fb0-b7ea-c95f321beb52	$2a$11$WpXerP7S6b1OaCLcJJhF9.1jiC6YDLEWOo49HKMZ9IjuUr/wyGVq6	\N	\N	\N	1	2017-04-26 09:32:52.015957	2017-04-26 09:32:52.015957	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	zatyismail81@yahoo.com	---\n- '3'\n	{}	2017-04-26 09:31:20.790983	2017-04-26 09:32:52.02161	2	t
116	email	26ef8e96-46f2-4033-abec-567de1669a74	$2a$11$Kvqc4sIq3GYbI/vGo6QeneGfJ2wo8uhKGQiCTWtm15Esar2dN1w7O	\N	\N	\N	1	2017-04-26 09:33:07.353711	2017-04-26 09:33:07.353711	42.153.62.200	42.153.62.200	\N	\N	\N	\N	\N	\N	\N	munirahmujahid1@gmail.com	---\n- '3'\n	{}	2017-04-26 09:32:49.15772	2017-04-26 09:33:07.359141	2	t
114	email	509c7577-26d9-427c-88db-71b963e5ad2e	$2a$11$hjtzLYy6WFrQDhDCA/TgRe05.gSFPvIuUMIWLDqQwfFa2RQCKx0Oi	\N	\N	\N	1	2017-04-26 09:33:51.24947	2017-04-26 09:33:51.24947	183.171.67.146	183.171.67.146	\N	\N	\N	\N	\N	\N	\N	anisamira05@gmail.com	---\n- '3'\n	{}	2017-04-26 09:31:43.424129	2017-04-26 09:33:51.25529	2	t
97	email	4a8cf30c-4def-4961-b92e-d4285287f4ed	$2a$11$4rSuwHmKzV4gsbLIOgqJWOVmbC5VHVTXEptjqqVNXxMdY7u7s9iOu	\N	\N	\N	1	2017-04-26 09:29:35.676778	2017-04-26 09:29:35.676778	115.164.217.39	115.164.217.39	\N	\N	\N	\N	\N	\N	\N	alisaabdullah97@gmail.com	---\n- '3'\n	{}	2017-04-26 09:29:03.943279	2017-04-26 09:29:35.682351	2	t
103	email	3ba525d5-4f5b-40bd-9dac-46659a99f7f1	$2a$11$fAhZPnMkAAmgTy7oUHYLSuMZTYxAMaF.rmI/k/1IyitqOagsZCCAC	\N	\N	\N	2	2017-04-26 09:34:08.156509	2017-04-26 09:30:45.855244	42.153.62.200	115.164.218.39	\N	\N	\N	\N	\N	\N	\N	aisyshh@gmail.com	---\n- '3'\n	{}	2017-04-26 09:30:01.49778	2017-04-26 09:34:08.162409	2	t
100	email	0b94aa47-6e48-4d68-ba67-cd1db8b92ac9	$2a$11$52sLHcimGw7ps7Ue0WwlX.7Xwp1yX3mFMwiybuk4v7CirTKhRZnoi	\N	\N	\N	1	2017-04-26 09:29:55.060154	2017-04-26 09:29:55.060154	183.171.91.192	183.171.91.192	\N	\N	\N	\N	\N	\N	\N	widad.aljundiyah@gmail.com	---\n- '3'\n	{}	2017-04-26 09:29:15.735349	2017-04-26 09:29:55.066355	2	t
95	email	31b4a039-bffa-434b-9830-7ba4748b54b1	$2a$11$pc55VFtk0HVqKOcUyH7XPelskhy6GU..mzTUF.MFK36iMj2mHW5hG	\N	\N	\N	1	2017-04-26 09:29:59.958828	2017-04-26 09:29:59.958828	183.171.73.217	183.171.73.217	\N	\N	\N	\N	\N	\N	\N	umaierahbalian@gmail.com	---\n- '3'\n	{}	2017-04-26 09:28:51.782498	2017-04-26 09:29:59.964296	2	t
92	email	9c904f42-337f-4ff9-a471-20302cf8dd07	$2a$11$.DYIzvvvhc42cb25UzbpqOizkb1.LBa2R72q3.d2Cp4GBzP4kt5qu	\N	\N	\N	1	2017-04-26 09:30:01.677177	2017-04-26 09:30:01.677177	183.171.76.46	183.171.76.46	\N	\N	\N	\N	\N	\N	\N	yumiehanafi@gmail.com	---\n- '3'\n	{}	2017-04-26 09:28:32.175243	2017-04-26 09:30:01.684052	2	t
101	email	e87fceab-5626-4ba1-b568-f4e7c3549e17	$2a$11$hlO0ptmK4jZg/N9KKEb1F../v0bpkl4V5lACMbsDC9Y1SjL5zLk3q	\N	\N	\N	1	2017-04-26 09:30:04.0903	2017-04-26 09:30:04.0903	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	hanani9310@gmail.com	---\n- '3'\n	{}	2017-04-26 09:29:37.63034	2017-04-26 09:30:04.096315	2	t
121	email	c82fd6d9-7ca9-4f16-9454-0cc2b60ab1b9	$2a$11$ebyUy3HPE01h2s9THh623enX/CxpqXgWbS9sZzH2Z4hiT6N5q8jCC	\N	\N	\N	1	2017-04-26 09:35:01.42429	2017-04-26 09:35:01.42429	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	husnaizzah97@gmail.com	---\n- '3'\n	{}	2017-04-26 09:33:53.648551	2017-04-26 09:35:01.429209	2	t
117	email	c512e12f-2601-4d0d-bca5-d468eb46bfe6	$2a$11$n38YE1DgLhQ2rC0xQiKPI.4z/XpyUrjt9j7OjrGijcZV28CDt/7Z6	\N	\N	\N	1	2017-04-26 09:36:47.507265	2017-04-26 09:36:47.507265	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	lyanafadzil97@gmail.com	---\n- '3'\n	{}	2017-04-26 09:33:10.562365	2017-04-26 09:36:47.512146	2	t
119	email	b8409114-99c4-4d9b-b28a-7bc37b6fa12a	$2a$11$LE8/pXeXJCKapd3r/Fe/oOuxGsRz7.grt4INjRmdfbc.tV9rlopxe	\N	\N	\N	1	2017-04-26 09:37:11.264483	2017-04-26 09:37:11.264483	183.171.67.185	183.171.67.185	\N	\N	\N	\N	\N	\N	\N	pencintaillahi97@gmail.com	---\n- '3'\n	{}	2017-04-26 09:33:31.952808	2017-04-26 09:37:11.271561	2	t
99	email	8741564b-2190-4707-8826-ba6f4ce16c54	$2a$11$AakID0wxudmB..36JOab..4J2RCb6M0lj5cQN8.OaOj5pptwFvzFa	\N	\N	\N	2	2017-04-26 09:45:49.16391	2017-04-26 09:30:05.904676	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	nurjannahcheseman@gmail.com	---\n- '3'\n	{}	2017-04-26 09:29:13.727369	2017-04-26 09:45:49.169353	2	t
120	email	58e721e8-baf8-4138-b253-124ba064a64b	$2a$11$z4esJbZh5JPuW5Qh6vkmqOJEHnG1LimdQk4PNFQ2U4/K69vhxftLa	\N	\N	\N	1	2017-04-26 09:35:00.009426	2017-04-26 09:35:00.009426	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	zaharahzakaria21@gmail.com	---\n- '3'\n	{}	2017-04-26 09:33:43.352695	2017-04-26 09:47:38.401562	2	t
98	email	590dee99-a7c9-46c5-87d0-34dbd3f29e48	$2a$11$NMDFxyQ1gwVoiTSlrriN/OuGmo7tMOvNb7f1g7DflrdrYBbPqIK8G	\N	\N	\N	1	2017-04-26 09:30:24.350163	2017-04-26 09:30:24.350163	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	atin_shuha94@yahoo.com	---\n- '3'\n	{}	2017-04-26 09:29:12.861591	2017-04-26 09:30:24.355694	2	t
102	email	c95b11a8-623f-4d3c-967b-15dc006c17b7	$2a$11$OJlvqfN1mVdMU8GqE2DCS.L70FivvmSuDmAezv9IiHcI.oFoUwwxu	\N	\N	\N	1	2017-04-26 09:30:30.612554	2017-04-26 09:30:30.612554	183.171.95.158	183.171.95.158	\N	\N	\N	\N	\N	\N	\N	yananasihah97@gmail.com	---\n- '3'\n	{}	2017-04-26 09:30:00.131322	2017-04-26 09:30:30.618113	2	t
106	email	fc382354-fc76-4834-8959-3e93772660ad	$2a$11$rsZR4AAziQIeKEZEQLinYu8B10NZkbBqnvrU3NWX0bXu7mhB7m1XK	\N	\N	\N	1	2017-04-26 09:30:52.593931	2017-04-26 09:30:52.593931	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	aunijamar@gmail.com	---\n- '3'\n	{}	2017-04-26 09:30:31.834648	2017-04-26 09:58:12.730399	2	t
93	email	6a0d8073-1108-49f1-8887-e164db329161	$2a$11$2V77RGzc.1y5vuRqh1EsQebquw03OrPFwXRA/gT8upnV7qliiQTpm	\N	\N	\N	1	2017-04-26 09:30:55.333722	2017-04-26 09:30:55.333722	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	mashshiro97@gmail.com	---\n- '3'\n	{}	2017-04-26 09:28:48.465402	2017-04-26 09:30:55.339178	2	t
107	email	4fa1b4f4-f37b-4560-b713-a83d16dc9936	$2a$11$uz52KgM/6/14E.hg3fRuGe0M69AQxyzCq/Svittmqj3mMOopAhBOC	\N	\N	\N	1	2017-04-26 09:31:04.708865	2017-04-26 09:31:04.708865	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	fatinfathiah22@gmail.com	---\n- '3'\n	{}	2017-04-26 09:30:32.017543	2017-04-26 09:31:04.714099	2	t
109	email	5f673d27-1b13-47c2-861b-7d06846f2375	$2a$11$8sKVC6pkGAuHa2RITSS9OuzihmAMkF8/Il3Auux6tMHJglsxKsVAa	\N	\N	\N	1	2017-04-26 09:31:09.428335	2017-04-26 09:31:09.428335	113.210.61.96	113.210.61.96	\N	\N	\N	\N	\N	\N	\N	shs3197@gmail.com	---\n- '3'\n	{}	2017-04-26 09:30:43.634642	2017-04-26 09:31:09.433647	2	t
110	email	3a3633f6-1b14-4220-ab6c-2f2116577b76	$2a$11$MtRYOARBtSKhLgZHF8dOtucyBuGhO19ewfzayS/3ChGGlE738Q73C	\N	\N	\N	1	2017-04-26 09:31:13.15681	2017-04-26 09:31:13.15681	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	hudawahida97@gmail.com	---\n- '3'\n	{}	2017-04-26 09:30:45.500584	2017-04-26 09:31:13.163593	2	t
2	email	07b7d2fe-963e-4630-84e2-1aeb09423fc7	$2a$11$7DQvVr/6DIsbSMJfEoCMe.bQ3V4e8RwWZdgwmkxqz2v3fKDLoZt8q	\N	\N	\N	5	2017-05-11 09:46:11.295314	2017-05-07 10:17:59.918568	42.153.39.255	42.153.50.168	\N	\N	\N	\N	\N	\N	\N	zamrigani91@gmail.com	---\n- '3'\n	{"QKSyEBx_5xjTDPR5rcLiKA":{"token":"$2a$10$z1OFU.1jxtQfZqb0hsDpkeIHiaTtxWURqxM2n0tT50xsqotIJaymC","expiry":1495705571}}	2017-04-25 13:41:31.144201	2017-05-15 10:03:29.947891	2	t
128	email	93ba9b65-d7e5-4573-a076-eb00f92d2143	$2a$11$QgHJr7w21Ab2ALRwAABPzut4nDmTH8PEZw20zOk5YIGvH.Mhfby8q	\N	\N	\N	1	2017-04-26 10:27:26.147593	2017-04-26 10:27:26.147593	113.210.101.116	113.210.101.116	\N	\N	\N	\N	\N	\N	\N	nurulsirly@gmail.com	---\n- '3'\n	{}	2017-04-26 09:57:49.048744	2017-04-26 10:37:17.618679	2	t
122	email	d8e1ef6a-872c-4525-aa2e-17d9f97eee08	$2a$11$dSilPWFYQ5wCu8ER11oEf.sxFmAbpqXPBILZT5AwaPlnkpszZi4h2	\N	\N	\N	1	2017-04-26 09:34:48.476438	2017-04-26 09:34:48.476438	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	ibnuramli96@gmail.com	---\n- '3'\n	{}	2017-04-26 09:33:55.094919	2017-04-26 09:34:48.482063	2	t
35	email	ed20242b-4025-4dbc-9d4e-e12488c13548	$2a$11$65Qu7i0XOXOoD3Pn9pyKgeq3YpF6rl1c7h562Q8.4uIDnbKxQ1k0e	\N	\N	\N	3	2017-04-27 01:39:06.648019	2017-04-26 00:58:15.856691	183.171.88.186	183.171.83.85	\N	\N	\N	\N	\N	\N	\N	athiyanadhirah@gmail.com	---\n- '3'\n	{}	2017-04-26 00:53:07.133891	2017-04-27 01:39:06.653964	2	t
131	email	fdf430e0-99ef-4b64-9285-1a0bf4b0899b	$2a$11$OcFzjjPloiS2PJQULMZHI.pnm8z2rbH47y23cup6EbAGyYjf8Xgbu	\N	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	sitisarahali07@gmail.com	---\n- '3'\n	{}	2017-04-26 15:01:22.312794	2017-04-27 12:58:27.756165	2	f
123	email	6e08b596-16de-4af1-b0c5-cfa59d8a1476	$2a$11$SXkTj4tNhpXzjR8bdbNIre6WIFzQkgIltdG6MsxEpytTyhk.2OhM6	\N	\N	\N	1	2017-04-26 09:35:11.33776	2017-04-26 09:35:11.33776	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	husainifarhan03@gmail.com	---\n- '3'\n	{}	2017-04-26 09:34:35.387865	2017-04-26 09:35:11.342755	2	t
124	email	17cdacbf-add1-49bd-a607-1cb9d0227e8c	$2a$11$d7k7tgFZZdsWSFCunUA2revXumHcH6MJrQkXdDsc6XC72cCGVs6AO	\N	\N	\N	1	2017-04-26 09:35:15.955235	2017-04-26 09:35:15.955235	42.153.62.200	42.153.62.200	\N	\N	\N	\N	\N	\N	\N	tihaniizani47@gmail.com	---\n- '3'\n	{}	2017-04-26 09:34:52.543061	2017-04-26 09:35:15.960017	2	t
118	email	43513a76-dcf0-4cd7-ad0d-2263c2d62704	$2a$11$zJFoU5duqDGMbBTujitwhOyZEWKSIJYjsjaFEI5DzdUzi7VWQ/P3e	\N	\N	\N	1	2017-04-26 09:35:30.661142	2017-04-26 09:35:30.661142	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	niksyira1997@gmail.com	---\n- '3'\n	{}	2017-04-26 09:33:15.862499	2017-04-26 09:35:30.666837	2	t
133	email	5147951c-739f-46dc-a310-5f3af93ff01e	$2a$11$mzgk1wgIWvNd5geBjN6VFesvu9/DSkVMSswaC0MZ1UbrEZbxJVCKG	\N	\N	\N	1	2017-04-28 04:10:27.210221	2017-04-28 04:10:27.210221	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	azyan@gmail.com	---\n- '3'\n	{}	2017-04-28 04:08:36.905918	2017-04-28 04:10:27.217784	2	t
132	email	de91f9a5-db1a-48e7-a0d7-7bd363750abd	$2a$11$PYRLSSSsOmYr33LPdkocmePh5v1f7fck2w2OEx9Id4fNsWGQDZUpy	\N	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	fityanummah93@gmail.com	---\n- '3'\n	{}	2017-04-27 00:18:21.878157	2017-04-28 05:33:00.211319	2	f
125	email	a05e6039-378c-4175-bef1-c9be43bc4726	$2a$11$Lr7cEnglARU3iUX6Kk3eluOSiBVM8R6eJfgQzpWBu2Jd4mK/.2XWm	\N	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	mujahidahawesome_97@yahoo.com	---\n- '3'\n	{}	2017-04-26 09:35:52.830964	2017-04-26 09:37:35.223829	2	t
126	email	a269c649-762c-4521-a9bf-5c0acc990218	$2a$11$cRwDGRLFOkypkLTaxpvzI.ja8QrdAZOR8Kba/fyARu9w4me.LAz56	\N	\N	\N	1	2017-04-26 09:39:06.224386	2017-04-26 09:39:06.224386	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	ainannajihah@gmail.com	---\n- '3'\n	{}	2017-04-26 09:36:06.715654	2017-04-26 09:39:06.229806	2	t
127	email	10303250-8683-4959-aef8-ac4cc1360445	$2a$11$0PlGNayScV6v8lVVPk4Edumq.l1SoIVOO9JZzru9PF2RLrtJvl73e	\N	\N	\N	1	2017-04-26 09:40:25.937568	2017-04-26 09:40:25.937568	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	nursyahirahmohdzain@gmail.com	---\n- '3'\n	{}	2017-04-26 09:39:06.687319	2017-04-26 09:40:25.942664	2	t
94	email	04f5a792-c048-416a-9626-1a579d9f540c	$2a$11$RoBs4EUsxZ8iGV.oeE8peO8llTYb6MNX1/cXRzmncrmOK3kHj58km	\N	\N	\N	2	2017-04-26 09:39:47.536005	2017-04-26 09:30:00.307025	210.48.222.13	210.48.222.13	\N	\N	\N	\N	\N	\N	\N	hidayahrustam3497@gmail.com	---\n- '3'\n	{}	2017-04-26 09:28:48.81363	2017-04-26 09:44:10.368735	2	t
134	email	7e5550b5-e8c6-4404-9006-db248a54c13b	$2a$11$jzREghKaxu790y.t1If.quwWeyrXruw4BdkttQqyzupAsf1Dh6ZOC	\N	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	abi.dzar@namlite.com	\N	{}	2017-05-04 08:47:44.16764	2017-05-07 10:54:52.029051	\N	f
1	email	79b69bee-420b-4c1a-aa6e-c9fcba6e0248	$2a$11$S0G//5XbRJiToyfNjaYz5e7kgIFZM4gQ6QlnY0W6bcXKlOZPrrY96	\N	\N	\N	62	2017-05-15 12:38:26.013036	2017-05-15 12:09:14.910409	37.56.23.195	202.188.36.108	\N	\N	\N	\N	Admin	\N	\N	admin@e-diwan.com	---\n- '1'\n	{"mC4bFlZGQlbTNtWGNCbW0Q":{"token":"$2a$10$o3i.1e6ONZTWR8.49w740uXBTBe1Bmx75mjffTrMMkKzT4h6H8eDa","expiry":1496015137},"PwVUl5Bi6x1-uXxMSZ5Yyg":{"token":"$2a$10$1tvbP5cGEx2M65BD/OwFF.IcdpaW4yc85jU5YEJkcUptuaYi2xWWS","expiry":1496017572},"DpB1gOaGmbQR46VT5uFNhQ":{"token":"$2a$10$./NK1oUQh2hcdsmAB26XNemjB9usDkicBtKPWieFi1b6TOFbmy.3S","expiry":1496026849},"6H9rcEPjaG_uHlAxKlzXug":{"token":"$2a$10$dLTURqMeqj3m5DHrKIjnYeFN.YjPcZ6EHVdL/dSm8rVybUMzDDqey","expiry":1496050742},"NDL7l8b09S3GkS8UU-0KXQ":{"token":"$2a$10$ndznVMkVRIb3uK6g/jxRkuHyFS09g4LKuwuVUWi2lhy9nONE6Dky2","expiry":1496051548},"VY1YEAdIQS9fvoISIwmfAQ":{"token":"$2a$10$o5BmwgtTKGGOFZDOPu6Rqe2N7n8l6Awu.pCY/nosGWU8Dvshn4f6.","expiry":1496056205},"tDl8leLV0_nyfLjUouwTJA":{"token":"$2a$10$b7GOF6QlkqPeUOr/Wshueex.HUXud2s2CVwks/Kdlb5UnEQlu4TUC","expiry":1496056301},"MWb3EDixOFPZ5uo7ePPJWw":{"token":"$2a$10$Di3ux.AYtvKFKQj4TBd7PendHQyxHcMtYdtiy2xc7I0KWA3cO63US","expiry":1496057541},"PYdcZpVDlTfE-G5CfwJz9g":{"token":"$2a$10$fmHBF0SFfaQ.396PB42.LO9ugSqgS4ewGlAAO3Y1T6QY7eTF92cmG","expiry":1496058129},"bHO2D5Pgg17dL_HbgypiEw":{"token":"$2a$10$Z4kcCJ0NkBAJpumL9u2w7eJ/8Dij5O7BXxXZ/xB2ylq/xxHpTTSPa","expiry":1496059754}}	2017-03-30 06:46:00.79767	2017-05-15 12:38:26.023752	\N	t
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 135, true);


--
-- Data for Name: word_references; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY word_references (id, reference_type, word, word_indexes, media, stanza_id, poem_id, line_id, description, created_at, updated_at, word_search, description_search) FROM stdin;
2	3	نَبْـكِ	---\n- '01'\n	\N	1	1	1	 من البكاء، يقال: بكى الرجل يبكي بكاءً وبكىً بالمد والقصر	2017-03-30 07:55:23.048006	2017-05-15 06:05:15.94108	نبك	\N
4	3	بِسِقْطِ	---\n- '06'\n	\N	1	1	1	السقط: المنقطع	2017-03-30 07:57:23.329872	2017-05-15 06:05:15.948299	بسقط	\N
7	3	الدَّخُـولِ فَحَومَلِ	---\n- 09\n- '10'\n	\N	1	1	1	 موضعان ما بين إِمَّرَةَ إلى أسود العين وهو جبل أي: بين أهل الدخول فأهل حومل	2017-03-30 08:09:04.295297	2017-05-15 06:05:15.958665	الدخول فحومل	\N
8	3	سِقْطِ اللِّـوَى	---\n- '06'\n- '07'\n	\N	1	1	1	 اسم موضع ويجوز ضم السين وفتحها وكسرها	2017-03-30 08:09:45.544027	2017-05-15 06:05:15.964048	سقط اللوى	\N
14	3	أَرْخَى	---\n- '03'\n	\N	1	1	2	سدل أو أرسل السّترَ وغيرَه أي: لم يضمه إلى بعضه	2017-03-30 09:43:30.399135	2017-05-15 06:05:15.969278	أرخى	\N
44	3	سَـاطِعُ	---\n- '10'\n	\N	3	3	14	مشتعلٌ منيرٌ مُضِيئٌ.	2017-03-30 10:31:34.111402	2017-05-15 06:05:16.13021	ساطع	\N
45	3	رَمَـاداً	---\n- '06'\n	\N	3	3	14	رماد النار هو التراب الأسود الذي تخلفه النار.	2017-03-30 10:36:27.051051	2017-05-15 06:05:16.135137	رمادا	\N
46	3	البِـرُّ	---\n- '01'\n	\N	3	3	15	هو الإحسان في كلّ شيئ.	2017-03-30 10:38:21.446196	2017-05-15 06:05:16.143208	البر	\N
47	3	مُضْمَـرَاتٌ	---\n- '03'\n	\N	3	3	15	جمع مفرده مُضْمَرٌ، والمضمر ما استتر في القلب وأَكَنَّهُ الضميرُ.	2017-03-30 10:38:50.839007	2017-05-15 06:05:16.151139	مضمرات	\N
48	3	التُّقَى	---\n- '05'\n	\N	3	3	15	أي: التقوى.	2017-03-30 10:39:35.834873	2017-05-15 06:05:16.158247	التقى	\N
51	3	وَدَائِـعُ	---\n- '10'\n	\N	3	3	15	 جمع مفرده وديعة، والوديعة على وزن فعيلة بمعنى مفعول أي مودعوة وهي الأمانة المستودعة للحفاظ عليها.	2017-03-30 10:42:52.861509	2017-05-15 06:05:16.170957	ودائع	\N
52	3	سُـدُولَهُ	---\n- '04'\n	\N	1	1	2	السُّدول جمعٌ مفردُه سُدْلٌ، وهي الستور، والواحد سُتْرٌ	2017-03-30 13:02:44.338561	2017-05-15 06:05:16.176136	سدوله	\N
53	3	الهُـمُـومِ	---\n- '07'\n	\N	1	1	2	 جمعٌ مفرده هَمٌّ، وهو ما يسبب الحزن للإنسان ويحمِّلُه الثقل، ويأتي الهَمُّ بمعنى الهمة	2017-03-30 13:03:22.612522	2017-05-15 06:05:16.184264	الهموم	\N
54	3	لِيَبْـتَلِي	---\n- 08\n	\N	1	1	2	الابتلاء الاختبار والامتحان	2017-03-30 13:03:56.055611	2017-05-15 06:05:16.191322	ليبتلي	\N
55	4	وَلَيـلٍ	---\n- '00'\n	\N	1	1	2	[الواو] واو ربّ، و [لَيلٍ] مجرور بِرُبَّ المقدرة وعلامة جرّه الكسرة، والأصل : [وَرُبَّ لَيلٍ]	2017-03-30 13:06:51.135714	2017-05-15 06:05:16.19773	وليل	\N
56	4	كَمَـوجِ	---\n- '01'\n	\N	1	1	2	 [الكاف] حرف جرّ، و [مَوجِ] مجرور بالكسرة، وهو مضاف	2017-03-30 13:07:18.552088	2017-05-15 06:05:16.20758	كموج	\N
57	4	البَحْرِ	---\n- '02'\n	\N	1	1	2	 مضاف إليه مجرور بالكسرة، والجار والمجرور في محل جر صفة لِـ [لَيلٍ] أي: رُبَّ لَيلٍ كَائِنٍ كَمَوجِ البَحْرِ	2017-03-30 13:07:47.306814	2017-05-15 06:05:16.218454	البحر	\N
58	4	أَرْخَى	---\n- '03'\n	\N	1	1	2	فعل ماضٍ مبني على الفتح المقدر على الألف، والفاعل ضمير مستتر تقديره [هُوَ] يعود إلى الليل.	2017-03-30 13:08:09.376383	2017-05-15 06:05:16.22966	أرخى	\N
59	4	سُـدُولَهُ	---\n- '04'\n	\N	1	1	2	مفعول به منصوب بالفتحة وهو مضاف، والضمير [الهاء] مضاف إليه مبني على الضم في محل جر. والجملة من الفعل وفاعله ومفعوله في محل خفض صفة لِـ [لَيل].	2017-03-30 13:08:32.886773	2017-05-15 06:05:16.243564	سدوله	\N
60	4	عَـلَـيَّ	---\n- '05'\n	\N	1	1	2	[على] حرف جرّ، و [الياء] ياء المتكلم مبنية على الفتح في محل جر، والجار والمجرور متعلقان بالفعل [أَرْخَى].	2017-03-30 13:08:54.938535	2017-05-15 06:05:16.256113	علي	\N
61	4	بِأَنْـوَاعِ	---\n- '06'\n	\N	1	1	2	[الباء] حرف جرّ، و [أَنْوَاعِ] مجرور بالكسرة، والجار والمجرور متعلقان بالفعل [أَرْخَى]، و [أَنْوَاعِ] مضاف.	2017-03-30 13:09:13.633915	2017-05-15 06:05:16.267025	بأنواع	\N
62	4	الهُـمُـومِ	---\n- '07'\n	\N	1	1	2	 مضاف إليه مجرور بالكسرة.	2017-03-30 13:10:04.475333	2017-05-15 06:05:16.28253	الهموم	\N
64	3	تَمَطَّى	---\n- '03'\n	\N	1	1	3	تمدد، يقال: تمطّى الرجل إذا تمدد أي: مَدَّ مَطَاهُ أي: ظهره. فيكون التمطي مدّ الظهر.	2017-03-30 13:11:13.378536	2017-05-15 06:05:16.296704	تمطى	\N
65	3	بِصُلْبِـهِ	---\n- '04'\n	\N	1	1	3	الصُّلْبُ هو العظم من الكتف إلى العَجْب ويراد به الظَّهْرُ والوَسَطُ، ويروى: (بِجَوزِه) أي بوسطه.	2017-03-30 13:11:39.103595	2017-05-15 06:05:16.301708	بصلبه	\N
66	3	وَأَرْدَفَ	---\n- '05'\n	\N	1	1	3	الإرداف الاتباع أو الرجوع. 	2017-03-30 13:11:57.12426	2017-05-15 06:05:16.306605	وأردف	\N
67	3	أَعْـجَـازًا	---\n- '06'\n	\N	1	1	3	جمعٌ مفردُه عَجْزٌ، وهو مؤخرة الجسم. 	2017-03-30 13:12:13.133212	2017-05-15 06:05:16.311543	أعجازا	\N
68	3	نَـاءَ	---\n- '07'\n	\N	1	1	3	مقلوب أصله نَأَى بمعنى بَعُدَ. 	2017-03-30 13:12:31.324867	2017-05-15 06:05:16.316655	ناء	\N
69	3	كَـلْكَلِ	---\n- 08\n	\N	1	1	3	هو الصدر ويجمع على كلاكل.	2017-03-30 13:12:58.445152	2017-05-15 06:05:16.322637	كلكل	\N
70	4	فَقُلْـتُ	---\n- '00'\n	\N	1	1	3	 [الفاء] حرف استئناف، و [قُلْتُ] فعل ماض مبني على السكون لاتصاله بضمير الرفع المتحرك، و [التاء] فاعل مبني على الضم في محل رفع.	2017-03-30 13:41:43.00559	2017-05-15 06:05:16.328723	فقلت	\N
655	4	مِنْ	---\n- 08\n	\N	7	7	45	حرف جرّ.	2017-05-15 04:32:07.215997	2017-05-15 06:05:19.731175	من	\N
16	4	نَبْـكِ	---\n- '01'\n	\N	1	1	1	فعل مضارع مجزوم بحذف الياء لأنه جواب الأمر [قِفَا]، والأصل [نَبْكِي]، وقيل: مجزوم لأنه جواب شرطٍ مقدر، أي: قفا، إن تقفا نبكِ. كقولك : أَطِعِ اللهَ، إن تُطعِ اللهَ تَدْخلْ الجنةَ.	2017-03-30 10:01:28.908367	2017-05-15 06:05:15.980196	نبك	\N
17	4	مِـنْ ذِكْرَى	---\n- '02'\n- '03'\n	\N	1	1	1	 [مِنْ] حرف جر معناها السبب، و [ذِكْرَى] مجرور بكسرة مقدرة على الألف منع من ظهورها التعذر، والجار والمجرور متعلقان بالفعل [نَبْكِ]، و [ذِكْرَى] مضاف.	2017-03-30 10:02:39.753359	2017-05-15 06:05:15.985501	من ذكرى	\N
18	4	حَبِيبٍ	---\n- '04'\n	\N	1	1	1	مضاف إليه مجرور بالكسرة مع التنوين.	2017-03-30 10:03:04.317597	2017-05-15 06:05:15.991234	حبيب	\N
19	4	وَمَنْزِلِ	---\n- '05'\n	\N	1	1	1	[الواو] حرف عطف، و [مَنْزِلِ] مجرور بالكسرة لأنه معطوف على [حَبِيبٍ].	2017-03-30 10:03:50.209361	2017-05-15 06:05:15.996733	ومنزل	\N
20	4	بِسِقْطِ	---\n- '06'\n	\N	1	1	1	[الباء] حرف جرٍ، و [سِقْطِ] مجرور بالكسرة، والجار والمجرور متعلقان بـ [قِفَا] أو بـ [نَبْكِ] أو بـ [مَنْزِلِ]، و [سِقْطِ] مضاف.	2017-03-30 10:04:12.73319	2017-05-15 06:05:16.002136	بسقط	\N
21	4	اللِّـوَى	---\n- '07'\n	\N	1	1	1	 مضافٌ إليه مجرور بالكسرة المقدرة على الألف منع من ظهورها التعذر.	2017-03-30 10:04:37.320693	2017-05-15 06:05:16.007272	اللوى	\N
22	4	بَينَ الدَّخُـولِ	---\n- 08\n- 09\n	\N	1	1	1	[بَينَ] ظرف مكان منصوب بالفتحة، وهو مضاف، و [الدَّخُولِ] مضاف إليه مجرور بالكسرة. 	2017-03-30 10:05:06.598045	2017-05-15 06:05:16.012486	بين الدخول	\N
23	4	فَحَومَلِ	---\n- '10'\n	\N	1	1	1	 [الفاء] حرف عطف، و [حَومَلِ] مجرور بالكسرة لأنه معطوف على الدخول، ويروى [وحَومَلِ] بالواو لا بالفاء وهو الأصل لأن [بَينَ] يُعطَفُ عليها بالواو، تقول: دارى بين المسجد والمدرسة.	2017-03-30 10:10:32.808554	2017-05-15 06:05:16.017488	فحومل	\N
26	3	أَكْنَافِ	---\n- '03'\n	\N	3	3	10	جمعٌ مفرده كَنَفٌ، وهي الجوانب.	2017-03-30 10:15:21.356765	2017-05-15 06:05:16.022529	أكناف	\N
27	3	جَارِ	---\n- '04'\n	\N	3	3	10	هو الجيران في المسكن والسفر والعمل وغيرها.	2017-03-30 10:15:47.442397	2017-05-15 06:05:16.027179	جار	\N
28	3	مَضِنَّةٍ	---\n- '05'\n	\N	3	3	10	يجوز كسر الضاد وفتحها، يقال: ضَنَّ بالشيء يَضَنُّ ضِنًّا وضِنَّةً - بكسر الضاد – أي، بَخِلَ فهو ضَنِينٌ أي: بخيلٌ، وَضَنَانَةً - بفتح الضاد - أي: جارٌ يُضَنُّ به ولا يُفرَّط فيه.	2017-03-30 10:22:38.901315	2017-05-15 06:05:16.033448	مضنة	\N
29	3	فَـارَقَنِي	---\n- '06'\n	\N	3	3	10	من الفراق.	2017-03-30 10:23:03.234158	2017-05-15 06:05:16.040152	فارقني	\N
31	3	بِـأَرْبَـدَ	---\n- 08\n	\N	3	3	10	أخو لبيد وهو الجار المفارق النافع، والياء بمعنى [مِنْ] أي: [فارقني مِنْ أَرْبَدَ جارٌ نافع].	2017-03-30 10:24:07.809509	2017-05-15 06:05:16.049043	بأربد	\N
32	3	جَـزِعٌ	---\n- '01'\n	\N	3	3	11	بفتح الجيم وكسر الزاي على وزن تَعِب مبالغة اسم الفاعل، أي كثير الجزع لا يتحمل المصائب ولا يصبر عليها فهو خَوَّار لدى المصيبة.	2017-03-30 10:24:50.476615	2017-05-15 06:05:16.054266	جزع	\N
33	3	فَـاجِـعُ	---\n- '11'\n	\N	3	3	11	اسم فاعل من الفجيعة وهي الرزِيَّةُ والمصيبة، ويقال: مفجوع في ماله وأهله.	2017-03-30 10:25:22.632126	2017-05-15 06:05:16.060661	فاجع	\N
34	3	طَرِيـفٌ	---\n- '03'\n	\N	3	3	12	الطريف ما جَدَّ من الأشياء، ويقال للمال الجديد: مستطرف، ويقال: شيئ استُطرفَ واستحدث، أمّا التليد فهو ما وُرِث عن الآباء والأجداد.	2017-03-30 10:26:00.381022	2017-05-15 06:05:16.06718	طريف	\N
35	3	أَحْـدَثَ	---\n- 08\n	\N	3	3	12	أوجد	2017-03-30 10:26:31.432812	2017-05-15 06:05:16.073312	أحدث	\N
36	3	جَازِعُ	---\n- '10'\n	\N	3	3	12	اسم فاعل فعله جَزَعَ، وهو الذي لا يتحمل الشدائد والمصائب.	2017-03-30 10:26:52.600295	2017-05-15 06:05:16.081918	جازع	\N
37	3	الدِّيَـارِ	---\n- '03'\n	\N	3	3	13	جمع تكسير مفرده دار.	2017-03-30 10:27:47.142199	2017-05-15 06:05:16.093415	الديار	\N
38	3	حَلُّـوهَا	---\n- '07'\n	\N	3	3	13	نَزَلُوا بها.	2017-03-30 10:28:20.49803	2017-05-15 06:05:16.099148	حلوها	\N
39	3	غَـدْوًا	---\n- 08\n	\N	3	3	13	معناها غدًا، وهو اليوم الذي بعد يومك.	2017-03-30 10:28:44.901772	2017-05-15 06:05:16.106714	غدوا	\N
40	3	بَـلاَقِـعُ	---\n- 09\n	\N	3	3	13	قِفَارٌ لا نبت فيها ولا ماء. 	2017-03-30 10:29:07.886366	2017-05-15 06:05:16.112055	بلاقع	\N
41	3	الشِّهَابِ	---\n- '03'\n	\N	3	3	14	هو القبس من النار، أو هو النار نفسُها.	2017-03-30 10:29:44.119718	2017-05-15 06:05:16.11771	الشهاب	\N
42	3	يَحُـورُ	---\n- '05'\n	\N	3	3	14	يصير وينقلب ويتغير.	2017-03-30 10:30:25.176797	2017-05-15 06:05:16.124412	يحور	\N
72	4	لَـمَّـا	---\n- '02'\n	\N	1	1	3	ظرف زمان فيه معنى الشرط لكنه غير جازم وهو مضاف.	2017-03-30 13:42:29.69943	2017-05-15 06:05:16.343418	لما	\N
73	4	تَمَطَّى	---\n- '03'\n	\N	1	1	3	 فعل ماض مبني على الفتح المقدر على الألف، والفاعل مستتر تقديره (هُوَ) يعود إلى الليل، والجملة الفعلية في محل جر مضاف إليه.	2017-03-30 13:42:50.824897	2017-05-15 06:05:16.348893	تمطى	\N
74	4	بِصُلْبِـهِ	---\n- '04'\n	\N	1	1	3	[الباء] حرف جر للتعدية، و [صُلْبِ] مجرور بالكسرة وهو مضاف، والضمير [الهاء] مضاف إليه مبني على الكسر في محل جر، والجار والمجرور متعلقان بـ [تَمَطَّى].	2017-03-30 13:43:17.656815	2017-05-15 06:05:16.448303	بصلبه	\N
75	4	وَأَرْدَفَ	---\n- '05'\n	\N	1	1	3	[الواو] حرف عطف، و [أَرْدَفَ] فعل ماض مبني على الفتح معطوف على [تَمَطَّى]، والفاعل مستتر تقديره (هُوَ) يعود إلى الليل.	2017-03-30 13:43:44.885431	2017-05-15 06:05:16.453351	وأردف	\N
76	4	أَعْـجَـازًا	---\n- '06'\n	\N	1	1	3	مفعول به منصوب بالفتحة.	2017-03-30 13:44:04.02067	2017-05-15 06:05:16.457879	أعجازا	\N
77	4	وَنَـاءَ	---\n- '07'\n	\N	1	1	3	 [الواو] حرف عطف، و [نَاءَ] فعل ماض مبني على الفتح معطوف على [تَمَطَّى]، والفاعل مستتر تقديره (هُوَ) يعود إلى الليل.	2017-03-30 13:44:22.229747	2017-05-15 06:05:16.462368	وناء	\N
78	4	بِكَـلْكَلِ	---\n- 08\n	\N	1	1	3	[الباء] حرف جر للتعدية، و [كَلْكَلِ] مجرور بالكسرة، والجار والمجرور متعلقان بِـ [نَاءَ]. 	2017-03-30 13:44:49.347753	2017-05-15 06:05:16.466862	بكلكل	\N
79	3	انْجَلِي	---\n- '05'\n	\N	1	1	4	 انكشف وتنح عني.	2017-03-30 13:45:23.597096	2017-05-15 06:05:16.471787	انجلي	\N
80	3	الإِصْبَـاحُ	---\n- 08\n	\N	1	1	4	الصُّبْح.	2017-03-30 13:45:56.547933	2017-05-15 06:05:16.476388	الإصباح	\N
81	3	أَمْثَلِ	---\n- '10'\n	\N	1	1	4	أفضل.	2017-03-30 13:46:32.950822	2017-05-15 06:05:16.481813	أمثل	\N
82	4	أَلاَ	---\n- '00'\n	\N	1	1	4	 حرف استفتاح وتنبيه لا محل له من الإعراب.	2017-03-30 14:09:12.191617	2017-05-15 06:05:16.486953	ألا	\N
83	4	أَيُّـهَا	---\n- '01'\n	\N	1	1	4	[أَيُّ] منادى محذوف منه حرف النداء مبني على الضم في محل نصب، و [ها] للتنبيه.	2017-03-30 14:09:56.115344	2017-05-15 06:05:16.492371	أيها	\N
84	4	اللَّيـلُ	---\n- '02'\n	\N	1	1	4	مرفوع بالضمة بدلُ كلٍّ من كل من [أَيُّ] أو صفة لها، والأصل: ألا يا أيها الليلُ.	2017-03-30 14:10:20.856381	2017-05-15 06:05:16.497986	الليل	\N
86	4	أَلاَ	---\n- '04'\n	\N	1	1	4	حرف استفتاح وتنبيه لا محل له من الإعراب، توكيد لفظي للأول.	2017-03-30 14:11:03.207454	2017-05-15 06:05:16.506857	ألا	\N
88	4	بِصُبْـحٍ	---\n- '06'\n	\N	1	1	4	 [الباء] حرف جرّ، و [صُبْحٍ] مجرور بالكسرة، وهما متعلقان بـ [انْجَلِ].	2017-03-30 14:12:30.041089	2017-05-15 06:05:16.516054	بصبح	\N
89	4	وَمَـا	---\n- '07'\n	\N	1	1	4	[الواو] حرف استئناف، [ما] نافية تعمل عمل ليس.	2017-03-30 14:12:49.222251	2017-05-15 06:05:16.520788	وما	\N
90	4	الإِصْبَـاحُ	---\n- 08\n	\N	1	1	4	 اسم ما النافية مرفوع بالضمة.	2017-03-30 14:13:11.723106	2017-05-15 06:05:16.525716	الإصباح	\N
91	4	مِنْكَ	---\n- 09\n	\N	1	1	4	[مِنْ] حرف جرّ، و [الكاف] مبني على الفتح في محل جرّ وهما متعلقان بِـ [أمثل].	2017-03-30 14:13:28.580802	2017-05-15 06:05:16.530388	منك	\N
92	4	بِأَمْثَلِ	---\n- '10'\n	\N	1	1	4	[الباء] حرف جر زائد، و [أَمْثَلِ] اسم تفضيل خبر ما منصوب محلاً مجرور لفظًا بالباء الزائدة لقافية الشعر أي : وما الإصباحُ أَمثلَ منك.	2017-03-30 14:13:48.698405	2017-05-15 06:05:16.536354	بأمثل	\N
93	3	فَيَا لَـكَ مِنْ لَيـلٍ	---\n- '00'\n- '01'\n- '02'\n- '03'\n	\N	1	1	5	في هذه الجملة معنى التعجب، كما تقول: يَا لَكَ مِنْ فَارِسٍ!.	2017-03-30 14:19:02.837847	2017-05-15 06:05:16.546343	فيا لك من ليل	\N
94	3	مُغَـارِ	---\n- '07'\n	\N	1	1	5	الحَبلُ الشديدُ المحكمُ الفتل، يقال: أَغَرْتُ الحَبْلَ إِغَارةً إِذَا شَدَدتَ فَتْلَهُ.	2017-03-30 14:19:35.15119	2017-05-15 06:05:16.555047	مغار	\N
95	3	بِيَذْبُلِ	---\n- '10'\n	\N	1	1	5	 اسم للجبل.	2017-03-30 14:20:29.286127	2017-05-15 06:05:16.569143	بيذبل	\N
96	4	فَيَا	---\n- '00'\n	\N	1	1	5	 [الفاء] حرف استئناف، و [يا] حرف نداءٍ وتعجب مبني على السكون لا محل له من الإعراب، والمنادى محذوف أي: يا عجبًا لك.	2017-03-30 14:29:47.699192	2017-05-15 06:05:16.581944	فيا	\N
97	4	لَـكَ	---\n- '01'\n	\N	1	1	5	 [اللام] حرف حرف جر، و [الكاف] مبني على الفتح في محل جرٍّ.	2017-03-30 14:30:07.79239	2017-05-15 06:05:16.589976	لك	\N
98	4	مِنْ لَيـلٍ	---\n- '02'\n- '03'\n	\N	1	1	5	 [من] حرف جر، و [لَيلٍ] مجرور بالكسرة لفظًا، وهما متعلقان بفعل مقدر أي: كان من ليل، ومحل [ليل] منصوب على أنّه تمييز.	2017-03-30 14:30:42.042649	2017-05-15 06:05:16.596067	من ليل	\N
99	4	كَـأَنَّ	---\n- '04'\n	\N	1	1	5	 من الحروف المشبهة بالفعل معناه التشبيه.	2017-03-30 14:31:00.515348	2017-05-15 06:05:16.602375	كأن	\N
100	4	نُجُـومَـهُ	---\n- '05'\n	\N	1	1	5	اسم كأنَّ منصوب بالفتحة وهو مضاف، والضمير [الهاء] مضاف إليه مبني على الضم في محل جرٍّ.	2017-03-30 14:31:22.15156	2017-05-15 06:05:16.608554	نجومه	\N
101	4	بِكُـلِّ	---\n- '06'\n	\N	1	1	5	[الباء] حرف جر، و [كُلِّ] مجرور بالكسرة وهو مضاف.	2017-03-30 14:31:49.030524	2017-05-15 06:05:16.61504	بكل	\N
104	4	شُـدَّتْ	---\n- 09\n	\N	1	1	5	فعل ماضٍ مبني للمجهول مبني على الفتح، و [التاء] تاء التأنيث الساكنة لا محل لها من الإعراب، ونائب الفاعل ضمير مستتر تقديره [هي] يعود إلى النجوم، والجملة الفعلية في محل رفع خبر [كأنّ].	2017-03-30 14:33:03.237612	2017-05-15 06:05:16.633555	شدت	\N
105	4	بِيَذْبُلِ	---\n- '10'\n	\N	1	1	5	 [الباء] حرف جر، و [يذبل] مجرور بالكسرة، وهما متعلقان بالفعل [شُدَّتْ]، وجملة: كأنّ واسمها وخبرها في محل جرٍّ نعت لليل.	2017-03-30 14:33:20.209074	2017-05-15 06:05:16.638654	بيذبل	\N
106	3	أَغْتَـدِي	---\n- '01'\n	\N	1	1	6	على وزن أفتَعِلُ من الغدوّ وهو الذهاب صباحًا، يقال: غدا يغدوا غدوًا، واغتدى اغتداءً بمعنى واحد.	2017-03-30 14:33:47.312509	2017-05-15 06:05:16.643483	أغتدي	\N
107	3	وُكُنَـاتِهَا	---\n- '04'\n	\N	1	1	6	جمعٌ مفرده وُكْنَةٌ - بضم الواو وسكون الكاف - وهي عِشُّ الطائر الذي يبيت فيه، ويروى [وُكُراتها] جمع وُكْرٍ - بضم فسكون - وهو مأوى الطائر في العِشِّ.	2017-03-30 14:34:12.338216	2017-05-15 06:05:16.648375	وكناتها	\N
108	3	مُنْـجَـرِدٍ	---\n- '05'\n	\N	1	1	6	ماضٍ في السير، وقيل: هو قليل الشَّعر أو قصيرُهُ	2017-03-30 14:34:42.642544	2017-05-15 06:05:16.653389	منجرد	\N
109	3	الأَوَابِـدِ	---\n- '07'\n	\N	1	1	6	الوحوش، يقال: تَأَبَّدَ الموضوعُ إذا تَوَحَّشَ.	2017-03-30 14:35:10.112982	2017-05-15 06:05:16.658592	الأوابد	\N
110	3	هَيـكَلِ	---\n- 08\n	\N	1	1	6	العظم من الخيل ومن الشجر، والهيكل الشيء الضخم.	2017-03-30 14:35:30.575735	2017-05-15 06:05:16.664015	هيكل	\N
111	4	وَقَـدْ	---\n- '00'\n	\N	1	1	6	[الواو] حرف استئناف، و [قد] حرف تأكيد وتحقيق.	2017-03-30 14:48:32.790127	2017-05-15 06:05:16.669035	وقد	\N
112	4	أَغْتَـدِي	---\n- '01'\n	\N	1	1	6	فعل مضارع مرفوع بالضمة المقدرة على الياء، وفاعله ضمير مستتر تقديره (أَنَا).	2017-03-30 14:49:09.139066	2017-05-15 06:05:16.67588	أغتدي	\N
113	4	وَالطَّيرُ	---\n- '02'\n	\N	1	1	6	 [الواو] واو الحال، أي: قد اغتدى في هذه الحال بفرس منجرد...إلخ، و [الطَّيرُ] مبتدأ مرفوع بالضمة.	2017-03-30 14:49:46.964878	2017-05-15 06:05:16.682692	والطير	\N
114	4	فِي	---\n- '03'\n	\N	1	1	6	 حرف جرّ مبني على السكون.	2017-03-30 14:50:24.359548	2017-05-15 06:05:16.688407	في	\N
116	4	بِمُنْـجَـرِدٍ	---\n- '05'\n	\N	1	1	6	 [الباء] حرف جرّ، و [منجرد] مجرور بالكسرة، والجار والمجرور متعلقان بِـ [اغتدي].	2017-03-30 14:51:19.825814	2017-05-15 06:05:16.702163	بمنجرد	\N
117	4	قَيـدِ	---\n- '06'\n	\N	1	1	6	نعت لمنجرد مجرور بالكسرة وهو مضاف.	2017-03-30 14:51:44.891006	2017-05-15 06:05:16.709592	قيد	\N
118	4	الأَوَابِـدِ	---\n- '07'\n	\N	1	1	6	مضاف إليه مجرور بالكسرة	2017-03-30 14:52:20.213342	2017-05-15 06:05:16.714712	الأوابد	\N
119	4	هَيـكَلِ	---\n- 08\n	\N	1	1	6	نعت ثانٍ لمنجرد مجرور بالكسرة	2017-03-30 14:52:50.549125	2017-05-15 06:05:16.720209	هيكل	\N
120	3	مِـكَـرٍّ	---\n- '00'\n	\N	1	1	7	يَكُرُّ على العدوّ إذا أُريد ذلك منه، والصيغة للمبالغة على وزن مِفَعْلٍ.	2017-03-30 14:54:15.386019	2017-05-15 06:05:16.725279	مكر	\N
121	3	مِـفَـرٍّ	---\n- '01'\n	\N	1	1	7	يفرّ فِرارًا.	2017-03-30 14:54:39.766875	2017-05-15 06:05:16.730152	مفر	\N
122	3	مُقْبِـلٍ	---\n- '02'\n	\N	1	1	7	 حَسَن الإِقبال.	2017-03-30 14:55:03.857843	2017-05-15 06:05:16.737246	مقبل	\N
123	3	مُدْبِـرٍ	---\n- '03'\n	\N	1	1	7	حَسَن الإِدبار.	2017-03-30 14:55:28.334901	2017-05-15 06:05:16.743991	مدبر	\N
124	3	مَعًا	---\n- '04'\n	\N	1	1	7	أي عنده هذا وعنده هذا، كما يقال: رجلٌ فَارِسٌ رَاجِلٌ.	2017-03-30 14:55:53.531452	2017-05-15 06:05:16.749319	معا	\N
125	3	جُـلْمُودِ	---\n- '05'\n	\N	1	1	7	 الصخرة العظيمة الصُلب، إذا كانت في أعلى الجبل كان أصلبَ لها.	2017-03-30 14:56:20.438482	2017-05-15 06:05:16.754765	جلمود	\N
126	3	حَطَّهُ	---\n- '07'\n	\N	1	1	7	أَلْقَاهُ من عُلُوٍّ إلى أسفل.	2017-03-30 14:56:38.017664	2017-05-15 06:05:16.760198	حطه	\N
127	3	السَّيلُ	---\n- 08\n	\N	1	1	7	الماء الجاري من الأعلى إلى الأدنى.	2017-03-30 14:56:57.144602	2017-05-15 06:05:16.765175	السيل	\N
128	3	عَلِ	---\n- '10'\n	\N	1	1	7	 فوق.	2017-03-30 14:57:22.230932	2017-05-15 06:05:16.771044	عل	\N
129	4	مِـكَـرٍّ مِـفَـرٍّ مُقْبِـلٍ مُدْبِـرٍ	---\n- '00'\n- '01'\n- '02'\n- '03'\n	\N	1	1	7	 أربعة نعوت لمنجرد مجرورة بالكسرة.	2017-03-30 15:01:19.998762	2017-05-15 06:05:16.776333	مكر مفر مقبل مدبر	\N
130	4	مَعًا	---\n- '04'\n	\N	1	1	7	حال منصوب بالفتحة بمعنى مجتمعة.	2017-03-30 15:01:41.92547	2017-05-15 06:05:16.781776	معا	\N
131	4	كَجُـلْمُودِ	---\n- '05'\n	\N	1	1	7	[الكاف] حرف جر، و [جُلْمُودِ] مجرور بالكسرة، والجار والمجرور متعلقان بمحذوف صفة أخرى للمنجرد، أي: كائن كجلمود صخر وهو مضاف.	2017-03-30 15:02:18.532901	2017-05-15 06:05:16.786622	كجلمود	\N
132	4	صَخْرٍ	---\n- '06'\n	\N	1	1	7	مضاف إليه مجرور بالكسرة.	2017-03-30 15:02:42.643519	2017-05-15 06:05:16.791604	صخر	\N
133	4	حَطَّهُ	---\n- '07'\n	\N	1	1	7	 فعل ماض مبني على الفتح، و [الهاء] ضمير يعود إلى الجلمود مفعول به مبني على الضم في محل نصب، و [السَّيل] فاعل مرفوع بالضمة.	2017-03-30 15:03:03.937218	2017-05-15 06:05:16.79711	حطه	\N
134	4	مِنْ عَلِ	---\n- 09\n- '10'\n	\N	1	1	7	 [من] حرف جر، [عَلِ] ظرف مكان مجرور بالكسرة أي من فوق، والجار والمجرور متعلقان بِـ [حَطَّهُ]، والجملة الفعلية في محل نصب حال من [جُلْمُودِ]	2017-03-30 15:03:30.774858	2017-05-15 06:05:16.803297	من عل	\N
135	3	كُمَيتٍ	---\n- '00'\n	\N	1	1	8	- بضم الكاف وفتح الميم - يطلق على المذكر والمؤنث من الخيول، وهو ما كان لونه بين الأسود والأحمر وهو تصغير [أَكْمَت]، وتَعُدُّ العربُ هذا النوعَ من أقوى الخيول.	2017-03-30 15:04:02.262126	2017-05-15 06:05:16.809978	كميت	\N
136	3	يَزِلُّ	---\n- '01'\n	\N	1	1	8	من الزوال وهو التحول والتَغَيُّرِ.	2017-03-30 15:04:25.1074	2017-05-15 06:05:16.816232	يزل	\N
137	3	اللَّبْـدُ	---\n- '02'\n	\N	1	1	8	 هو الشعر والصوف المتلبد الذي يوضع تحت السّرج فوق ظهر الخيل ليكون مقعدًا للفارس.	2017-03-30 15:04:45.937092	2017-05-15 06:05:16.823242	اللبد	\N
138	3	عَـنْ حَـالِ مَتْنِهِ	---\n- '03'\n- '04'\n- '05'\n	\N	1	1	8	يروى [عن حاذِ متنه] والحالُ والحاذُ بمعنى واحد وهو الوسط ومتن الفرس ظهره أي: وسط ظهر الفرس.	2017-03-30 15:05:12.56897	2017-05-15 06:05:16.828805	عن حال متنه	\N
139	3	كَمَـا	---\n- '06'\n	\N	1	1	8	 مثل ما.	2017-03-30 15:05:32.634361	2017-05-15 06:05:16.835587	كما	\N
140	3	زَلَّتِ	---\n- '07'\n	\N	1	1	8	تحوّلت وتغيرت.	2017-03-30 15:05:48.776866	2017-05-15 06:05:16.843367	زلت	\N
141	3	الصَّـفْـوَاءُ	---\n- 08\n	\N	1	1	8	وهو الحجر الصلبُ الأملس وهو ممدود، والمقصور منه الصفا ويقال: الصفوان.	2017-03-30 15:06:14.557192	2017-05-15 06:05:16.850309	الصفواء	\N
142	3	المُتَنَـزِّلِ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n	\N	1	1	8	 السيل من ماء المطر إذا انحدر من عُلُوٍّ. 	2017-03-30 15:06:59.050853	2017-05-15 06:05:16.857033	المتنزل	\N
144	3	وَدِيعَـةٌ	---\n- '04'\n	\N	3	3	16	تقدم شرحها في البيت السابق، ويروى: [إلا ودائع].	2017-03-30 16:18:59.302617	2017-05-15 06:05:16.885471	وديعة	\N
145	3	بُـدَّ	---\n- '06'\n	\N	3	3	16	معناها مَفَرُّ أو مَهْرَبُ، وتكون غالبًا مسبوقة بنفي، أي: لا بُدَّ ولا مفرَّ ولا مهربَ.	2017-03-30 16:19:38.330168	2017-05-15 06:05:16.895314	بد	\N
146	3	تُـرَدَّ	---\n- 09\n	\N	3	3	16	تُرجع إلى أصحابها.	2017-03-30 16:20:12.730459	2017-05-15 06:05:16.902834	ترد	\N
147	3	يَمْضُـونَ	---\n- '00'\n	\N	3	3	17	يذهبون، ويروى: [وَيَغْدون].	2017-03-30 16:20:50.182836	2017-05-15 06:05:16.909389	يمضون	\N
148	3	أَرْسَـالاً	---\n- '01'\n	\N	3	3	17	جماعةً بعد جماعة، وفريقًا إثر فريق.	2017-03-30 16:21:30.869953	2017-05-15 06:05:16.914661	أرسالا	\N
149	3	نَخْلُفُ	---\n- '02'\n	\N	3	3	17	خَلَفَ يَخْلُفُ من باب نَصَرَ يَنْصُرُ، أي: نبقى ونأتي بعدهم وخَلَفَهُم، يقال: خَلَفْتُهُ: أي جِئْتُ بعدهُ.	2017-03-30 16:22:25.383576	2017-05-15 06:05:16.922065	نخلف	\N
150	3	ضَـمَّ	---\n- '05'\n	\N	3	3	17	جمع، يقال: ضَمَمْمتُهُ ضَمًّا فَانْضَمَّ، أي: جمعتُهُ فانجمع.	2017-03-30 16:23:13.004833	2017-05-15 06:05:16.930127	ضم	\N
151	3	أُخْرَى	---\n- '06'\n	\N	3	3	17	مؤنث آخر، أي: المتأخرة عن قطيع الإبل.	2017-03-30 16:23:37.765331	2017-05-15 06:05:16.937236	أخرى	\N
152	3	التَّالِيَـاتِ	---\n- '07'\n	\N	3	3	17	جمع مفرده تالية، وهي التي تسير في آواخر الإبل، يقال: تلوت الرجل أتلوه تُلُوًّا إذا تبعتَهُ.	2017-03-30 16:25:14.880189	2017-05-15 06:05:16.94443	التاليات	\N
153	3	المُشَـايِعُ	---\n- 08\n	\N	3	3	17	هو الذي يقود الإبل ويزجرها ويُتْبِعُ بعضَها بعضًا، تقول العرب: شيَّعتُ رمضانَ بِسِتٍّ من شوّال، أي: أتبعته بها.	2017-03-30 16:25:55.39552	2017-05-15 06:05:16.950204	المشايع	\N
154	3	عَـامِـلاَنِ	---\n- '03'\n	\N	3	3	18	مثنى مفرده عامِلٌ.	2017-03-30 16:26:50.643792	2017-05-15 06:05:16.955744	عاملان	\N
156	3	رَافِـعُ	---\n- 09\n	\N	3	3	18	اسم فاعل وهو الذي يقيم البناء ويرفعه ويشيده.	2017-03-30 16:30:17.846849	2017-05-15 06:05:16.967414	رافع	\N
157	3	سَعِيـدٌ	---\n- '01'\n	\N	3	3	19	صفة مشبهة باسم الفاعل، فعلُهُ سَعِدَ يسعَد من باب تَعِبَ يَتعَبُ من السعادة.	2017-03-30 16:31:14.148879	2017-05-15 06:05:16.973258	سعيد	\N
158	3	آخِـذٌ	---\n- '02'\n	\N	3	3	19	اسم فاعل فعلُهُ أَخَذَ يأخُذُ من باب نَصَرَ ينصُرُ.	2017-03-30 16:31:45.731878	2017-05-15 06:05:16.978529	آخذ	\N
159	3	لِنَصِيـبِـهِ	---\n- '03'\n	\N	3	3	19	ويروى [بنصيبه]، والنصيب الحِصَّةُ، ويجمع على أنصبةٍ وأَنْصِباء ونُصُبٍ.	2017-03-30 16:32:09.626954	2017-05-15 06:05:16.984574	لنصيبه	\N
160	3	شَـقِيٌّ	---\n- '05'\n	\N	3	3	19	صفة مشبهة باسم الفاعل فعلُهُ شقِي يشقَى من باب تَعِب يتعَبُ من الشقاوةِ وهي ضِدُّ السعادة.	2017-03-30 16:33:05.65415	2017-05-15 06:05:16.989484	شقي	\N
161	3	المَعِيـشَةِ	---\n- '06'\n	\N	3	3	19	مكسبُ الإنسان الذي يعيش به.	2017-03-30 16:33:34.58594	2017-05-15 06:05:16.994492	المعيشة	\N
162	3	قَـانِـعُ	---\n- '07'\n	\N	3	3	19	راضٍ بما قُدِّرَ له من المعيشة، يقال: قَنِعْتُ به قَنْعًا أي: رضِيتُ به.	2017-03-30 16:34:09.459457	2017-05-15 06:05:17.000223	قانع	\N
163	3	وَرَائِي	---\n- '01'\n	\N	3	3	20	بمعنى أمامي وقدامي ومستقبل حياتي، قال تعالى: ﴿وَيَذَرُونَ وَرَاءهُمْ يَوْماً ثَقِيلاً﴾ الدهر: 27.	2017-03-30 16:35:19.306397	2017-05-15 06:05:17.007318	ورائي	\N
165	3	تَـرَاخَـتْ	---\n- '03'\n	\N	3	3	20	طالت وامتدّت وأَبْطأت، يقال: تراخي الأمر تراخيًا: امتدَّ زمانُهُ، وفي الأمر تراخٍ، أي: فُسْحَةٌ. وما بينك وبينه متراخٍ، أي: متباعد.	2017-03-30 16:39:26.824271	2017-05-15 06:05:17.012711	تراخت	\N
166	3	مَنِيَّتِي	---\n- '04'\n	\N	3	3	20	المنية الموت من المَنِّ وهو القَطعُ لأنّه يقطع العمر.	2017-03-30 16:40:07.290614	2017-05-15 06:05:17.021708	منيتي	\N
167	3	لُـزُومُ العَـصَا	---\n- '05'\n- '06'\n	\N	3	3	20	التوكؤ عليها.	2017-03-30 16:40:45.114063	2017-05-15 06:05:17.027971	لزوم العصا	\N
168	3	تُحْنَى	---\n- '07'\n	\N	3	3	20	تعطف عليها، وتنثني عليها، يقال: حَنَيتُ العودَ أَحنيه حَنْيًا، وحنوته أحنوه حنوًا: ثنيته.	2017-03-30 16:41:11.57506	2017-05-15 06:05:17.03356	تحنى	\N
169	3	الأَصَابِعُ	---\n- 09\n	\N	3	3	20	جمع مفردُهُ إِصْبَعٌ والمراد بها هنا أصابع يد الإنسان.	2017-03-30 16:41:30.496992	2017-05-15 06:05:17.039817	الأصابع	\N
178	3	أَخْبَـارَ	---\n- '01'\n	\N	3	3	21	جمع مفردُهُ خبر.	2017-03-30 16:46:08.21192	2017-05-15 06:05:17.055541	أخبار	\N
180	3	مَضَـتْ	---\n- '04'\n	\N	3	3	21	ذهبت وانتهت. 	2017-03-30 16:49:46.755892	2017-05-15 06:05:17.06929	مضت	\N
181	3	أَدِبُّ	---\n- '05'\n	\N	3	3	21	أسير على الأرض، يقال: دبَّ الجيشُ دبيبًا: ساروا سيرًا لينًا، وكلّ حيوان في الأرض دابَّةٌ.	2017-03-30 16:50:23.644587	2017-05-15 06:05:17.074818	أدب	\N
182	3	رَاكِـعُ	---\n- 09\n	\N	3	3	21	منحني الظهر.	2017-03-30 16:50:55.579785	2017-05-15 06:05:17.083718	راكع	\N
183	3	أَصْبَحْـتُ	---\n- '00'\n	\N	3	3	22	صِرْتُ.	2017-03-30 16:54:15.569204	2017-05-15 06:05:17.089569	أصبحت	\N
184	3	غَيَّرَ	---\n- '03'\n	\N	3	3	22	بَدَّلَ، ويروى: [أَخْلَقَ].	2017-03-30 16:55:00.878701	2017-05-15 06:05:17.102306	غير	\N
185	3	جَفْنَهُ	---\n- '04'\n	\N	3	3	22	الجَفْنُ هو غلاف السيف وغِمْدُهُ.	2017-03-30 16:55:44.256264	2017-05-15 06:05:17.122101	جفنه	\N
186	3	تَقَـادُمُ	---\n- '05'\n	\N	3	3	22	مصدر فعلُهُ تَقَادَمَ يتقادمُ تقادمًا، أي: قديمًا.	2017-03-30 16:56:23.874308	2017-05-15 06:05:17.131146	تقادم	\N
187	3	عَهْـدِ	---\n- '06'\n	\N	3	3	22	معناه الزمن القديم.	2017-03-30 16:58:01.126343	2017-05-15 06:05:17.136741	عهد	\N
188	3	القَيـنِ	---\n- '07'\n	\N	3	3	22	يطلق على الحَدَّاد الذي يصنع السيوف ويجمع على قُيُون.	2017-03-30 16:58:38.653677	2017-05-15 06:05:17.143952	القين	\N
189	3	النَّصْلُ	---\n- 08\n	\N	3	3	22	حديدة السيف، أي: هي السيف نفسه.	2017-03-30 16:59:55.79479	2017-05-15 06:05:17.149764	النصل	\N
190	3	قَاطِعُ	---\n- 09\n	\N	3	3	22	اسم فاعل من قطع يقطع قطعًا، فهو قَاطِعٌ.	2017-03-30 17:00:32.080235	2017-05-15 06:05:17.155859	قاطع	\N
192	3	مَـوعِـدٌ عَلَيـكَ	---\n- '04'\n- '05'\n	\N	3	3	23	يروى: موعد علينا، أي: واجبة عليك.	2017-03-30 17:02:35.729407	2017-05-15 06:05:17.17169	موعد عليك	\N
193	3	المَنِـيَّـةَ	---\n- '03'\n	\N	3	3	23	الموت بالأجل.	2017-03-30 17:03:13.56533	2017-05-15 06:05:17.178521	المنية	\N
194	3	دَانٍ	---\n- '06'\n	\N	3	3	23	اسم فاعل من دَنَا يَدْنُو فهو دَانٍ، أي: قريب. وأصله: دانِيٌ، فاسكنت الياء لثقل الضمة عليها ثم حذفت لسكونها وسكون التنوين.	2017-03-30 17:03:43.954431	2017-05-15 06:05:17.186457	دان	\N
196	3	طُّلُـوعِ	---\n- '07'\n	\N	3	3	23	قريب الأجل.	2017-03-30 17:05:16.846694	2017-05-15 06:05:17.194011	طلوع	\N
197	3	طَـالِـعُ	---\n- 08\n	\N	3	3	23	يأتي بعد الداني للطلوع لأنّه متخلف يسيرًا عنه.	2017-03-30 17:05:51.674783	2017-05-15 06:05:17.2055	طالع	\N
198	3	أَعَـاذِلَ	---\n- '00'\n	\N	3	3	24	[الهمزة] حرف نداء، و [عاذلَ] منادى مرخم، أي: يا عاذِلَةً، والعذلُ اللَّومُ. 	2017-03-30 17:07:06.994084	2017-05-15 06:05:17.210812	أعاذل	\N
199	3	يُـدْرِيـكِ	---\n- '02'\n	\N	3	3	24	يُعْلِمُكِ.	2017-03-30 17:07:39.636328	2017-05-15 06:05:17.216221	يدريك	\N
200	3	تَظَنِّـياً	---\n- '04'\n	\N	3	3	24	ظَنًّا وتخمينًا.	2017-03-30 17:12:26.088293	2017-05-15 06:05:17.225375	تظنيا	\N
201	3	ارْتَـحَلَ	---\n- '06'\n	\N	3	3	24	ذهب وغادر.	2017-03-30 17:13:51.811984	2017-05-15 06:05:17.2332	ارتحل	\N
202	3	الفِتْـيَانُ	---\n- '07'\n	\N	3	3	24	جمع مفردُهُ فتى، ويروى: [إذا رَحَلَ السُّفَارُ] أي: المسافرون.	2017-03-30 17:14:28.099417	2017-05-15 06:05:17.246716	الفتيان	\N
203	3	رَاجِعُ	---\n- '10'\n	\N	3	3	24	عائد من رحلته.	2017-03-30 17:14:58.840039	2017-05-15 06:05:17.254995	راجع	\N
204	3	تُبَـكِّي	---\n- '00'\n	\N	3	3	25	على وزن تُفَرِّحُ، أي: تجعل غيرها باكيًا.	2017-03-30 17:16:00.650902	2017-05-15 06:05:17.26333	تبكي	\N
205	3	إِثْرِ	---\n- '02'\n	\N	3	3	25	إِثْرِ\t: هو التتبع عن قرب، يقال: جِئتُ على أَثَرِه - بفتحتين - وعلى إِثْره - بكسر الهمزة وسكون الثاء - تتبعته عن قرب.	2017-03-30 17:16:22.877131	2017-05-15 06:05:17.271166	إثر	\N
206	3	مَضَى	---\n- '05'\n	\N	3	3	25	ذهب وغادر.	2017-03-30 17:17:05.172396	2017-05-15 06:05:17.278358	مضى	\N
207	3	أَخْـدَانَ	---\n- 08\n	\N	3	3	25	جمع مفردُهُ خَدَنٌ، وهو الصديق والأخ، وقيل: هو الصديق في السرِّ.	2017-03-30 17:17:35.239186	2017-05-15 06:05:17.284231	أخدان	\N
208	3	الشَّبَـابِ	---\n- 09\n	\N	3	3	25	هو سنّ الإنسان قبل سنّ الكهولة.	2017-03-30 17:18:45.37517	2017-05-15 06:05:17.289849	الشباب	\N
209	3	الرَّعَـارِعُ	---\n- '10'\n	\N	3	3	25	هو سنّ الإنسان قبل سنّ الكهولة.	2017-03-30 17:19:12.032058	2017-05-15 06:05:17.295347	الرعارع	\N
210	3	الرَّعَـارِعُ	---\n- '10'\n	\N	3	3	25	مع مفردُهُ رَعْرَعٌ، ويقال للأنثى: رعرعة، وهم الأحداث صغار السنّ.	2017-03-30 17:20:26.613226	2017-05-15 06:05:17.301103	الرعارع	\N
212	3	أَحْـدَثَ	---\n- '02'\n	\N	3	3	26	أوجد.	2017-03-30 17:21:38.977893	2017-05-15 06:05:17.314004	أحدث	\N
213	3	الدَّهْـرُ	---\n- '03'\n	\N	3	3	26	الزمن.	2017-03-30 17:22:08.806153	2017-05-15 06:05:17.319648	الدهر	\N
214	3	بِالفَتَى	---\n- '04'\n	\N	3	3	26	يروى: للفتى.	2017-03-30 17:22:52.157442	2017-05-15 06:05:17.325274	بالفتى	\N
216	3	لَعَمْرُكَ	---\n- '00'\n	\N	3	3	27	العَمْرُ - بفتح العين وضمها لغتان - مصدر فعلُهُ من باب عَلِمَ يعلَمُ، يقال: عَمِرَ يَعْمَرُ عَمْرًا وعُمرًا إذا طال عُمرُه فهو عامر وبه سُمي تفاؤلاً وسمى بالمضارع أيضًا، فقيل: يعمر. وتدخل لام القسم على المصدر المفتوح العين، فتقول: لَعَمْرُك لأفعلنَّ، والمعنى: وحياتك وبقائك.	2017-03-30 17:23:57.625968	2017-05-15 06:05:17.336955	لعمرك	\N
217	3	مَا تَـدْرِي	---\n- '01'\n- '02'\n	\N	3	3	27	ما تعلمُ.	2017-03-30 17:24:41.693472	2017-05-15 06:05:17.343437	ما تدري	\N
218	3	الضَّوَارِبُ	---\n- '03'\n	\N	3	3	27	جمع مفردهُ ضاربة، والمقصود بها الخيل التي تضرب الحصى بعضه ببعض حين تعدو وتركض.	2017-03-30 17:25:29.408114	2017-05-15 06:05:17.349365	الضوارب	\N
219	3	الحَصَى	---\n- '04'\n	\N	3	3	27	معروف جمع مفرده حصاة. 	2017-03-30 17:26:06.27219	2017-05-15 06:05:17.357675	الحصى	\N
220	3	زَاجِـرَاتُ	---\n- '06'\n	\N	3	3	27	جمع مفردُهُ زاجرة، والمراد بها الطيور، وسميت الملائكة بالزاجرات لأنّها تزجر السحاب وتسوقه، وقيل: لأنّها تزجر عن المعاصي.	2017-03-30 17:27:35.346919	2017-05-15 06:05:17.365209	زاجرات	\N
221	3	صَـانِـعُ	---\n- '10'\n	\N	3	3	27	فاعل ومنفذ ما قدره وقضاه.	2017-03-30 17:28:00.678075	2017-05-15 06:05:17.382656	صانع	\N
222	3	يَذُوقُ	---\n- '05'\n	\N	3	3	28	يأتيه الأجل.	2017-03-30 17:32:25.377616	2017-05-15 06:05:17.391162	يذوق	\N
223	3	المَنَـايَا	---\n- '06'\n	\N	3	3	28	جمع مفردُهُ منيّة وهي الموت.	2017-03-30 17:33:05.496004	2017-05-15 06:05:17.399439	المنايا	\N
224	3	الغَيـثُ	---\n- 09\n	\N	3	3	28	المطر.	2017-03-30 17:33:39.672301	2017-05-15 06:05:17.408312	الغيث	\N
243	4	وَقَـدْ	---\n- '00'\n	\N	3	3	10	[الواو] حرف استئناف، و [قد] حرف تحقيق مبني على السكون لا محل له من الإعراب.	2017-03-30 17:44:34.480915	2017-05-15 06:05:17.415225	وقد	\N
244	4	كُنْـتُ	---\n- '01'\n	\N	3	3	10	[كان] فعل ماضٍ ناقص مبني على السكون لاتصاله بضمير الرفع، و [التاء] اسمها مبني على الضم في محل رفع.	2017-03-30 17:45:01.965696	2017-05-15 06:05:17.421265	كنت	\N
245	4	فِي	---\n- '02'\n	\N	3	3	10	حرف جرّ.	2017-03-30 17:45:24.246827	2017-05-15 06:05:17.427812	في	\N
246	4	أَكْنَافِ	---\n- '03'\n	\N	3	3	10	مجرور بالكسرة وهما متعلقان بمحذوف في محل نصب خبر [كان] وهو مضاف، أي: كنت موجودًا بأكناف.	2017-03-30 17:45:54.292159	2017-05-15 06:05:17.433982	أكناف	\N
247	4	جَارِ	---\n- '04'\n	\N	3	3	10	مضاف إليه مجرور بالكسرة وهو مضاف. 	2017-03-30 17:46:14.481765	2017-05-15 06:05:17.440149	جار	\N
248	4	مَضِنَّةٍ	---\n- '05'\n	\N	3	3	10	مضاف إليه مجرور بالكسرة.	2017-03-30 17:46:41.63122	2017-05-15 06:05:17.446418	مضنة	\N
249	4	فَفَـارَقَنِي	---\n- '06'\n	\N	3	3	10	[الفاء] حرف عطف، و [فارق] فعل ماض مبني على الفتح، و [النون] نون الوقاية، و [الياء] مفعول به مقدّم مبني على السكون في محل نصب.	2017-03-30 17:47:08.385051	2017-05-15 06:05:17.452294	ففارقني	\N
250	4	جَـارٌ	---\n- '07'\n	\N	3	3	10	فاعل مرفوع مؤخّر. والجملة الفعلية [ففارقني ...] معطوفة على جملة [كنتُ].	2017-03-30 17:48:05.583948	2017-05-15 06:05:17.459318	جار	\N
251	4	بِـأَرْبَـدَ	---\n- 08\n	\N	3	3	10	 [الباء] حرف جر بمعنى [مِنْ]، و [أربد] مجرور بالفتحة لأنّه ممنوع من الصرف، وهما متعلقان بِـ [فارقني].	2017-03-30 17:48:33.953309	2017-05-15 06:05:17.465439	بأربد	\N
252	4	نَـافِـعُ	---\n- 09\n	\N	3	3	10	نعت لِـ [جارٌ] مرفوع بالضمة.	2017-03-30 17:48:54.028762	2017-05-15 06:05:17.47207	نافع	\N
253	4	فَلاَ	---\n- '00'\n	\N	3	3	11	[الفاء] حرف استئناف، و [لا] نافية.	2017-03-30 17:52:37.602658	2017-05-15 06:05:17.480856	فلا	\N
254	4	جَـزِعٌ	---\n- '01'\n	\N	3	3	11	خبرُ مبتدءٍ مقدّر أي: فلا أنا جَزِعٌ.	2017-03-30 17:53:02.027152	2017-05-15 06:05:17.492311	جزع	\N
255	4	إِنْ	---\n- '02'\n	\N	3	3	11	حرف شرط جازم لفعلين مبني على السكون.	2017-03-30 17:53:29.875082	2017-05-15 06:05:17.502013	إن	\N
256	4	فَـرَّقَ	---\n- '03'\n	\N	3	3	11	فعل ماضٍ مبني على الفتح في محل جزم فعل الشرط.	2017-03-30 17:53:48.610476	2017-05-15 06:05:17.507305	فرق	\N
257	4	الدَّهْـرُ	---\n- '04'\n	\N	3	3	11	فاعل مرفوع بالضمة.	2017-03-30 17:54:26.93171	2017-05-15 06:05:17.512696	الدهر	\N
258	4	بَينَنَا	---\n- '05'\n	\N	3	3	11	[بينَ] ظرف مكان منصوب بالفتحة وهو مضاف، و [نا] مضاف إليه مبني على السكون في محل جرٍّ متعلّق بِـ [فرَّق] وجواب الشرط مقدّر أي: فلا أجزع.	2017-03-30 17:55:30.689231	2017-05-15 06:05:17.533309	بيننا	\N
259	4	وَكُـلُّ	---\n- '06'\n	\N	3	3	11	[الواو] حرف استئناف، و [كلّ] مبتدأٌ مرفوع بالضمة وهو مضاف.	2017-03-30 17:55:56.784932	2017-05-15 06:05:17.538874	وكل	\N
260	4	فَتىً	---\n- '07'\n	\N	3	3	11	مضاف إليه مجرور بالكسرة المقدّرة على الألف المحذوفة لالتقاء الساكنين: وهما الألف والتنوين.	2017-03-30 17:56:27.309007	2017-05-15 06:05:17.54341	فتى	\N
261	4	يَوماً	---\n- 08\n	\N	3	3	11	ظرف زمان منصوب بالفتحة متعلّق بِـ [فاجع].	2017-03-30 17:57:07.675913	2017-05-15 06:05:17.548699	يوما	\N
262	4	بِهِ	---\n- 09\n	\N	3	3	11	[الباء] حرف جر، و [الهاء] ضمير مبني على الكسر في محل جر وهما متعلقان بِـ [فاجع].	2017-03-30 17:57:36.853925	2017-05-15 06:05:17.553705	به	\N
263	4	الدَّهْـرُ	---\n- '10'\n	\N	3	3	11	مبتدأ ثانٍ مرفوع بالضمة.	2017-03-30 17:58:10.939282	2017-05-15 06:05:17.558587	الدهر	\N
264	4	فَـاجِـعُ	---\n- '11'\n	\N	3	3	11	خبر المبتدأ الثاني مرفوع بالضمة. والجملة الاسمية في محل رفع خبر للمبتدأ الأوّل، أي: وكلُّ فتى الدهرُ فاجعٌ به يومًا.	2017-03-30 17:58:35.816696	2017-05-15 06:05:17.563098	فاجع	\N
266	4	أَنَـا	---\n- '01'\n	\N	3	3	12	مبتدأ مبني على السكون في محل رفع.	2017-03-30 18:07:31.077968	2017-05-15 06:05:17.574574	أنا	\N
268	4	طَرِيـفٌ	---\n- '03'\n	\N	3	3	12	فاعل مؤخّر مرفوع بالضمة.	2017-03-30 18:08:18.808848	2017-05-15 06:05:17.584918	طريف	\N
269	4	بِفَرْحَـةٍ	---\n- '04'\n	\N	3	3	12	 [الباء] حرف جرّ، و [فرحة] مجرورة بالكسرة، وهما متعلقان بالفعل [يأتيني]. والجملة الفعلية [يأتيني طريفٌ] في محل رفع خبر المبتدأ.	2017-03-30 18:08:47.114375	2017-05-15 06:05:17.590161	بفرحة	\N
270	4	وَلاَ	---\n- '05'\n	\N	3	3	12	[الواو] حرف عطف، و [لا] حرف نفي.	2017-03-30 18:09:08.124242	2017-05-15 06:05:17.595391	ولا	\N
271	4	أَنَـا	---\n- '06'\n	\N	3	3	12	مبتدأ مبني على السكون في محل رفع.	2017-03-30 18:10:38.044417	2017-05-15 06:05:17.60019	أنا	\N
272	4	مِمَّا	---\n- '07'\n	\N	3	3	12	[من] حرف جر، [ما] اسم موصول بمعنى الذي.	2017-03-30 18:11:07.219607	2017-05-15 06:05:17.605248	مما	\N
273	4	أَحْـدَثَ	---\n- 08\n	\N	3	3	12	فعل ماضٍ مبني على الفتح.	2017-03-30 18:13:08.406286	2017-05-15 06:05:17.610392	أحدث	\N
274	4	الدَّهْـرُ	---\n- 09\n	\N	3	3	12	 فاعل مرفوع بالضمة. والجملة الفعلية [أحدث الدهر] صلة الموصول لا محل لها من الإعراب، والضمير الرابط محذوف، أي: من الذي أحدثه الدهرُ.	2017-03-30 18:13:48.423938	2017-05-15 06:05:17.615525	الدهر	\N
275	4	جَازِعُ	---\n- '10'\n	\N	3	3	12	خبر المبتدأ [أنا] مرفوع بالضمة. وجملة [ولا أنا مما أحدث الدهر جازع] معطوفة على الجملة الأولى في الشطر الأوّل من البيت.	2017-03-30 18:14:18.629161	2017-05-15 06:05:17.621305	جازع	\N
276	4	وَمَا	---\n- '00'\n	\N	3	3	13	[الواو] حرف استئناف، و [ما] حرف نفي.	2017-03-30 18:15:07.423792	2017-05-15 06:05:17.628461	وما	\N
277	4	النَّـاسُ	---\n- '01'\n	\N	3	3	13	مبتدأ مرفوع بالضمة.	2017-03-30 18:15:23.942503	2017-05-15 06:05:17.633875	الناس	\N
278	4	إِلاَّ	---\n- '02'\n	\N	3	3	13	أداة استثناء غير عاملة.	2017-03-30 18:15:47.399877	2017-05-15 06:05:17.638975	إلا	\N
279	4	كَالدِّيَـارِ	---\n- '03'\n	\N	3	3	13	[الكاف] حرف جر معناه التشبيه، و [الديار] مجرور بالكسرة وهما متعلقان بمحذوف خبر المبتدأ، أي: وما الناس إلا كائنون كالديارِ.	2017-03-30 18:16:07.900487	2017-05-15 06:05:17.644265	كالديار	\N
280	4	وَأَهْـلُهَا	---\n- '04'\n	\N	3	3	13	[الواو] واو الحال، و [أهلُ] مبتدأ مرفوع بالضمة وهو مضاف، والضمير [ها] مضاف إليه مبني على السكون في محل جرّ.	2017-03-30 18:16:27.999711	2017-05-15 06:05:17.648852	وأهلها	\N
281	4	بِهَا	---\n- '05'\n	\N	3	3	13	[الباء] حرف جرّ، و [ها] ضمير مبني على السكون في محل جرّ وهما متعلقان بمحذوف خبر، أي: وأهلها كائنون بها. والجملة الاسمية في محل نصب حال من الديار.	2017-03-30 18:17:05.516651	2017-05-15 06:05:17.653387	بها	\N
282	4	يَـومَ	---\n- '06'\n	\N	3	3	13	ظرف زمان منصوب بالفتحة.	2017-03-30 18:17:27.828226	2017-05-15 06:05:17.658351	يوم	\N
284	4	وَغَـدْوًا	---\n- 08\n	\N	3	3	13	[الواو] حرف عطف، و [غَدْوًا] معطوف على يوم منصوب بالفتحة. 	2017-03-30 18:18:07.758433	2017-05-15 06:05:17.667246	وغدوا	\N
285	4	بَـلاَقِـعُ	---\n- 09\n	\N	3	3	13	خبر مرفوع بالضمة لمبتدأ محذوف تقديره: وَغَدوًا هي بلاقِعُ.	2017-03-30 18:18:22.82469	2017-05-15 06:05:17.671772	بلاقع	\N
286	4	وَمَا	---\n- '00'\n	\N	3	3	14	[الواو] حرف استئناف، و [ما] حرف نفي.	2017-03-30 18:18:57.402927	2017-05-15 06:05:17.676047	وما	\N
287	4	الـمَـرْءُ	---\n- '01'\n	\N	3	3	14	مبتدأ مرفوع بالضمة.	2017-03-30 18:19:27.530598	2017-05-15 06:05:17.68084	المرء	\N
288	4	إلاَّ	---\n- '02'\n	\N	3	3	14	أداة استثناء غير عاملة.	2017-03-30 18:20:22.32278	2017-05-15 06:05:17.685652	إلا	\N
289	4	كَالشِّهَابِ	---\n- '03'\n	\N	3	3	14	[الكاف] حرف جر، و [الشهاب] مجرور بالكسرة وهما متعلقان بمحذوف خبر المبتدأ، أي: وما المرءُ إلا كائن كالشهاب.	2017-03-30 18:20:46.104855	2017-05-15 06:05:17.690459	كالشهاب	\N
290	4	وَضَوئِهِ	---\n- '04'\n	\N	3	3	14	[الواو] حرف عطف، و [ضوء] معطوف على الشهاب مجرور بالكسرة وهو مضاف، و [الهاء] مضاف إليه مبني على الكسر في محل جرّ.	2017-03-30 18:21:25.692671	2017-05-15 06:05:17.697491	وضوئه	\N
291	4	يَحُـورُ	---\n- '05'\n	\N	3	3	14	فعل مضارع مرفوع بالضمة، والفاعل مستتر تقديره [هو] يعود إلى الشهاب. والجملة الفعلية [يحورُ] في محل نصب حال من الشهاب.	2017-03-30 18:21:50.593335	2017-05-15 06:05:17.702429	يحور	\N
292	4	رَمَـاداً	---\n- '06'\n	\N	3	3	14	مفعول به منصوب بالفتحة.	2017-03-30 18:22:25.466676	2017-05-15 06:05:17.707299	رمادا	\N
293	4	بَعْـدَ	---\n- '07'\n	\N	3	3	14	ظرف زمان منصوب بالفتحة وهو مضاف.	2017-03-30 18:22:43.697565	2017-05-15 06:05:17.712452	بعد	\N
294	4	إِذْ	---\n- 08\n	\N	3	3	14	ظرف زمان ماضٍ مبني على السكون في محل جرّ مضاف إليه.	2017-03-30 18:24:00.158047	2017-05-15 06:05:17.717081	إذ	\N
295	4	هُوَ	---\n- 09\n	\N	3	3	14	مبتدأ مبني على الفتح في محل رفع.	2017-03-30 18:24:23.21728	2017-05-15 06:05:17.72227	هو	\N
296	4	سَـاطِعُ	---\n- '10'\n	\N	3	3	14	خبر المبتدأ مرفوع بالضمة. والجملة الاسمية: [هو ساطع] في محل جرّ بالإضافة إلى [إِذْ].	2017-03-30 18:24:59.48509	2017-05-15 06:05:17.727676	ساطع	\N
298	4	وَمَا	---\n- '00'\n	\N	3	3	15	[الواو] حرف استئناف، و [ما] حرف نفي.	2017-03-30 18:26:14.428552	2017-05-15 06:05:17.732452	وما	\N
299	4	البِـرُّ	---\n- '01'\n	\N	3	3	15	مبتدأ مرفوع بالضمة.	2017-03-30 18:26:37.18937	2017-05-15 06:05:17.737671	البر	\N
300	4	إِلاَّ	---\n- '02'\n	\N	3	3	15	أداة استثناء غير عاملة.	2017-03-30 18:26:58.627593	2017-05-15 06:05:17.742838	إلا	\N
301	4	مُضْمَـرَاتٌ	---\n- '03'\n	\N	3	3	15	خبر مرفوع بالضمة.	2017-03-30 18:27:17.215442	2017-05-15 06:05:17.747812	مضمرات	\N
302	4	مِنَ	---\n- '04'\n	\N	3	3	15	حرف جرّ.	2017-03-30 18:27:50.994545	2017-05-15 06:05:17.752103	من	\N
304	4	وَمَا المَـالُ إِلاَّ مُـعْـمَـرَاتٌ	---\n- '06'\n- '07'\n- 08\n- 09\n	\N	3	3	15	تعرب كإعراب [وما البرّ إلا مضمرات].	2017-03-30 18:28:37.479729	2017-05-15 06:05:17.769619	وما المال إلا معمرات	\N
305	4	وَدَائِـعُ	---\n- '10'\n	\N	3	3	15	نعت لمعمرات مرفوع بالضمة.	2017-03-30 18:29:01.528195	2017-05-15 06:05:17.778435	ودائع	\N
306	4	وَمَا المَـالُ	---\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n	\N	3	3	16	إعراب الشطر الأوّل من هذا البيت كإعراب البيت السابق.	2017-03-30 18:30:30.668945	2017-05-15 06:05:17.787161	وما المال	\N
308	4	وَلاَ	---\n- '05'\n	\N	3	3	16	[الواو] حرف استئناف، و [لا] نافية للجنس.	2017-03-30 18:31:40.740553	2017-05-15 06:05:17.802793	ولا	\N
309	4	بُـدَّ	---\n- '06'\n	\N	3	3	16	اسم لا النافية للجنس مبني على الفتح في محل نصب.	2017-03-30 18:32:12.946289	2017-05-15 06:05:17.807772	بد	\N
310	4	يَـوماً	---\n- '07'\n	\N	3	3	16	ظرف زمان منصوب بالفتحة متعلق بالفعل [تُرَدَّ].	2017-03-30 18:32:56.55701	2017-05-15 06:05:17.813285	يوما	\N
311	4	أَنْ	---\n- 08\n	\N	3	3	16	حرف مصدري ناصب للفعل المضارع.	2017-03-30 18:33:27.026712	2017-05-15 06:05:17.818981	أن	\N
312	4	تُـرَدَّ	---\n- 09\n	\N	3	3	16	فعل مضارع مبني للمجهول منصوب بالفتحة.	2017-03-30 18:33:47.711117	2017-05-15 06:05:17.824938	ترد	\N
313	4	الـوَدَائِـعُ	---\n- '10'\n	\N	3	3	16	نائب فاعل مرفوع بالضمة. والمصدر المؤول من [أنْ] و [تُرَدَّ] في محل جرّ بمن المقدّرة، والجار والمجرور متعلقان بمحذوف في محل رفع خبر [لا] النافية للجنس، أي: لا بدَّ مِنْ رَدِّ الودائع يومًا.	2017-03-30 18:34:07.363723	2017-05-15 06:05:17.830122	الودائع	\N
314	4	وَيَمْضُـونَ	---\n- '00'\n	\N	3	3	17	 [الواو] حرف استئناف، و [يمضي] فعل مضارع مرفوع بثبوت النون لأنّه من الأفعال الخمسة، و [الواو] فاعل مبني على السكون في محل رفع. 	2017-03-30 18:34:33.16476	2017-05-15 06:05:17.834918	ويمضون	\N
315	4	أَرْسَـالاً	---\n- '01'\n	\N	3	3	17	حال من فاعل [يمضون] منصوب بالفتحة، وهو مؤول بالمشتق أي: يمضون مُرَتَّبِينَ.	2017-03-30 18:34:54.475032	2017-05-15 06:05:17.839578	أرسالا	\N
316	4	وَنَخْلُفُ	---\n- '02'\n	\N	3	3	17	 [الواو] حرف عطف، و [نخلف] فعل مضارع مرفوع بالضمة، والفاعل مستتر تقديره نحن. والجملة الفعلية معطوفة على التي قبلها.	2017-03-30 18:35:13.539886	2017-05-15 06:05:17.844124	ونخلف	\N
317	4	بَعْدَهُمْ	---\n- '03'\n	\N	3	3	17	[بعد] ظرف زمان منصوب بالفتحة متعلق بِـ [نخلف] وهو مضاف، و [هم] مضاف إليه مبني على السكون في محل جرّ.	2017-03-30 18:35:36.740683	2017-05-15 06:05:17.849129	بعدهم	\N
318	4	كَمَا	---\n- '04'\n	\N	3	3	17	[الكاف] حرف جرّ معناه التشبيه، و [ما] حرف مصدري مبني على السكون لا محل له من الإعراب.	2017-03-30 18:35:54.039585	2017-05-15 06:05:17.854768	كما	\N
319	4	ضَـمَّ	---\n- '05'\n	\N	3	3	17	فعل ماض مبني على الفتح.	2017-03-30 18:36:14.972641	2017-05-15 06:05:17.859365	ضم	\N
320	4	أُخْرَى	---\n- '06'\n	\N	3	3	17	مفعول به مقدّم منصوب بالفتحة المقدّرة على الألف، وهو مضاف.	2017-03-30 18:36:42.144674	2017-05-15 06:05:17.864154	أخرى	\N
321	4	التَّالِيَـاتِ	---\n- '07'\n	\N	3	3	17	مضاف إليه مجرور بالكسرة.	2017-03-30 18:37:00.710075	2017-05-15 06:05:17.868888	التاليات	\N
323	4	وَمَا النَّـاسُ إِلاَّ عَـامِـلاَنِ	---\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n	\N	3	3	18	كما تقدم مثيله في الأبيات السابقة، وعاملان مرفوع بالألف لأنّه مثنى.	2017-03-30 18:38:07.833189	2017-05-15 06:05:17.878389	وما الناس إلا عاملان	\N
324	4	فَعَـامِلٌ	---\n- '04'\n	\N	3	3	18	[الفاء] زائدة تفسيرية لِـ [عاملان]، و [عامل] بدل من [عاملان] مرفوع بالضمة.	2017-03-30 18:38:28.843202	2017-05-15 06:05:17.884307	فعامل	\N
325	4	يُتَبِّـرُ	---\n- '05'\n	\N	3	3	18	فعل مضارع مرفوع بالضمة، والفاعل مستتر تقديره [هو] يعود إلى العامل. والجملة الفعلية في محل رفع نعت لِـ [عامل] لأنّ الجمل بعد النكرات صفات.	2017-03-30 18:39:01.474747	2017-05-15 06:05:17.890192	يتبر	\N
326	4	مَـا	---\n- '06'\n	\N	3	3	18	اسم موصول بمعنى الذي مفعول به لِـ [يتبِّرُ] مبني على السكون في محل نصب. 	2017-03-30 18:39:22.824878	2017-05-15 06:05:17.896609	ما	\N
327	4	يَبْنِي	---\n- '07'\n	\N	3	3	18	فعل مضارع مرفوع بالضمة المقدّرة على الياء، والفاعل مستتر تقديره [هو]. والجملة الفعلية صلة الموصول لا محل لها من الإعراب، والرابط ضمير المفعول به المقدّر أي: ما يبنيه.	2017-03-30 18:39:53.185637	2017-05-15 06:05:17.901608	يبني	\N
328	4	وَآخَـرُ	---\n- 08\n	\N	3	3	18	[الواو] حرف عطف، و [آخر] معطوف على [عاملٌ] مرفوع بالضمة، أو هو نعت لمنعوت محذوف تقديره: وعاملٌ آخر رافعُ. 	2017-03-30 18:40:11.816277	2017-05-15 06:05:17.906171	وآخر	\N
329	4	رَافِـعُ	---\n- 09\n	\N	3	3	18	نعت لآخر مرفوع بالضمة.	2017-03-30 18:40:29.158282	2017-05-15 06:05:17.911582	رافع	\N
330	4	فَمِنْـهُمْ	---\n- '00'\n	\N	3	3	19	[الفاء] حرف استئناف، و [من] حرف جرّ معناه التبعيض، و [هم] ضمير مبني على السكون في محل جرّ وهما متعلقان بمحذوف في محل رفع خبر مقدّم. 	2017-03-30 18:41:24.591424	2017-05-15 06:05:17.916303	فمنهم	\N
331	4	سَعِيـدٌ	---\n- '01'\n	\N	3	3	19	نعت مرفوع بالضمة لمنعوت مقدّر وهو المبتدأ المؤخّر أي: إنسان سعيد كائنٌ منهم.	2017-03-30 18:41:43.770178	2017-05-15 06:05:17.9216	سعيد	\N
332	4	آخِـذٌ	---\n- '02'\n	\N	3	3	19	نعت ثانٍ مرفوع بالضمة، وفاعله ضمير مستتر تقديره [هو] يعود إلى الإنسان.	2017-03-30 18:42:09.322354	2017-05-15 06:05:17.927863	آخذ	\N
333	4	لِنَصِيـبِـهِ	---\n- '03'\n	\N	3	3	19	[اللام] حرف جرّ زائد، و [نصيب] مجرور بالكسرة لفظًا منصوب محلاً لأنّه مفعول به لآخذ وهو مضاف، و [الهاء] مضاف إليه مبني على الكسر في محل جرّ.	2017-03-30 18:42:34.076694	2017-05-15 06:05:17.93267	لنصيبه	\N
335	4	بِالمَعِيـشَةِ	---\n- '06'\n	\N	3	3	19	[الباء] حرف جر، و [المعيشة] مجرور بالكسرة، وهما متعلقان بِـ [قانع]. 	2017-03-30 18:43:28.258697	2017-05-15 06:05:17.945534	بالمعيشة	\N
336	4	قَـانِـعُ	---\n- '07'\n	\N	3	3	19	نعت ثانٍ مرفوع بالضمة.	2017-03-30 18:43:51.124318	2017-05-15 06:05:17.954612	قانع	\N
338	4	وَرَائِي	---\n- '01'\n	\N	3	3	20	 [وراء] ظرف مكان منصوب بالفتحة المقدّرة منع من ظهورها كسرة ياء المتكلّم، وهو مضاف، و [الياء] مضاف إليه مبنية على السكون في محل جرّ. والظرف متعلق بمحذوف في محل نصب خبر ليس، أي: أليس لزوم العصا كائنٍا ورائي؟.	2017-03-30 18:45:15.019724	2017-05-15 06:05:17.96992	ورائي	\N
339	4	إِنْ	---\n- '02'\n	\N	3	3	20	حرف شرط جازم لفعلين.	2017-03-30 18:45:39.826422	2017-05-15 06:05:17.97472	إن	\N
340	4	تَـرَاخَـتْ	---\n- '03'\n	\N	3	3	20	[تراخَى] فعل ماضٍ مبني على الفتح المقدّر على الألف المحذوف لالتقاء الساكنين في محل جزم فعل الشرط، و [التاء] تاء التأنيث الساكنة لا محل لها من الإعراب.	2017-03-30 18:46:06.130372	2017-05-15 06:05:17.979035	تراخت	\N
342	4	لُـزُومُ	---\n- '05'\n	\N	3	3	20	اسم ليس مؤخّر مرفوع بالضمة وهو مضاف.	2017-03-30 18:47:00.747559	2017-05-15 06:05:17.988307	لزوم	\N
343	4	العَـصَا	---\n- '06'\n	\N	3	3	20	مضاف إليه مجرور بكسرة مقدّرة على الألف.	2017-03-30 18:48:06.965358	2017-05-15 06:05:17.99383	العصا	\N
344	4	تُحْنَى	---\n- '07'\n	\N	3	3	20	فعل مضارع مبني للمجهول مرفوع بالضمة المقدّرة على الألف.	2017-03-30 18:48:25.806475	2017-05-15 06:05:17.998571	تحنى	\N
345	4	عَلَيـهَا	---\n- 08\n	\N	3	3	20	[على] حرف جرّ، و [ها] ضمير مبني على السكون في محل جرّ، وهما متعلقان بالفعل [تُحنى].	2017-03-30 18:48:51.52401	2017-05-15 06:05:18.003153	عليها	\N
346	4	الأَصَابِعُ	---\n- 09\n	\N	3	3	20	نائب فاعل مرفوع بالضمة. والجملة الفعلية [تحنى عليها الأصابع] في محل نصب حال من العصا.	2017-03-30 18:49:21.630716	2017-05-15 06:05:18.008252	الأصابع	\N
347	4	أُخَبِّرُ	---\n- '00'\n	\N	3	3	21	فعل مضارع مرفوع بالضمة، والفاعل مستتر وجوبًا تقديره [أنا].	2017-03-30 18:49:55.34293	2017-05-15 06:05:18.013214	أخبر	\N
348	4	أَخْبَـارَ	---\n- '01'\n	\N	3	3	21	مفعول به منصوب بالفتحة وهو مضاف.	2017-03-30 18:50:14.481219	2017-05-15 06:05:18.018927	أخبار	\N
349	4	القُـرُونِ	---\n- '02'\n	\N	3	3	21	 مضاف إليه مجرور بالكسرة. والجملة الفعلية في محل نصب حال من ياء المتكلّم في قوله [ورائي] في البيت السابق، أي: حال كوني أخبر أخبار القرون .	2017-03-30 18:50:41.139174	2017-05-15 06:05:18.023744	القرون	\N
350	4	الَّتِي	---\n- '03'\n	\N	3	3	21	اسم موصول مبني على السكون في محل جرّ عطف بيان من [القرون].	2017-03-30 18:50:58.011114	2017-05-15 06:05:18.02932	التي	\N
351	4	مَضَـتْ	---\n- '04'\n	\N	3	3	21	[مضى] فعل ماضٍ مبني على الفتح المقدّر على الألف المحذوفة لالتقاء الساكنين، و [التاء] تاء التأنيث الساكنة لا محل لها من الإعراب، والفاعل مستتر تقديره [هي] يعود إلى [القرون]. والجملة الفعلية لا محل لها من الإعراب صلة الموصول.	2017-03-30 18:51:18.750886	2017-05-15 06:05:18.034033	مضت	\N
352	4	أَدِبُّ	---\n- '05'\n	\N	3	3	21	فعل مضارع مرفوع بالضمة، والفاعل مستتر وجوبًا تقديره [أنا]. والجملة الفعلية في محل نصب حال ثان من ياء المتكلّم في قوله [ورائي] في البيت السابق.	2017-03-30 18:52:01.136429	2017-05-15 06:05:18.04202	أدب	\N
353	4	كَـأَنِّي	---\n- '06'\n	\N	3	3	21	[كأنّ] من الحروف المشبهة بالفعل تفيد التشبيه، و [الياء] ياء المتكلّم اسم [كأنَّ] مبني على السكون في محل نصب.	2017-03-30 18:52:23.741421	2017-05-15 06:05:18.046939	كأني	\N
354	4	كُـلَّمَا	---\n- '07'\n	\N	3	3	21	[كلَّ] ظرف زمان ملازم للنصب بالفتحة وهو مضاف، و [ما] اسم نكرة بمعنى وقت مضاف إليه مبنية على السكون في محل جرّ أي: كلّ وقت مضى.	2017-03-30 18:52:58.365925	2017-05-15 06:05:18.051396	كلما	\N
355	4	قُـمْتُ	---\n- 08\n	\N	3	3	21	[قام] فعل ماضٍ مبني على السكون لاتصاله بضمير الرفع، و [التاء] فاعل مبني على الضم في محل رفع. وجملة [قمت] في محل جرّ نعت لِـ [ما]. وجملة [كلما قمت] لا محل لها من الإعراب اعتراضية بين اسم كأنَّ وخبرها. ويجوز إعراب [ما] على أنّها حرف مصدري دال على الظرفية الزمانية لا محل لها من الإعراب. وجملة [قمت] صلة الموصول الحرفي لا محل لها من الإعراب، أي: كلَّ وقتِ قيامٍ.	2017-03-30 18:53:33.92527	2017-05-15 06:05:18.055929	قمت	\N
356	4	رَاكِـعُ	---\n- 09\n	\N	3	3	21	خبر كأنَّ مرفوع بالضمة.	2017-03-30 18:54:14.167643	2017-05-15 06:05:18.063109	راكع	\N
357	4	فَأَصْبَحْـتُ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n	\N	3	3	22	[الفاء] حرف استئناف، و [أصبح] فعل ماضٍ ناقص من أخوات كان، مبني على السكون لاتصاله بضمير الرفع، و [التاء] اسم أصبح مبني على الضم في محل رفع. 	2017-03-30 18:54:42.936225	2017-05-15 06:05:18.068447	فأصبحت	\N
358	4	مِثْـلَ	---\n- '01'\n	\N	3	3	22	خبر أصبح منصوب بالفتحة وهو مضاف.	2017-03-30 18:55:00.844543	2017-05-15 06:05:18.074971	مثل	\N
359	4	السَّيفِ	---\n- '02'\n	\N	3	3	22	مضاف إليه مجرور بالكسرة.	2017-03-30 18:55:34.302275	2017-05-15 06:05:18.080323	السيف	\N
360	4	غَيَّرَ	---\n- '03'\n	\N	3	3	22	فعل ماضٍ مبني على الفتح. 	2017-03-30 18:55:56.35552	2017-05-15 06:05:18.084883	غير	\N
361	4	جَفْنَهُ	---\n- '04'\n	\N	3	3	22	[جفنَ] مفعول به مقدّم منصوب بالفتحة وهو مضاف، و [الهاء] مضاف إليه مبني على الضم في محل جرّ.	2017-03-30 18:56:20.973156	2017-05-15 06:05:18.090026	جفنه	\N
363	4	عَهْـدِ	---\n- '06'\n	\N	3	3	22	مضاف إليه مجرور بالكسرة وهو مضاف.	2017-03-30 18:57:26.610465	2017-05-15 06:05:18.104916	عهد	\N
365	4	وَالنَّصْلُ	---\n- 08\n	\N	3	3	22	[الواو] واو الحال، و [النصل] مبتدأ مرفوع بالضمة.	2017-03-30 18:58:10.927903	2017-05-15 06:05:18.115089	والنصل	\N
366	4	قَاطِعُ	---\n- 09\n	\N	3	3	22	خبر مرفوع بالضمة. والجملة الاسمية [والنصل قاطع] في محل نصب حال ثانٍ من السيف.	2017-03-30 18:58:41.031527	2017-05-15 06:05:18.121994	قاطع	\N
367	4	فَلاَ	---\n- '00'\n	\N	3	3	23	[الفاء] حرف استئناف، و [لا] ناهية جازمة.	2017-03-30 18:59:09.54042	2017-05-15 06:05:18.127896	فلا	\N
368	4	تَبْعَـدَنْ	---\n- '01'\n	\N	3	3	23	[تبعد] فعل مضارع مبني على الفتح في محل جزم، و [النون] نون التأكيد الخفيفة مبنية على السكون، والفاعل مستتر تقديره [أنت].	2017-03-30 18:59:38.318733	2017-05-15 06:05:18.132747	تبعدن	\N
369	4	إِنَّ	---\n- '02'\n	\N	3	3	23	من الحروف المشبهة بالفعل تفيد التاكيد.	2017-03-30 19:00:09.753347	2017-05-15 06:05:18.140935	إن	\N
370	4	المَنِـيَّـةَ	---\n- '03'\n	\N	3	3	23	اسم إنَّ منصوب بالفتحة.	2017-03-30 19:00:30.261786	2017-05-15 06:05:18.150574	المنية	\N
371	4	مَـوعِـدٌ	---\n- '04'\n	\N	3	3	23	خبر إنَّ مرفوع بالضمة.	2017-03-30 19:00:56.178801	2017-05-15 06:05:18.156334	موعد	\N
372	4	عَلَيـكَ	---\n- '05'\n	\N	3	3	23	[على] حرف جرّ، و [الكاف] ضمير مبني على الفتح في محل جرّ وهما متعلقان بِـ [موعد].	2017-03-30 19:01:14.28854	2017-05-15 06:05:18.162833	عليك	\N
374	4	للِطُّلُـوعِ	---\n- '07'\n	\N	3	3	23	[اللام] حرف جر، و [الطلوع] مجرور بالكسرة وهما متعلقان باسم الفاعل [دانٍ].	2017-03-30 19:01:58.192096	2017-05-15 06:05:18.174095	للطلوع	\N
375	4	وَطَـالِـعُ	---\n- 08\n	\N	3	3	23	[الواو] حرف عطف، و [طالعُ] معطوف على دانٍ مرفوع بالضمة أي: وإنسانٌ طالعٌ.	2017-03-30 19:02:26.454038	2017-05-15 06:05:18.179374	وطالع	\N
376	4	أَعَـاذِلَ	---\n- '00'\n	\N	3	3	24	 [الهمزة] حرف نداء مثل "يا"، و [عاذلَ] منادى منصوب بفتحة مقدّرة على التاء المحذوفة للترخيم، والأصل: يا عاذلةً، وأبقى اللام مفتوحة على لغة من ينتظر كما هو معروف في باب ترخيم المنادى.	2017-03-30 19:03:40.671457	2017-05-15 06:05:18.18453	أعاذل	\N
377	4	مَـا	---\n- '01'\n	\N	3	3	24	اسم استفهام مبتدأ مبني على السكون في محل رفع.	2017-03-30 19:04:01.275145	2017-05-15 06:05:18.189718	ما	\N
378	4	يُـدْرِيـكِ	---\n- '02'\n	\N	3	3	24	[يدرى] فعل مضارع مرفوع بالضمة المقدّرة على الياء، و [الكاف] مفعول به مبني على الكسر في محل نصب، والفاعل ضمير مستتر تقديره [هو] عائد إلى [ما]. وجملة [يدريك] في محل رفع خبر المبتدأ [ما].	2017-03-30 19:04:34.735588	2017-05-15 06:05:18.194252	يدريك	\N
379	4	إِلاَّ	---\n- '03'\n	\N	3	3	24	حرف استثناء.	2017-03-30 19:04:59.977731	2017-05-15 06:05:18.198806	إلا	\N
380	4	تَظَنِّـياً	---\n- '04'\n	\N	3	3	24	مستثنى منصوب بالفتحة.	2017-03-30 19:05:45.454936	2017-05-15 06:05:18.206646	تظنيا	\N
381	4	إِذَا	---\n- '05'\n	\N	3	3	24	ظرف للمستقبل مُتضمِّنَةً معنى الشرط غير جازمة مضافةٌ إلى شرطها منصوبةٌ بجوابها مبنية على السكون في محل نصب.	2017-03-30 19:06:12.565978	2017-05-15 06:05:18.211169	إذا	\N
382	4	ارْتَـحَلَ	---\n- '06'\n	\N	3	3	24	فعل ماضٍ مبني على الفتح، فعل الشرط لِـ [إذا].	2017-03-30 19:06:45.44124	2017-05-15 06:05:18.216518	ارتحل	\N
383	4	الفِتْـيَانُ	---\n- '07'\n	\N	3	3	24	فاعل مرفوع بالضمة. وجملة [ارتحل الفتيان] في محل جرّ مضاف إليه. وجواب إذا مقدّر، أي: إذا ارتحل الفتيان يدريك ظنًّا من هو راجع].	2017-03-30 19:07:12.696523	2017-05-15 06:05:18.221663	الفتيان	\N
384	4	مَـنْ	---\n- 08\n	\N	3	3	24	اسم موصول بمعنى الذي مبنية على السكون في محل نصب مفعول ثان [ليدريك].	2017-03-30 19:07:34.218148	2017-05-15 06:05:18.226596	من	\N
385	4	هُوَ	---\n- 09\n	\N	3	3	24	مبتدأ مبني على الفتح في محل رفع.	2017-03-30 19:07:58.538789	2017-05-15 06:05:18.231309	هو	\N
386	4	رَاجِعُ	---\n- '10'\n	\N	3	3	24	خبر مرفوع بالضمة. والجملة الاسمية [هو راجع] لا محل لها من الإعراب صلة الموصول.	2017-03-30 19:08:18.879913	2017-05-15 06:05:18.235798	راجع	\N
387	4	تُبَـكِّي	---\n- '00'\n	\N	3	3	25	فعل مضارع مضعّف العين مرفوع بالضمة المقدّرة على الياء للثقل، والفاعل مستتر تقديره [هي] يعود إلى العاذلة المذكورة في البيت السابق. 	2017-03-30 19:09:04.22335	2017-05-15 06:05:18.242075	تبكي	\N
388	4	عَلَى	---\n- '01'\n	\N	3	3	25	حرف جرّ.	2017-03-30 19:09:24.648547	2017-05-15 06:05:18.248372	على	\N
389	4	إِثْرِ	---\n- '02'\n	\N	3	3	25	مجرور بالكسرة وهما متعلقان بالفعل [تُبكّي] وهو مضاف.	2017-03-30 19:09:53.305954	2017-05-15 06:05:18.2533	إثر	\N
390	4	الشَّبَـابِ	---\n- 09\n	\N	3	3	25	مضاف إليه مجرور بالكسرة.	2017-03-30 19:10:49.272707	2017-05-15 06:05:18.258142	الشباب	\N
391	4	الَّذِي	---\n- '04'\n	\N	3	3	25	اسم موصول مبني على السكون.	2017-03-30 19:11:15.48314	2017-05-15 06:05:18.263307	الذي	\N
392	4	مَضَى	---\n- '05'\n	\N	3	3	25	فعل ماض مبني على فتح مقدّر على الألف للتعذّر، والفاعل مستتر تقديره [هو] يعود إلى [الشباب]. والجملة الفعلية لا محل لها من الإعراب صلة الموصول. وجملة [الذي مضى] في محل نصب حال من [الشباب].	2017-03-30 19:11:45.603871	2017-05-15 06:05:18.26869	مضى	\N
393	4	أَلاَ	---\n- '06'\n	\N	3	3	25	حرفُ استفتاح مبني على السكون لا محل له من الإعراب.	2017-03-30 19:12:13.154609	2017-05-15 06:05:18.275383	ألا	\N
394	4	إِنَّ	---\n- '07'\n	\N	3	3	25	حرفُ استفتاح مبني على السكون لا محل له من الإعراب.	2017-03-30 19:13:33.375734	2017-05-15 06:05:18.280767	إن	\N
395	4	أَخْـدَانَ	---\n- 08\n	\N	3	3	25	 اسم إنّ منصوب بالفتحة وهو مضاف.	2017-03-30 19:13:51.891045	2017-05-15 06:05:18.285841	أخدان	\N
396	4	الشَّبَـابِ	---\n- 09\n	\N	3	3	25	مضاف إليه مجرور بالكسرة.	2017-03-30 19:14:28.761242	2017-05-15 06:05:18.292624	الشباب	\N
399	4	مِمَّا	---\n- '01'\n	\N	3	3	26	[من] حرف جرّ معناها السبب، و [ما] اسم موصول بمعنى الذي مبني على السكون في محل جرّ وهما متعلقان بالفعل [تجزع].	2017-03-30 19:15:44.758518	2017-05-15 06:05:18.315198	مما	\N
400	4	أَحْـدَثَ	---\n- '02'\n	\N	3	3	26	فعل ماض مبني على الفتح.	2017-03-30 19:16:13.318086	2017-05-15 06:05:18.322292	أحدث	\N
401	4	الدَّهْـرُ	---\n- '03'\n	\N	3	3	26	فاعل مرفوع بالضمة. والجملة الفعلية لا محل لها من الإعراب صلة الموصول، والرابط محذوف تقديره: أحدثَهُ.	2017-03-30 19:17:29.793539	2017-05-15 06:05:18.329092	الدهر	\N
402	4	بِالفَتَى	---\n- '04'\n	\N	3	3	26	 [الباء] حرف جرّ، و [الفتى] مجرور بالكسرة المقدّرة على الألف للتعذّر وهما متعلقان بالفعل [أحْدَثَ].	2017-03-30 19:17:53.318328	2017-05-15 06:05:18.334552	بالفتى	\N
403	4	وَأَيُّ	---\n- '05'\n	\N	3	3	26	[الواو] حرف استئناف، و [أَيُّ] اسم استفهام مبتدأ مبني على الضم في محل رفع، وهي مضاف.	2017-03-30 19:18:17.353871	2017-05-15 06:05:18.33969	وأي	\N
404	4	كَـرِيمٍ	---\n- '06'\n	\N	3	3	26	مضاف إليه مجرور بالكسرة، وأصله نعت لمنعوت محذوف، أي: وأيُّ إنسان كريم.	2017-03-30 19:18:45.769024	2017-05-15 06:05:18.346219	كريم	\N
405	4	لَـمْ	---\n- '07'\n	\N	3	3	26	حرف نفي وقلب وجزم.	2017-03-30 19:19:13.327531	2017-05-15 06:05:18.351118	لم	\N
406	4	تُصِبْـهُ	---\n- 08\n	\N	3	3	26	[تصب] فعل مضارع مجزوم بالسكون، و [الهاء] ضمير مفعول به مبني على الضم في محل نصب.	2017-03-30 19:19:35.745313	2017-05-15 06:05:18.355988	تصبه	\N
407	4	القَـوَارِعُ	---\n- 09\n	\N	3	3	26	فاعل مرفوع بالضمة. وجملة [لم تصبه القوارع] في محل رفع خبر [أَيُّ].	2017-03-30 19:20:10.697652	2017-05-15 06:05:18.361061	القوارع	\N
409	4	مَا	---\n- '01'\n	\N	3	3	27	حرف نفي غير عاملة.	2017-03-30 19:20:57.955683	2017-05-15 06:05:18.395392	ما	\N
410	4	تَـدْرِي	---\n- '02'\n	\N	3	3	27	فعل مضارع مرفوع بالضمة المقدّرة على الياء للثقل.	2017-03-30 19:21:27.482702	2017-05-15 06:05:18.400527	تدري	\N
411	4	الضَّوَارِبُ	---\n- '03'\n	\N	3	3	27	فاعل مرفوع بالضمة.	2017-03-30 19:21:59.700367	2017-05-15 06:05:18.405142	الضوارب	\N
412	4	بِالحَصَى	---\n- '04'\n	\N	3	3	27	[الباء] حرف جرّ، و [الحصى] مجرور بالكسرة المقدّرة على الألف للتعذّر.	2017-03-30 19:22:18.485259	2017-05-15 06:05:18.409632	بالحصى	\N
413	4	وَلاَ	---\n- '05'\n	\N	3	3	27	[الواو] حرف عطف، و [لا] حرف نفي غير عامل.	2017-03-30 19:22:35.762786	2017-05-15 06:05:18.41538	ولا	\N
414	4	زَاجِـرَاتُ	---\n- '06'\n	\N	3	3	27	فاعل مرفوع بالضمة لفعل مقدّر، أي: ولا تدري زاجرات، وهو مضاف.	2017-03-30 19:22:52.387716	2017-05-15 06:05:18.420539	زاجرات	\N
415	4	الطَّيرِ	---\n- '07'\n	\N	3	3	27	مضاف إليه مجرور بالكسرة. والجملة الفعلية المنفية معطوفة على التي قبلها.	2017-03-30 19:23:10.534281	2017-05-15 06:05:18.425083	الطير	\N
416	4	مَا	---\n- 08\n	\N	3	3	27	اسم موصول بمعنى الذي مبني على السكون في محل نصب مفعول به لِـ [تدري].	2017-03-30 19:23:28.552432	2017-05-15 06:05:18.429898	ما	\N
417	4	اللّهُ	---\n- 09\n	\N	3	3	27	مبتدأ مرفوع بالضمة.	2017-03-30 19:23:49.002579	2017-05-15 06:05:18.434471	الله	\N
418	4	صَـانِـعُ	---\n- '10'\n	\N	3	3	27	خبر مرفوع بالضمة. والجملة الاسمية لا محل لها من الإعراب صلة الموصول. والرابط مقدّر، أي: ما الله صانعه.	2017-03-30 19:24:12.27852	2017-05-15 06:05:18.439	صانع	\N
419	4	سَلُوهُنَّ	---\n- '00'\n	\N	3	3	28	[سل] فعل أمر مبني على حذف النون، و [الواو] فاعل مبني على الضم في محل رفع، و [هُنّ] ضمير المؤنثات مفعول به مبني على الفتح في محل نصب يعود إلى [الضوارب] و [الزاجرات] في البيت السابق.  	2017-03-30 19:24:36.056478	2017-05-15 06:05:18.443472	سلوهن	\N
420	4	إِنْ	---\n- '01'\n	\N	3	3	28	حرف شرط جازم لفعلين.	2017-03-30 19:24:51.883498	2017-05-15 06:05:18.448174	إن	\N
422	4	مَتَـى	---\n- '03'\n	\N	3	3	28	اسم استفهام مبتدأ مبني على السكون في محل رفع.	2017-03-30 19:25:35.864254	2017-05-15 06:05:18.457078	متى	\N
423	4	الفَتَى	---\n- '04'\n	\N	3	3	28	مبتدأ ثانٍ مرفوع بالضمة المقدّر على الألف للتعذّر.	2017-03-30 19:25:54.228277	2017-05-15 06:05:18.461616	الفتى	\N
424	4	يَذُوقُ	---\n- '05'\n	\N	3	3	28	فعل مضارع مرفوع بالضمة، والفاعل مستتر تقديره [هو] يعود إلى [الفتى]. والجملة في محل رفع خبر المبتدأ الثاني وهو وخبره خبر [متى].	2017-03-30 19:26:21.822602	2017-05-15 06:05:18.466036	يذوق	\N
425	4	المَنَـايَا	---\n- '06'\n	\N	3	3	28	مفعول به منصوب بالفتحة المقدّرة على الألف للتعذّر.	2017-03-30 19:26:38.637455	2017-05-15 06:05:18.470306	المنايا	\N
426	4	أَو	---\n- '07'\n	\N	3	3	28	حرف عطف.	2017-03-30 19:26:56.393727	2017-05-15 06:05:18.474641	أو	\N
427	4	مَتَـى	---\n- 08\n	\N	3	3	28	كإعراب [متى] الأولى	2017-03-30 19:27:13.759771	2017-05-15 06:05:18.479219	متى	\N
428	4	الغَيـثُ	---\n- 09\n	\N	3	3	28	مبتدأ ثانٍ مرفوع بالضمة.	2017-03-30 19:27:37.538767	2017-05-15 06:05:18.484815	الغيث	\N
430	4	وَاقِعُ	---\n- '10'\n	\N	3	3	28	خبر المبتدأ الثاني مرفوع بالضمة. والجملة من المبتدأ وخبره في محل رفع خبر [متى].	2017-03-30 19:28:42.005747	2017-05-15 06:05:18.489562	واقع	\N
431	3	أَتَانِي	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n	\N	6	6	31	وصلني وبلغني الخبر	2017-05-13 04:46:14.000562	2017-05-15 06:05:18.495053	أتاني	\N
433	3	لُمْتَنِـي	---\n- '04'\n	\N	6	6	31	من اللوم وهو العذلُ.	2017-05-13 04:47:42.028508	2017-05-15 06:05:18.508831	لمتني	\N
434	3	أُهتَمُّ مِنْهَا	---\n- 08\n- 09\n	\N	6	6	31	أُحْزَنُ منها.	2017-05-13 04:48:09.355231	2017-05-15 06:05:18.514251	أهتم منها	\N
435	3	أَنْصَـبُ	---\n- '10'\n	\N	6	6	31	أتعب	2017-05-13 04:48:32.31695	2017-05-15 06:05:18.520344	أنصب	\N
437	4	أَبَيتَ	---\n- '01'\n	\N	6	6	31	[أبى] فعل ماضٍ مبني على السكون لاتصاله بضمير الرفع، و [التاء] فاعل مبني على الفتح في محل رفع.	2017-05-13 04:49:52.214074	2017-05-15 06:05:18.535355	أبيت	\N
438	4	اللَّعْنَ	---\n- '02'\n	\N	6	6	31	مفعول به منصوب بالفتحة. وجملة [أبيت اللعن] اعتراضية لا محل لها من الإعراب.	2017-05-13 04:50:15.82711	2017-05-15 06:05:18.540067	اللعن	\N
439	4	أَنَّـكَ	---\n- '03'\n	\N	6	6	31	[أنّ] من الحروف المشبهة بالفعل، و [الكاف] اسمها مبني على الفتح في محل نصب.	2017-05-13 04:50:54.992733	2017-05-15 06:05:18.544811	أنك	\N
440	4	لُمْتَنِـي	---\n- '04'\n	\N	6	6	31	[لامَ] فعل ماضٍ مبني على السكون لاتصاله بضمير الرفع، و [التاء] فاعل مبني على الفتح في محل رفع، و [النون] نون الوقاية، و [الياء] مفعول به مبني على السكون في محل نصب، و [أنَّ] تسبك مع خبرها بمصدر لتكون فاعل الفعل الأوّل أي: أتاني لومُكَ. 	2017-05-13 04:51:18.547522	2017-05-15 06:05:18.549103	لمتني	\N
441	4	وَتِلْكَ	---\n- '06'\n	\N	6	6	31	[الواو] حرف استئناف، و [تلك] اسم إشارة مبتدأ مبني على الفتح في محل رفع.	2017-05-13 04:53:10.154557	2017-05-15 06:05:18.553664	وتلك	\N
442	4	الَّتِي	---\n- '07'\n	\N	6	6	31	اسم موصول مبني على السكون.	2017-05-13 04:53:42.160712	2017-05-15 06:05:18.558224	التي	\N
443	4	أُهتَمُّ مِنْهَا	---\n- 08\n- 09\n	\N	6	6	31	فعل مضارع مبني للمجهول مرفوع بالضمة، و [منها] جار ومجرور في محل رفع نائب الفاعل. وجملة [أهتم منها] لا محل لها من الإعراب صلة الموصول. وجملة [التي أهتم منها] في محل رفع خبر المبتدأ [تلك].	2017-05-13 04:54:05.816634	2017-05-15 06:05:18.562997	أهتم منها	\N
444	4	وأَنْصَـبُ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n	\N	6	6	31	[الواو] حرف عطف، و [أنصب] فعل مضارع مرفوع بالضمة، والفاعل مستتر تقديره [أنا]. وجملة [وأنصب] معطوفة على [أهتم].	2017-05-13 04:54:27.998689	2017-05-15 06:05:18.570429	وأنصب	\N
446	3	العَائِـدَاتِ	---\n- '02'\n	\N	6	6	32	جمع مفردُهُ عائدة، وهنّ زائرات المريض ويعني الشاعر أنهن زرنه وهو مريض.	2017-05-13 05:04:08.842784	2017-05-15 06:05:18.58602	العائدات	\N
447	3	فَرَشْنَنِـي	---\n- '03'\n	\N	6	6	32	بسطنَ لي	2017-05-13 05:04:29.684748	2017-05-15 06:05:18.590384	فرشنني	\N
448	3	هَرَاساً	---\n- '05'\n	\N	6	6	32	 جمع مفردُهُ هَرَاسَةٌ وهي نوع من أنواع الشجر الذي فيه شوك كبير وكثير.	2017-05-13 05:04:48.783067	2017-05-15 06:05:18.595359	هراسا	\N
449	3	يُعلَى	---\n- '07'\n	\N	6	6	32	يُرْفَعُ، يقال: علا يعلو إذا ارتفع.	2017-05-13 05:05:14.003901	2017-05-15 06:05:18.599973	يعلى	\N
450	3	فِرَاشِي	---\n- 08\n	\N	6	6	32	الفراش ما يبسط ليجلس عليه الإنسان أو ينام وهو بمعنى مفروش. 	2017-05-13 05:05:42.85561	2017-05-15 06:05:18.605381	فراشي	\N
451	3	يُقْشَبُ	---\n- 09\n	\N	6	6	32	يمزج يُخلط ويوضع لي من جديد.	2017-05-13 05:06:18.227015	2017-05-15 06:05:18.609817	يقشب	\N
452	4	فَبِتُّ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n	\N	6	6	32	[الفاء] حرف عطف فيه معنى السببية، و [بات] فعل ماضٍ مبني على السكون لاتصاله بضمير الرفع وهي من أخوات [كان]، و [التاء] اسمها مبني على الضم في محل رفع، والفعل معطوف على [أهتم] في البيت السابق، وسيأتي خبر [بات]. 	2017-05-13 05:07:01.567472	2017-05-15 06:05:18.614955	فبت	\N
453	4	كَأَنَّ	---\n- '01'\n	\N	6	6	32	من الحروف المشبهة بالفعل معناها التشبيه.	2017-05-13 05:07:20.478769	2017-05-15 06:05:18.621191	كأن	\N
454	4	العَائِـدَاتِ	---\n- '02'\n	\N	6	6	32	اسم [كأنّ] منصوب بالكسرة نيابة عن الفتحة لأنّه جمع مذكر سالم.	2017-05-13 05:07:43.610725	2017-05-15 06:05:18.625929	العائدات	\N
455	4	فَرَشْنَنِـي	---\n- '03'\n	\N	6	6	32	[فرش] فعل ماضٍ مبني على السكون لاتصاله بنون النسوة، و [النون الأولى] نون النسوة فاعل مبنية على الفتح في محل رفع، و [النون الثانية] نون الوقاية حرف لا محل له من الإعراب، و [الياء] ياء المتكلم مبنية على السكون في محل نصب بنزع الخافض وأصلها في محل جر باللام أي: فَرَشْنَ لِي. وجملة [فرشنني] في محل رفع خبر [:أنَّ]. وجملة [كأنّ العائدات فرشنني] في محل نصب خبر [بات].	2017-05-13 05:08:03.520143	2017-05-15 06:05:18.630512	فرشنني	\N
456	4	هَرَاساً	---\n- '05'\n	\N	6	6	32	مفعول به منصوب بالفتحة للفعل [فرش].	2017-05-13 05:08:25.139555	2017-05-15 06:05:18.634761	هراسا	\N
457	4	بِهِ	---\n- '06'\n	\N	6	6	32	[الباء] حرف جر، و [الهاء] مبني على الكسر في محل جرّ وهما متعلقان بالفعل بعده [يُعلى].	2017-05-13 05:08:58.113325	2017-05-15 06:05:18.641511	به	\N
458	4	يُعلَى	---\n- '07'\n	\N	6	6	32	فعل مضارع مبني للمجهول مرفوع بالضمة المقدّرة على الألف للتعذّر.	2017-05-13 05:09:20.020345	2017-05-15 06:05:18.646101	يعلى	\N
492	3	جَانِـبٌ	---\n- '04'\n	\N	6	6	35	هو الجزء من الأرض وقصد به هنا: لي متسعٌ من العيش وتمكنٌ حيثُ قربُهُ من السلطة.	2017-05-13 06:32:20.860142	2017-05-15 06:05:18.841393	جانب	\N
461	3	حَلَفْتُ	---\n- '00'\n	\N	6	6	33	أقسمتُ يمينًا بالله.	2017-05-13 05:56:34.446928	2017-05-15 06:05:18.6605	حلفت	\N
462	3	أَترُكْ	---\n- '02'\n	\N	6	6	33	أَدَعْ	2017-05-13 05:56:59.746424	2017-05-15 06:05:18.664942	أترك	\N
463	3	رِيبَـةً	---\n- '04'\n	\N	6	6	33	ظنًّا أو شكًّا.	2017-05-13 05:57:22.587968	2017-05-15 06:05:18.66931	ريبة	\N
464	3	وَرَاءَ	---\n- '07'\n	\N	6	6	33	بعد.	2017-05-13 05:57:40.309438	2017-05-15 06:05:18.673649	وراء	\N
465	3	مَذهَبُ	---\n- '10'\n	\N	6	6	33	اسم مكان بمعنى الملجأ.	2017-05-13 05:58:01.913849	2017-05-15 06:05:18.678022	مذهب	\N
466	4	حَلَفْتُ	---\n- '00'\n	\N	6	6	33	[حلف] فعل ماضٍ مبني على السكون لاتصاله بضمير الرفع، و [التاء] فاعل مبني على الضم في محل رفع. 	2017-05-13 05:58:28.325696	2017-05-15 06:05:18.682494	حلفت	\N
467	4	فَلَمْ	---\n- '01'\n	\N	6	6	33	[الفاء] حرف عطف فيها معنى السببية، و [لم] حرف نفي وقلب وجزم.	2017-05-13 05:58:53.62657	2017-05-15 06:05:18.687111	فلم	\N
468	4	أَترُكْ	---\n- '02'\n	\N	6	6	33	فعل مضارع مجزوم بالسكون والفاعل مستتر تقديره [أنا]. والجملة الفعلية معطوفة على [حلفت].	2017-05-13 05:59:11.469644	2017-05-15 06:05:18.693062	أترك	\N
469	4	لِنَفْسِكَ	---\n- '03'\n	\N	6	6	33	[اللام] حرف جرّ، و [نفسِ] مجرور بالكسرة وهو مضاف، و [الكاف] مضاف إليه مبني على الفتح في محل جرّ. والجار والمجرور متعلقان بالفعل [أترك].	2017-05-13 05:59:36.598506	2017-05-15 06:05:18.698201	لنفسك	\N
470	4	رِيبَـةً	---\n- '04'\n	\N	6	6	33	مفعول به منصوب بالفتحة.	2017-05-13 05:59:59.515869	2017-05-15 06:05:18.702684	ريبة	\N
471	4	وَلَيسَ	---\n- '06'\n	\N	6	6	33	[الواو] حرف استئناف، و [ليس] فعل ماضٍ مبني على الفتح من أخوات كان.	2017-05-13 06:00:26.10249	2017-05-15 06:05:18.707224	وليس	\N
472	4	وَرَاءَ	---\n- '07'\n	\N	6	6	33	ظرف مكان منصوب بالفتحة متعلق بمحذوف خبر [ليس] مقدّم أي: وليس كائنًا وراء الله، وهو مضاف.	2017-05-13 06:00:45.608405	2017-05-15 06:05:18.711581	وراء	\N
473	4	الله	---\n- 08\n	\N	6	6	33	مضاف إليه ولفظه مجرور بالكسرة.	2017-05-13 06:01:02.137631	2017-05-15 06:05:18.716655	الله	\N
474	4	لِلمَـرْءِ	---\n- 09\n	\N	6	6	33	[اللام] حرف جرّ، و [المرءِ] مجرور بالكسرة وهما متعلقان بِـ [مذهب].	2017-05-13 06:01:23.01705	2017-05-15 06:05:18.722433	للمرء	\N
475	4	مَذهَبُ	---\n- '10'\n	\N	6	6	33	اسم [ليس] مؤخّر مرفوع بالضمة. والأصل: ليس مذهبٌ للمرء كائنًا وراءَ اللهِ.	2017-05-13 06:01:43.631854	2017-05-15 06:05:18.727048	مذهب	\N
476	3	بُلِّغْتَ	---\n- '03'\n	\N	6	6	34	بلغك الخبر ووصل إليك.	2017-05-13 06:10:22.03407	2017-05-15 06:05:18.732471	بلغت	\N
477	3	خِيَانَـةً	---\n- '05'\n	\N	6	6	34	الخيانة ضدّ الأمانة، يقال: خان الأمانةَ يخونها خَونًا وخيانة ومخانةً إذا فرّط فيها، ويقال: خان العهد إذا نكث فيه.	2017-05-13 06:10:47.951388	2017-05-15 06:05:18.736894	خيانة	\N
478	3	مُبْلِغُكَ	---\n- '07'\n	\N	6	6	34	اسم فاعل فعله أبلغ يُبلغُ فهو مُبلِغ.	2017-05-13 06:11:07.492735	2017-05-15 06:05:18.741324	مبلغك	\N
479	3	الوَاشِي	---\n- 08\n	\N	6	6	34	هو الذي يكذب ويجمّلُ ويحسّنُ كذبَهُ، ويقال له: النَّمَّام والواشي اسم فاعل، يقال: وشَى به عند السلطان وَشْيًا: سعى به، ووشى في كلامه وَشْيًا: كذب.	2017-05-13 06:11:23.475149	2017-05-15 06:05:18.745899	الواشي	\N
480	3	أَغَشُّ	---\n- 09\n	\N	6	6	34	اسم تفضيل أي: أكثر غشًا، يقال: غَشَّهُ غّشًّا، أي: لم ينصحه وزيَّنَ له غيرَ المصلحةِ.	2017-05-13 06:11:40.878216	2017-05-15 06:05:18.753599	أغش	\N
481	3	أَكْذَبُ	---\n- '10'\n	\N	6	6	34	اسم تفضيل، أي: أكثر كذبًا، والكذب إخبار لا يطابق الواقع.	2017-05-13 06:11:57.859484	2017-05-15 06:05:18.76269	أكذب	\N
483	4	كُنْتَ	---\n- '01'\n	\N	6	6	34	[كان] فعل ماضٍ ناقص مبني على السكون في محل جزم فعل الشرط. و [التاء] اسم كان مبني على الفتح في محل رفع.	2017-05-13 06:27:43.681951	2017-05-15 06:05:18.781244	كنت	\N
484	4	قَدْ	---\n- '02'\n	\N	6	6	34	حرف تحقيق لا محل له من الإعراب.	2017-05-13 06:28:04.808758	2017-05-15 06:05:18.787698	قد	\N
485	4	بُلِّغْتَ	---\n- '03'\n	\N	6	6	34	[بُلِّغَ] فعل ماضٍ مبني للمجهول مبني على السكون لاتصاله بضمير الرفع، و [التاء] نائب فاعل مبني على الفتح في محل رفع وأصله المفعول الأوّل. والجملة الفعلية في محل نصب خبر [كان].	2017-05-13 06:29:00.915266	2017-05-15 06:05:18.794192	بلغت	\N
486	4	عَنِّي	---\n- '04'\n	\N	6	6	34	[عن] حرف جرّ، و [النون] المدغمة نون الوقاية، و [الياء] ياء المتكلم مبنية على السكون في محل جرّ، وهما متعلقان بالفعل [بُلِّغْتَ].	2017-05-13 06:29:20.526343	2017-05-15 06:05:18.800708	عني	\N
487	4	خِيَانَـةً	---\n- '05'\n	\N	6	6	34	مفعول به ثانٍ منصوب بالفتحة.	2017-05-13 06:29:38.086286	2017-05-15 06:05:18.807811	خيانة	\N
488	4	لَمُبْلِغُكَ	---\n- '07'\n	\N	6	6	34	[اللام] لام الابتداء يفيد التوكيد، و [مُبْلِغُ] مبتدأ مرفوع بالضمة وهو مضاف، و [الكاف] مضاف إليه مبني على الفتح في محل جرّ بالإضافة وفي محل نصب على أنّه مفعول [مبلغ].	2017-05-13 06:30:14.409949	2017-05-15 06:05:18.814046	لمبلغك	\N
489	4	الوَاشِي	---\n- 08\n	\N	6	6	34	اعل لِـ [مبلغ] مرفوع بالضمة المقدّرة على الياء للثقل. 	2017-05-13 06:30:34.504873	2017-05-15 06:05:18.821059	الواشي	\N
490	4	أَغَشُّ	---\n- 09\n	\N	6	6	34	خبر المبتدأ [مبلغُ] مرفوع بالضمة.	2017-05-13 06:31:01.767197	2017-05-15 06:05:18.828218	أغش	\N
491	4	وَأَكْذَبُ	---\n- '10'\n	\N	6	6	34	[الواو] حرف عطف، و [أكذب] معطوف على [أغشُّ] مرفوع بالضمة. وجملة [لَمُبْلِغُكَ الوَاشِي أَغَشُّ وَأَكْذَبُ] في محل جزم جواب الشرط.	2017-05-13 06:31:30.072626	2017-05-15 06:05:18.834824	وأكذب	\N
494	3	مَذْهَبُ	---\n- '10'\n	\N	6	6	35	اسم مكان الذهاب	2017-05-13 06:33:13.348585	2017-05-15 06:05:18.855197	مذهب	\N
496	4	كُنْتُ	---\n- '01'\n	\N	6	6	35	[كان] فعل ماضٍ ناقص مبني على السكون لاتصاله بضمير الرفع، و [التاء] اسمها مبني على الضم في محل رفع.	2017-05-13 06:35:04.643163	2017-05-15 06:05:18.873355	كنت	\N
497	4	امْرَأً	---\n- '02'\n	\N	6	6	35	خبر [كان] منصوب بالفتحة. وجملة [كنت امرأً] في محل رفع خبر [لكنَّ].	2017-05-13 06:35:24.180492	2017-05-15 06:05:18.878583	امرأ	\N
498	4	لِيَ	---\n- '03'\n	\N	6	6	35	[اللام] حرف جرّ، و [الياء] ياء المتكلم مبنية على السكون في محل جرّ وحركت بالفتحة لوزن الشعر، وهما متعلقان بمحذوف خبر أوّل مقدّم.	2017-05-13 06:35:44.394788	2017-05-15 06:05:18.883403	لي	\N
499	4	جَانِـبٌ	---\n- '04'\n	\N	6	6	35	مبتدأ مؤخّر مرفوع بالضمة، والأصل: جانب كائن لي من الأرض. والجملة الاسمية في محل نصب نعت [امرأً].	2017-05-13 06:36:03.467454	2017-05-15 06:05:18.888195	جانب	\N
500	4	مِنَ	---\n- '06'\n	\N	6	6	35	حرف جرّ وحركت النون بالفتحة لالتقاء الساكنين.	2017-05-13 06:36:23.490305	2017-05-15 06:05:18.892877	من	\N
501	4	الأَرْضِ	---\n- '07'\n	\N	6	6	35	مجرور بالكسرة وهما متعلقان بمحذوف خبر ثانٍ، أي: جانب كائن لي كائن من الأرض.	2017-05-13 06:36:45.880049	2017-05-15 06:05:18.898184	الأرض	\N
502	4	فِيهِ	---\n- 08\n	\N	6	6	35	[في] حرف جرّ، و [الهاء] ضمير مبني على الكسر في محل جرّ يعود إلى الجانب وهما متعلقان بمحذوف خبر مقدّم.	2017-05-13 06:37:05.146695	2017-05-15 06:05:18.902463	فيه	\N
503	3	مُسْتَرَادٌ	---\n- 09\n	\N	6	6	35	مبتدأ مؤخّر مرفوع بالضمة.	2017-05-13 06:37:29.007122	2017-05-15 06:05:18.907252	مستراد	\N
504	4	وَمَذْهَبُ	---\n- '10'\n	\N	6	6	35	[الواو] حرف عطف، و [مذهب] معطوف على [مستراد] مرفوع بالضمة، أي: مستراد ومذهبٌ كائن فيه.	2017-05-13 06:37:53.89094	2017-05-15 06:05:18.912514	ومذهب	\N
505	3	مُلُوكٌ	---\n- '00'\n	\N	6	6	36	جمعٌ مفردُهُ مَلِكٌ ويعني بهم الملوك الغساسنة في بلاد الشام.	2017-05-13 06:41:35.414493	2017-05-15 06:05:18.917011	ملوك	\N
506	3	إِخْـوَانٌ	---\n- '01'\n	\N	6	6	36	وأراد بهم بني عمه الغسانيين الذي أكرموه.	2017-05-13 06:41:53.143505	2017-05-15 06:05:18.921596	إخوان	\N
507	3	أُحَكَّـمُ	---\n- '06'\n	\N	6	6	36	بمعنى أتحكم في أموال هؤلاء الملوك بما أشاء.	2017-05-13 06:42:11.911804	2017-05-15 06:05:18.926014	أحكم	\N
508	3	أُقَـرَّبُ	---\n- 09\n	\N	6	6	36	أكون قريبًا من الملوك.	2017-05-13 06:42:35.607129	2017-05-15 06:05:18.930411	أقرب	\N
509	4	مُلُوكٌ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n	\N	6	6	36	خبر لمبتدأ محذوف تقديره: [هم ملوك] مرفوع بالضمة.	2017-05-13 06:43:50.111574	2017-05-15 06:05:18.935692	ملوك	\N
510	4	وَإِخْـوَانٌ	---\n- '01'\n	\N	6	6	36	[الواو] حرف عطف، و [إخوان] معطوف على [ملوك] مرفوع بالضمة.	2017-05-13 06:44:20.89178	2017-05-15 06:05:18.942019	وإخوان	\N
511	4	إِذَا	---\n- '02'\n	\N	6	6	36	ظرفية لما يستقبل من الزمان شرطية غير جازمة.	2017-05-13 06:44:39.147418	2017-05-15 06:05:18.946368	إذا	\N
512	4	مَـا	---\n- '03'\n	\N	6	6	36	حرف زائد، أي: إذا أتيتهم.	2017-05-13 06:44:56.422189	2017-05-15 06:05:18.950898	ما	\N
514	4	أُحَكَّـمُ	---\n- '06'\n	\N	6	6	36	فعل مضارع مبني للمجهول مرفوع بالضمة، ونائب الفاعل مستتر تقديره [أنا]. والجملة جواب الشرط لِـ [إذا].	2017-05-13 06:45:31.176847	2017-05-15 06:05:18.962556	أحكم	\N
515	4	فِي	---\n- '07'\n	\N	6	6	36	حرف جرّ. 	2017-05-13 06:45:50.933951	2017-05-15 06:05:18.968288	في	\N
516	4	أَمْوَالِهِمْ	---\n- 08\n	\N	6	6	36	[أموالِ] مجرور بالكسرة وهو مضاف، و [هم] مضاف إليه مبني على السكون في محل جرّ، وهما متعلقان بالفعل [أُحكّمُ].	2017-05-13 06:46:12.660329	2017-05-15 06:05:18.974605	أموالهم	\N
517	4	وَأُقَـرَّبُ	---\n- 09\n	\N	6	6	36	[الواو] حرف عطف، و [أقرب] فعل مضارع مرفوع بالضمة، ونائب الفاعل مستتر تقديره [أنا].	2017-05-13 06:46:44.499788	2017-05-15 06:05:18.979716	وأقرب	\N
518	3	اصْطَنَعْتَهُمْ	---\n- '04'\n	\N	6	6	37	قربتهم وأكرمتهم وصنعتَ منهم أصدقاء لك.	2017-05-13 06:48:21.024921	2017-05-15 06:05:18.985424	اصطنعتهم	\N
519	3	تَرَهُمْ	---\n- '07'\n	\N	6	6	37	تَحْسَبُهُم وتظنّهم.	2017-05-13 06:48:39.940912	2017-05-15 06:05:18.989992	ترهم	\N
520	3	أَذْنَبُوا	---\n- '11'\n	\N	6	6	37	وقعوا في الذنب.	2017-05-13 06:48:58.84003	2017-05-15 06:05:18.994591	أذنبوا	\N
521	4	كَفِعْلِكَ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n- '11'\n	\N	6	6	37	[الكاف] حرف جرّ فيه معنى التشبيه، و [فعلِ] مجرور بالكسرة وهو مضاف، و [الكاف] مضاف إليه مبني على الفتح في محل جرّ، وهما متعلقان بمحذوف أي: فعلتُ كفِعلِكَ.	2017-05-13 06:56:21.76816	2017-05-15 06:05:19.000001	كفعلك	\N
522	4	فِي	---\n- '01'\n	\N	6	6	37	حرف جرّ.	2017-05-13 06:56:41.671729	2017-05-15 06:05:19.007258	في	\N
523	4	قَومٍ	---\n- '02'\n	\N	6	6	37	مجرور بالكسرة وهما متعلقان بالمصدر [فعلِكَ].	2017-05-13 06:58:43.382277	2017-05-15 06:05:19.011867	قوم	\N
524	4	أَرَاكَ	---\n- '03'\n	\N	6	6	37	[أرى] فعل ماضٍ مبني على السكون، والفاعل مستتر تقديره [أنا]، و [الكاف] مفعول به مبني على الفتح في محل نصب.	2017-05-13 06:59:13.372803	2017-05-15 06:05:19.017793	أراك	\N
525	4	اصْطَنَعْتَهُمْ	---\n- '04'\n	\N	6	6	37	[اصطنع] فعل ماضٍ مبني على السكون لاتصاله بضمير الرفع، و [التاء] فاعل مبني على الفتح في محل رفع، و [هم] مفعول به مبني على السكون في محل نصب.	2017-05-13 06:59:32.830655	2017-05-15 06:05:19.022589	اصطنعتهم	\N
526	4	فَلَمْ	---\n- '06'\n	\N	6	6	37	[الفاء] حرف عطف، و [لم] حرف نفي وقلب وجزم.	2017-05-13 06:59:49.297433	2017-05-15 06:05:19.0277	فلم	\N
528	4	فِي	---\n- 08\n	\N	6	6	37	حرف جرّ.	2017-05-13 07:00:30.445839	2017-05-15 06:05:19.038267	في	\N
529	4	شُكْرِ	---\n- 09\n	\N	6	6	37	مجرور بالكسرة، وهما متعلقان بالفعل [ترهم]، وهو مضاف.	2017-05-13 07:00:53.635045	2017-05-15 06:05:19.043095	شكر	\N
530	4	ذَلِكَ	---\n- '10'\n	\N	6	6	37	[ذا] مضاف إليه اسم إشارة مبني على السكون في محل جرّ، و [اللام] حرف للبعد، و [الكاف] حرف للخطاب لا محل لهما من الإعراب.	2017-05-13 07:01:14.285691	2017-05-15 06:05:19.047482	ذلك	\N
532	3	الوَعِيدِ	---\n- '02'\n	\N	6	6	38	الوعيد عند الإطلاق يكون تهديدًا بالشرّ، والوعد عند الإطلاق يكون وعدًا بالخير، وعند التقييد يستعمل كلّ واحد بالخير والشرّ، فيقال: وعيدٌ بالجنة ووعد بالنار.	2017-05-13 07:07:30.761305	2017-05-15 06:05:19.057051	الوعيد	\N
533	3	مَطَلِيٌّ	---\n- '07'\n	\N	6	6	38	الطِّلاء – بكسر الطاء – على وزن كتاب كلّ ما يُطلى به وشيءٌ مطليٌّ أي: مصبوغ بالطلاء.	2017-05-13 07:07:45.730573	2017-05-15 06:05:19.061536	مطلي	\N
534	3	القَارُ	---\n- 09\n	\N	6	6	38	هو القطرن الذي يطلى به البعير الأجرب فيُشفى ولا يَعدِي غيره من القطيع، ويطلق على القار الزفت.	2017-05-13 07:08:00.014074	2017-05-15 06:05:19.06592	القار	\N
535	3	أَجْرَبُ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n	\N	6	6	38	الجرب مرض يحدث تحت الجلد تنتج منه بثور جلدية وربما حصل بسببه عند الحيوان ضعف وهزال.	2017-05-13 07:08:14.121161	2017-05-15 06:05:19.071379	أجرب	\N
536	4	فَلاَ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n	\N	6	6	38	[الفاء] حرف استئناف، و [لا] ناهية جازمة فيها معنى الدعاء والالتماس.	2017-05-13 07:09:19.300951	2017-05-15 06:05:19.079028	فلا	\N
537	4	تَتْرُكَنِّـي	---\n- '01'\n	\N	6	6	38	[تترك] فعل مضارع مبني على الفتح لاتصاله بنون التوكيد في محل جزم بلا الناهية، و [النون] نون التوكيد الثقيلة مبنية على الفتح لا محل لها من الإعراب وكسرت النون لأجل ياء المتكلّم، و [الياء] ياء المتكلم مفعول به مبنية على السكون في محل نصب، والفاعل ضمير مستتر تقديره [أنت].	2017-05-13 07:09:36.769877	2017-05-15 06:05:19.088411	تتركني	\N
538	4	بِالوَعِيدِ	---\n- '02'\n	\N	6	6	38	[الباء] حرف جرّ، و [الوعيد] مجرور بالكسرة وهما متعلقان بالفعل [تترك]. 	2017-05-13 07:10:01.240077	2017-05-15 06:05:19.09357	بالوعيد	\N
539	4	كَأَنَّنِـي	---\n- '03'\n	\N	6	6	38	[كأنّ] من الحروف المشبهة بالفعل، و [النون] الثانية للوقاية، و [الياء] ياء المتكلم اسم [كأنّ] مبنية على السكون في محل نصب، وسيأتي خبرها.	2017-05-13 07:10:18.128953	2017-05-15 06:05:19.098548	كأنني	\N
540	4	إِلَى	---\n- '05'\n	\N	6	6	38	حرف جرّ.	2017-05-13 07:10:31.848582	2017-05-15 06:05:19.103928	إلى	\N
541	4	النَّاسِ	---\n- '06'\n	\N	6	6	38	مجرور بالكسرة وهما متعلقان بالفعل [تترك].	2017-05-13 07:10:46.448622	2017-05-15 06:05:19.108991	الناس	\N
542	4	مَطَلِيٌّ	---\n- '07'\n	\N	6	6	38	خبر [كأنّ] مرفوع بالضمة.	2017-05-13 07:11:02.876372	2017-05-15 06:05:19.114487	مطلي	\N
543	4	بِهِ	---\n- 08\n	\N	6	6	38	[الباء] حرف جرّ، و [الهاء] مبني على الكسر في محل جرّ وهما متعلقان بمحذوف خبر مقدّم أي: القارُ كائنٌ به.	2017-05-13 07:11:19.70306	2017-05-15 06:05:19.121587	به	\N
544	4	القَارُ	---\n- 09\n	\N	6	6	38	مبتدأ مؤخر مرفوع بالضمة. والجملة من المبتدأ وخبره في محل رفع خبر ثاني لِـ [كأنّ].	2017-05-13 07:11:38.660595	2017-05-15 06:05:19.129738	القار	\N
545	4	أَجْرَبُ	---\n- '10'\n	\N	6	6	38	خبر ثالث لِـ [كأنّ] مرفوع بالضمة.	2017-05-13 07:11:58.998475	2017-05-15 06:05:19.137577	أجرب	\N
546	3	أَلَمْ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n- '11'\n	\N	6	6	39	هذا استفهام معناه التقرير والإثبات أي: رأيت أنّ الله أعطاك، كقوله تعالى: ﴿أَلَمْ نَشْرَحْ لَكَ صَدْرَكَ﴾ الشرح: 1، أي: شرحناه.	2017-05-13 07:12:26.685475	2017-05-15 06:05:19.144056	ألم	\N
547	3	سُـورَةً	---\n- '05'\n	\N	6	6	39	العزة والمنعة والقوة والغلبة: مأخوذة من سور المدينة وهو البناء المحيط بها.	2017-05-13 07:12:48.538704	2017-05-15 06:05:19.151727	سورة	\N
548	3	مَلْكٍ	---\n- 09\n	\N	6	6	39	المَلْكُ – بكسر الميم وفتحها – مصدر فعله مَلَكَ من باب ضَرَبَ.	2017-05-13 07:13:07.056867	2017-05-15 06:05:19.157365	ملك	\N
549	4	يَتَذَبْـذَبُ	---\n- '11'\n	\N	6	6	39	يتردّد ويضطرب ويَحَارُ.	2017-05-13 07:13:28.525184	2017-05-15 06:05:19.174529	يتذبذب	\N
550	4	أَلَمْ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n- '11'\n	\N	6	6	39	[الهمزة] للاستفهام، و [لم] حرف نفي وقلب وجزم.	2017-05-13 07:14:34.996299	2017-05-15 06:05:19.181504	ألم	\N
551	4	تَرَ	---\n- '01'\n	\N	6	6	39	فعل مضارع مجزوم بحذف حرف العلة وهو الألف، والفاعل مستتر تقديره [أنت].	2017-05-13 07:14:52.689827	2017-05-15 06:05:19.189636	تر	\N
552	4	أَنَّ	---\n- '02'\n	\N	6	6	39	من الحروف المشبهة بالفعل.	2017-05-13 07:15:09.379633	2017-05-15 06:05:19.195204	أن	\N
553	4	اللهَ	---\n- '03'\n	\N	6	6	39	اسم [أنّ] منصوب بالفتحة، وسيأتي خبرها.	2017-05-13 07:15:25.108705	2017-05-15 06:05:19.199739	الله	\N
554	4	أَعطَـاكَ	---\n- '04'\n	\N	6	6	39	[أعطى] فعل ماضٍ مبني على الفتح المقدّر على الألف، والفاعل مستتر تقديره [هو] يعود إلى [الله]، و [الكاف] مفعول أوّل مبني على الفتح في محل نصب.	2017-05-13 07:15:45.57229	2017-05-15 06:05:19.20406	أعطاك	\N
556	4	تَرَى	---\n- '07'\n	\N	6	6	39	فعل مضارع مرفوع بالضمة المقدّرة على الألف للتعذّر. والفاعل مستتر تقديره [أنت].	2017-05-13 07:16:23.425291	2017-05-15 06:05:19.212858	ترى	\N
558	4	مَلْكٍ	---\n- 09\n	\N	6	6	39	مضاف إليه مجرور بالكسرة.	2017-05-13 07:16:54.683477	2017-05-15 06:05:19.2257	ملك	\N
560	4	يَتَذَبْـذَبُ	---\n- '11'\n	\N	6	6	39	فعل مضارع مرفوع بالضمة، والفاعل مستتر تقديره [هو] يعود إلى [الملك]. وجملة [ترى كلّ ملك ...] في محل نصب حال من الكاف في [أعطاك]. وجملة [دونها يتذبذب] في محل جر نعت لِـ [ملكٍ].	2017-05-13 07:17:30.295949	2017-05-15 06:05:19.237494	يتذبذب	\N
561	3	كَوَاكِبٌ	---\n- '03'\n	\N	6	6	40	جمع مفردُهُ كوكب، والمراد به النجم في السماء.	2017-05-13 07:23:25.234059	2017-05-15 06:05:19.243534	كواكب	\N
562	3	طَلَعَتْ	---\n- '06'\n	\N	6	6	40	شرقت الشمس.	2017-05-13 07:23:41.985269	2017-05-15 06:05:19.249128	طلعت	\N
563	3	يَبْدُ	---\n- 08\n	\N	6	6	40	يظهر	2017-05-13 07:24:04.027159	2017-05-15 06:05:19.25385	يبد	\N
564	4	فَإِنَّكَ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n	\N	6	6	40	[الفاء] للاستئناف فيها معنى السبب، [إنّ] من الحروف المشبهة بالفعل، و [الكاف] اسمها مبني على الفتح في محل نصب.	2017-05-13 07:25:35.694448	2017-05-15 06:05:19.25927	فإنك	\N
565	4	شَمْسٌ	---\n- '01'\n	\N	6	6	40	خبر [إن] مرفوع بالضمة.	2017-05-13 07:25:54.002962	2017-05-15 06:05:19.265557	شمس	\N
566	4	وَالمُلُـوكُ	---\n- '02'\n	\N	6	6	40	[الواو] حرف عطف، و [الملوك] مبتدأ مرفوع بالضمة.	2017-05-13 07:26:14.231338	2017-05-15 06:05:19.269965	والملوك	\N
567	4	كَوَاكِبٌ	---\n- '03'\n	\N	6	6	40	خبر مرفوع بالضمة. والجملة الاسمية هذه معطوفة على اسم [إنّ] وخبرها.	2017-05-13 07:26:38.683951	2017-05-15 06:05:19.274337	كواكب	\N
568	4	إِذَا	---\n- '05'\n	\N	6	6	40	ظرف زمان شرطية غير جازمة مبنية على السكون في محل نصب بجوابها وهو [يبد].	2017-05-13 07:26:55.242332	2017-05-15 06:05:19.278685	إذا	\N
569	4	طَلَعَتْ	---\n- '06'\n	\N	6	6	40	[طَلَعَ] فعل ماضٍ مبني على الفتح، و [التاء] تاء التأنيث، والفاعل مستتر تقديره [هي] يعود إلى [الشمس]. وجملة [طلعتْ] شرط لِـ [إذا].	2017-05-13 07:28:09.065827	2017-05-15 06:05:19.283297	طلعت	\N
570	4	لَمْ	---\n- '07'\n	\N	6	6	40	حرف نفي وقلب وجزم. 	2017-05-13 07:28:58.143145	2017-05-15 06:05:19.287656	لم	\N
571	4	يَبْدُ	---\n- 08\n	\N	6	6	40	فعل مضارع مجزوم بحذف حرف العلّة وهو الواو، والأصل [يبدو].	2017-05-13 07:29:14.054201	2017-05-15 06:05:19.292092	يبد	\N
572	4	مِنْهُنَّ	---\n- 09\n	\N	6	6	40	[من] حرف جرّ، و [هنّ] ضمير مبني على الفتح في محل جرّ، وهما متعلقان بالفعل [يبدُ].	2017-05-13 07:29:33.635444	2017-05-15 06:05:19.296749	منهن	\N
574	4	كَوكَبُ	---\n- '10'\n	\N	6	6	40	فاعل [يبدو] مرفوع بالضمة. وجملة [لَمْ يَبْدُ مِنْهُنَّ كَوكَبُ] جواب [إذا].	2017-05-13 07:31:15.808106	2017-05-15 06:05:19.301218	كوكب	\N
576	3	أَخاً	---\n- '02'\n	\N	6	6	41	صاحبًا.	2017-05-13 07:33:50.082354	2017-05-15 06:05:19.310111	أخا	\N
577	3	تَلُمُّـهُ	---\n- '04'\n	\N	6	6	41	تجمعه، يقال: لممتُ شعثهه لَمًّا أي: أصلحتُ من حاله ما تشعَّثَ.	2017-05-13 07:34:09.919968	2017-05-15 06:05:19.314368	تلمه	\N
578	3	شَعَثٍ	---\n- '07'\n	\N	6	6	41	الشعثُ الانتشار والتفرّق كما يتشعّث رأس السواك، وفي الدعاء: لَمَّ اللهُ شعثكم أي: جمع أمركم.	2017-05-13 07:34:29.313787	2017-05-15 06:05:19.31965	شعث	\N
579	3	أَيُّ الرِّجَالِ	---\n- 08\n- 09\n	\N	6	6	41	استفهام إنكاري أي: لا تجد رجلاً مهذّبًا كاملاً.	2017-05-13 07:34:46.842697	2017-05-15 06:05:19.324227	أي الرجال	\N
580	3	المُهَذَّبُ	---\n- '10'\n	\N	6	6	41	اسم مفعول فعلُهُ هََذَّبَ يُهَذِّبُ، وهو المبرَّأُ من العيوب وسوء الأخلاق.	2017-05-13 07:35:08.344723	2017-05-15 06:05:19.328897	المهذب	\N
581	4	وَلَسْتَ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n- '11'\n	\N	6	6	41	[الواو] حرف استئناف، و [ليس] فعل ماضٍ فيه معنى النفي من أخوات كان، و [التاء] اسمها مبني على الفتح في محل رفع. 	2017-05-13 07:36:35.746384	2017-05-15 06:05:19.334259	ولست	\N
582	4	بِمُسْتَبْـقٍ	---\n- '01'\n	\N	6	6	41	[الباء] حرف جرّ زائد، و [مستبق] خبر كان منصوب محلاً مجرور لفظًا بالكسرة، والأصل: ولستَ مستبقيًا.	2017-05-13 07:42:04.287107	2017-05-15 06:05:19.341867	بمستبق	\N
583	4	أَخاً	---\n- '02'\n	\N	6	6	41	مفعول به منصوب بالفتحة لِـ [مستبق] لأنّه اسم فاعل يعمل عمل فعله.	2017-05-13 07:42:20.390565	2017-05-15 06:05:19.347695	أخا	\N
584	4	لاَ	---\n- '03'\n	\N	6	6	41	نافية غير جازمة.	2017-05-13 07:42:36.312318	2017-05-15 06:05:19.353207	لا	\N
585	4	تَلُمُّـهُ	---\n- '04'\n	\N	6	6	41	[تَلُمُّ] فعل مضارع مرفوع بالضمة، والفاعل مستتر تقديره [أنت]، و [الهاء] ضمير مفعول به مبني على الضم في محل نصب يعود إلى [الأخ].	2017-05-13 07:42:55.85852	2017-05-15 06:05:19.358111	تلمه	\N
586	4	عَلَى	---\n- '06'\n	\N	6	6	41	حرف جرّ.	2017-05-13 07:43:15.768253	2017-05-15 06:05:19.363026	على	\N
587	4	شَعَثٍ	---\n- '07'\n	\N	6	6	41	مجرور بالكسرة وهما متعلقان بالفعل [تلمه].	2017-05-13 07:43:33.346657	2017-05-15 06:05:19.367543	شعث	\N
588	4	أَيُّ	---\n- 08\n	\N	6	6	41	اسم استفهام مبتدأ مبنية على الضم في محل رفع وهو مضاف.	2017-05-13 07:43:50.86987	2017-05-15 06:05:19.372206	أي	\N
589	4	الرِّجَالِ	---\n- 09\n	\N	6	6	41	مضاف إليه مجرور بالكسرة.	2017-05-13 07:44:08.186322	2017-05-15 06:05:19.377362	الرجال	\N
590	4	المُهَذَّبُ	---\n- '10'\n	\N	6	6	41	خبر مرفوع بالضمة. والجملة استئنافية لا محل لها من الإعراب.	2017-05-13 07:44:33.369718	2017-05-15 06:05:19.38306	المهذب	\N
591	3	مَظْلُوماً	---\n- '02'\n	\N	6	6	42	اسم مفعول على وزن مضروب.	2017-05-13 07:46:37.753384	2017-05-15 06:05:19.388208	مظلوما	\N
592	3	عَبْـدٌ	---\n- '03'\n	\N	6	6	42	من العبودية والخدمة والرّق.	2017-05-13 07:46:56.40264	2017-05-15 06:05:19.394159	عبد	\N
593	3	ذَا	---\n- 08\n	\N	6	6	42	بمعنى صاحب.	2017-05-13 07:47:13.757792	2017-05-15 06:05:19.400369	ذا	\N
622	4	مِنْ	---\n- '10'\n	\N	7	7	43	حرف جر.	2017-05-15 04:02:48.043321	2017-05-15 06:05:19.545444	من	\N
595	3	مِثْلُكَ	---\n- '10'\n	\N	6	6	42	شبيهك. 	2017-05-13 07:47:50.59927	2017-05-15 06:05:19.411542	مثلك	\N
596	3	يُعْتِبُ	---\n- '11'\n	\N	6	6	42	يعفو ويصفح من أعتب إذا أزال العتب، مثل: أشكاه أزال عنه الشكاية.	2017-05-13 07:48:14.614198	2017-05-15 06:05:19.416467	يعتب	\N
597	4	فَإِنْ	---\n- '00'\n	\N	6	6	42	[الفاء] حرف استئناف، و [إن] حرف شرط جازم لفعلين.	2017-05-13 07:49:25.599674	2017-05-15 06:05:19.42139	فإن	\N
599	4	مَظْلُوماً	---\n- '02'\n	\N	6	6	42	خبر [كان] منصوب بالفتحة.	2017-05-13 07:50:12.802161	2017-05-15 06:05:19.430849	مظلوما	\N
600	4	فَعَبْـدٌ	---\n- '03'\n	\N	6	6	42	[الفاء] رابطة للجواب لا محل لها من الإعراب، و [عبدٌ] خبر لمبتدأ محذوف مرفوع بالضمة أي: فأنا عبدٌ.	2017-05-13 07:50:30.83657	2017-05-15 06:05:19.435632	فعبد	\N
601	4	ظَلَمْتَـهُ	---\n- '04'\n	\N	6	6	42	[ظلمَ] فعل ماض مبني على السكون لاتصاله بضمير الرفع، و [التاء] فاعل مبني على الفتح في محل رفع، و [الهاء] ضمير مفعول به مبني على الضم في محل نصب. وجملة [ظلمته] في محل رفع نعت لِـ [عبد]. وجملة [فعبد ظلمته] في محل جزم جواب الشرط.	2017-05-13 07:50:49.703353	2017-05-15 06:05:19.440449	ظلمته	\N
602	4	وَإِنْ	---\n- '06'\n	\N	6	6	42	[الواو] حرف عطف، و [إن] حرف شرط جازم لفعلين.	2017-05-13 07:51:09.420543	2017-05-15 06:05:19.445316	وإن	\N
603	4	تَكُ	---\n- '07'\n	\N	6	6	42	فعل مضارع مجزوم بالسكون فعل الشرط كما مَرَّ في [أكُ]، واسمها ضمير مستتر تقديره [أنت].	2017-05-13 07:51:27.709194	2017-05-15 06:05:19.449829	تك	\N
604	4	ذَا	---\n- 08\n	\N	6	6	42	خبر [كان] منصوب بالألف لأنّه من الأسماء الستة وهو مضاف.	2017-05-13 07:51:47.942135	2017-05-15 06:05:19.454767	ذا	\N
605	4	عُتْبَى	---\n- 09\n	\N	6	6	42	مضاف إليه مجرور بالكسرة المقدّرة على الألف للتعذّر.	2017-05-13 07:52:03.492741	2017-05-15 06:05:19.459402	عتبى	\N
606	4	فَمِثْلُكَ	---\n- '10'\n	\N	6	6	42	[الفاء] حرف رابط للجواب، و [مثلُ] مبتدأ مرفوع بالضمة وهو مضاف، و [الكاف] مضاف غليه مبني على الفتح في محل جرّ.	2017-05-13 07:52:24.119336	2017-05-15 06:05:19.464191	فمثلك	\N
607	4	يُعْتِبُ	---\n- '11'\n	\N	6	6	42	فعل مضارع مرفوع بالضمة، والفاعل مستتر تقديره [هو] يعود لِـ [مثلك]. والجملة الفعلية في محل رفع خبر المبتدأ. وجملة [فَمِثلُكَ يُعْتِبُ] في محل جزم جواب الشرط.	2017-05-13 07:52:42.426723	2017-05-15 06:05:19.469439	يعتب	\N
609	3	قَذًى	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n- '11'\n- '12'\n	\N	7	7	43	يروي: [ما هَاجَ حُزْنَكِ أم بالعَين عُوَّارُ]، والقذى جمعٌ مفردهُ قَذَاةٌ وهو ما يقع في العين والماء من تراب ووسخ وغيرهما.	2017-05-15 03:58:10.144687	2017-05-15 06:05:19.476276	قذى	\N
610	3	عُوَّارُ	---\n- '04'\n	\N	7	7	43	العُوَّارُ اللحم الذي ينزع من العين بعد ما يذرُّ عليه الذرو وهو نوع من أمراض العيون كالرمد والغَمَصِ.	2017-05-15 03:58:27.396456	2017-05-15 06:05:19.484435	عوار	\N
611	3	ذَرَفَتْ	---\n- '07'\n	\N	7	7	43	قطرت قطرًا متتابعًا، ويروى: أم أَقفرت.	2017-05-15 03:58:41.979628	2017-05-15 06:05:19.489351	ذرفت	\N
612	3	خَلَتْ	---\n- 09\n	\N	7	7	43	فرغت.	2017-05-15 03:58:53.851921	2017-05-15 06:05:19.494327	خلت	\N
613	4	قَذًى	---\n- '00'\n	\N	7	7	43	مبتدأ مرفوع بالضمة المقدّرة على الألف المنقلبة عن ياءٍ، والأصل قَذَيٌ فقلبت الياء ألفًا لتحركها وانفتاح ما قبلها ثم حذفت الألف لاجتماع الساكنين: [الألف والتنوين]. وقيل: المبتدأ همزة الاستفهام مقدّرة أي: أَقَذًى؟ وحذفها جائز سواء تقدمت على [أَمْ] أم لم تتقدمها. 	2017-05-15 03:59:57.857508	2017-05-15 06:05:19.499269	قذى	\N
614	4	بِعَينِكِ	---\n- '01'\n	\N	7	7	43	[الباء] حرف جرّ، و [عين] مجرور بالكسرة وهو مضاف، و [الكاف] مضاف إليه مبني على الكسر في محل جرّ، والجار والمجرور متعلقان بمحذوف خبر المبتدأ، أي: أَقذًى كائنٌ بعينكٍ؟ 	2017-05-15 04:00:16.599153	2017-05-15 06:05:19.503849	بعينك	\N
615	4	أَمْ	---\n- '02'\n	\N	7	7	43	حرف عطف يطلب بها وبالهمزة قبلها التعيين وتسمى [أم المتصلة] وتسمى أيضًا [أم المُعَادِلة] لاتصالها بما قبلها وبما بعدها، ولمعادلتها للهمزة في إفادة التسوية لذلك أصبح الاستفهام بالهمزة وأم.	2017-05-15 04:00:32.913365	2017-05-15 06:05:19.50838	أم	\N
616	4	بِالعَينِ	---\n- '03'\n	\N	7	7	43	[الباء] حرف جر، و [العين] مجرور بالكسرة، وهما متعلقان بمحذوف خبر مقدّم.	2017-05-15 04:01:11.503648	2017-05-15 06:05:19.513711	بالعين	\N
617	4	عُوَّارُ	---\n- '04'\n	\N	7	7	43	مبتدأ مؤخر مرفوع بالضمة، أي: أم عوَّارٌ كائنٌ بالعين.	2017-05-15 04:01:29.558371	2017-05-15 06:05:19.519147	عوار	\N
618	4	أَمْ	---\n- '06'\n	\N	7	7	43	كإعراب السابقة.	2017-05-15 04:01:44.043785	2017-05-15 06:05:19.525241	أم	\N
619	4	ذَرَفَتْ	---\n- '07'\n	\N	7	7	43	[ذرف] فعل ماض مبني على الفتح، و [التاء] تاء التأنيث الساكنة مبنية على السكون لا محل لها من الإعراب، والفاعل مستتر تقديره [هي] يعود إلى العين.	2017-05-15 04:01:58.787178	2017-05-15 06:05:19.530727	ذرفت	\N
620	4	إِذْ	---\n- 08\n	\N	7	7	43	للتعليل، فقيل هي حرف علة بمنزلة لام العلة مبنية على السكون لا محل لها من الإعراب، وقيل: هي ظرف لما مضى من الزمان فيها معنى التعليل مبنية على السكون في محل نصب.	2017-05-15 04:02:14.853768	2017-05-15 06:05:19.535875	إذ	\N
621	4	خَلَتْ	---\n- 09\n	\N	7	7	43	[خلى] فعل ماض مبني على الفتح المقدّر، والأصل [خَلَيَتْ]، و [التاء] تاء التأنيث الساكنة لا محل لها من الإعراب.	2017-05-15 04:02:31.100957	2017-05-15 06:05:19.540401	خلت	\N
624	4	الدَّارُ	---\n- '12'\n	\N	7	7	43	فاعل للفعل [خلت] مرفوع بالضمة.	2017-05-15 04:09:36.7462	2017-05-15 06:05:19.557877	الدار	\N
625	3	كَأَنَّ عَينِي	---\n- '00'\n- '01'\n	\N	7	7	44	وفي رواية [كأنَّ دمعي].	2017-05-15 04:10:22.661737	2017-05-15 06:05:19.563513	كأن عيني	\N
626	3	لِذِكْرَاهُ	---\n- '02'\n	\N	7	7	44	بسبب تذكرها له.	2017-05-15 04:10:40.63937	2017-05-15 06:05:19.568809	لذكراه	\N
628	3	فَيضٌ	---\n- '06'\n	\N	7	7	44	كثير، يقال: فاض السيل يفيض فيضًا: كثر وسال من شَفَةِ الوادي.	2017-05-15 04:11:23.025058	2017-05-15 06:05:19.578999	فيض	\N
629	3	يَسِيلُ	---\n- '07'\n	\N	7	7	44	يجري، يقال: سال الماء يسيل سيلاً وسَيلانًا إذا طغى وجرى وغَلَبَ السيلُ على مياه الأمطار الجارية في الأودية.	2017-05-15 04:11:34.774839	2017-05-15 06:05:19.586111	يسيل	\N
630	3	الخَدَّينِ	---\n- 09\n	\N	7	7	44	مثنى مفرده خَدٌّ، والخدَّ أحد جانبي الوجه بجوار اللحي من الجانبين. 	2017-05-15 04:11:46.934059	2017-05-15 06:05:19.595013	الخدين	\N
631	3	مِدْرَارُ	---\n- '10'\n	\N	7	7	44	كثير، يقال: دَرَّ اللبنُ وغيرُه دَرًّا من باب ضَرَبَ وقَتَلَ، إذا كَثُرَ ومدرار على وزن مفعال صيغة مبالغة مثل [منحار] أي: كثير النحر.	2017-05-15 04:12:00.746078	2017-05-15 06:05:19.602126	مدرار	\N
632	4	كَأَنَّ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n	\N	7	7	44		2017-05-15 04:14:02.525565	2017-05-15 06:05:19.607537	كأن	\N
633	4	كَأَنَّ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n	\N	7	7	44	من الحروف المشبهة بالفعل.	2017-05-15 04:14:20.492366	2017-05-15 06:05:19.616596	كأن	\N
634	4	عَينِي	---\n- '01'\n	\N	7	7	44	[عين] اسم [كأنّ] منصوب بالفتحة المقدّرة على النون منع من ظهورها كسرة ياء المتكلم وهو مضاف، و [الياء] مضاف إليه مبنية على السكون في محل جرٍّ.	2017-05-15 04:14:41.422546	2017-05-15 06:05:19.624188	عيني	\N
635	4	لِذِكْرَاهُ	---\n- '02'\n	\N	7	7	44	[اللام] حرف جرّ معناها التعليل، و [ذكرى] مجرور بالكسرة المقدّرة على الألف للتعذّر وهو مضاف، و [الهاء] مضاف إليه مبني على الضم في محل جرّ، والجار والمجرور متعلقان بالفعل [خطرت].	2017-05-15 04:14:59.957991	2017-05-15 06:05:19.62902	لذكراه	\N
636	4	إِذَا	---\n- '03'\n	\N	7	7	44	اسم زمان فيها معنى الشرط غير جازمة مبنية على السكون في محل نصب.	2017-05-15 04:15:14.450153	2017-05-15 06:05:19.633819	إذا	\N
637	4	خَطَرَتْ	---\n- '04'\n	\N	7	7	44	[خطر] فعل ماض مبني على الفتح، و [التاء] تاء التأنيث الساكنة، والفاعل مستتر تقديره [هي] يعود إلى ذكراه. والجملة [لذكراه إذا حظرت] اعتراضية بين اسم [كأنّ] وخبرها. 	2017-05-15 04:15:31.26762	2017-05-15 06:05:19.638515	خطرت	\N
638	4	فَيضٌ	---\n- '06'\n	\N	7	7	44	خبر [كأنّ] مرفوع بالضمة. 	2017-05-15 04:15:52.434477	2017-05-15 06:05:19.643208	فيض	\N
639	4	يَسِيلُ	---\n- '07'\n	\N	7	7	44	فعل مضارع مرفوع بالضمة، والفاعل ضمير مستتر تقديره [هو] يعود إلى [الفيض]. والجملة الفعلية في محل رفع نعت أول لِـ [فيض].	2017-05-15 04:16:06.828433	2017-05-15 06:05:19.647956	يسيل	\N
640	4	عَلَى	---\n- 08\n	\N	7	7	44	حرف جرّ.	2017-05-15 04:16:21.887812	2017-05-15 06:05:19.652656	على	\N
641	4	الخَدَّينِ	---\n- 09\n	\N	7	7	44	مجرور بالياء لأنّه مثنى خّدٍّ.	2017-05-15 04:16:37.648348	2017-05-15 06:05:19.658118	الخدين	\N
642	4	مِدْرَارُ	---\n- '10'\n	\N	7	7	44	نعت ثانٍ لِـ [فيض] مرفوع بالضمة. وجواب [إذا] مقدّر يُفهم من السياق، أي: إذا خطرتْ سالَ الدمع على الخدين.	2017-05-15 04:16:49.081291	2017-05-15 06:05:19.663916	مدرار	\N
643	3	العَبْرَى	---\n- '03'\n	\N	7	7	45	هي التي لا تجفُّ عينُها من الدموع بل تنهمل دموعها انهمالاً.	2017-05-15 04:21:03.610428	2017-05-15 06:05:19.669196	العبرى	\N
645	3	وَدُونَهُ	---\n- '07'\n	\N	7	7	45	أي: وعليه وفوقه.	2017-05-15 04:25:37.749474	2017-05-15 06:05:19.678774	ودونه	\N
646	3	جَدِيدِ التُّرْبِ	---\n- 09\n- '10'\n	\N	7	7	45	جديد الأرض وجديد التراب ما أثير من باطن الأرض.	2017-05-15 04:25:58.888564	2017-05-15 06:05:19.685755	جديد الترب	\N
647	3	أَسْتَـارُ	---\n- '11'\n	\N	7	7	45	جمع مفرده شِبرٌ، والشِّبرُ –بكسر الشين- ما بين طرفي الإصبع الخنصر والإصبع الإبهام بتفريج كف اليد المعتاد.	2017-05-15 04:26:12.676545	2017-05-15 06:05:19.690842	أستار	\N
648	4	تَبْكِي	---\n- '00'\n	\N	7	7	45	فعل مضارع مرفوع بالضمة المقدّرة على الياء للثقل، والفاعل مستتر تقديره [هي] يعود إلى [العين] المتقدم ذكرها في البيت السابق.	2017-05-15 04:28:32.325909	2017-05-15 06:05:19.696709	تبكي	\N
649	4	لِصَخْرٍ	---\n- '01'\n	\N	7	7	45	[اللام] حرف جرّ معناه التعليل، و [صخرٍ] مجرور بالكسرة وهما متعلقان بالفعل [تبكي].	2017-05-15 04:28:50.575315	2017-05-15 06:05:19.701342	لصخر	\N
650	4	هِيَ	---\n- '02'\n	\N	7	7	45	مبتدأ مبني على السكون في محل رفع.	2017-05-15 04:29:06.884867	2017-05-15 06:05:19.706356	هي	\N
651	4	العَبْرَى	---\n- '03'\n	\N	7	7	45	خبر مرفوع بضمة مقدّرة على الألف للتعذر.	2017-05-15 04:29:23.172667	2017-05-15 06:05:19.711545	العبرى	\N
652	4	وَقَدْ	---\n- '04'\n	\N	7	7	45	[الواو] واو الحال، و [قد] حرفٌ معناه التحقيق.	2017-05-15 04:29:39.705887	2017-05-15 06:05:19.716737	وقد	\N
653	4	وَلَهَتْ	---\n- '05'\n	\N	7	7	45	[وَلَهَ] فعل ماض مبني على الفتح، و [التاء] تاء التأنيث الساكنة، والفاعل مستتر تقديره [هي] يعود إلى [العين]. وجملة [وقد ولهت] في محل نصب حالٌ من فاعل [تبكي].	2017-05-15 04:29:58.617858	2017-05-15 06:05:19.721434	ولهت	\N
654	4	وَدُونَهُ	---\n- '07'\n	\N	7	7	45	[الواو] واو الحال، و [دونَ] ظرف مكان منصوب بالفتحة وهو مضاف، و [الهاء] مضاف إليه مبني على الضم في محل جرّ. والظرف متعلق بمحذوف خبر مقدم أول أي: [أستار كائنة دونه].	2017-05-15 04:30:17.399137	2017-05-15 06:05:19.726264	ودونه	\N
657	4	التُّرْبِ	---\n- '10'\n	\N	7	7	45	مضاف إليه مجرور بالكسرة. 	2017-05-15 04:34:20.468314	2017-05-15 06:05:19.74177	الترب	\N
659	3	خُنَاسٌ	---\n- '01'\n	\N	7	7	46	تعني نفسها [الخنساء].	2017-05-15 04:35:02.60818	2017-05-15 06:05:19.753015	خناس	\N
660	3	مَا تَنْفَكُّ	---\n- '02'\n- '03'\n	\N	7	7	46	أي: باقية لأنّ [ما] نافية، و [تنفك] معناها النفي، ونفي النفي إثبات.	2017-05-15 04:35:19.211559	2017-05-15 06:05:19.760015	ما تنفك	\N
661	3	عَمَرَتْ	---\n- '05'\n	\N	7	7	46	أي مدّة عمرها وحياتها أي: ما عاشت.	2017-05-15 04:35:33.10764	2017-05-15 06:05:19.768813	عمرت	\N
662	3	رَنِيـنٌ	---\n- 09\n	\N	7	7	46	صياحٌ، يقال: رَنَّ الشيئ يَرِنُّ –من باب ضرب- رنينًا صَوَّتَ، وله رَنَّةٌ، أي: صيحة.	2017-05-15 04:35:48.198148	2017-05-15 06:05:19.7738	رنين	\N
663	3	مِفْتَـارُ	---\n- '11'\n	\N	7	7	46	مبالغة اسم الفاعل، أي: أصابها الفتر والضعف والانكسار بصورة شديدة، ويقال: فتر عن العمل فتورًا من باب قعد، انكسرت حدّته ولانَ يعدَ شِدَّتهِ.	2017-05-15 04:36:01.748837	2017-05-15 06:05:19.782575	مفتار	\N
664	4	تَبْكِي	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n- '11'\n	\N	7	7	46	فعل مضارع مرفوع بالضمة المقدّرة على الياء للثقل.	2017-05-15 04:37:14.455537	2017-05-15 06:05:19.788549	تبكي	\N
665	4	خُنَاسٌ	---\n- '01'\n	\N	7	7	46	فاعل مرفوع بالضمة ممنوع من الصرف للعلمية والتأنيث ونون لضرورة الشعر.	2017-05-15 04:37:28.238243	2017-05-15 06:05:19.795859	خناس	\N
666	4	فَمَا	---\n- '02'\n	\N	7	7	46	[الفاء] حرف عطف، و [ما] نافية غير عاملة.	2017-05-15 04:37:44.252481	2017-05-15 06:05:19.800679	فما	\N
667	4	تَنْفَكُّ	---\n- '03'\n	\N	7	7	46	فعل مضارع مرفوع بالضمة من أخوات [كان]، واسمها مستتر تقديره [هما] يعود إلى [خُناس].	2017-05-15 04:38:03.150878	2017-05-15 06:05:19.805236	تنفك	\N
668	4	مَا	---\n- '04'\n	\N	7	7	46	حرف مصدري معناه المدّة والزمان.	2017-05-15 04:38:18.311965	2017-05-15 06:05:19.810041	ما	\N
669	4	عَمَرَتْ	---\n- '05'\n	\N	7	7	46	[عَمَرَ] فعل ماض مبني على الفتح، و [التاء] تاء التأنيث الساكنة أي: مدّة عمرها. 	2017-05-15 04:38:37.438498	2017-05-15 06:05:19.814429	عمرت	\N
670	4	لَهَا	---\n- '07'\n	\N	7	7	46	[اللام] حرف جرّ، و [ها] ضمير مبني على السكون في محل جرّ، وهما متعلقان بمحذوف خبر مقدم أي: رنين كائنٌ لها.	2017-05-15 04:38:52.231448	2017-05-15 06:05:19.819249	لها	\N
671	4	عَلَيـهِ	---\n- 08\n	\N	7	7	46	[على] حرف جرّ، و [الهاء] ضمير مبني على الكسر في محل جر وهما متعلقان بمحذوف خبر ثان للمبتدأ المؤخّر، أي: رنينٌ كائن لها كائنٌ عليه.	2017-05-15 04:39:07.718138	2017-05-15 06:05:19.824491	عليه	\N
672	4	رَنِيـنٌ	---\n- 09\n	\N	7	7	46	مبتدأ مؤخر مرفوع بالضمة.	2017-05-15 04:39:22.23838	2017-05-15 06:05:19.829629	رنين	\N
673	4	وَهْيَ	---\n- '10'\n	\N	7	7	46	[الواو] واو الحال، و [هي] ضمير منفصل مبني على الفتح في محل رفع مبتدأ.	2017-05-15 04:39:37.566266	2017-05-15 06:05:19.835509	وهي	\N
674	4	مِفْتَـارُ	---\n- '11'\n	\N	7	7	46	خبر المبتدأ مرفوع بالضمة. وجملة [هي مفتار] في محل نصب حال.	2017-05-15 04:39:52.749402	2017-05-15 06:05:19.841405	مفتار	\N
675	3	حَقَّ	---\n- '04'\n	\N	7	7	47	وجب وثبت.	2017-05-15 04:51:55.27151	2017-05-15 06:05:19.846131	حق	\N
677	3	ضَرَّارُ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n- '11'\n- '12'\n	\N	7	7	47	مبالغة اسم الفاعل، مثل: شَرَّاب، أي: كثير الضرر.	2017-05-15 04:52:39.900988	2017-05-15 06:05:19.857357	ضرار	\N
678	4	تَبْكِي خُنَاسٌ	---\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n- '11'\n- '12'\n	\N	7	7	47	تقدم إعرابه في البيت السابق	2017-05-15 04:58:05.633043	2017-05-15 06:05:19.865617	تبكي خناس	\N
679	4	عَلَى	---\n- '02'\n	\N	7	7	47	حرف جرّ.	2017-05-15 04:58:20.467303	2017-05-15 06:05:19.87263	على	\N
680	4	صَخْرٍ	---\n- '03'\n	\N	7	7	47	مجرور بالكسرة وهما متعلقان بِـ [تبكي].	2017-05-15 05:02:14.480616	2017-05-15 06:05:19.877321	صخر	\N
681	4	وَحَقَّ	---\n- '04'\n	\N	7	7	47	[الواو] واو الحال، و [حقّ] فعل ماض مبني على الفتح، والفاعل مستتر تقديره [هو] يعود إلى البكاء المفهوم من الفعل [تبكي]. والجملة الفعلية في محل نصب حال من [خناس].	2017-05-15 05:02:36.870002	2017-05-15 06:05:19.882035	وحق	\N
682	4	لَهَا	---\n- '05'\n	\N	7	7	47	[اللام] حرف جرّ، و [ها] ضمير مبني على السكون في محل جرّ وهما متعلقان بالفعل [حَقَّ].	2017-05-15 05:02:51.990891	2017-05-15 06:05:19.886526	لها	\N
683	4	إِذْ	---\n- '07'\n	\N	7	7	47	ظرف زمان للماضي مبنية على السكون في محل نصب.	2017-05-15 05:03:05.856721	2017-05-15 06:05:19.891495	إذ	\N
684	4	رَابَهَا	---\n- 08\n	\N	7	7	47	[راب] فعل ماض مبني على الفتح، و [ها] ضمير مبني على السكون في محل نصب مفعول به مقدّم على الفاعل.	2017-05-15 05:03:21.353646	2017-05-15 06:05:19.896215	رابها	\N
685	4	الدَّهْرُ	---\n- 09\n	\N	7	7	47	فاعل [رَابَ] مرفوع بالضمة.	2017-05-15 05:03:36.469443	2017-05-15 06:05:19.90104	الدهر	\N
686	4	إِنَّ	---\n- '10'\n	\N	7	7	47	من الحروف المشبهة بالفعل.	2017-05-15 05:03:50.6942	2017-05-15 06:05:19.905878	إن	\N
687	4	الدَّهْرُ	---\n- 09\n	\N	7	7	47	اسم [إنَّ] منصوب بالفتحة.	2017-05-15 05:04:06.487244	2017-05-15 06:05:19.924647	الدهر	\N
688	4	ضَرَّارُ	---\n- '12'\n	\N	7	7	47	خبر [إنَّ] مرفوع بالضمة.	2017-05-15 05:04:20.537052	2017-05-15 06:05:19.930317	ضرار	\N
689	3	لاَ بُدَّ	---\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n- '11'\n- '12'\n	\N	7	7	48	لا محيد عنه ولا مَفَرَّ منه، والفعل منهُ [بَدَدَ] ولا يعرف استعماله إلا مقرونًا بالنفي.	2017-05-15 05:16:49.275966	2017-05-15 06:05:19.937271	لا بد	\N
690	3	مِيتَةٍ	---\n- '03'\n	\N	7	7	48	المِيتة –بكسر الميم- مصدر لبيان الهيئة والحالة والنوع، يقال: مات مِيتة حَسَنَةً. وأمّا بفتح الميم فيكون المصدر لبيان العدد وليس مقصودًا هنا.	2017-05-15 05:17:05.080024	2017-05-15 06:05:19.945507	ميتة	\N
1	3	قِفَا	---\n- '00'\n	\N	1	1	1	 .أمر بالوقوف، يقال: وقف الرجل في الموضع يقف وقوفًا	2017-03-30 07:54:45.589616	2017-05-15 06:05:15.918982	قفا	\N
5	3	اللِّـوَى	---\n- '07'\n	\N	1	1	1	 .المكان الذي يكون فيه آخر الرمل رقيقًا أي: مُنْقَطَعُ الرمل	2017-03-30 07:58:30.249225	2017-05-15 06:05:15.953533	اللوى	\N
15	4	قِفَا	---\n- '00'\n	\N	1	1	1	[قِفْ] فعل أمر، من وَقَفَ يَقِفُ، [الألف] فاعل مبني على السكون في محل رفع، وهو ألف الاثنين حقيقةً إذا كان يخاطب صاحبيه، أو مجازًا إذا كان يخاطب واحدًا بألف الاثنين جريًا على عادة العرب في إجراء الاثنين والجمع على الواحد. قال تعالى: ﴿أَلْقِيَا فِي جَهَنَّمَ كُلَّ كَفَّارٍ عَنِيدٍ﴾ (سورة ق : الآية 24)، أمر الله تعالى الملَكَ خازنَ النار - وهو واحد - أن يُلقِيَ فيها كلَّ كفّارٍ عنيد. وعلى هذا فيكون فعل الأمر [قِف] مبنيًّا على حذف النون في محل جزم لأنه من الأفعال الخمسة. ويمكن أن يكون الأمر للواحد، والفاعل مستتر تقديره (أَنْتَ)، أما الألف فللوقف إذ الأصل: [قِفَنْ] بنون التوكيد الخفيفة، لكنها قلبت ألفًا في حال الوقف، كقوله تعالى: ﴿لَنَسْفَعَنْ﴾ (سورة العلق : الآية 15)، فلو وقفت على النون قلبتها ألفًا وقلت: [لَنَسْفَعا]، لكن الشاعر حمل وصل الكلام على وقفه وأبقى الألف، وعلى هذا يكون فعل الأمر مبنيًا على الفتح في محل جزم لاتصاله بنون التوكيد الخفيفة.	2017-03-30 10:00:22.4037	2017-05-15 06:05:15.974016	قفا	\N
50	3	مُـعْـمَـرَاتٌ	---\n- 09\n	\N	3	3	15	جمع مفرده مُعْمَرةٌ، والمعمر الذي يوضع وديعة، أو الذي يُعَمِّرُ ويبقى مفيدًا ما بقي العمر، قالت العرب: هذه الدار لك عُمْرَى، أي: إنها لك لم ما عمرت. ويروى: عاريات ودائع.	2017-03-30 10:42:14.483716	2017-05-15 06:05:16.163372	معمرات	\N
63	4	لِيَبْـتَلِي	---\n- 08\n	\N	1	1	2	[اللام] لام كي الناصبة، و [يَبْتَلِي] فعل مضارع منصوب بالفتحة المقدرة على الياء، والفاعل مستتر تقديره (هو) يعود إلى الليل والمفعول مقدر أيضًا، والأصل: لكي يبتليني، فحذفت [كي] ونون الوقاية وياء المتكلم، وسكنت الياء لأغراض الوزن، ويجوز أن يكون الناصب للفعل [أن] الناصبة مقدرة بعد اللام أي: لأنْ يبتلينيَ.	2017-03-30 13:10:27.318118	2017-05-15 06:05:16.291262	ليبتلي	\N
71	4	لَـهُ	---\n- '01'\n	\N	1	1	3	[اللام] حرف جرّ، و [الهاء] مبني على الضم في محل جرّ متعلقان بـ [قُلْتُ]، والضمير [الهاء] يعود إلى الليل، أي : فقلت لليل، والبيتان اللذان بعد هذا البيت في محل نصب مقول القول.	2017-03-30 13:42:03.863467	2017-05-15 06:05:16.334463	له	\N
85	4	الطَّـوِيلُ	---\n- '03'\n	\N	1	1	4	 نعت لـ [اللَّيلُ] مرفوع، ويجوز نصب [اللَّيلَ وَالطَّوِيلَ] على أنه تابع لمحل [أيُّ] وهو النصب.	2017-03-30 14:10:43.488514	2017-05-15 06:05:16.502382	الطويل	\N
87	4	انْجَلِي	---\n- '05'\n	\N	1	1	4	فعل أمر مبني على حذف الياء، والفاعل مستتر تقديره (أَنْتَ) والأصل في لام [انْجَلِي] أن يكون ساكنًا لكنه كسر ليدل على الياء المحذوفة، مثل [ارْمِ] ثم اتبع الكسرة ياءً لغرض الوزن الشعري، والعرب تعمل ذلك، فتُتبع الفتحةُ ألفًا كقوله تعالى: ﴿سَنُقْرِؤُكَ فَلا تَنسَى﴾ (سورة الأعلى: الآية 6). والأصل: [تَنْسَ] مجزوم بلا الناهية، وتُتبع الضمةُ واوًا في قولهم: [لم تهجُو]، والأصل: [لم تهجُ] مجزوم بلم.	2017-03-30 14:12:00.635332	2017-05-15 06:05:16.5113	انجلي	\N
102	4	مُغَـارِ	---\n- '07'\n	\N	1	1	5	 مضاف إليه مجرور بالكسرة، وهو مضاف.	2017-03-30 14:32:13.99097	2017-05-15 06:05:16.620256	مغار	\N
103	4	الفَتْـلِ	---\n- 08\n	\N	1	1	5	مضاف إليه مجرور بالكسرة، وقوله [بكلّ مغار الفتل] شبه جملة في محل نصب حال مقدم من الضمير المستتر في شدّت.	2017-03-30 14:32:37.64673	2017-05-15 06:05:16.62725	الفتل	\N
115	4	وُكُنَـاتِهَا	---\n- '04'\n	\N	1	1	6	مجرور بالكسرة لأنه جمع مؤنث سالم، وهو مضاف، والضمير [ها] مبني على السكون في محل جر مضاف إليه، والجار والمجرور متعلقان بمحذوف خبر المبتدأ أي: كائنةٌ في وكناتها، والجملة الاسمية هذه في محل نصب حال.	2017-03-30 14:50:50.655089	2017-05-15 06:05:16.693793	وكناتها	\N
143	3	الأَهْلُـونَ	---\n- '02'\n	\N	3	3	16	جمع يشبه جمع المذكر السالم مفرده أهلٌ، والأهل هم الأقارب من الأولاد والآباء والأمهات والأحفاد وغيرهم. ويروى: [وما الناس والأموال]. 	2017-03-30 16:18:27.730415	2017-05-15 06:05:16.872394	الأهلون	\N
155	3	يُتَبِّـرُ	---\n- '05'\n	\N	3	3	18	 تَبَرَ يَتْبُرُ من باب نَصَرَ يَنْصُرُ ويجوز أن يكون من باب تَعِبَ يَتْعَبُ بمعنى هلك وهدم، ويتبر يهلك ويفسد ويهدم. قال تعالى: ﴿وَلِيُتَبِّرُواْ مَا عَلَوْاْ تَتْبِيراً﴾ الإسراء: 7.	2017-03-30 16:27:24.095477	2017-05-15 06:05:16.961021	يتبر	\N
692	3	عِبَـرٌ	---\n- '06'\n	\N	7	7	48	ويروى [غِيَرٌ] بالغين المعجمة، والعِبَرُ جمعٌ مفرده عِبرَةٌ بمعنى الاعتبار والاتعاظ والتذكر.	2017-05-15 05:17:30.014662	2017-05-15 06:05:19.954949	عبر	\N
693	3	الدَّهْرُ	---\n- 08\n	\N	7	7	48	الزمان والوقت.	2017-05-15 05:17:46.26243	2017-05-15 06:05:19.960234	الدهر	\N
694	3	صَرْفِهِ	---\n- '10'\n	\N	7	7	48	أحداثه.	2017-05-15 05:17:58.213131	2017-05-15 06:05:19.96509	صرفه	\N
695	3	حَولٌ	---\n- '11'\n	\N	7	7	48	أي: تقلّب وتحوّلٌ بأهله.	2017-05-15 05:18:11.301047	2017-05-15 06:05:19.969682	حول	\N
696	3	أَطْوَارُ	---\n- '12'\n	\N	7	7	48	جمع مفرده طور، أي: أحوال، حال بعد حال في التقلبات والتغيرات.	2017-05-15 05:18:27.324944	2017-05-15 06:05:19.974236	أطوار	\N
697	4	لاَ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n- '11'\n- '12'\n	\N	7	7	48	نافية للجنس تعمل عمل [إنّ].	2017-05-15 05:20:17.411719	2017-05-15 06:05:19.979711	لا	\N
698	4	بُدَّ	---\n- '01'\n	\N	7	7	48	اسم [لا] مبني على الفتح في محل نصب.	2017-05-15 05:20:35.665602	2017-05-15 06:05:19.988123	بد	\N
177	3	أُخَبِّرُ	---\n- '00'\n	\N	3	3	21	أثنْبِئُ والخبر هو النبأ.	2017-03-30 16:45:45.023534	2017-05-15 06:05:17.045668	أخبر	\N
179	3	القُـرُونِ	---\n- '02'\n	\N	3	3	21	جمعٌ مفرده قَرْنٌ، وهو الجيل من النّاس، فقيل: ثمانون سنة، وقيل: سبعون.	2017-03-30 16:48:26.293068	2017-05-15 06:05:17.064276	القرون	\N
191	3	لاَ تَبْعَـدَنْ	---\n- '00'\n- '01'\n	\N	3	3	23	من البَعْدِ، بفتح الباء والفعل بَعَدَ يبعَدُ وهو دعاء له. وأمّا البُعد بضم الباء فهو ضدّ القرب وفعلُهُ بَعُدَ يبْْعُدُ، وهو دعاء عليه.	2017-03-30 17:02:01.464074	2017-05-15 06:05:17.165725	لا تبعدن	\N
211	3	أَتَجْزَعُ	---\n- '00'\n	\N	3	3	26	الهمزة للاستفهام الإنكاري، أي: لا تجزع، والجزع الضعف عن تحمل ما ينزل من المصائب وهو ضدّ الصبر.	2017-03-30 17:21:08.370114	2017-05-15 06:05:17.307171	أتجزع	\N
215	3	القَـوَارِعُ	---\n- 09\n	\N	3	3	26	جمع مفردُهُ قارعة، وهي المصائب والدواهي التي تقرع القلوب وسميت القيامة والساعة بالقارعة لأنّها تقرع الخلائق بأهوالها وأفزاعها، والعرب تقول: قَرَعَتهُم القارعة وَفَقَرَتْهُم الفاقِرة: إذا وقع بهم أمرٌ فظيع.	2017-03-30 17:23:15.365263	2017-05-15 06:05:17.331196	القوارع	\N
265	4	فَلاَ	---\n- '01'\n	\N	3	3	12	[الفاء] حرف استئناف، و [لا] حرف نفي.	2017-03-30 18:07:12.503176	2017-05-15 06:05:17.568411	فلا	\N
267	4	يَـأْتِينِي	---\n- '02'\n	\N	3	3	12	[يأتي] فعل مضارع مرفوع بالضمة المقدّرة على الياء للثقل. و [النون] للوقاية، و [الياء] مفعول به مقدّم مبنية على السكون في محل نصب.	2017-03-30 18:07:53.958273	2017-05-15 06:05:17.579816	يأتيني	\N
283	4	حَلُّـوهَا	---\n- '07'\n	\N	3	3	13	[حَلَّ] فعل ماضٍ مبني على الضم لاتصاله بواو الجماعة، و [الواو] فاعل مبني على السكون في محل رفع، و [ها] مفعول به مبني على السكون في محل نصب، والأصل [حَلُّوا فيها] فحذف حرف الجر ووصل الضمير بالفعل. و [يوم] مضاف، وجملة [حلوها] مضاف إليه.	2017-03-30 18:17:51.417227	2017-05-15 06:05:17.662756	حلوها	\N
303	4	التُّقَى	---\n- '05'\n	\N	3	3	15	 مجرور بالكسرة المقدّرة على الألف، وهما متعلقان بفعل مقدّر أي: كائنات من التقى.	2017-03-30 18:28:09.910029	2017-05-15 06:05:17.76011	التقى	\N
307	4	وَالأَهْلُـونَ	---\n- '02'\n	\N	3	3	16	[الواو] حرف عطف، و [الأهلون] معطوف على المال مرفوع بالواو لأنّه ملحق بجمع المذكر السالم.	2017-03-30 18:30:49.496534	2017-05-15 06:05:17.797781	والأهلون	\N
322	4	المُشَـايِعُ	---\n- 08\n	\N	3	3	17	فاعل مؤخّر لِـ [ضَمَّ] ويُسْبَكَ الفعل [ضَمَّ]، و [ما] المصدرية فيؤوِّلاَن بِمصدر يَكُونُ محله الجر بالكاف، أي: كَضَمِّ المشايعِ أُخرى التاليات.	2017-03-30 18:37:18.244159	2017-05-15 06:05:17.873303	المشايع	\N
334	4	وَمِنْـهُمْ شَـقِيٌّ	---\n- '04'\n- '05'\n	\N	3	3	19	[الواو] حرف عطف على [فمنهم سعيد] وتعرب كإعرابها.	2017-03-30 18:43:04.887176	2017-05-15 06:05:17.937322	ومنهم شقي	\N
337	4	أَلَيـسَ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n	\N	3	3	20	[الهمزةُ] حرف استفهام، و [ليس] فعل ماضٍ ناقص من أخوات [كان] مبني على الفتح.	2017-03-30 18:44:45.658712	2017-05-15 06:05:17.962112	أليس	\N
341	4	مَنِيَّتِي	---\n- '04'\n	\N	3	3	20	[منية] فاعل مرفوع بالضمة المقدّرة منع من ظهورها كسرة ياء المتكلّم وهي مضاف، و [الياء] مضاف إليه مبنية على السكون في محل جرّ. وجملة [إن تراخت منيتي] معترضة بين خبر ليس واسمها لا محل لها من الإعراب، وجواب الشرط مقدّر يفهم من السياق، أي: إن تراخت منيتي لزمت العصا. 	2017-03-30 18:46:28.235723	2017-05-15 06:05:17.983471	منيتي	\N
362	4	تَقَـادُمُ	---\n- '05'\n	\N	3	3	22	فاعل [غَيَّرَ] مؤخّر مرفوع بالضمة وهو مضاف من إضافة المصدر إلى فاعله.	2017-03-30 18:57:00.28418	2017-05-15 06:05:18.094818	تقادم	\N
364	4	القَيـنِ	---\n- '07'\n	\N	3	3	22	مضاف إليه مجرور بالكسرة. والجملة الفعلية [غَيَّر جفنه تقام عهد القين] في محل نصب حال من السيف.	2017-03-30 18:57:48.59152	2017-05-15 06:05:18.110235	القين	\N
373	4	فَـدَانٍ	---\n- '06'\n	\N	3	3	23	[الفاء] تفسيرية، [دانٍ] نعت لمنعوت مقدّر مرفوع بالضمة المقّدرة على الياء المحذوفة لالتقاء الساكنين لأنّ الأصل: دَانِيٌّ كما تقدّم في شرح المفردات. والتقدير: فإنسانٌ دانٍ للطلوع.	2017-03-30 19:01:39.433161	2017-05-15 06:05:18.168196	فدان	\N
397	4	الرَّعَـارِعُ	---\n- '10'\n	\N	3	3	25	خبر إنّ مرفوع بالضمة.	2017-03-30 19:14:57.356143	2017-05-15 06:05:18.299643	الرعارع	\N
398	4	أَتَجْزَعُ	---\n- '00'\n	\N	3	3	26	 [الهمزة] حرف استفهام إنكاري مبنية على الفتح لا محل لها من الإعراب، و [تجزع] فعل مضارع مرفوع بالضمة، والفاعل مستتر تقديره [هي] يعود إلى [العاذلة] في الأبيات السابقة.	2017-03-30 19:15:22.970399	2017-05-15 06:05:18.306421	أتجزع	\N
408	4	لَعَمْرُكَ	---\n- '00'\n	\N	3	3	27	[اللام] حرف قسم مبني على الفتح لا محل له من الإعراب، و [عَمرُ] مبتدأ مرفوع بالضمة وهو مضاف، و [الكاف] مضاف إليه مبني على الفتح في محل جرّ، والخبر محذوف، أي: لَعمرُك قَسَمِي.	2017-03-30 19:20:36.156506	2017-05-15 06:05:18.36586	لعمرك	\N
421	4	كَذَّبْتُمُونِي	---\n- '02'\n	\N	3	3	28	[كَذَّبَ] فعل ماضٍ مبني على السكون في محل جزم فعل الشرط، و [التاء والميم والواو] ضمير جمع المذكَّر، وأصله [أنتم] بإشباع ضمة الميم واوًا، وهو فاعل مبني على السكون في محل رفع، و [النون] للوقاية، و [الياء] مفعول به مبنية على السكون في محل نصب، وجواب الشرط مقدّر أي: [إن كذّبتموني فاسئلوا]. وجملة [إن كذّبتموني] اعتراضية لا محل لها من الإعراب.	2017-03-30 19:25:13.187549	2017-05-15 06:05:18.452482	كذبتموني	\N
432	3	أَبَيتَ اللَّعْنَ	---\n- '01'\n- '02'\n	\N	6	6	31	امتنعت أن تأتي بشيء تستحق عليه اللعنَ، أو امتنعت أن تلعن أحدًا لسموّ أخلاقكَ.	2017-05-13 04:47:06.260333	2017-05-15 06:05:18.502148	أبيت اللعن	\N
744	3	صَخْرُ	---\n- '01'\n	\N	7	7	52	هو أخو الخنساء المَرثي.	2017-05-15 08:31:25.339765	2017-05-15 08:31:25.339765	صخر	هو أخو الخنساء المرثي.
436	4	أَتَانِي	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n	\N	6	6	31	أتى] فعل ماضٍ مبني على الفتح المقدّر على الألف منع من ظهوره التعذّر، و [النون] نون الوقاية حرف لا محل له من الإعراب، و [الياء] ياء المتكلّم مفعول به مبني على السكون في محل نصب، وسيأتي فاعل الفعل. 	2017-05-13 04:49:24.272962	2017-05-15 06:05:18.527649	أتاني	\N
445	3	فَبِتُّ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n	\N	6	6	32	سهرتُ الليل، يقال: بَاتَ يبيتُ بيتوتةً ومبيتًا ومَبَاتًا، واسم الفاعل منه بَائِت، وتستعمل قليلاً بمعنى نام ليلاً، لكنَّ أغلب معناها: فعل ذلكَ الفعلَ بالليل، ولا يكون إلا مع سَهر الليل، كقوله تعالى: ﴿وَالَّذِينَ يَبِيتُونَ لِرَبِّهِمْ سُجَّداً وَقِيَاماً﴾ الفرقان: 64.	2017-05-13 05:03:09.678208	2017-05-15 06:05:18.579467	فبت	\N
459	4	فِرَاشِي	---\n- 08\n	\N	6	6	32	[فراش] نائب فاعل مرفوع بالضمة المقدّرة منع من ظهورها كسرة ياء المتكلم وهو مضاف، و [الياء] مضاف إليه مبنية على السكون في محل جرّ.	2017-05-13 05:09:40.873926	2017-05-15 06:05:18.650483	فراشي	\N
460	4	ويُقْشَبُ	---\n- 09\n	\N	6	6	32	[الواو] حرف عطف، و [يقشب] فعل مضارع مرفوع بالضمة مبني للمجهول، ونائب الفاعل مستتر تقديره [هو] يعود إلى [الفراش]. والجملة الفعلية معطوفة على جملة [يُعلى فراشي].	2017-05-13 05:10:05.550701	2017-05-15 06:05:18.655827	ويقشب	\N
482	4	لَئِنْ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n	\N	6	6	34	[اللام] لام القسم مبني على الفتح لا محل له من الإعراب وهو واقع في جواب القسم الذي هو [حلفت] في البيت السابق. و [إن] حرف شرط جازم لفعلين.	2017-05-13 06:26:47.906181	2017-05-15 06:05:18.771335	لئن	\N
493	3	مُسْتَرَادٌ	---\n- 09\n	\N	6	6	35	من التردد وهو الإقبال والإخبار أي: حرّية الحركة.	2017-05-13 06:32:45.370529	2017-05-15 06:05:18.847823	مستراد	\N
495	4	وَلَكِنَّنِي	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n	\N	6	6	35	 [الواو] حرف استئناف، و [لكنّ] من الحروف المشبهة بالفعل تفيد معنى الاستدراك، و [النون] نون الوقاية، و [الياء] ياء المتكلم اسم [لكنّ] مبنية على السكون في محل نصب.	2017-05-13 06:34:43.698851	2017-05-15 06:05:18.863682	ولكنني	\N
513	4	أَتَيتُهُمْ	---\n- '04'\n	\N	6	6	36	[أتى] فعل ماضٍ مبني على السكون لاتصاله بضمير الرفع، وأصل ألف [أتى] ياء [أَتَيَ]|، و [التاء] فاعل مبني على الضم في محل رفع، و [هم] ضمير مفعول به مبني على السكون في محل نصب. وجملة [أتيتهم] شرط لِـ[إذا].	2017-05-13 06:45:15.430952	2017-05-15 06:05:18.957808	أتيتهم	\N
527	4	تَرَهُمْ	---\n- '07'\n	\N	6	6	37	[تر] فعل مضارع مجزوم بحذف حرف العلة الألف، والفاعل مستتر تقديره [أنت]، و [هم] مفعول به أوّل مبني على السكون في محل نصب. والجملة الفعلية معطوفة على [اصطنعتهم].	2017-05-13 07:00:11.512438	2017-05-15 06:05:19.032499	ترهم	\N
531	4	أَذْنَبُوا	---\n- '11'\n	\N	6	6	37	[أذنب] فعل ماضٍ مبني على الضم لاتصاله بواو الجماعة، و [الواو] فاعل مبني على السكون في محل رفع. وجملة [أذنبوا] في محل نصب مفعول ثان للفعل [ترهم] أي: لم ترهم مذنبين.	2017-05-13 07:01:34.387958	2017-05-15 06:05:19.052263	أذنبوا	\N
555	4	سُـورَةً	---\n- '05'\n	\N	6	6	39	 مفعول ثانٍ منصوب بالفتحة. والجملة من الفعل وفاعله ومفعوليه في محل رفع خبر [أنّ]. وجملة [أنّ الله أعطاك سورة] في تأويل مصدر مفعول به للفعل [ترى]، أي: ألم ترَ أعطاءَ اللهِ إِياك سورةً.	2017-05-13 07:16:03.997857	2017-05-15 06:05:19.208437	سورة	\N
557	4	كُلَّ	---\n- 08\n	\N	6	6	39	مفعول به منصوب بالفتحة وهو مضاف.	2017-05-13 07:16:38.566883	2017-05-15 06:05:19.217393	كل	\N
559	4	دُونَهَا	---\n- '10'\n	\N	6	6	39	[دون] ظرف مكان منصوب بالفتحة وهو مضاف، و [ها] ضمير مضاف إليه مبني على السكون في محل جرّ، والظرف متعلّق بالفعل [يتذبذب].	2017-05-13 07:17:12.055141	2017-05-15 06:05:19.232064	دونها	\N
575	3	مُسْتَبْـقٍ	---\n- '01'\n	\N	6	6	41	اسم فاعل فعلُهُ سداسي الحروف وهو استبقى، مثل: مُسْتَغْفِر وفعلُهُ استغفر. والمستبقي هو الذي يعفو عن الزلة ويحافظ على بقاء أخيه وصديقه معه على المودّة والمحبّة.	2017-05-13 07:33:32.549554	2017-05-15 06:05:19.305706	مستبق	\N
594	3	عُتْبَى	---\n- 09\n	\N	6	6	42	العُتبى اسم من الإعتاب وفعله أعتب ومعنى الهمزة الإزالة، يقال: أعتبته أَزلت عنه العتب، والعتب هو اللوم بِتَسَّخُطٍ، واستعتب: طلب الاعتابَ.	2017-05-13 07:47:33.42654	2017-05-15 06:05:19.405534	عتبى	\N
598	4	أَكُ	---\n- '01'\n	\N	6	6	42	فعل مضارع مجزم بالسكون فعل الشرط، وأصلها: [أكونُ] فلما سكنت النون للجزم حذف الواو لالتقاء الساكنين فصارت [أكُنْ] ثم حذفت النون على بعض اللغات العربية لأنّ ما بعدها متحرك فصارت [أكُ] وقد ورد ذلك في القرآن الكريم قال تعالى: ﴿وَقَدْ خَلَقْتُكَ مِن قَبْلُ وَلَمْ تَكُ شَيْئاً﴾ مريم: 9، أي: ولم تكنْ. واسم [كان] مستتر تقديره [أنا].	2017-05-13 07:49:57.149319	2017-05-15 06:05:19.425641	أك	\N
623	4	أَهْلِهَا	---\n- '11'\n	\N	7	7	43	[أهل] مجرور بالكسرة وهو مضاف، و [ها] مضاف إليه مبني على السكون في محل جرّ. والجار والمجرور متعلقان بالفعل [خلت].	2017-05-15 04:09:23.308821	2017-05-15 06:05:19.551312	أهلها	\N
627	3	خَطَرَتْ	---\n- '04'\n	\N	7	7	44	حضرت ذكراه في بالها، يقال: خطر ببالي، وعلى بالي خَطْرًا وخطورًا، والخاطر ما يخطر في القلب من تدبير الأمور.	2017-05-15 04:10:54.103194	2017-05-15 06:05:19.573558	خطرت	\N
644	3	وَلَهَتْ	---\n- '05'\n	\N	7	7	45	ويروى: [وقد ذَرَفَتْ]، ويروى: [وَقَدْ ثكلت]، والوله ما يصيب الإنسان من شدّة الجزع على الميت من ولد وغيره، والوالِهُ المرأة التي شَفَّها الحُزْنُ على ولدها، والوَالِهُ المشتاق أيضًا.	2017-05-15 04:21:22.321834	2017-05-15 06:05:19.674144	ولهت	\N
656	4	جَدِيدِ	---\n- 09\n	\N	7	7	45	مجرور بالكسرة وهما متعلقان بمحذوف خبر ثانٍ مقدم وهو مضاف.	2017-05-15 04:32:25.401991	2017-05-15 06:05:19.736207	جديد	\N
658	4	أَسْتَـارُ	---\n- '11'\n	\N	7	7	45	مبتدأ مؤخر مرفوع بالضمة. والأصل: أستار كائنة دونه كائنة من جديد التراب. والجملة هذه في محل نصب حال من صخرٍ.	2017-05-15 04:34:35.60403	2017-05-15 06:05:19.747459	أستار	\N
676	3	رَابَهَا	---\n- 08\n	\N	7	7	47	حَيَّرَهَا، يقال: رَابَ يروب رَوبًا إذا تحيَّر وصار كمن سكر من نوم أو شبع أي: اختلط عقله. ويقال: راب يَريب ريبًا وقع في الريب وهو الشك.	2017-05-15 04:52:22.927968	2017-05-15 06:05:19.850929	رابها	\N
691	3	صَرْفِهَا	---\n- '05'\n	\N	7	7	48	حدوثها، يقال: [صرف الدهر] أي: حادثه.	2017-05-15 05:17:16.953562	2017-05-15 06:05:19.950391	صرفها	\N
699	4	مِنْ	---\n- '02'\n	\N	7	7	48	حرف جرّ.	2017-05-15 06:49:06.439916	2017-05-15 06:49:06.439916	من	حرف جر.
700	4	مِيتَةٍ	---\n- '03'\n	\N	7	7	48	مجرور بالكسرة وهما متعلقان بمحذوف خبر [لا]، أي: لا بدّ حاصلٌ من مِيتة.	2017-05-15 06:49:20.597109	2017-05-15 06:49:20.597109	ميتة	مجرور بالكسرة وهما متعلقان بمحذوف خبر [لا]، أي: لا بد حاصل من ميتة.
712	3	نِعْمَ	---\n- '07'\n	\N	7	7	49	من أفعال المدح والثناء.	2017-05-15 06:57:54.552747	2017-05-15 06:57:54.552747	نعم	من أفعال المدح والثناء.
702	4	فِي	---\n- '04'\n	\N	7	7	48	حرف جرّ.	2017-05-15 06:49:54.853734	2017-05-15 06:49:54.853734	في	حرف جر.
703	4	صَرْفِهَا	---\n- '05'\n	\N	7	7	48	[صرف] مجرور بالكسرة وهو مضاف، و [ها] مضاف إليه مبني على السكون في محل جرّ وهما متعلقان بمحذوف في محل رفع خبر مقدّم، أي: عِبَرٌ كائِنَةٌ في صرفها. 	2017-05-15 06:50:10.214765	2017-05-15 06:50:10.214765	صرفها	[صرف] مجرور بالكسرة وهو مضاف، و [ها] مضاف إليه مبني على السكون في محل جر وهما متعلقان بمحذوف في محل رفع خبر مقدم، أي: عبر كائنة في صرفها. 
704	4	عِبَـرٌ	---\n- '06'\n	\N	7	7	48	مبتدأ مؤخر مرفوع بالضمة.	2017-05-15 06:50:26.42289	2017-05-15 06:50:26.42289	عبر	مبتدأ مؤخر مرفوع بالضمة.
705	4	وَالدَّهْرُ	---\n- 08\n	\N	7	7	48	[الواو] حرف استئناف، و [الدهر] مبتدأ مرفوع بالضمة.	2017-05-15 06:50:43.256468	2017-05-15 06:50:43.256468	والدهر	[الواو] حرف استئناف، و [الدهر] مبتدأ مرفوع بالضمة.
706	4	فِي	---\n- 09\n	\N	7	7	48	حرف جرّ.	2017-05-15 06:51:05.196549	2017-05-15 06:51:05.196549	في	حرف جر.
707	4	صَرْفِهِ	---\n- '10'\n	\N	7	7	48	[صرف] مجرور بالكسرة، و [الهاء] ضمير مبني على الكسر في محل جرّ وهما متعلقان بالمصدر [حَولٌ].	2017-05-15 06:51:20.645838	2017-05-15 06:51:20.645838	صرفه	[صرف] مجرور بالكسرة، و [الهاء] ضمير مبني على الكسر في محل جر وهما متعلقان بالمصدر [حول].
708	4	حَولٌ	---\n- '11'\n	\N	7	7	48	خبر المبتدأ [الدهر] مرفوع بالضمة.	2017-05-15 06:51:39.954742	2017-05-15 06:51:39.954742	حول	خبر المبتدأ [الدهر] مرفوع بالضمة.
709	4	وَأَطْوَارُ	---\n- '12'\n	\N	7	7	48	[الواو] حرف عطف، و [أطوار] معطوف على [حولٌ] مرفوع بالضمة.	2017-05-15 06:52:26.447877	2017-05-15 06:52:26.447877	وأطوار	[الواو] حرف عطف، و [أطوار] معطوف على [حول] مرفوع بالضمة.
710	3	أَبُو عَمْروٍ	---\n- '03'\n- '04'\n	\N	7	7	49	هي كنيةُ صخر أخي الشاعرة الخنساء	2017-05-15 06:55:08.997018	2017-05-15 06:55:08.997018	أبو عمرو	هي كنية صخر أخي الشاعرة الخنساء
711	3	يَسُودُكُمُ	---\n- '05'\n	\N	7	7	49	يكون سيّدًا عليكم، يقال: ساد يسود سيادةً، والاسم السودَدُ وهو المجد والشرف فهو سيد وهي سيّدةٌ وسيّد القوم رئيسهم وأكرمُهم.	2017-05-15 06:57:31.444707	2017-05-15 06:57:31.444707	يسودكم	يكون سيدا عليكم، يقال: ساد يسود سيادة، والاسم السودد وهو المجد والشرف فهو سيد وهي سيدة وسيد القوم رئيسهم وأكرمهم.
713	3	المُعَمَّـمُ	---\n- 08\n	\N	7	7	49	المُسّوَّدُ الذي جعله الناسُ سيّدًا عليهم، يقال: عُمِّمَ الرجلُ –بالبناء للمجهول- أي: سُوِّدَ. 	2017-05-15 06:58:11.402979	2017-05-15 06:58:11.402979	المعمم	المسود الذي جعله الناس سيدا عليهم، يقال: عمم الرجل –بالبناء للمجهول- أي: سود. 
714	3	لِلدَّاعِيـنَ	---\n- 09\n	\N	7	7	49	الطالبين المنادين المستنجدين.	2017-05-15 06:58:25.005176	2017-05-15 06:58:25.005176	للداعين	الطالبين المنادين المستنجدين.
715	3	نَصَّـارُ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n	\N	7	7	49	مبالغة اسم الفاعل، أي: كثير النصر.	2017-05-15 06:58:36.893216	2017-05-15 06:58:36.893216	نصار	مبالغة اسم الفاعل، أي: كثير النصر.
716	4	قَدْ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n	\N	7	7	49	حرف تحقيق لا محل له من الإعراب.	2017-05-15 06:59:55.736928	2017-05-15 06:59:55.736928	قد	حرف تحقيق لا محل له من الإعراب.
717	4	كَانَ	---\n- '01'\n	\N	7	7	49	فعل ماض ناقص مبني على الفتح.	2017-05-15 07:00:25.022629	2017-05-15 07:00:25.022629	كان	فعل ماض ناقص مبني على الفتح.
718	4	فِيكُم	---\n- '02'\n	\N	7	7	49	[في] حرف جرّ، و [كم] ضمير مبني على السكون في محل جرّ وهما متعلقان بالفعل [يسودكم].	2017-05-15 07:01:29.992748	2017-05-15 07:01:29.992748	فيكم	[في] حرف جر، و [كم] ضمير مبني على السكون في محل جر وهما متعلقان بالفعل [يسودكم].
719	4	أَبُو	---\n- '03'\n	\N	7	7	49	اسم [كان] مرفوع بالواو لأنّه من الأسماء الستة وهو مضاف.	2017-05-15 07:01:56.249019	2017-05-15 07:01:56.249019	أبو	اسم [كان] مرفوع بالواو لأنه من الأسماء الستة وهو مضاف.
720	4	عَمْروٍ	---\n- '04'\n	\N	7	7	49	مضاف إليه مجرور بالكسرة.	2017-05-15 07:02:10.217951	2017-05-15 07:02:10.217951	عمرو	مضاف إليه مجرور بالكسرة.
742	4	الصَّدْرِ	---\n- 09\n	\N	7	7	50	مضاف إليه مجرور بالكسرة، أي: جريءُ الصدر في الحروب.	2017-05-15 08:30:34.003113	2017-05-15 08:30:34.003113	الصدر	مضاف إليه مجرور بالكسرة، أي: جريء الصدر في الحروب.
743	4	مِهْصَارُ	---\n- '10'\n	\N	7	7	50	خبر رابع مرفوع بالضمة.	2017-05-15 08:30:48.648088	2017-05-15 08:30:48.648088	مهصار	خبر رابع مرفوع بالضمة.
721	4	يَسُودُكُمُ	---\n- '05'\n	\N	7	7	49	[يسود] فعل مضارع مرفوع بالضمة، والفاعل ضمير مستتر تقديره [هو] يعود إلى [أبي عمرو]، و [كم] ضمير مبني على السكون في محل نصب. والجملة الفعلية [يسودكم] في محل نصب خبر [كان] أي: كان أبو عمروٍ سيّدًا عليكم.	2017-05-15 07:02:38.974838	2017-05-15 07:02:38.974838	يسودكم	[يسود] فعل مضارع مرفوع بالضمة، والفاعل ضمير مستتر تقديره [هو] يعود إلى [أبي عمرو]، و [كم] ضمير مبني على السكون في محل نصب. والجملة الفعلية [يسودكم] في محل نصب خبر [كان] أي: كان أبو عمرو سيدا عليكم.
722	4	نِعْمَ	---\n- '07'\n	\N	7	7	49	فعل ماضٍ مبني على الفتح.	2017-05-15 07:02:53.755303	2017-05-15 07:02:53.755303	نعم	فعل ماض مبني على الفتح.
723	4	المُعَمَّـمُ	---\n- 08\n	\N	7	7	49	فاعل مرفوع بالضمة.	2017-05-15 07:03:08.02673	2017-05-15 07:03:08.02673	المعمم	فاعل مرفوع بالضمة.
724	4	لِلدَّاعِيـنَ	---\n- 09\n	\N	7	7	49	[اللام] حرف جرّ، و [الداعين] مجرور بالياء لأنّه جمع مذكر سالم، وهما متعلقان بمحذوف خبر مقدّم.	2017-05-15 07:03:23.57377	2017-05-15 07:03:23.57377	للداعين	[اللام] حرف جر، و [الداعين] مجرور بالياء لأنه جمع مذكر سالم، وهما متعلقان بمحذوف خبر مقدم.
725	4	نَصَّـارُ	---\n- '10'\n	\N	7	7	49	مبتدأ مؤخر مرفوع بالضمة، أي: نصارٌ للداعين.	2017-05-15 07:03:40.447444	2017-05-15 07:03:40.447444	نصار	مبتدأ مؤخر مرفوع بالضمة، أي: نصار للداعين.
726	3	صُلْبُ	---\n- '00'\n	\N	7	7	50	مصدر فعله صَلُبَ يصلُبُ من باب حسُن يَحْسُنُ، يقال: صَلُبَ الشيءُ صَلاَبَةً: اشتدَّ وقَوِيَ، فهو صُلْبٌ. ومكانٌ صُلْبٌ: غليظٌ شديدٌ.	2017-05-15 08:18:07.517011	2017-05-15 08:18:07.517011	صلب	مصدر فعله صلب يصلب من باب حسن يحسن، يقال: صلب الشيء صلابة: اشتد وقوي، فهو صلب. ومكان صلب: غليظ شديد.
727	3	النَّحِيزَةِ	---\n- '01'\n	\N	7	7	50	الطبيعة، وأصلها: طريقة من الرمل سوداء ممتدة كأنّها خيط مستوية مع الأرض خشنةٌ.	2017-05-15 08:18:42.277803	2017-05-15 08:18:42.277803	النحيزة	الطبيعة، وأصلها: طريقة من الرمل سوداء ممتدة كأنها خيط مستوية مع الأرض خشنة.
728	3	صُلْبُ	---\n- '00'\n	\N	7	7	50	مصدر فعله صَلُبَ يصلُبُ من باب حسُن يَحْسُنُ، يقال: صَلُبَ الشيءُ صَلاَبَةً: اشتدَّ وقَوِيَ، فهو صُلْبٌ. ومكانٌ صُلْبٌ: غليظٌ شديدٌ.	2017-05-15 08:19:00.53803	2017-05-15 08:19:00.53803	صلب	مصدر فعله صلب يصلب من باب حسن يحسن، يقال: صلب الشيء صلابة: اشتد وقوي، فهو صلب. ومكان صلب: غليظ شديد.
729	3	وَهَّابٌ	---\n- '02'\n	\N	7	7	50	مبالغة اسم الفاعل فعله وَهَبَ يَهَبُ إذا أعطى، أي: كثير العطاء.	2017-05-15 08:19:16.703021	2017-05-15 08:19:16.703021	وهاب	مبالغة اسم الفاعل فعله وهب يهب إذا أعطى، أي: كثير العطاء.
730	3	مَنَعُوا	---\n- '04'\n	\N	7	7	50	حجبوا العطاء عن الناس.	2017-05-15 08:25:43.631155	2017-05-15 08:25:43.631155	منعوا	حجبوا العطاء عن الناس.
731	3	جَرِيءُ	---\n- 08\n	\N	7	7	50	شجاع مقدام، وهو اسم فاعل من جَرُؤَ جَرَاءَةً على وزن ضَخُمَ ضَخَامَةً. واجترأ على القول –بالهمزة- أسرع بالهجوم عليه من غير توقُّفٍ.	2017-05-15 08:26:11.856076	2017-05-15 08:26:11.856076	جريء	شجاع مقدام، وهو اسم فاعل من جرؤ جراءة على وزن ضخم ضخامة. واجترأ على القول –بالهمزة- أسرع بالهجوم عليه من غير توقف.
732	3	الصَّدْرِ	---\n- 09\n	\N	7	7	50	صدر الإنسان معروف وسمي صدرًا لأنّه المتقدم إذا رُمِي به.	2017-05-15 08:26:31.257583	2017-05-15 08:26:31.257583	الصدر	صدر الإنسان معروف وسمي صدرا لأنه المتقدم إذا رمي به.
733	3	مِهْصَارُ	---\n- '10'\n	\N	7	7	50	صيغة مبالغة لاسم الفاعل، وهو الذي يهصر الأعناق، أي: يدقها.	2017-05-15 08:26:42.836817	2017-05-15 08:26:42.836817	مهصار	صيغة مبالغة لاسم الفاعل، وهو الذي يهصر الأعناق، أي: يدقها.
734	4	صُلْبُ	---\n- '00'\n	\N	7	7	50	خبر أوّل لمبتدأ محذوف تقديره: هو صلب مرفوع بالضمة وهو مضاف.	2017-05-15 08:28:00.430613	2017-05-15 08:28:00.430613	صلب	خبر أول لمبتدأ محذوف تقديره: هو صلب مرفوع بالضمة وهو مضاف.
735	4	النَّحِيزَةِ	---\n- '01'\n	\N	7	7	50	مضاف إليه مجرور بالكسرة.	2017-05-15 08:28:16.980029	2017-05-15 08:28:16.980029	النحيزة	مضاف إليه مجرور بالكسرة.
736	4	وَهَّابٌ	---\n- '02'\n	\N	7	7	50	خبر ثان مرفوع بالضمة.	2017-05-15 08:28:35.608489	2017-05-15 08:28:35.608489	وهاب	خبر ثان مرفوع بالضمة.
737	4	إِذَا	---\n- '03'\n	\N	7	7	50	ظرفية لما يستقبل من الزمان شرطية غير جازمة مبنية على السكون في محل نصب بجوابها.	2017-05-15 08:28:55.8632	2017-05-15 08:28:55.8632	إذا	ظرفية لما يستقبل من الزمان شرطية غير جازمة مبنية على السكون في محل نصب بجوابها.
738	4	مَنَعُوا	---\n- '04'\n	\N	7	7	50	[منع] فعل ماض مبني على الضم، و [الواو] ضمير فاعل مبني على السكون في محل رفع، والمفعول مقدر أي: منعوا العطاءَ. والجملة شرط لِـ [إذا]، وجوابها مقدر أي: إذا منعوا وَهَبَ.	2017-05-15 08:29:16.415067	2017-05-15 08:29:16.415067	منعوا	[منع] فعل ماض مبني على الضم، و [الواو] ضمير فاعل مبني على السكون في محل رفع، والمفعول مقدر أي: منعوا العطاء. والجملة شرط ل [إذا]، وجوابها مقدر أي: إذا منعوا وهب.
739	4	وَفِي	---\n- '06'\n	\N	7	7	50	[الواو] حرف عطف، و [في] حرف جرّ.	2017-05-15 08:29:45.180917	2017-05-15 08:29:45.180917	وفي	[الواو] حرف عطف، و [في] حرف جر.
740	4	الحُرُوبِ	---\n- '07'\n	\N	7	7	50	مجرور بالكسرة وهما متعلقان بِـ [جريءٌ].	2017-05-15 08:30:05.303346	2017-05-15 08:30:05.303346	الحروب	مجرور بالكسرة وهما متعلقان ب [جريء].
741	4	جَرِيءُ	---\n- 08\n	\N	7	7	50	خبر ثالث مرفوع بالضمة وهو مضاف.  	2017-05-15 08:30:19.497674	2017-05-15 08:30:19.497674	جريء	خبر ثالث مرفوع بالضمة وهو مضاف.  
745	3	وَرَّادَ	---\n- '02'\n	\N	7	7	52	مبالغة اسم الفاعل. والمقصود بِـ [ورَّاد ماءٍ] الموت لإقدامه على الحرب.	2017-05-15 08:31:40.365525	2017-05-15 08:31:40.365525	وراد	مبالغة اسم الفاعل. والمقصود ب [وراد ماء] الموت لإقدامه على الحرب.
746	3	تَنَاذَرَهُ	---\n- '05'\n	\N	7	7	52	أي: أنذر بعضهم بعضًا بهول الموت وصعوبته، ويروى [تبادره]، ويروى [توارده].	2017-05-15 08:31:56.173295	2017-05-15 08:31:56.173295	تناذره	أي: أنذر بعضهم بعضا بهول الموت وصعوبته، ويروى [تبادره]، ويروى [توارده].
747	3	أَهْـلُ المَوَارِدِ	---\n- '07'\n- 08\n	\N	7	7	52	أصحاب الحروب.	2017-05-15 08:32:13.821737	2017-05-15 08:32:13.821737	أهل الموارد	أصحاب الحروب.
748	3	عَارُ	---\n- '12'\n	\N	7	7	52	الشيء الذي يلزم منه عيب أو سبٌّ، ويقال: عيَّرته، أي: قبحته.	2017-05-15 08:32:31.469729	2017-05-15 08:32:31.469729	عار	الشيء الذي يلزم منه عيب أو سب، ويقال: عيرته، أي: قبحته.
749	4	يَا	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n- '11'\n- '12'\n	\N	7	7	52	حرف نداء مبني على السكون لا محل له من الإعراب.	2017-05-15 08:35:21.211892	2017-05-15 08:35:21.211892	يا	حرف نداء مبني على السكون لا محل له من الإعراب.
750	4	صَخْرُ	---\n- '01'\n	\N	7	7	52	منادى مبني على الضم في محل نصب لأنّه مفرد علم.	2017-05-15 08:35:40.626568	2017-05-15 08:35:40.626568	صخر	منادى مبني على الضم في محل نصب لأنه مفرد علم.
751	4	وَرَّادَ	---\n- '02'\n	\N	7	7	52	نعت لِـ [صخر] منصوب بالفتحة تابع لمحل المنادى لأنّ نعت المنادى إذا كان مضافًا وليس فيه الألف واللام يتعين نصبه على المحل، كأنه منادى مستقلٌ، أي: يا ورَّادَ ماءٍ، و [ورَّاد] مضاف.	2017-05-15 08:35:56.786617	2017-05-15 08:35:56.786617	وراد	نعت ل [صخر] منصوب بالفتحة تابع لمحل المنادى لأن نعت المنادى إذا كان مضافا وليس فيه الألف واللام يتعين نصبه على المحل، كأنه منادى مستقل، أي: يا وراد ماء، و [وراد] مضاف.
752	4	مَـاءٍ	---\n- '03'\n	\N	7	7	52	مضاف إليه مجرور بالكسرة.	2017-05-15 08:36:22.294657	2017-05-15 08:36:22.294657	ماء	مضاف إليه مجرور بالكسرة.
753	4	قَـدْ	---\n- '04'\n	\N	7	7	52	حرف تحقيق لا محل لها من الإعراب.	2017-05-15 08:36:42.543425	2017-05-15 08:36:42.543425	قد	حرف تحقيق لا محل لها من الإعراب.
754	4	تَنَاذَرَهُ	---\n- '05'\n	\N	7	7	52	[تناذر] فعل ماض مبني على الفتح، و [الهاء] مفعول به مبني على الضم في محل نصب.	2017-05-15 08:36:59.084787	2017-05-15 08:36:59.084787	تناذره	[تناذر] فعل ماض مبني على الفتح، و [الهاء] مفعول به مبني على الضم في محل نصب.
755	4	أَهْـلُ	---\n- '07'\n	\N	7	7	52	فاعل مرفوع بالضمة وهو مضاف.	2017-05-15 08:37:16.960812	2017-05-15 08:37:16.960812	أهل	فاعل مرفوع بالضمة وهو مضاف.
756	4	المَوَارِدِ	---\n- 08\n	\N	7	7	52	مضاف إليه مجرور بالكسرة.	2017-05-15 08:37:32.892513	2017-05-15 08:37:32.892513	الموارد	مضاف إليه مجرور بالكسرة.
757	4	مَا	---\n- 09\n	\N	7	7	52	نافية.	2017-05-15 08:37:45.394609	2017-05-15 08:37:45.394609	ما	نافية.
758	4	فِي	---\n- '10'\n	\N	7	7	52	حرف جرّ.	2017-05-15 08:37:59.609243	2017-05-15 08:37:59.609243	في	حرف جر.
759	4	وِرْدِهِ	---\n- '11'\n	\N	7	7	52	[وردِ] مجرور بالكسرة وهو مضاف، و [الهاء] مضاف إليه مبني على الكسر في محل جرّ، وهما متعلقان بمحذوف خبر مقدّم.	2017-05-15 08:38:14.285124	2017-05-15 08:38:14.285124	ورده	[ورد] مجرور بالكسرة وهو مضاف، و [الهاء] مضاف إليه مبني على الكسر في محل جر، وهما متعلقان بمحذوف خبر مقدم.
760	4	عَارُ	---\n- '12'\n	\N	7	7	52	مبتدأ مؤخر مرفوع بالضمة، أي: ما عارٌ كائن في وردِهِ.	2017-05-15 08:38:27.599385	2017-05-15 08:38:27.599385	عار	مبتدأ مؤخر مرفوع بالضمة، أي: ما عار كائن في ورده.
761	3	مَشَى	---\n- '00'\n	\N	7	7	53	من المشي وهو عدم الوقوف.	2017-05-15 08:39:14.045526	2017-05-15 08:39:14.045526	مشى	من المشي وهو عدم الوقوف.
762	3	السَّبَنْتَى	---\n- '01'\n	\N	7	7	53	ويقال: السبندى -بالدال- والمعنى واحد وهو كل سَبُعٍ من أسد وذئب ونمر. فكل واحد منها جَرِيءٌ. ولذلك سمي المقاتل الشجاع الجريء [سَبَنْتَى].	2017-05-15 08:39:28.504275	2017-05-15 08:39:28.504275	السبنتى	ويقال: السبندى -بالدال- والمعنى واحد وهو كل سبع من أسد وذئب ونمر. فكل واحد منها جريء. ولذلك سمي المقاتل الشجاع الجريء [سبنتى].
763	3	هَيجَاءَ	---\n- '03'\n	\N	7	7	53	-بالمدّ والقصر- يقال: هاجت الحرب هَيجًا إذا قامت ونشبت.	2017-05-15 08:39:44.046611	2017-05-15 08:39:44.046611	هيجاء	-بالمد والقصر- يقال: هاجت الحرب هيجا إذا قامت ونشبت.
764	3	مُعْضِلَةٍ	---\n- '04'\n	\N	7	7	53	شديدة.	2017-05-15 08:40:00.807129	2017-05-15 08:40:00.807129	معضلة	شديدة.
765	3	أَنْيَابٌ	---\n- 08\n	\N	7	7	53	جمعٌ مفرده ناب، والناب من الأسنان هو الذي يلي الرَّباعيات. وعددها أربعة اثنان فوق واثنان أسفل.	2017-05-15 08:40:20.385912	2017-05-15 08:40:20.385912	أنياب	جمع مفرده ناب، والناب من الأسنان هو الذي يلي الرباعيات. وعددها أربعة اثنان فوق واثنان أسفل.
766	3	أَظْفَـارُ	---\n- 09\n	\N	7	7	53	جمعٌ مفرده ظفرٌ، والظفر معروف في اليدين والرجلين.	2017-05-15 08:40:35.265283	2017-05-15 08:40:35.265283	أظفار	جمع مفرده ظفر، والظفر معروف في اليدين والرجلين.
767	4	مَشَى	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n	\N	7	7	53	فعل ماض مبني على الفتح المقدّر على الألف.	2017-05-15 08:41:46.198623	2017-05-15 08:41:46.198623	مشى	فعل ماض مبني على الفتح المقدر على الألف.
768	4	السَّبَنْتَى	---\n- '01'\n	\N	7	7	53	فاعل مرفوع بالضمة المقدّرة على الألف للتعذر.	2017-05-15 08:42:15.566977	2017-05-15 08:42:15.566977	السبنتى	فاعل مرفوع بالضمة المقدرة على الألف للتعذر.
769	4	إِلَى	---\n- '02'\n	\N	7	7	53	حرف جرّ.	2017-05-15 08:42:29.055608	2017-05-15 08:42:29.055608	إلى	حرف جر.
770	4	هَيجَاءَ	---\n- '03'\n	\N	7	7	53	مجرور بالفتحة نيابة عن الكسرة لأنّه ممنوع من الصرف، وهما متعلقان بالفعل [مشى].	2017-05-15 08:42:55.874823	2017-05-15 08:42:55.874823	هيجاء	مجرور بالفتحة نيابة عن الكسرة لأنه ممنوع من الصرف، وهما متعلقان بالفعل [مشى].
771	4	مُعْضِلَةٍ	---\n- '04'\n	\N	7	7	53	نعت لِـ [هيجاء] مجرور بالكسرة.	2017-05-15 08:43:29.140752	2017-05-15 08:43:29.140752	معضلة	نعت ل [هيجاء] مجرور بالكسرة.
772	4	لَهُ	---\n- '06'\n	\N	7	7	53	[اللام] حرف جرّ، و [الهاء] ضمير مبني على الضم في محل جرّ، وهما متعلقان بمحذوف خبر مقدّم.	2017-05-15 08:43:44.138917	2017-05-15 08:43:44.138917	له	[اللام] حرف جر، و [الهاء] ضمير مبني على الضم في محل جر، وهما متعلقان بمحذوف خبر مقدم.
773	4	سِلاَحَـانِ	---\n- '07'\n	\N	7	7	53	مبتدأ مؤخر مرفوع بالألف لأنّه مثنى، أي: سلاحان كائنان له.	2017-05-15 08:44:04.799366	2017-05-15 08:44:04.799366	سلاحان	مبتدأ مؤخر مرفوع بالألف لأنه مثنى، أي: سلاحان كائنان له.
774	4	أَنْيَابٌ	---\n- 08\n	\N	7	7	53	بدل من [سلاحان] مرفوع بالضمة.	2017-05-15 08:44:18.068087	2017-05-15 08:44:18.068087	أنياب	بدل من [سلاحان] مرفوع بالضمة.
775	4	وَأَظْفَـارُ	---\n- 09\n	\N	7	7	53	[الواو] حرف حرف عطف، و [أظفار] معطوف على [أنياب] مرفوع بالضمة.	2017-05-15 08:44:33.262654	2017-05-15 08:44:33.262654	وأظفار	[الواو] حرف حرف عطف، و [أظفار] معطوف على [أنياب] مرفوع بالضمة.
776	3	وَمَا	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n	\N	7	7	54	ويروى [فما].	2017-05-15 08:44:59.029669	2017-05-15 08:44:59.029669	وما	ويروى [فما].
777	3	عَجُـولٌ	---\n- '01'\n	\N	7	7	54	العجول من الإبل التي يموت ولدها وهو صغير، وسميت عجولاً لعجلتها في جَيئتها وذهابها جزعًا وحزنًا على ولدها الصغير.	2017-05-15 09:24:56.955229	2017-05-15 09:24:56.955229	عجول	العجول من الإبل التي يموت ولدها وهو صغير، وسميت عجولا لعجلتها في جيئتها وذهابها جزعا وحزنا على ولدها الصغير.
778	3	بَوٍّ	---\n- '03'\n	\N	7	7	54	البوُّ من الإبل الحُوار، وقيل: هو ولد الناقة الذي ينحر ويحشى جلده تبنًا أو شيئًا آخر من أنواع الشجر ويُدْنى من أُمِّ الفصيل، فَتَرْأَمَهُ وتعطف عليه فتدر عليه لبنها.	2017-05-15 09:25:14.057801	2017-05-15 09:25:14.057801	بو	البو من الإبل الحوار، وقيل: هو ولد الناقة الذي ينحر ويحشى جلده تبنا أو شيئا آخر من أنواع الشجر ويدنى من أم الفصيل، فترأمه وتعطف عليه فتدر عليه لبنها.
779	3	تُطِيفُ	---\n- '04'\n	\N	7	7	54	تدور حوله.	2017-05-15 09:25:28.648985	2017-05-15 09:25:28.648985	تطيف	تدور حوله.
780	3	إِعْـلاَنٌ وَإِسْـرَارُ	---\n- 09\n- '10'\n	\N	7	7	54	تفسير للحنينين، أي: حنين بصوتٍ منخفض وحنين بصوتٍ عالٍ. ويروى [إصغار وإكبار].	2017-05-15 09:25:54.625872	2017-05-15 09:25:54.625872	إعلان وإسرار	تفسير للحنينين، أي: حنين بصوت منخفض وحنين بصوت عال. ويروى [إصغار وإكبار].
781	4	وَمَا	---\n- '00'\n	\N	7	7	54	[الواو] أو [الفاء] على رواية، حرف استئناف، و [ما] نافية.	2017-05-15 09:27:09.291611	2017-05-15 09:27:09.291611	وما	[الواو] أو [الفاء] على رواية، حرف استئناف، و [ما] نافية.
782	4	عَجُـولٌ	---\n- '01'\n	\N	7	7	54	صفة مشبهة مبتدأ مرفوع بالضمة، وفاعلها ضمير مستقر سدّ مسدَّ الخبر تقديره [هما].	2017-05-15 09:27:24.223692	2017-05-15 09:27:24.223692	عجول	صفة مشبهة مبتدأ مرفوع بالضمة، وفاعلها ضمير مستقر سد مسد الخبر تقديره [هما].
783	4	عَلَى	---\n- '02'\n	\N	7	7	54	حرف جرّ.	2017-05-15 09:27:37.803309	2017-05-15 09:27:37.803309	على	حرف جر.
784	4	بَوٍّ	---\n- '03'\n	\N	7	7	54	مجرور بالكسرة وهما متعلقان بِـ [عجول].	2017-05-15 09:27:52.896779	2017-05-15 09:27:52.896779	بو	مجرور بالكسرة وهما متعلقان ب [عجول].
785	4	تُطِيفُ	---\n- '04'\n	\N	7	7	54	فعل مضارع مرفوع بالضمة، والفاعل مستتر تقديره [هو] يعود إلى [العجول].	2017-05-15 09:28:11.294448	2017-05-15 09:28:11.294448	تطيف	فعل مضارع مرفوع بالضمة، والفاعل مستتر تقديره [هو] يعود إلى [العجول].
786	4	بِهِ	---\n- '05'\n	\N	7	7	54	[الباء] حرف جرّ، و [الهاء] مبني على الكسر في محل جرّ وهما متعلقان بالفعل [تطيف].	2017-05-15 09:28:25.674719	2017-05-15 09:28:25.674719	به	[الباء] حرف جر، و [الهاء] مبني على الكسر في محل جر وهما متعلقان بالفعل [تطيف].
787	4	لَهَا	---\n- '07'\n	\N	7	7	54	[اللام] حرف جرّ، و [ها] مبني على السكون في محل جرّ، وهما متعلقان بمحذوف خبر مقدم.	2017-05-15 09:28:41.10385	2017-05-15 09:28:41.10385	لها	[اللام] حرف جر، و [ها] مبني على السكون في محل جر، وهما متعلقان بمحذوف خبر مقدم.
788	4	حَنِينَـانِ	---\n- 08\n	\N	7	7	54	إقيال وإدبار، تعرب كإعراب [سلاحان أنياب وأظفار في البيت السابق].	2017-05-15 09:28:58.41311	2017-05-15 09:28:58.41311	حنينان	إقيال وإدبار، تعرب كإعراب [سلاحان أنياب وأظفار في البيت السابق].
789	3	تَرْتَعُ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n	\N	7	7	55	يروى: [ترتع ما غَفَلَتْ]، ويروى: [ترتع في غَفْلَةٍ] ومعنى ترتع ترعى.	2017-05-15 09:29:21.680642	2017-05-15 09:29:21.680642	ترتع	يروى: [ترتع ما غفلت]، ويروى: [ترتع في غفلة] ومعنى ترتع ترعى.
790	3	ادّكَرَتْ	---\n- '05'\n	\N	7	7	55	تذكرت وأصل الفعل: [اِذْتَكَرَ] على وزن [افتعل]. فقلبت الذال دالاً وادغمت في التاء فصار [اِدَّكَرَ]. 	2017-05-15 09:29:33.926716	2017-05-15 09:29:33.926716	ادكرت	تذكرت وأصل الفعل: [اذتكر] على وزن [افتعل]. فقلبت الذال دالا وادغمت في التاء فصار [ادكر]. 
791	4	تَرْتَعُ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n	\N	7	7	55	فعل مضارع مرفوع بالضمة، والفاعل مستتر تقديره [هي].	2017-05-15 09:39:17.024213	2017-05-15 09:39:17.024213	ترتع	فعل مضارع مرفوع بالضمة، والفاعل مستتر تقديره [هي].
792	4	مَا	---\n- '01'\n	\N	7	7	55	اسم موصول بمعنى الذي.	2017-05-15 09:39:30.01219	2017-05-15 09:39:30.01219	ما	اسم موصول بمعنى الذي.
793	4	رَتَعَتْ	---\n- '02'\n	\N	7	7	55	[رتع] فعل ماض مبني على الفتح، و [التاء] تاء التأنيث الساكنة، والفاعل مستتر تقديره [هي] أي الناقة، والمفعول به مقدّر يكون رابطًا للصلة بموصولها، أي: ترتع الذي رتعته.	2017-05-15 09:39:51.02353	2017-05-15 09:39:51.02353	رتعت	[رتع] فعل ماض مبني على الفتح، و [التاء] تاء التأنيث الساكنة، والفاعل مستتر تقديره [هي] أي الناقة، والمفعول به مقدر يكون رابطا للصلة بموصولها، أي: ترتع الذي رتعته.
794	4	حَتَّى	---\n- '03'\n	\N	7	7	55	حرف ابتداء، وقيل: حرف جرّ.	2017-05-15 09:40:05.635973	2017-05-15 09:40:05.635973	حتى	حرف ابتداء، وقيل: حرف جر.
795	4	إِذَا	---\n- '04'\n	\N	7	7	55	تكون ظرفَ زمان منصوبة إن قلنا: إن حتى حرف ابتداء، وإن قلنا: إن حتى حرف جرّ فلا تكون ظرفًا منصوبًا بل اسمًا للوقت مجرورًا بِـ [حتى].	2017-05-15 09:40:19.562643	2017-05-15 09:40:19.562643	إذا	تكون ظرف زمان منصوبة إن قلنا: إن حتى حرف ابتداء، وإن قلنا: إن حتى حرف جر فلا تكون ظرفا منصوبا بل اسما للوقت مجرورا ب [حتى].
796	4	ادّكَرَتْ	---\n- '05'\n	\N	7	7	55	[اِدَّكر] فعل ماض مبني على الفتح، و [التاء] تاء التأنيث الساكنة، والفاعل مستتر تقديره [هي] يعود إلى [الناقة]. والجملة شرط لِـ [إذا].	2017-05-15 09:40:35.674976	2017-05-15 09:40:35.674976	ادكرت	[ادكر] فعل ماض مبني على الفتح، و [التاء] تاء التأنيث الساكنة، والفاعل مستتر تقديره [هي] يعود إلى [الناقة]. والجملة شرط ل [إذا].
797	4	فَإِنَّـمَا	---\n- '07'\n	\N	7	7	55	[الفاء] رابطة للجواب، و [إنما] أداة حصر.	2017-05-15 09:40:56.225824	2017-05-15 09:40:56.225824	فإنما	[الفاء] رابطة للجواب، و [إنما] أداة حصر.
798	4	هِـيَ	---\n- 08\n	\N	7	7	55	مبتدأ مبني على الفتح في محل رفع.	2017-05-15 09:41:09.975055	2017-05-15 09:41:09.975055	هي	مبتدأ مبني على الفتح في محل رفع.
799	4	إِقْبَـالٌ	---\n- 09\n	\N	7	7	55	نائبة عن الخبر مرفوعة بالضمة، أي: ذاتُ إقبالٍ.	2017-05-15 09:41:24.845209	2017-05-15 09:41:24.845209	إقبال	نائبة عن الخبر مرفوعة بالضمة، أي: ذات إقبال.
800	4	وَإِدْبَـارُ	---\n- '10'\n	\N	7	7	55	[الواو] حرف عطف، و [إدبار] معطوف على [إقبال] مرفوع بالضمة، أي: وذات إدبار. وجملة [فإنما ...ألخ] جواب [إذا].	2017-05-15 09:41:41.631093	2017-05-15 09:41:41.631093	وإدبار	[الواو] حرف عطف، و [إدبار] معطوف على [إقبال] مرفوع بالضمة، أي: وذات إدبار. وجملة [فإنما ...ألخ] جواب [إذا].
801	3	تَسْمَنُ	---\n- '01'\n	\N	7	7	56	السَّمنُ ما يُعمَل من لبن البقر والغنم. ويقال: سمِنَ يَسمَنُ من باب تَعِبَ يَتْعَبُ إذا كثر لحمه وشحمه.	2017-05-15 09:42:08.902059	2017-05-15 09:42:08.902059	تسمن	السمن ما يعمل من لبن البقر والغنم. ويقال: سمن يسمن من باب تعب يتعب إذا كثر لحمه وشحمه.
802	3	رُبِعَتْ	---\n- '06'\n	\N	7	7	56	أصابها مطر الربيع. وفي رواية [رَتَعَتْ] أي: رعتْ. 	2017-05-15 09:42:22.131405	2017-05-15 09:42:22.131405	ربعت	أصابها مطر الربيع. وفي رواية [رتعت] أي: رعت. 
803	3	تَحْنَـانٌ	---\n- '10'\n	\N	7	7	56	يقال: حنت الناقة إذا طَرَّبتْ في إثر ولدها.	2017-05-15 09:42:36.754412	2017-05-15 09:42:36.754412	تحنان	يقال: حنت الناقة إذا طربت في إثر ولدها.
804	3	تَـسْجَـارُ	---\n- '11'\n	\N	7	7	56	إذا أخرجت الناقة صوتًا من حلقها ولم تفتح فاها. ويقال: إذا حنَّتْ الناقة وطرَّبَتْ في إثر ولدها فهو تحنان، وإذا مَدَّتْ حنينها قيل: سَجَّرَتْ.	2017-05-15 09:42:51.299882	2017-05-15 09:42:51.299882	تسجار	إذا أخرجت الناقة صوتا من حلقها ولم تفتح فاها. ويقال: إذا حنت الناقة وطربت في إثر ولدها فهو تحنان، وإذا مدت حنينها قيل: سجرت.
805	4	لاَ	---\n- '00'\n- '01'\n- '02'\n- '03'\n- '04'\n- '05'\n- '06'\n- '07'\n- 08\n- 09\n- '10'\n- '11'\n	\N	7	7	56	نافية.	2017-05-15 09:43:58.363652	2017-05-15 09:43:58.363652	لا	نافية.
806	4	تَسْمَنُ	---\n- '01'\n	\N	7	7	56	فعل مضارع مرفوع بالضمة، والفاعل مستتر تقديره [هي] يعود إلى [الناقة].	2017-05-15 09:44:11.89906	2017-05-15 09:44:11.89906	تسمن	فعل مضارع مرفوع بالضمة، والفاعل مستتر تقديره [هي] يعود إلى [الناقة].
807	4	الدَّهْرَ	---\n- '02'\n	\N	7	7	56	ظرف زمان منصوب بالفتحة.	2017-05-15 09:44:27.2914	2017-05-15 09:44:27.2914	الدهر	ظرف زمان منصوب بالفتحة.
808	4	فِي	---\n- '03'\n	\N	7	7	56	حرف جرّ.	2017-05-15 09:44:38.904993	2017-05-15 09:44:38.904993	في	حرف جر.
809	4	أَرْضٍ	---\n- '04'\n	\N	7	7	56	مجرور بالكسرة وهما متعلقان بالفعل [تَسْمَنُ].	2017-05-15 09:44:53.771273	2017-05-15 09:44:53.771273	أرض	مجرور بالكسرة وهما متعلقان بالفعل [تسمن].
810	4	وَإِنْ	---\n- '05'\n	\N	7	7	56	[الواو] حرف عطف، و [إن] حرف شرط جازم لفعلين.	2017-05-15 09:45:18.806342	2017-05-15 09:45:18.806342	وإن	[الواو] حرف عطف، و [إن] حرف شرط جازم لفعلين.
811	4	رُبِعَتْ	---\n- '06'\n	\N	7	7	56	[رُبِعَ] فعل ماض للمجهول مبني على الفتح، و [التاء] تاء التأنيث الساكنة لا محل لها من الإعراب. ونائب الفاعل مستتر تقديره [هي] يعود إلى [الناقة]. وعلى رواية [رَتَعَتْ] بالبناء للمعلوم فالإعراب كذلك. والجملة في محل جزم شرط لِـ [إِنْ]. والجواب مقدّر أي: وإنْ رَتَعَتْ لا تَسْمَنُ. 	2017-05-15 09:46:19.113707	2017-05-15 09:46:19.113707	ربعت	[ربع] فعل ماض للمجهول مبني على الفتح، و [التاء] تاء التأنيث الساكنة لا محل لها من الإعراب. ونائب الفاعل مستتر تقديره [هي] يعود إلى [الناقة]. وعلى رواية [رتعت] بالبناء للمعلوم فالإعراب كذلك. والجملة في محل جزم شرط ل [إن]. والجواب مقدر أي: وإن رتعت لا تسمن. 
812	4	فَإِنَّمَا هِيَ تَحْنَـانٌ وَتَـسْجَـارُ	---\n- 08\n- 09\n- '10'\n- '11'\n	\N	7	7	56	تعرب كإعراب [فإنما هي إقبال وإدبار] في البيت السابق.	2017-05-15 09:46:35.975006	2017-05-15 09:46:35.975006	فإنما هي تحنان وتسجار	تعرب كإعراب [فإنما هي إقبال وإدبار] في البيت السابق.
813	4	بِأَوجَـدَ	---\n- '01'\n	\N	7	7	57	بأشدَّ وجدًا ومحبة وشوقًا. والبيت جواب لقولها في الأبيات السابقة [وما عَجُولٌ ....]، أي: ما ناقةٌ عجولٌ بأشدَّ وجدًا على ولدها مني على صخرٍ.	2017-05-15 09:48:31.252222	2017-05-15 09:48:31.252222	بأوجد	بأشد وجدا ومحبة وشوقا. والبيت جواب لقولها في الأبيات السابقة [وما عجول ....]، أي: ما ناقة عجول بأشد وجدا على ولدها مني على صخر.
814	3	إِحْـلاَءٌ وَإِمْرَارُ	---\n- 08\n- 09\n	\N	7	7	57	إن الدهر يأتي بالحلو المحبوب والمرّ المكروه ويأتي بمحبة ومشقَّةٍ. ويقال: ما أحلى وما أَمَرَّ، أي: ما أتى بحُلَوةٍ ولا بِمُرَّةٍ. 	2017-05-15 09:48:49.826944	2017-05-15 09:48:49.826944	إحلاء وإمرار	إن الدهر يأتي بالحلو المحبوب والمر المكروه ويأتي بمحبة ومشقة. ويقال: ما أحلى وما أمر، أي: ما أتى بحلوة ولا بمرة. 
815	4	يَومًا	---\n- '00'\n	\N	7	7	57	ظرف منصوب بالفتحة أي: في يومٍ.	2017-05-15 09:49:56.393034	2017-05-15 09:49:56.393034	يوما	ظرف منصوب بالفتحة أي: في يوم.
816	4	بِأَوجَـدَ	---\n- '01'\n	\N	7	7	57	[الباء] حرف جرّ، و [أوجدَ] اسم تفضيل مجرور بالفتحة نيابة عن الكسرة.	2017-05-15 09:50:11.523651	2017-05-15 09:50:11.523651	بأوجد	[الباء] حرف جر، و [أوجد] اسم تفضيل مجرور بالفتحة نيابة عن الكسرة.
817	4	مِنِّي	---\n- '02'\n	\N	7	7	57	[من] حرف جرّ. و [النون] الثانية نون الوقاية. و [الياء] ياء المتكلّم مبنية على السكون في محل جر وهما متعلقان بِـ [أوجدَ]. 	2017-05-15 09:50:28.88364	2017-05-15 09:50:28.88364	مني	[من] حرف جر. و [النون] الثانية نون الوقاية. و [الياء] ياء المتكلم مبنية على السكون في محل جر وهما متعلقان ب [أوجد]. 
818	4	يَـومَ	---\n- '03'\n	\N	7	7	57	ظرف زمان منصوب بالفتحة.	2017-05-15 09:50:44.00699	2017-05-15 09:50:44.00699	يوم	ظرف زمان منصوب بالفتحة.
819	4	فَارَقَنِي	---\n- '04'\n	\N	7	7	57	[فَارَقَ] فعل ماض مبني على الفتح. و [النون] للوقاية. و [الياء] مفعول به مبني على السكون في محل نصب.	2017-05-15 09:51:03.445582	2017-05-15 09:51:03.445582	فارقني	[فارق] فعل ماض مبني على الفتح. و [النون] للوقاية. و [الياء] مفعول به مبني على السكون في محل نصب.
820	4	صَخْرٌ	---\n- '06'\n	\N	7	7	57	فاعل مرفوع بالضمة.	2017-05-15 09:51:15.70486	2017-05-15 09:51:15.70486	صخر	فاعل مرفوع بالضمة.
821	4	وَلِلدَّهْرِ	---\n- '07'\n	\N	7	7	57	[الواو] حرف استئناف. و [اللام] حرف جرّ. و [الدهر] مجرور بالكسرة وهما متعلقان بمحذوف خبر مقدّم.	2017-05-15 09:51:33.166293	2017-05-15 09:51:33.166293	وللدهر	[الواو] حرف استئناف. و [اللام] حرف جر. و [الدهر] مجرور بالكسرة وهما متعلقان بمحذوف خبر مقدم.
822	4	إِحْـلاَءٌ	---\n- 08\n	\N	7	7	57	مبتدأ مؤخّر مرفوع بالضمة.	2017-05-15 09:51:45.413216	2017-05-15 09:51:45.413216	إحلاء	مبتدأ مؤخر مرفوع بالضمة.
823	4	وَإِمْرَارُ	---\n- 09\n	\N	7	7	57	[الواو] حرف عطف. و [امرار] معطوف على [إحلاء] مرفوع بالضمة، أي: إحلاء وامرار كائنان للدهر.	2017-05-15 09:52:00.401277	2017-05-15 09:52:00.401277	وإمرار	[الواو] حرف عطف. و [امرار] معطوف على [إحلاء] مرفوع بالضمة، أي: إحلاء وامرار كائنان للدهر.
\.


--
-- Name: word_references_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('word_references_id_seq', 823, true);


--
-- Name: ckeditor_assets_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ckeditor_assets
    ADD CONSTRAINT ckeditor_assets_pkey PRIMARY KEY (id);


--
-- Name: comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);


--
-- Name: lesson_lines_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY lesson_lines
    ADD CONSTRAINT lesson_lines_pkey PRIMARY KEY (id);


--
-- Name: lessons_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY lessons
    ADD CONSTRAINT lessons_pkey PRIMARY KEY (id);


--
-- Name: line_references_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY line_references
    ADD CONSTRAINT line_references_pkey PRIMARY KEY (id);


--
-- Name: lines_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY lines
    ADD CONSTRAINT lines_pkey PRIMARY KEY (id);


--
-- Name: poems_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY poems
    ADD CONSTRAINT poems_pkey PRIMARY KEY (id);


--
-- Name: poets_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY poets
    ADD CONSTRAINT poets_pkey PRIMARY KEY (id);


--
-- Name: semesters_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY semesters
    ADD CONSTRAINT semesters_pkey PRIMARY KEY (id);


--
-- Name: stanzas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY stanzas
    ADD CONSTRAINT stanzas_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: word_references_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY word_references
    ADD CONSTRAINT word_references_pkey PRIMARY KEY (id);


--
-- Name: idx_ckeditor_assetable; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_ckeditor_assetable ON ckeditor_assets USING btree (assetable_type, assetable_id);


--
-- Name: idx_ckeditor_assetable_type; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_ckeditor_assetable_type ON ckeditor_assets USING btree (assetable_type, type, assetable_id);


--
-- Name: index_comments_on_lesson_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_comments_on_lesson_id ON comments USING btree (lesson_id);


--
-- Name: index_comments_on_semester_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_comments_on_semester_id ON comments USING btree (semester_id);


--
-- Name: index_comments_on_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_comments_on_user_id ON comments USING btree (user_id);


--
-- Name: index_lesson_lines_on_lesson_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_lesson_lines_on_lesson_id ON lesson_lines USING btree (lesson_id);


--
-- Name: index_lesson_lines_on_line_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_lesson_lines_on_line_id ON lesson_lines USING btree (line_id);


--
-- Name: index_lessons_on_semester_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_lessons_on_semester_id ON lessons USING btree (semester_id);


--
-- Name: index_line_references_on_line_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_line_references_on_line_id ON line_references USING btree (line_id);


--
-- Name: index_lines_on_stanza_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_lines_on_stanza_id ON lines USING btree (stanza_id);


--
-- Name: index_poems_on_poet_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_poems_on_poet_id ON poems USING btree (poet_id);


--
-- Name: index_semesters_on_user_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_semesters_on_user_id ON semesters USING btree (user_id);


--
-- Name: index_stanzas_on_poem_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_stanzas_on_poem_id ON stanzas USING btree (poem_id);


--
-- Name: index_users_on_approved; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_users_on_approved ON users USING btree (approved);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);


--
-- Name: index_users_on_uid_and_provider; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_uid_and_provider ON users USING btree (uid, provider);


--
-- Name: index_word_references_on_line_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_word_references_on_line_id ON word_references USING btree (line_id);


--
-- Name: index_word_references_on_poem_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_word_references_on_poem_id ON word_references USING btree (poem_id);


--
-- Name: index_word_references_on_stanza_id; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX index_word_references_on_stanza_id ON word_references USING btree (stanza_id);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: fk_rails_0079fda145; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY lines
    ADD CONSTRAINT fk_rails_0079fda145 FOREIGN KEY (stanza_id) REFERENCES stanzas(id);


--
-- Name: fk_rails_00a9db1905; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY word_references
    ADD CONSTRAINT fk_rails_00a9db1905 FOREIGN KEY (line_id) REFERENCES lines(id);


--
-- Name: fk_rails_162d37b0a6; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY line_references
    ADD CONSTRAINT fk_rails_162d37b0a6 FOREIGN KEY (line_id) REFERENCES lines(id);


--
-- Name: fk_rails_26303e1a3e; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT fk_rails_26303e1a3e FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_32435f7716; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY lessons
    ADD CONSTRAINT fk_rails_32435f7716 FOREIGN KEY (poem_id) REFERENCES poems(id);


--
-- Name: fk_rails_36293fac27; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY semesters
    ADD CONSTRAINT fk_rails_36293fac27 FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: fk_rails_41217d89ec; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT fk_rails_41217d89ec FOREIGN KEY (semester_id) REFERENCES semesters(id);


--
-- Name: fk_rails_56b9549e54; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY lessons
    ADD CONSTRAINT fk_rails_56b9549e54 FOREIGN KEY (poet_id) REFERENCES poets(id);


--
-- Name: fk_rails_7dd4b166d0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY word_references
    ADD CONSTRAINT fk_rails_7dd4b166d0 FOREIGN KEY (poem_id) REFERENCES poems(id);


--
-- Name: fk_rails_7fcf5fead9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY lessons
    ADD CONSTRAINT fk_rails_7fcf5fead9 FOREIGN KEY (semester_id) REFERENCES semesters(id);


--
-- Name: fk_rails_9ff999ddca; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stanzas
    ADD CONSTRAINT fk_rails_9ff999ddca FOREIGN KEY (poem_id) REFERENCES poems(id);


--
-- Name: fk_rails_dd2c7bf771; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY lesson_lines
    ADD CONSTRAINT fk_rails_dd2c7bf771 FOREIGN KEY (lesson_id) REFERENCES lessons(id);


--
-- Name: fk_rails_e26c1e13fa; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY word_references
    ADD CONSTRAINT fk_rails_e26c1e13fa FOREIGN KEY (stanza_id) REFERENCES stanzas(id);


--
-- Name: fk_rails_e4485b4e7f; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY lesson_lines
    ADD CONSTRAINT fk_rails_e4485b4e7f FOREIGN KEY (line_id) REFERENCES lines(id);


--
-- Name: fk_rails_fe825869a5; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT fk_rails_fe825869a5 FOREIGN KEY (lesson_id) REFERENCES lessons(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

