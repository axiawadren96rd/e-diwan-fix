json.extract! @lesson, :id, :name, :title, :objective,:content, :created_at, :updated_at, :semester_id
json.lines @lesson.lines  do |line| 
   json.id line.id
  json.text line.text
  json.line_references line.line_references do | line_ref |
    json.extract! line_ref, :content, :id , :reference_type_text, :media, :reference_type
  end
  json.splitted_text line.splitted_text
  json.word_references line.word_references do | word|
    json.extract! word , :id , :line_id, :poem_id, :stanza_id, :reference_type, :word , :word_indexes      
  end
end
if @lesson.poem
  json.poem do
    json.extract! @lesson.poem, :id, :poem_text, :description, :poet, :title, :created_at, :updated_at
    json.stanzas @lesson.poem.stanzas do |stanza|
      json.id stanza.id
      json.lines stanza.lines do |line|
        json.id line.id
        json.text line.text
        json.line_references line.line_references do | line_ref |
          json.extract! line_ref, :content, :id , :reference_type_text
        end
        json.splitted_text line.splitted_text
        json.word_references line.word_references do | word|
          json.extract! word , :id , :line_id, :poem_id, :stanza_id, :reference_type, :word , :word_indexes      
        end
      end
    end
    json.audio @lesson.poem.audio
  end
  
end

if @lesson.poet
  json.poet do
    json.extract! @lesson.poet, :id, :name, :year, :created_at, :updated_at , :biography
    json.audio @lesson.poet.media
  end
end

json.content @lesson.content 