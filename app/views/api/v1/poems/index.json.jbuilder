
json.array! @poems do | poem |
	json.extract! poem, :id, :description, :poet, :title, :created_at, :updated_at
end
