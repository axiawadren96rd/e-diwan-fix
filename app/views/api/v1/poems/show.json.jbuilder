json.extract! @poem, :id, :title, :poem_text, :description,  :audio, :poet, :created_at, :updated_at
json.stanzas @poem.stanzas do |stanza|
  json.id stanza.id
  json.lines stanza.lines do |line|
    json.id line.id
    json.text line.text
    json.line_references line.line_references do | line_ref |
      json.line_ref line_ref
      json.content line_ref.content
    end
    json.splitted_text line.splitted_text
    json.word_references line.word_references do | word|
      json.extract! word , :id , :line_id, :poem_id, :stanza_id, :reference_type, :word , :word_indexes      
    end
  end
end