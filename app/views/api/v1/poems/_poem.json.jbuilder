json.extract! poem, :id, :poem_text, :description, :audio, :poet, :title, :created_at, :updated_at
json.url poem_url(poem, format: :json)
json.avatar_url poem.avatar_url
json.words poem.word_references.words, :id, :reference_type, :poem_id, :word, :word_indexes, :stanza_id, :line_id, :description, :media_url
json.videos poem.word_references.videos, :id, :reference_type, :poem_id, :word, :word_indexes, :stanza_id, :line_id, :description, :media_url
json.images poem.word_references.images, :id, :reference_type, :poem_id, :word, :word_indexes, :stanza_id, :line_id, :description, :media_url
json.audios poem.word_references.audios, :id, :reference_type, :poem_id, :word, :word_indexes, :stanza_id, :line_id, :description, :media_url
json.grammars poem.word_references.grammars, :id, :reference_type, :poem_id, :word, :word_indexes, :stanza_id, :line_id, :description, :media_url
