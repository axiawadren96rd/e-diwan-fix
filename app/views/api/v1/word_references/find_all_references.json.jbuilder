
json.word_references @word_references do | word_reference|
  json.extract! word_reference, :id , :reference_type, :word, :word_indexes, :media, :line_id, :description, :media_type
  json.media_url word_reference.media_url
  json.media_type word_reference.media_type
end
json.word_reference_titles @word_references.word_reference_titles do | word_reference |
  json.extract! word_reference, :id, :media_type, :type_title
end
