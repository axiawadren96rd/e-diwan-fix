json.extract! word_reference, :id , :reference_type, :word, :word_indexes, :media, :line_id, :description
json.media_url word_reference.media_url
json.media_type word_reference.media_type