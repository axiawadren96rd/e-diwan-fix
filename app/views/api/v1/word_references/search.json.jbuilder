
json.array! @words do | word |
	json.extract! word, :id , :reference_type, :word, :word_indexes, :media, :line_id, :description
end