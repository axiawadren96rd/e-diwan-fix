json.array! @comments do |comment|
  json.user comment.user.name if comment.user
  json.user_id comment.user_id
  json.extract! comment, :id, :comment, :created_at, :updated_at
end