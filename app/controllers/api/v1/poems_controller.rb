class Api::V1::PoemsController < Api::V1::BaseController

#  before_action :authenticate_user! 
  # GET /api/v1/poems
  # GET /api/v1/poems.json
  def index
    @poems = Poem.all.order(:id)
  end


  # GET /api/v1/poems/1
  # GET /api/v1/poems/1.json
  def show
    @poem = Poem.find(params[:id])
    @stanza = Stanza.new(:poem => @poem)
  end

end
