class Api::V1::PoetsController < Api::V1::BaseController
  #before_action :authenticate_user! 
  # GET /api/v1/poets
  # GET /api/v1/poets.json
  def index
    @poets = Poet.all.order(:id)
  end


  # GET /api/v1/poets/1
  # GET /api/v1/poets/1.json
  def show
    @poet = Poet.find(params[:id])
    
  end

end
