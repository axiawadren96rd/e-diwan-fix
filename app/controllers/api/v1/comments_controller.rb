class Api::V1::CommentsController < Api::V1::BaseController

  before_filter :authenticate_user! , only: [:create , :index]
  def index

    @lesson = Lesson.find(params[:lesson_id])
    @comments = @lesson.comments.current_semester.order(:created_at)
  end

  # GET /api/v1/Lessons
  # GET /api/v1/Lessons.json
  def create
    if user_signed_in? 
      @comment = Comment.new(secure_params)
      @comment.semester = Semester.currently_activated
      @comment.user = current_user
      respond_to do |format|
        if @comment.save
          format.json
        else
          format.json { render json: @comment.errors, status: :unprocessable_entity }
        end
      end
    else
      render json: {}, status: :not_created
    end

  end


  # GET /api/v1/Lessons/1
  # GET /api/v1/Lessons/1.json
  def show
    @lesson = Lesson.find(params[:id])
  end



  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def secure_params
      params.require(:comment).permit( :lesson_id , :comment )
    end


end
