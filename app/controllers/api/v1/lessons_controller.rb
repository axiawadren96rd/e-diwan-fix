class Api::V1::LessonsController < Api::V1::BaseController

  # GET /api/v1/Lessons
  # GET /api/v1/Lessons.json
  def index

    if user_signed_in?  
      @lessons = Lesson.all.order(:name)
    else
      @lessons = Lesson.previews
    end
  end


  # GET /api/v1/Lessons/1
  # GET /api/v1/Lessons/1.json
  def show
    
    #if user_signed_in?  
      @lesson = Lesson.find(params[:id])    
    #else
    #  @lesson = Lesson.previews.where(:id => params[:id]).first
    #end
  end

end
