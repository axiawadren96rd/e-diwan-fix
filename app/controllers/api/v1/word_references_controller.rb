 class Api::V1::WordReferencesController < Api::V1::BaseController

  def index
    word_index = params[:word_index]
    poem_id = params[:poem_id]
    line_id = params[:line_id]
    @word_references = WordReference.where(" word_index = ? and poem_id = ? and line_id = ? " , word_index, poem_id , line_id ).order(:id).page(params[:page]).per(20)
  end

  def show
    @word_reference = WordReference.find(params[:id])
  end

  def search
    @words = WordReference.search(params[:query]).order(:word).page(params[:page]).per(10)
  end

  def find
    @word_reference = WordReference.find(params[:id])
  end

  def find_all_references
    @word_references = WordReference.word_indexes_with_line(params[:word_index], params[:line_id])
    #@word_reference_titles = @word_references.word_reference_titles()
    
    #@word_references = WordReference.find(params[:id])
  end
end
