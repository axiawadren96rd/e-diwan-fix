class GlossaryController < BaseSecureController
  def index
    @words = WordReference.search(params[:query]).includes(:poem).page(params[:page])
    
  end


  def show_glossary_word
    @word_references = WordReference.word_with_line(params[:word], params[:line_id])
    @word_reference_titles = WordReference.select(:reference_type).word_with_line(params[:word], params[:line_id]).group(:reference_type)
    render :partial => "show_glossary_word"
  end
  def show
  end

end
