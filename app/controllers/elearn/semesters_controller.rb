class Elearn::SemestersController <  Elearn::BaseController
  before_action :set_elearn_semester, only: [:show, :edit, :update, :destroy ]

  # GET /elearn/semesters
  # GET /elearn/semesters.json
  def index
    @elearn_semesters = Semester.all.order(:name).page(params[:page])
  end

  # GET /elearn/semesters/1
  # GET /elearn/semesters/1.json
  def show
  end

  # GET /elearn/semesters/new
  def new
    @elearn_semester = Semester.new
  end

  # GET /elearn/semesters/1/edit
  def edit
  end

  # POST /elearn/semesters
  # POST /elearn/semesters.json
  def create
    @elearn_semester = Semester.new(elearn_semester_params)
    @elearn_semester.user = current_user
    respond_to do |format|
      if @elearn_semester.save
        format.html { redirect_to [:elearn, :semesters], notice: 'Semester was successfully created.' }
        format.json { render :show, status: :created, location: @elearn_semester }
      else
        format.html { render :new }
        format.json { render json: @elearn_semester.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /elearn/semesters/1
  # PATCH/PUT /elearn/semesters/1.json
  def update
    respond_to do |format|
      if @elearn_semester.update(elearn_semester_params)
        format.html { redirect_to  [:elearn, :semesters], notice: 'Semester was successfully updated.' }
        format.json { render :show, status: :ok, location: @elearn_semester }
      else
        format.html { render :edit }
        format.json { render json: @elearn_semester.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /elearn/semesters/1
  # DELETE /elearn/semesters/1.json
  def destroy
    @elearn_semester.destroy
    respond_to do |format|
      format.html { redirect_to elearn_semesters_url, notice: 'Semester was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def activate
    Semester.update_all(activated: false)

    @semester = Semester.find(params[:semester_id])
    @semester.activated = true
     respond_to do |format|
      if @semester.save()
        format.html { redirect_to  [:elearn, :semesters], notice: 'Semester was successfully updated.' }
        format.json { render :show, status: :ok, location: @elearn_semester }
      else
        format.html { render :edit }
        format.json { render json: @elearn_semester.errors, status: :unprocessable_entity }
      end
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_elearn_semester
      @elearn_semester = Semester.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def elearn_semester_params
      params.require(:semester).permit(:name, :user_id, :status, :activated)
    end
end
