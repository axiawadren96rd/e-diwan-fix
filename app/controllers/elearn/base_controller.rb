class Elearn::BaseController < ApplicationController
  before_action :authenticate_user!
#  before_filter :check_redirect


private
  def check_redirect
    if !current_user
      redirect_to home_path , alert: "Only activated user can access the resource"
    end
  end
end