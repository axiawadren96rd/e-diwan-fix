class Elearn::CommentsController < Elearn::BaseController
  
  def index
    @lesson = Lesson.find(params[:lesson_id])
    @comments = Comment.where(:lesson_id => params[:lesson_id], :semester_id => params[:semester_id])

    render :partial => "comments"
  end
  def new
  end


  def create
    @lesson = Lesson.find(params[:lesson_id])
    @comment = @lesson.comments.new(secured_params)
    @comment.user = current_user 
    @comment.semester = Semester.currently_activated
    respond_to do |format|
      if @comment.save
        format.html { redirect_to [:elearn , @comment.lesson ], notice: 'Comment was successfully added.' }
        format.json { render :show, status: :created, location: @comment }
      else
        format.html { render :new }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @lesson = Lesson.find(params[:lesson_id])
    @comment = Comment.find(params[:id])
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to elearn_lesson_path(@lesson.id), notice: 'Comment was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def secured_params
    params.require(:comment).permit(:comment)
  end
end
