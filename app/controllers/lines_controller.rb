class LinesController < BaseSecureController
  def new

    @poem = Poem.find(params[:poem_id])
    @stanza = Stanza.find(params[:stanza_id])
    @line = Line.new(:stanza => @stanza)
    render :partial => "new"
  end

  def show
    @line = Line.find(params[:id])
    @word_references = @line.word_references    
    render :partial => "show"
  end

  def create
    @poem = Poem.find(params[:poem_id])
    @stanza = Stanza.find(params[:stanza_id])
    @line = @stanza.lines.new(secure_params)

    respond_to do |format|
      if @line.save
        format.html { redirect_to poem_path(@poem), notice: 'Line was successfully created.' }
        format.json { render :show, status: :created, location: @poem }
      else
        format.html { render :new }
        format.json { render json: @line.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @line = Line.find(params[:id])    
    @stanza = @line.stanza
    @poem = @stanza.poem
    
    render :partial => "edit"
  end

  def update
    @line = Line.find(params[:id])    
    @stanza = @line.stanza
    @poem = @stanza.poem
    @line.word_references.destroy_all
    
    
    respond_to do |format|
      if @line.update(secure_params)
        format.html { redirect_to @poem, notice: 'Line was successfully updated.' }
        format.json { render :show, status: :ok, location: @poem }
      else
        format.html { render :edit }
        format.json { render json: @poem.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @line = Line.find(params[:id])
    @line.word_references.destroy_all
    @line.destroy
    @poem = @line.stanza.poem

    #TODO Delete all Words under the stanza
    respond_to do |format|
      format.html { redirect_to poem_path(@poem), notice: 'Line was successfully destroyed.' }
      format.json { head :no_content }
    end
  end



  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def secure_params
      params.require(:line).permit(:text, :audio)
    end
end
