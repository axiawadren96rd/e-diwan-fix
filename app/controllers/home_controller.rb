class HomeController < ApplicationController
  
  layout 'home_layout'  
  
#  before_filter :redirect_if_logged_in

  def index
  end


  def lessons_preview
    @lessons = Lesson.previews
    respond_to do |format|    
      format.html { render "elearn/lessons/index" }
      format.json {}    
    end
  end

  def show_preview_lesson    
    @lesson = Lesson.where(id: params[:id], status: 3).first
    if(!@lesson)
      respond_to do |format|    
        format.html { redirect_to root_path }
        format.json {}    
      end
    else
      @comments = @lesson.comments.current_semester      
      respond_to do |format|    
        format.html { render "elearn/lessons/show" }
        format.json {}    
      end
    end
  end

  def show_preview_word
    @word_references = WordReference.word_indexes_with_line(params[:id], params[:line_id])
    @word_reference_titles = WordReference.select(:reference_type).word_indexes_with_line(params[:id], params[:line_id]).group(:reference_type)
    
    respond_to do |format|    
      format.html { render :partial => "word_references/show" }
      format.json {}    
    end
    
  end

private  
  def redirect_if_logged_in
    if current_user
      redirect_to elearn_lessons_path 
    end
  end

end
