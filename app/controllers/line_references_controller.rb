class LineReferencesController < BaseSecureController
  

  def new

    @poem = Poem.find(params[:poem_id])
    @stanza = Stanza.find(params[:stanza_id])
    @line = Line.find(params[:line_id])    

    @line_ref = LineReference.new(:line => @line)

    render :partial => "new"
  end



  def create
    @line_ref = LineReference.new(secure_params)


    respond_to do |format|
      if @line_ref.save
        format.html { redirect_to @line_ref.line.stanza.poem, notice: 'Line Reference was successfully created.' }
        format.json { render :show, status: :created, location: @line_ref }
      else
        format.html { render :new }
        format.json { render json: @line_ref.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @line_ref = LineReference.find(params[:id]) 

    @poem = Poem.find(params[:poem_id])
    @stanza = Stanza.find(params[:stanza_id])
    @line = Line.find(params[:line_id])    
    render :partial => "edit"
  end


  def update
    @line_ref = LineReference.find(params[:id]) 
    @poem = @line_ref.line.stanza.poem
    respond_to do |format|
      if @line_ref.update(secure_params)
        format.html { redirect_to @poem, notice: 'Line was successfully updated.' }
        format.json { render :show, status: :ok, location: @poem }
      else
        format.html { render :edit }
        format.json { render json: @poem.errors, status: :unprocessable_entity }
      end
    end

  end
  def destroy
    @line_ref = LineReference.find(params[:id]) 
    @poem = @line_ref.line.stanza.poem
    @line_ref.destroy
    respond_to do |format|
      format.html { redirect_to poem_url(@poem), notice: 'Word was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def secure_params
      params.require(:line_reference).permit( :line_id , :reference_type, :content , :media)
    end


end
