class StanzasController < BaseSecureController
  #before_action :set_poem, only: [:show, :edit, :update, :destroy]
  def create
    @poem = Poem.find(params[:poem_id])
    @stanza = Stanza.new(:poem => @poem)

    respond_to do |format|
      if @stanza.save
        format.html { redirect_to poem_path(@poem), notice: 'Stanza was successfully created.' }
        format.json { render :show, status: :created, location: @stanza }
      else
        format.html { render :new }
        format.json { render json: @poem.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @stanza = Stanza.find(params[:id])
    @poem = @stanza.poem
    @stanza.lines.destroy_all
    @stanza.destroy

    #TODO Delete all Words under the stanza
    respond_to do |format|
      format.html { redirect_to poem_path(@poem), notice: 'Stanza and Lines was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_poem
      @poem = Poem.find(params[:poem_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def secure_params
      params.require(:stanza).permit(:poem_id)
    end
end
