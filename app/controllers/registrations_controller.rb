class RegistrationsController < Devise::RegistrationsController
  after_action :create_default, only: :create

  private


  def create_default
    if @user.persisted?
      @user.status_id = 2
      @user.roles << "3"
      @user.save
    end
    NotifierMailer.welcome_notifier(@user).deliver
  end
end
