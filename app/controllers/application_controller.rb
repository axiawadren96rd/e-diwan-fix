class ApplicationController < ActionController::Base
  
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :exception
  protect_from_forgery with: :null_session, only: Proc.new { |c| c.request.format.json? }
  
  layout :layout_by_resource
  skip_before_action :verify_signed_out_user

protected
  def after_sign_in_path_for(resource)
    elearn_lessons_path    
  end


  def after_sign_out_path_for(resource)
    root_path
  end

  def layout_by_resource
    if devise_controller?
      'home_layout'
    else
      "artificial"
    end
  end
end
