class Admin::BaseController < ApplicationController
  before_action :authenticate_user!
  before_filter :check_redirect


private
  def check_redirect
    if !current_user.has_role? ["admin"]
      redirect_to home_path , alert: "Only Administrator can access the resource"
    end
  end
end