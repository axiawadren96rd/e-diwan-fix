class BaseSecureController < ApplicationController
  #protect_from_forgery with: :null_session, only: Proc.new { |c| c.request.format.json? }
  #before_action :authenticate_user!
#  before_filter :check_redirect


private
  def check_redirect
    if !current_user.approved?
      redirect_to home_path , alert: "Only activated user can access the resource"
    end
  end
end