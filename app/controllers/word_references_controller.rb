class WordReferencesController < BaseSecureController


  def find
    word_index = params[:word_index]
    poem_id = params[:poem_id]
    line_id = params[:line_id]
    @word_references = WordReference.where(" word_index = ? and poem_id = ? and line_id = ? " , word_index, poem_id , line_id )
    render :partial => "show"
  end



  def show
    @word_references = WordReference.word_indexes_with_line(params[:id], params[:line_id])
    @word_reference_titles = WordReference.select(:reference_type).word_indexes_with_line(params[:id], params[:line_id]).group(:reference_type)
    render :partial => "show"
  end


  def new

    @poem = Poem.find(params[:poem_id])
    @stanza = Stanza.find(params[:stanza_id])
    @line = Line.find(params[:line_id])
    @word = params[:poem][:word]
    @word_index = params[:poem][:word_indexes]

    @word_ref = WordReference.new(:poem => @poem , :stanza => @stanza , :line => @line, :word => @word, :word_indexes => @word_index)


    render :partial => "new"
  end

  def create
    @word_ref = WordReference.new(secure_params)


    respond_to do |format|
      if @word_ref.save
        format.html { redirect_to @word_ref.poem, notice: 'Word Reference was successfully created.' }
        format.json { render :show, status: :created, location: @word_ref }
      else
        format.html { render :new }
        format.json { render json: @word_ref.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @word_ref = WordReference.find(params[:id])
    @poem = @word_ref.poem
    @word_ref.destroy
    respond_to do |format|
      format.html { redirect_to poem_url(@poem), notice: 'Word was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def secure_params
      params.require(:word_reference).permit(:poem_id, :stanza_id, :line_id , :word , :word_indexes , :reference_type, :description , :media)
    end
end
