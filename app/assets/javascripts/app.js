$(function () {

  $.ajaxSetup({
    error:function(jqXHR, textStatus, errorThrown){        
      var html = "<div class='modal-content'><div class='modal-header'>"+errorThrown;
      html +="<button type='button' class='btn btn-ar btn-default pull-right' data-dismiss='modal'>Close</button><div>";
      html += "<div class='modal-body'>"+jqXHR.responseText+"</div>";
      html += "<div class='modal-footer'>";
      html += "</div></div>"
      $("#modal .modal-dialog").html(html)      
      $("#modal").modal("show")
    }
  });

  $(".modal-submit").off("click").on("click",function(e){
      e.preventDefault();      
      var action = $(this).attr("href");
      var header = $(this).data("header");
      $.ajax({
          url: action,
          method: "GET",
          success: function(data, textStatus, jqXHR){            
            $("#modal .modal-dialog").html(data)
            $("#modal").modal("show");
          }
      });

  });
    
  $(function () {  
    $(".readmore").readmore();
  });

  setTimeout(function(){
    if ($('#alert').length > 0) {
      $('#alert').remove();
    }
  }, 3000)
});