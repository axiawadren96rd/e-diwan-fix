$(function () {

  $(".poem-stanza-line-word").off("click").on("click",function(e){
        e.preventDefault();
        var $form = $(this).closest("form");        
        var action = $(this).attr("href");
        $("#poem_poem_id").val($(this).data("poem-id"));
        $("#poem_stanza_id").val($(this).data("stanza-id"));
        $("#poem_line_id").val($(this).data("line-id"));

        $("#poem_word_index").val($(this).data("word-index"));
        $("#poem_word").val($(this).html());
        
        $.ajax({
            url: action,
            method: "patch",
            data: $form.serialize(),
            success: function(data, textStatus, jqXHR){
                    
              $("#modal .modal-header > h3").html("Word Reference")
              $("#modal .modal-body").html(data)
              $("#modal").modal("show");
        
            }
        });

  });


});