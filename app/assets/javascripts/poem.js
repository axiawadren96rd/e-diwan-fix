//= require lodash.min

$(function () {

  var start_word_index;
  var end_word_index;

  $(".poem-stanza-line-word").off("mousedown").on("mousedown",function(e){
    start_word_index = $(this).data("word-index");    
  });

  $(".poem-stanza-line-word").off("mouseup").on("mouseup",function(e){
        e.preventDefault();

        var $form = $(this).closest("form");        
        var id = $(this).parent().data("poem-id");
        var stanza_id = $(this).parent().data("stanza-id");
        var line_id = $(this).parent().data("line-id");
        
        //convert to string and remove unnecessary whitespace     
        var selected_text = (window.getSelection() + "").replace(/\s\s+/g,' ').trim(); 
        

        var word_index = $(this).data("word-index");
        end_word_index = word_index;
        var word_count = selected_text.split(" ").length;
        var line_text = $(this).parent().text();

        var word_indexes = processIndexForSelectedWord(word_index, word_count, selected_text ,line_text);
        //console.dir(word_indexes)
        
        if(!word_indexes || selected_text.trim().length == 0){
          return;
        }

        $("#poem_poem_id").val(id);
        $("#poem_stanza_id").val(stanza_id);
        $("#poem_line_id").val(line_id);
        $("#poem_word_indexes").val(word_indexes);
        var action = "/poems/" + id + "/stanzas/" + stanza_id + "/lines/"+line_id+"/word_references/new";

        $("#poem_word").val(selected_text);
        
        $.ajax({
            url: action,
            method: "GET",
            data: $form.serialize(),
            success: function(data, textStatus, jqXHR){
                    
              $("#modal .modal-dialog").html(data)
              
              $("#modal").modal("show");
        
            }
        });

  });

  $("#show-detail-btn").on("click", function(e){
    e.preventDefault();
    
    if($(this).hasClass("active") == false){
      $(".detail-line").removeClass("hidden");
    }else{
      $(".detail-line").addClass("hidden");
    }
  });



  $("#lesson_poem_id").off("change").on("change",function(e){
    var poem_id = $(this).val();
    $("#lesson_line_ids").empty()
    if(poem_id){
      $.ajax({
            url: "/poems/" + poem_id + "/lines",
            method: "GET",          
            success: function(data, textStatus, jqXHR){              
              $.each(data, function(i,d){              
                $("#lesson_line_ids").append('<option value="'+d.id+'">'+d.text+'</option>');
              })
        
            }
        });
    }
  });


  $('[data-toggle="tooltip"]').tooltip(); 

  function processIndexForSelectedWord(word_index, word_count, selected_text ,line_text){
    var start = start_word_index <= end_word_index ? start_word_index : end_word_index ;
    var end = start_word_index >= end_word_index ? start_word_index : end_word_index ;
    //console.log(start , start_word_index)
    //console.log(end , end_word_index)
    if(typeof start_word_index == 'undefined' || typeof end_word_index == 'undefined' ){
      return false
    }else{

      return _.range(start, end + 1, 1)
    }
  }


});