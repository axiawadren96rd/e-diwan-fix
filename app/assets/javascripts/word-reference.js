//= require lodash.min

$(function () {
    $(document).ready(function(){        
      $(".reference_type_radio").on("change",function(e){
        var value = $(this).data("type");
        var text = $(this).data("text");
        if(value === "audio" || value === "image" || value === "video"){
            $("#word_content").removeClass("hidden");
            $("#word_file").removeClass("hidden");
            $("#word_file .text").html(text);
            $("#word_content .text").html("Description");
        }else if(value === "translation"){
          $("#word_content").removeClass("hidden");
          $("#word_content .text").attr("dir", "ltr");
          $("#word_content .text").attr("lang", "");
          $("#word_content input").attr("dir", "ltr");
          $("#word_content input").attr("lang", "");
          $("#word_file").addClass("hidden");
          $("#word_content .text").html(text);
        }else{
            $("#word_content").removeClass("hidden");
            $("#word_file").addClass("hidden");
            $("#word_content .text").html(text);
        }

      });

      $(".line_reference_type_radio").on("change",function(e){
        var value = $(this).data("type");
        var text = $(this).data("text");        
        if(value === "audio" || value === "image" || value === "video"){
            $("#line_content").addClass("hidden");
            $("#line_file").removeClass("hidden");
            $("#line_file .text").html(text);
        }else{
            $("#line_content").removeClass("hidden");
            $("#line_file").addClass("hidden");
            $("#line_content .text").html(text);
        }

      });

      if($(".line_reference_type_radio:checked").length > 0){
        $(".line_reference_type_radio:checked").trigger("change")
      }
      
//      CKEDITOR.replace('line_reference_content');
    
      if (CKEDITOR.instances['line_reference_content']) { 
        CKEDITOR.instances['line_reference_content'].destroy();
        CKEDITOR.replace('line_reference_content'); 
      } 

    })  

});