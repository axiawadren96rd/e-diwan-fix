//= require readmore

$(function () {  
  
  $("#show-detail-btn").on("click", function(e){
    e.preventDefault();
    
    if($(this).hasClass("active") == false){
      $(".detail-line").removeClass("hidden");
    }else{
      $(".detail-line").addClass("hidden");
    }
  });


  $(".poem-stanza-line-info").on("click" , function(e){
   var word_index = $(this).data("word-index");
    var text = $(this).text();
    var line_id = $(this).parent().data("line-id");
    var action = "/show_preview_word/" + word_index + "?line_id=" + line_id;
    $.ajax({
      url: action,
      method: "GET",      
      success: function(data, textStatus, jqXHR){
        
        $("#modal .modal-dialog").html(data)
        
        $("#modal").modal("show");
  
      }
    });


  });

  $("#select_semester").on("change",function(e){
    var lesson_id = $(this).data("lesson-id");
    var semester_id = $(this).val();    
    $.ajax({
      url: "/elearn/lessons/"+lesson_id+"/comments?semester_id=" + semester_id,
      method: "GET",      
      success: function(data, textStatus, jqXHR){
        $("#comments_section").html(data);
      }
    });

  })

  $(".poem-stanza-line-word").on("click" , function(e){
    var word_index = $(this).data("word-index");
    var text = $(this).text();
    var line_id = $(this).parent().data("line-id");
    var action = "/word_references/" + word_index + "?line_id=" + line_id;

    $.ajax({
      url: action,
      method: "GET",      
      success: function(data, textStatus, jqXHR){
        $("#modal .modal-dialog").html(data)
        $("#modal").modal("show");
  
      }
    });


  });


  

});