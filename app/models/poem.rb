class Poem < ActiveRecord::Base
    belongs_to :poet 
    has_many :stanzas, dependent: :destroy
    has_many :word_references, dependent: :destroy
  
    mount_uploader :avatar, AvatarUploader
    mount_uploader :audio, MediaUploader
    mount_uploader :document, MediaUploader

    after_create :create_stanza
    
    paginates_per 10

    def poem_lines
      Line.joins(:stanza).where("poem_id = ? " , self.id).select("lines.id, stanza_id, text")

    end

    def poem_text
      text = ""
      Line.joins(:stanza).where("poem_id = ? " , self.id).select("lines.id, stanza_id, text").each do |line|
        text += "#{line.text} \n\r"
      end
      return text

    end

    def create_stanza
      self.stanzas.create!
    end
end
