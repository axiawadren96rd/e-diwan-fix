class LineReference < ActiveRecord::Base
  belongs_to :line

  mount_uploader :media, MediaUploader

  REF_TYPES = {
    1 => ["meaning", "المعنى الإجمالي"],
    2 => ["fann","التصوير الفني"],
    3 => ["balaghah","الجانب البلاغي"],
    4 => ["audio","الصوت"],   
    5 => ["image","الصورة"],    
    6 => ["video","الفيديو"],    
    7 => ["translation","Translation"],    
  }


  def reference_type_text
    case self.reference_type
      when 1
        "المعنى الإجمالي"
      when 2
        "التصوير الفني"
      when 3
        "الجانب البلاغي"
      when 4
        "الصوت"
      when 5
        "الصورة"
      when 6
        "الفيديو"
      when 7
        "Translation"
    end
  end
end
