class Comment < ActiveRecord::Base
  belongs_to :lesson
  belongs_to :user
  belongs_to :semester
  validates :comment, presence: true

  scope :current_semester , ->() { where(:semester => Semester.currently_activated) }

  paginates_per 2
end
