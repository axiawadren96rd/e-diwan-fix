class Poet < ActiveRecord::Base
  has_many :poems
  
  mount_uploader :media, MediaUploader
  mount_uploader :document, MediaUploader
  def biography_text
    biography.gsub(/<\/?[^>]*>/, '').gsub(/\n\n+/, "\n").gsub(/^\n|\n$/, '')
  end
end
