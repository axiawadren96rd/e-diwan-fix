class User < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :omniauthable
  include DeviseTokenAuth::Concerns::User

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  after_create :set_status
  after_create :send_admin_mail

  has_many :comments

  serialize :roles, Array

  before_save -> do
    self.uid = SecureRandom.uuid
  end

  ROLES = {
    1 => "Admin",
    2 => "Lecturer",
    3 => "Student"
  }

  STATUSES = {
    1 => "Active",
    2 => "Not Active"
  }

  def set_status
    if self.has_role?(["admin" , "lecturer"])
     self.approved = 1
     self.save
    end
  end

  def send_admin_mail
    #AdminMailer.new_user_waiting_for_approval(self).deliver
  end

  def active_for_authentication?
    super && approved?
  end

  def inactive_message
    if !approved?
      :not_approved
    else
      super # Use whatever other message
    end
  end

  def is_admin
    if roles.include?(1)
      return true
    else
      return false
    end
  end

  def has_role?( role_array )
    if roles
      if role_array.include?("admin")
        return roles.include?("1")
      end
      if role_array.include?("lecturer")
        return roles.include?("2")
      end
      if role_array.include?("student")
        return roles.include?("3")
      end
    end
    return false
  end



  def role_names
    if roles
      roles.map! do |element|
        if element == "1"
           "Admin"
        elsif element == "2"
           "Lecturer"
        elsif element == "3"
           "Student"
        else
        end
      end
    end
  end



end
