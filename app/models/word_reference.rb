class WordReference < ActiveRecord::Base
  belongs_to :stanza
  belongs_to :poem
  belongs_to :line

  mount_uploader :media, MediaUploader

  before_create :add_leading_zero_to_index
  after_create :add_word_and_desc


  serialize :word_indexes

  scope :videos , ->() { where(:reference_type => 1) }
  scope :images , ->() { where(:reference_type => 5) }
  scope :audios , ->() { where(:reference_type => 2) }
  scope :words , ->() { where(:reference_type => 3) }
  scope :word_translations , ->() { where(:reference_type => 6) }
  scope :grammars , ->() { where(:reference_type => 4) }
  scope :with_line, -> (id){
    where(:line_id => id)
  }
  scope :search , ->(query) {

    query.strip! if query
    where("word LIKE ? OR word_search LIKE ? ", "%#{query}%" , "%#{query}%").uniq("word , poem_id , line_id")
  }
  scope :word_with_line, -> (word, line_id) {where(" line_id = ? ", line_id ).where("word LIKE ?" , "%#{word}%")}
  scope :word_indexes_with_line, -> (index, line_id) { 
    where("(word_indexes LIKE ?) AND line_id = ? AND LENGTH(word_indexes) < 50", "%#{index.rjust(2, '0')}%", line_id ) 
  }
  scope :word_indexes_filtered, -> (index) { where("(word_indexes LIKE ?)", "%#{sprintf('%02d', index)}%" ) }

  scope :word_reference_titles , -> {select(:reference_type).group(:reference_type)}
  paginates_per 50

  # def self.search(terms = "")
  #   terms = "" if terms == nil
  #   sanitized = sanitize_sql_array(["to_tsquery('arabic', ?)", terms.gsub(/\s/,"+")])
  #   WordReference.where("search_vector @@ #{sanitized}")
  # end

  REF_TYPES = {
    #1 => ["video", "فيديو"],
    3 => ["dictionary","المفردات"],
    4 => ["grammar","الإعراب"],
    6 => ["translation","Word Translation"],
  }

  def add_word_and_desc
    self.word_search =  WordReference.remove_harakat(self.word)
    self.description_search =  WordReference.remove_harakat(self.description)
    self.save
  end

  def type_title
    case self.reference_type
      when 1
         "فيديو"
      when 3
        "المفردات"
      when 4
        "الإعراب"
      when 5
        "صورة"
      when 6
        'word translation'
    end
  end



  def media_type
    case self.reference_type
      when 1
        'video'
      when 4
        'grammar'
      when 3
        'dictionary'
      when 5
        'image'
      when 6
        'word translation'
    end
  end

  def add_leading_zero_to_index
    self.word_indexes =  word_indexes.split(",").map {|n| sprintf("%02d" , n)}

  end

 def self.remove_harakat(input)
    #//Remove honorific sign
    input=input.gsub("\u0610", "");#//ARABIC SIGN SALLALLAHOU ALAYHE WA SALLAM
    input=input.gsub("\u0611", "");#//ARABIC SIGN ALAYHE ASSALLAM
    input=input.gsub("\u0612", "");#//ARABIC SIGN RAHMATULLAH ALAYHE
    input=input.gsub("\u0613", "");#//ARABIC SIGN RADI ALLAHOU ANHU
    input=input.gsub("\u0614", "");#//ARABIC SIGN TAKHALLUS

    #//Remove koranic anotation
    input=input.gsub("\u0615", "");#//ARABIC SMALL HIGH TAH
    input=input.gsub("\u0616", "");#//ARABIC SMALL HIGH LIGATURE ALEF WITH LAM WITH YEH
    input=input.gsub("\u0617", "");#//ARABIC SMALL HIGH ZAIN
    input=input.gsub("\u0618", "");#//ARABIC SMALL FATHA
    input=input.gsub("\u0619", "");#//ARABIC SMALL DAMMA
    input=input.gsub("\u061A", "");#//ARABIC SMALL KASRA
    input=input.gsub("\u06D6", "");#//ARABIC SMALL HIGH LIGATURE SAD WITH LAM WITH ALEF MAKSURA
    input=input.gsub("\u06D7", "");#//ARABIC SMALL HIGH LIGATURE QAF WITH LAM WITH ALEF MAKSURA
    input=input.gsub("\u06D8", "");#//ARABIC SMALL HIGH MEEM INITIAL FORM
    input=input.gsub("\u06D9", "");#//ARABIC SMALL HIGH LAM ALEF
    input=input.gsub("\u06DA", "");#//ARABIC SMALL HIGH JEEM
    input=input.gsub("\u06DB", "");#//ARABIC SMALL HIGH THREE DOTS
    input=input.gsub("\u06DC", "");#//ARABIC SMALL HIGH SEEN
    input=input.gsub("\u06DD", "");#//ARABIC END OF AYAH
    input=input.gsub("\u06DE", "");#//ARABIC START OF RUB EL HIZB
    input=input.gsub("\u06DF", "");#//ARABIC SMALL HIGH ROUNDED ZERO
    input=input.gsub("\u06E0", "");#//ARABIC SMALL HIGH UPRIGHT RECTANGULAR ZERO
    input=input.gsub("\u06E1", "");#//ARABIC SMALL HIGH DOTLESS HEAD OF KHAH
    input=input.gsub("\u06E2", "");#//ARABIC SMALL HIGH MEEM ISOLATED FORM
    input=input.gsub("\u06E3", "");#//ARABIC SMALL LOW SEEN
    input=input.gsub("\u06E4", "");#//ARABIC SMALL HIGH MADDA
    input=input.gsub("\u06E5", "");#//ARABIC SMALL WAW
    input=input.gsub("\u06E6", "");#//ARABIC SMALL YEH
    input=input.gsub("\u06E7", "");#//ARABIC SMALL HIGH YEH
    input=input.gsub("\u06E8", "");#//ARABIC SMALL HIGH NOON
    input=input.gsub("\u06E9", "");#//ARABIC PLACE OF SAJDAH
    input=input.gsub("\u06EA", "");#//ARABIC EMPTY CENTRE LOW STOP
    input=input.gsub("\u06EB", "");#//ARABIC EMPTY CENTRE HIGH STOP
    input=input.gsub("\u06EC", "");#//ARABIC ROUNDED HIGH STOP WITH FILLED CENTRE
    input=input.gsub("\u06ED", "");#//ARABIC SMALL LOW MEEM

    #//Remove tatweel
    input=input.gsub("\u0640", "");

    #//Remove tashkeel
    input=input.gsub("\u064B", "");#//ARABIC FATHATAN
    input=input.gsub("\u064C", "");#//ARABIC DAMMATAN
    input=input.gsub("\u064D", "");#//ARABIC KASRATAN
    input=input.gsub("\u064E", "");#//ARABIC FATHA
    input=input.gsub("\u064F", "");#//ARABIC DAMMA
    input=input.gsub("\u0650", "");#//ARABIC KASRA
    input=input.gsub("\u0651", "");#//ARABIC SHADDA
    input=input.gsub("\u0652", "");#//ARABIC SUKUN
    input=input.gsub("\u0653", "");#//ARABIC MADDAH ABOVE
    input=input.gsub("\u0654", "");#//ARABIC HAMZA ABOVE
    input=input.gsub("\u0655", "");#//ARABIC HAMZA BELOW
    input=input.gsub("\u0656", "");#//ARABIC SUBSCRIPT ALEF
    input=input.gsub("\u0657", "");#//ARABIC INVERTED DAMMA
    input=input.gsub("\u0658", "");#//ARABIC MARK NOON GHUNNA
    input=input.gsub("\u0659", "");#//ARABIC ZWARAKAY
    input=input.gsub("\u065A", "");#//ARABIC VOWEL SIGN SMALL V ABOVE
    input=input.gsub("\u065B", "");#//ARABIC VOWEL SIGN INVERTED SMALL V ABOVE
    input=input.gsub("\u065C", "");#//ARABIC VOWEL SIGN DOT BELOW
    input=input.gsub("\u065D", "");#//ARABIC REVERSED DAMMA
    input=input.gsub("\u065E", "");#//ARABIC FATHA WITH TWO DOTS
    input=input.gsub("\u065F", "");#//ARABIC WAVY HAMZA BELOW
    input=input.gsub("\u0670", "");#//ARABIC LETTER SUPERSCRIPT ALEF

    #//Replace Waw Hamza Above by Waw
    #input=input.gsub("\u0624", "\u0648");

    #//Replace Ta Marbuta by Ha
    #input=input.gsub("\u0629", "\u0647");

    #//Replace Ya
    #// and Ya Hamza Above by Alif Maksura
    #input=input.gsub("\u064A", "\u0649");
    #input=input.gsub("\u0626", "\u0649");

    #// Replace Alifs with Hamza Above/Below
    #// and with Madda Above by Alif
    #input=input.gsub("\u0622", "\u0627");
    #input=input.gsub("\u0623", "\u0627");
    #input=input.gsub("\u0625", "\u0627");
  end

end
