class Lesson < ActiveRecord::Base
  belongs_to :poem
  belongs_to :poet
  has_many :comments

  has_many :poem_lines, foreign_key: "lesson_id", class_name: LessonLine 
  has_many :lines , class_name: Line , through: :poem_lines 
  

  scope :previews , ->() { where(:status => 3) }

  STATUSES = {
    2 => "Active",    
    3 => "Active In Preview",    
    1 => "Not Active"
    
  }

  
  def status_text
    case self.status
      when 1
        "Not Active"
      when 2
        "Active"
      when 3
        "Active In Preview"
      end
  end

end
