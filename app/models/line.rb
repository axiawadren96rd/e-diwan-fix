class Line < ActiveRecord::Base
  belongs_to :stanza

  has_many :word_references
  has_many :line_references

  has_many :poem_lines, foreign_key: "lesson_id", class_name: LessonLine 
  has_many :lessons , class_name: Lesson , through: :poem_lines 
  
  

  def splitted_text
    text.split(" ")
  end
end
