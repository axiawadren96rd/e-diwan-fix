class Semester < ActiveRecord::Base
  belongs_to :user
  has_many :comments

  scope :currently_activated , ->() { where(:activated => true).first }  
  STATUSES = {
    1 => "Not Active",
    2 => "Active",    
    3 => "Active In Preview"    
  }

  


end
