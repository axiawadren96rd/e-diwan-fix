class Stanza < ActiveRecord::Base
  belongs_to :poem
  has_many :lines, dependent: :destroy  
end
