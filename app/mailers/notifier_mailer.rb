class NotifierMailer < ApplicationMailer
  default from: "staging@arabicara.com"

  def welcome_notifier(user)
    attachments.inline['logo.png'] = File.read("#{Rails.root}/app/assets/images/logo.png")
    @user = user
    mail(to: @user.email, subject: 'Welcome To E-Diwan!')
  end

  def approved_notifier(user)
    attachments.inline['logo.png'] = File.read("#{Rails.root}/app/assets/images/logo.png")
    @user = user
    mail(to: @user.email, subject: 'Your Account Has Been Approved By Admin')
  end


  def disapproved_notifier(user)
    attachments.inline['logo.png'] = File.read("#{Rails.root}/app/assets/images/logo.png")
    @user = user
    mail(to: @user.email, subject: 'Account Disapproved By Admin!')
  end


end
