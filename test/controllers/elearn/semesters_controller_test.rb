require 'test_helper'

class Elearn::SemestersControllerTest < ActionController::TestCase
  setup do
    @elearn_semester = elearn_semesters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:elearn_semesters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create elearn_semester" do
    assert_difference('Elearn::Semester.count') do
      post :create, elearn_semester: { activated: @elearn_semester.activated, name: @elearn_semester.name, status: @elearn_semester.status, user_id: @elearn_semester.user_id }
    end

    assert_redirected_to elearn_semester_path(assigns(:elearn_semester))
  end

  test "should show elearn_semester" do
    get :show, id: @elearn_semester
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @elearn_semester
    assert_response :success
  end

  test "should update elearn_semester" do
    patch :update, id: @elearn_semester, elearn_semester: { activated: @elearn_semester.activated, name: @elearn_semester.name, status: @elearn_semester.status, user_id: @elearn_semester.user_id }
    assert_redirected_to elearn_semester_path(assigns(:elearn_semester))
  end

  test "should destroy elearn_semester" do
    assert_difference('Elearn::Semester.count', -1) do
      delete :destroy, id: @elearn_semester
    end

    assert_redirected_to elearn_semesters_path
  end
end
