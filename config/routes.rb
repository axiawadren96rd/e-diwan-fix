Rails.application.routes.draw do



  get 'glossary' => "glossary#index"

  resources :glossary do
    collection do
      get "show_glossary_word"
      get "search"
    end
  end

  get 'account' => "account#index"
  patch 'account/update'

  get 'home' => "home#index"
  get 'lessons_preview' => "home#lessons_preview"
  get 'show_preview_lesson/:id' => "home#show_preview_lesson"
  get 'show_preview_word/:id' => "home#show_preview_word"

  devise_for :users, controllers: { registrations: "registrations" } 


  mount Ckeditor::Engine => '/ckeditor'

  namespace :api do
    scope :v1 do
      mount_devise_token_auth_for 'User', at: 'auth'
    end
  end

  resources :poets

  namespace :elearn do
    resources :semesters do
      patch 'activate'
    end
    resources :lessons do
      resources :comments
    end
  end

  resources :poems do
    get "add_audio"
    get "lines"
    resources :stanzas do
      resources :lines do
        patch 'word_references/find'
        resources :word_references
        resources :line_references
      end
    end
  end


  namespace :api , :defaults => { :format => 'json' } do
    namespace :v1 do
      resources :poems, :only => [:show, :index]
      resources :poets, :only => [:show, :index]
      resources :lessons do
        resources :comments
      end
      get 'word_references/find'
      get 'word_references/search'
      get 'word_references/find_all_references'
      resources :word_references , :only => [:show, :index]

    end
  end

  resources :word_references
  resources :lines , :only => [:show]

  namespace :admin do
    resources :users do
      patch 'approve'
      patch 'disapprove'
    end
  end

  get "videos" => "home#videos"
  get "audios" => "home#audios"

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
