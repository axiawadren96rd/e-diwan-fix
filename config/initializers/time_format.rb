date_formats = { default: "%e/%B/%Y" }
time_formats = { default: "%d/%m/%Y %I:%M %p"  }

Time::DATE_FORMATS.merge! time_formats
Date::DATE_FORMATS.merge! date_formats
