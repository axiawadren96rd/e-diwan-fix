# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user = User.create!({:name => "Admin" ,:email => "admin@e-diwan.com", :password => "adminpassword", :password_confirmation => "adminpassword" , :roles => ["1"] , :approved => true})
semester = Semester.create!({:name => "Default Semester" , :user => user , :status => 1 , :activated => true})
#poet =Poet.create!(:name => )

