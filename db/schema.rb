# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170515054514) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "adminpack"

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "comments", force: :cascade do |t|
    t.integer  "lesson_id"
    t.string   "title"
    t.text     "comment"
    t.integer  "status_id"
    t.integer  "user_id"
    t.integer  "semester_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "comments", ["lesson_id"], name: "index_comments_on_lesson_id", using: :btree
  add_index "comments", ["semester_id"], name: "index_comments_on_semester_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "lesson_lines", force: :cascade do |t|
    t.integer  "lesson_id"
    t.integer  "line_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "lesson_lines", ["lesson_id"], name: "index_lesson_lines_on_lesson_id", using: :btree
  add_index "lesson_lines", ["line_id"], name: "index_lesson_lines_on_line_id", using: :btree

  create_table "lessons", force: :cascade do |t|
    t.string   "name"
    t.string   "title"
    t.text     "objective"
    t.text     "content"
    t.integer  "poem_id"
    t.integer  "poet_id"
    t.string   "document"
    t.integer  "user_id"
    t.integer  "status"
    t.integer  "semester_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "lessons", ["semester_id"], name: "index_lessons_on_semester_id", using: :btree

  create_table "line_references", force: :cascade do |t|
    t.integer  "line_id"
    t.text     "content"
    t.string   "media"
    t.integer  "reference_type"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "line_references", ["line_id"], name: "index_line_references_on_line_id", using: :btree

  create_table "lines", force: :cascade do |t|
    t.integer  "stanza_id"
    t.string   "text"
    t.string   "text2"
    t.string   "media"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "lines", ["stanza_id"], name: "index_lines_on_stanza_id", using: :btree

  create_table "poems", force: :cascade do |t|
    t.text     "description"
    t.string   "author"
    t.string   "title"
    t.string   "audio"
    t.string   "document"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "poet_id"
    t.string   "avatar"
  end

  add_index "poems", ["poet_id"], name: "index_poems_on_poet_id", using: :btree

  create_table "poets", force: :cascade do |t|
    t.string   "name"
    t.string   "year"
    t.text     "biography"
    t.string   "media"
    t.string   "document"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "semesters", force: :cascade do |t|
    t.string   "name"
    t.integer  "user_id"
    t.integer  "status"
    t.boolean  "activated"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "semesters", ["user_id"], name: "index_semesters_on_user_id", using: :btree

  create_table "stanzas", force: :cascade do |t|
    t.integer  "poem_id"
    t.integer  "location"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "stanzas", ["poem_id"], name: "index_stanzas_on_poem_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "provider",               default: "email", null: false
    t.string   "uid",                    default: "",      null: false
    t.string   "encrypted_password",     default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "name"
    t.string   "nickname"
    t.string   "image"
    t.string   "email"
    t.text     "roles"
    t.json     "tokens"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "status_id"
    t.boolean  "approved",               default: false,   null: false
  end

  add_index "users", ["approved"], name: "index_users_on_approved", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true, using: :btree

  create_table "word_references", force: :cascade do |t|
    t.integer  "reference_type"
    t.string   "word"
    t.string   "word_indexes"
    t.string   "media"
    t.integer  "stanza_id"
    t.integer  "poem_id"
    t.integer  "line_id"
    t.text     "description"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "word_search"
    t.text     "description_search"
  end

  add_index "word_references", ["line_id"], name: "index_word_references_on_line_id", using: :btree
  add_index "word_references", ["poem_id"], name: "index_word_references_on_poem_id", using: :btree
  add_index "word_references", ["stanza_id"], name: "index_word_references_on_stanza_id", using: :btree

  add_foreign_key "comments", "lessons"
  add_foreign_key "comments", "semesters"
  add_foreign_key "comments", "users"
  add_foreign_key "lesson_lines", "lessons"
  add_foreign_key "lesson_lines", "lines"
  add_foreign_key "lessons", "poems"
  add_foreign_key "lessons", "poets"
  add_foreign_key "lessons", "semesters"
  add_foreign_key "line_references", "lines"
  add_foreign_key "lines", "stanzas"
  add_foreign_key "semesters", "users"
  add_foreign_key "stanzas", "poems"
  add_foreign_key "word_references", "lines"
  add_foreign_key "word_references", "poems"
  add_foreign_key "word_references", "stanzas"
end
