class CreateStanzas < ActiveRecord::Migration
  def change
    create_table :stanzas do |t|
      t.references :poem, index: true
      t.integer :location

      t.timestamps null: false
    end
    add_foreign_key :stanzas, :poems
  end
end
