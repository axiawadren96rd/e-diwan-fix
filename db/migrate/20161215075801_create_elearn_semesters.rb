class CreateElearnSemesters < ActiveRecord::Migration
  def change
    create_table :semesters do |t|
      t.string :name
      t.references :user, index: true
      t.integer :status
      t.boolean :activated

      t.timestamps null: false
    end
    add_foreign_key :semesters, :users
  end
end
