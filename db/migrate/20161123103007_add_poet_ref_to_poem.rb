class AddPoetRefToPoem < ActiveRecord::Migration
  def change
    add_reference :poems, :poet, index: true, foreign_key: true
  end
end
