class AddAvatarToPoems < ActiveRecord::Migration
  def change
    add_column :poems, :avatar, :string
  end
end
