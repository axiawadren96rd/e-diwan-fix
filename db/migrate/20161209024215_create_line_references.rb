class CreateLineReferences < ActiveRecord::Migration
  def change
    create_table :line_references do |t|
      t.references :line, index: true
      t.text :content
      t.string :media
      t.integer :reference_type

      t.timestamps null: false
    end
    add_foreign_key :line_references, :lines
  end
end
