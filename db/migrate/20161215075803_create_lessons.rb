class CreateLessons < ActiveRecord::Migration
  def change
    create_table :lessons do |t|
      t.string :name
      t.string :title
      t.text :objective
      t.text :content
      t.references :poem
      t.references :poet
      t.string :document
      t.references :user
      t.integer :status      
      t.references :semester, index: true

      t.timestamps null: false
    end
    add_foreign_key :lessons, :poems
    add_foreign_key :lessons, :poets
    add_foreign_key :lessons, :semesters
  end
end
