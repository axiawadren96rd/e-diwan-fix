class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.references :lesson, index: true
      t.string :title
      t.text :comment
      t.integer :status_id
      t.references :user, index: true
      t.references :semester, index: true

      t.timestamps null: false
    end
    add_foreign_key :comments, :lessons
    add_foreign_key :comments, :users
    add_foreign_key :comments, :semesters
  end
end
