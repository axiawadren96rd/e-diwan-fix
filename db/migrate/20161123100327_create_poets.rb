class CreatePoets < ActiveRecord::Migration
  def change
    create_table :poets do |t|
      t.string :name
      t.string :year
      t.text :biography
      t.string :media
      t.string :document

      t.timestamps null: false
    end
  end
end
