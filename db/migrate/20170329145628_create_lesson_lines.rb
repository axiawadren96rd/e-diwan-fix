class CreateLessonLines < ActiveRecord::Migration
  def change
    create_table :lesson_lines do |t|
      t.references :lesson, index: true
      t.references :line, index: true

      t.timestamps null: false
    end
    add_foreign_key :lesson_lines, :lessons
    add_foreign_key :lesson_lines, :lines
  end
end
