class AddWordSearchToWordReference < ActiveRecord::Migration
  def up
    add_column :word_references, :word_search, :string
    add_column :word_references , :description_search , :text

    WordReference.find_each do | word |
    	word.word_search = WordReference.remove_harakat(word.word)
    	word.save
    end
  end
  def down
  	remove_column :word_references, :word_search
    remove_column :word_references , :description_search

  end
end
