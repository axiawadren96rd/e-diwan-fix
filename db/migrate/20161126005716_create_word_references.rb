class CreateWordReferences < ActiveRecord::Migration
  def change
    create_table :word_references do |t|
      t.integer :reference_type
      t.string :word
      t.string :word_indexes
      t.string :media
      t.references :stanza, index: true
      t.references :poem, index: true
      t.references :line, index: true
      t.text :description

      t.timestamps null: false
    end
    add_foreign_key :word_references, :stanzas
    add_foreign_key :word_references, :poems
    add_foreign_key :word_references, :lines
  end
end
