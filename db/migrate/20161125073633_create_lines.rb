class CreateLines < ActiveRecord::Migration
  def change
    create_table :lines do |t|
      t.references :stanza, index: true
      t.string :text
      t.string :text2
      t.string :media
      t.string :description

      t.timestamps null: false
    end
    add_foreign_key :lines, :stanzas
  end
end
