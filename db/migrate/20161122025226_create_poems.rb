class CreatePoems < ActiveRecord::Migration
  def change
    create_table :poems do |t|
      t.text :description
      t.string :author
      t.string :title
      t.string :audio
      t.string :document
      t.timestamps null: false
    end
  end
end
