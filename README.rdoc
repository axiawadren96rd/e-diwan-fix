== e-Diwan

e-Diwanushi’ir is a system for learning and understanding Arabic Poetries. 
It has multimedia elements for every poem in that system. The web base is more 
detailed system than mobile application since mobile application is used for 
displaying only for the users. Even though both are different platform but they 
are giving same impact to the user to learn on Arabic Poetries

--- Application Technology
- Web Application (Ruby On Rails)
- Hybrid Mobile Application (Ionic Framework)

--- Development Environment Setup For Web Application
- Ruby On Rails normal installation
